import 'package:flutter/material.dart';
import 'package:igob_salud_app/helps/TablaDinamicaClass.dart';

class TablaDinamicaHospitales extends StatelessWidget {
//  TablaDinamicaHospitales({this.listHospitales, this.hospitalId});

  List<TablaDinamicaClass> listOfColumns;

  TablaDinamicaHospitales({this.listOfColumns});
  @override
  Widget build(BuildContext context) {
    var heightHead = 18.0;
    var colorHead = Colors.redAccent;
    var TextStyleHead = TextStyle(
      color: Colors.white,
      fontSize: 8.0,
    );

    var heightTablebody = 18.0;

    var colorTableBody = Colors.white;

    var TextStyleTableBody = TextStyle(
      color: Colors.black,
      fontSize: 9.0,
    );

    var TextStyleTableBodyEspecialidad = TextStyle(
      color: Colors.black,
      fontSize: 6.0,
    );
/*
    List<TablaDinamicaClass> listOfColumns = [];

    listOfColumns.add(new TablaDinamicaClass(mes: "Enero1",merced: "45", pinos: "85", cotahuma: "89",portada: "8"));
    listOfColumns.add(new TablaDinamicaClass(mes: "febrero",merced: "4", pinos: "8", cotahuma: "22",portada: "90"));

*/

    List<TableRow> rowsTable = [];

    /**
     * HEADER
     * */

    rowsTable.add(TableRow(children: [
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "MES",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "La Merced",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Los Pinos",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Cotahuma",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "La Portada",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
    ]));

    /**
     * FIN HEADER
     * */

    listOfColumns.forEach((TablaDinamicaClass item) {
      // print("* item:  ${item}");

      rowsTable.add(TableRow(children: [
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item.mes,
                textAlign: TextAlign.center,
                style: TextStyleTableBodyEspecialidad,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item.merced,
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item.pinos,
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item.cotahuma,
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item.portada,
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
      ]));
    });

    // TODO: implement build
    return Container(
      child: SizedBox(
        child: Table(
          border: TableBorder.all(),
          children: rowsTable,
        ),
      ),
    );
  }
}
