import 'package:flutter/material.dart';

///import 'package:advanced_share/advanced_share.dart' show AdvancedShare;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:screenshot/screenshot.dart';
import 'dart:io';
import 'dart:convert' as convert;
import 'package:esys_flutter_share/esys_flutter_share.dart';

class ShareGraph extends StatelessWidget {
  final String title;
  final String description;
  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;

  ShareGraph(
      {this.title,
      this.description,
      this.screenshotController,
      this.scaffoldKey});

  void handleResponse(response, {String appName}) {
    if (response == 0) {
      print("failed.");
    } else if (response == 1) {
      print("success");
    } else if (response == 2) {
      print("application isn't installed");
      if (appName != null) {
        print("La app no esta instalado");

        scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("${appName} no esta instalado."),
          duration: Duration(seconds: 9),
        ));
      }
    }
  }

  Widget _offsetPopup() => PopupMenuButton<int>(
        padding: EdgeInsets.all(0.0),
        tooltip: 'Compartir',
        onSelected: (value) {
          screenshotController
              .capture(delay: Duration(milliseconds: 10))
              .then((File image) async {
            //print("Capture Done");

            List<int> imageList_ = image.readAsBytesSync();
            String base64_ = convert.Base64Encoder().convert(imageList_);

            String BASE64_IMAGE = "data:image/png;base64,${base64_}";
/*
        if (value == 1) {
          AdvancedShare.whatsapp(msg: description, url: BASE64_IMAGE)
              .then((response) {
            handleResponse(response, appName: "Whatsapp");
          });
        }
        if (value == 2) {
          AdvancedShare.gmail(
              subject: title, msg: description, url: BASE64_IMAGE)
              .then((response) {
            handleResponse(response, appName: "Gmail");
          });
        }
        if (value == 3) {
          AdvancedShare.generic(msg: description, url: BASE64_IMAGE)
              .then((response) {
            handleResponse(response);
          });
        }
*/
            // print("base64_ ${base64_} ");
          }).catchError((onError) {
            print("onError:  ${onError}");
            scaffoldKey.currentState.showSnackBar(new SnackBar(
              content:
                  new Text("Error al capturar la imagen. Vuelva a intentar"),
              duration: Duration(seconds: 9),
            ));
          });
        },
        itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  MdiIcons.whatsapp,
                  color: Colors.black45,
                )
              ],
            ),
          ),
          PopupMenuItem(
            value: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  MdiIcons.email,
                  color: Colors.black45,
                )
              ],
            ),
          ),
          PopupMenuItem(
            value: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  MdiIcons.shareVariant,
                  color: Colors.black45,
                )
              ],
            ),
          ),
        ],
        elevation: 10.0,
        icon: Icon(
          MdiIcons.share,
          color: Colors.black,
          semanticLabel: "Compartir",
          size: 32,
        ),
      );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Padding(padding: EdgeInsets.fromLTRB(0.0, 5.0, 15.0, 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(
                MdiIcons.dragVertical,
                color: Colors.black38,
                size: 32,
              ),
              Text(title,
                  style: TextStyle(
                      color: Colors.black54,
                      //fontSize: 10.0,
                      fontFamily: "WorkSansBold")),
            ],
          ),
          Container(
            width: 40,
            child: Material(
              color: Colors.white,
              child: Center(
                child: Ink(
                  decoration: const ShapeDecoration(
                    color: Colors.redAccent,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    //  splashColor: Colors.amber,

                    icon: Icon(MdiIcons.shareVariant),
                    color: Colors.white,
                    onPressed: shareAction,
                  ),
                ),
              ),
            ),
          )
          /*   Container(
          color: Colors.white,
          child: _offsetPopup(),
        )*/
        ],
      ),
      ),
    );
  }

  void shareAction() {
    screenshotController
        .capture(delay: Duration(milliseconds: 10))
        .then((File image) async {
      List<int> imageList_ = image.readAsBytesSync();

      try {
        await Share.file('esys image', 'esys.png', imageList_, 'image/png',
            text: description);
      } catch (e) {
        scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("Error al compartir"),
          duration: Duration(seconds: 9),
        ));
      }
    });
  }
}
