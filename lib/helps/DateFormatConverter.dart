String dateTimeFormat(DateTime dateTime) {
  String date = dateTime.toString().split(" ")[0];
  return date;
}

DateTime CambiaFecha(DateTime fechaInicio) {
  var fecha_ = fechaInicio.toString().split(" ")[0];

  var minutos_ = fechaInicio.toString().split(" ")[1];
  print("minutos_ :: ${minutos_}");
  var dia = fecha_.toString().split("-")[2];
  var mes = fecha_.toString().split("-")[1];
  var anio = fecha_.toString().split("-")[0];

  var mesSum = int.parse(mes) - 1;
  var fechaF = "${anio}-${mesSum}-${dia} 00:00:00.0000";
  print("fecha_ :: ${fecha_}");

  return DateTime.parse(fechaF.toString().trim());
}

int codigoMes(String mes) {
  int res = 1;
  switch (mes) {
    case 'ENERO':
      res = 1;
      break;
    case 'FEBRERO':
      res = 2;
      break;
    case 'MARZO':
      res = 3;
      break;
    case 'ABRIL':
      res = 4;
      break;
    case 'MAYO':
      res = 5;
      break;
    case 'JUNIO':
      res = 6;
      break;
    case 'JULIO':
      res = 7;
      break;
    case 'AGOSTO':
      res = 8;
      break;
    case 'SEPTIEMBRE':
      res = 9;
      break;

    case 'OCTUBRE':
      res = 10;
      break;

    case 'NOVIEMBRE':
      res = 11;
      break;

    case 'DICIEMBRE':
      res = 12;
      break;

    default:
      res = 1;
  }
  return res;
}
