import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:igob_salud_app/LoginPage.dart';
import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:easy_alert/easy_alert.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';

void main() async{
//var sdf=  await GlobalConfiguration().loadFromAsset("app_routes");
  runApp(new AlertProvider(
    child: new MyApp(),
    config: new AlertConfig(ok: "OK"),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIIS',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {



  void initState() {

    getDataSession();
  }

  Future<void> getDataSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var usuario_ = await prefs.getString('usuario');
    var nombre_ = await prefs.getString('nombre');


    if(usuario_ != null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          HomePage()), (Route<dynamic> route) => false);
       //   HomePageDLC()), (Route<dynamic> route) => false);
    }else {


      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          LoginPage()), (Route<dynamic> route) => false);

    }

  //  print("usuario_ ${usuario_}");
   // print("nombre_ ${nombre_}");
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Bienvenido',
            ),
        SpinKitCircle(
          color: Colors.red,
          size: 80.0,

        )
            ,
          ],
        ),
      ),
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
