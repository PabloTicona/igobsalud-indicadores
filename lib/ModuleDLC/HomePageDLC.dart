import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/Menu.dart';
import 'package:igob_salud_app/ModuleDLC/pages/adjudicacionProponente/buscarProponente.dart';
import 'package:igob_salud_app/ModuleDLC/pages/buscarContrato/buscarContrato.dart';
import 'package:igob_salud_app/ModuleDLC/pages/contactos/contactosDlc.dart';
import 'package:igob_salud_app/ModuleDLC/pages/organizacion/organigrama.dart';
import 'package:igob_salud_app/ModuleDLC/pages/reporteAnalista/buscarAnalista.dart';
import 'package:igob_salud_app/ModuleDLC/pages/reporteAnalistaModalidad/reporteAnalistaModalidad.dart';
import 'package:igob_salud_app/ModuleDLC/pages/reporteDAUE/buscarDAUE.dart';
import 'package:igob_salud_app/ModuleDLC/pages/reporteTotal/buscarTotales.dart';
import 'package:igob_salud_app/ModuleDLC/pages/reporteUe/reporteUe.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/buscaProceso.dart';
import 'package:igob_salud_app/pages/HomePage.dart';

class HomePageDLC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: HomePageDLC_(),
    );
  }
}

class HomePageDLC_ extends StatefulWidget {
  @override
  _HomePageDlcState createState() => _HomePageDlcState();
}

class _HomePageDlcState extends State<HomePageDLC_> {
  List<Menu> menus = [];

  CargarMenu() {
    menus.add(new Menu(
        nombre: "BUSCAR PROCESO",
        icono: Icon(
          Icons.search,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Busqueda por proceso',
        ),
        descripcion: "gestión anterior o vigente",
        pagina: BuscaProceso()));
    menus.add(new Menu(
        nombre: "BUSCAR CONTRATO",
        icono: Icon(
          Icons.description,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Buscar por número de contrato',
        ),
        descripcion: "Buscar por número de contrato",
        pagina: BuscarContrato()));
    menus.add(new Menu(
        nombre: "BUSCAR PROPONENTE",
        icono: Icon(
          Icons.perm_contact_calendar,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Busqueda de proponentes',
        ),
        descripcion: "Busqueda de proponentes y contratos.",
        pagina: BuscaProponente()));
    menus.add(new Menu(
        nombre: "REPORTE POR MODALIDAD",
        icono: Icon(
          Icons.table_chart,
          color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Total de trámites registrados en sistema',
        ),
        descripcion:
            "Total de modalidades vigentes.",
        pagina: BuscarTotales()));
    menus.add(new Menu(
        nombre: "REPORTE POR ANALISTA",
        icono: Icon(
          Icons.pie_chart,
          color: Colors.red[400],
          size: 80.0,
          semanticLabel:
              'Reporte por analista, asesor legal, secretario ó responsable de infraestructura',
        ),
        descripcion: "Procesos asignados a analistas.",
        pagina: BuscarAnalista()));
    menus.add(new Menu(
        nombre: "REPORTE DE ANALISTA Y MODALIDAD",
        icono: Icon(
          Icons.timeline,
          color: Colors.red[400],
          size: 80.0,
          semanticLabel:
              'Reporte por analista y sus procesos en las diferentes modalidades',
        ),
        descripcion:
            "Analista asignado con sus modalidades.",
        pagina: ReporteAnalistaModalidad()));
    menus.add(new Menu(
        nombre: "REPORTE DIRECCIÓN ADMINISTRATIVA",
        icono: Icon(
          Icons.assessment,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Reporte por dirección y unidad',
        ),
        descripcion: "Procesos por modalidades.",
        pagina: BuscarDAUE()));
    menus.add(new Menu(
        nombre: "REPORTE UNIDAD EJECUTORA",
        icono: Icon(
          Icons.assessment,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Reporte por unidad',
        ),
        descripcion: "Procesos por modalidad.",
        pagina: ReporteUe()));
    /*menus.add(new Menu(
        nombre: "REPORTE POR TOTAL DE PROCESOS",
        icono: Icon(
          Icons.trending_up,
          color: Colors.orangeAccent,
          size: 80.0,
          semanticLabel: 'Reporte por modalidad',
        ),
        descripcion: "Reporte por modalidad de proceso.",
        pagina: BuscarModalidad()));*/
    menus.add(new Menu(
        nombre: "ORGANIZACIÓN",
        icono: Icon(
          Icons.group,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Organización',
        ),
        descripcion: "Unidades y dependencias.",
        pagina: Organigrama()));
    menus.add(new Menu(
        nombre: "CONTACTOS",
        icono: Icon(
          Icons.contact_mail,
           color: Colors.red[400],
          size: 80.0,
          semanticLabel: 'Teléfonos y contactos',
        ),
        descripcion: "Dirección y teléfonos",
        pagina: ContactosDlc()));
  }

  @override
  void initState() {
    super.initState();
    CargarMenu();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("D.L.C."),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            )
          },
        ),

      ),
      body: GridView.count(
        crossAxisCount: 2,
        // Generate 100 widgets that display their index in the List.
        children: List.generate(menus.length, (index) {
          return Padding(
            padding: EdgeInsets.all(4.0),
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              elevation: 15.0,
              child: InkWell(
                borderRadius: BorderRadius.circular(8.0),
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: menus[index].icono,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          
                          Text(
                            menus[index].nombre,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ],
                      ),
Container(
  width: MediaQuery.of(context).size.width * 0.5,
  height: 45,
  child:                       Column(
    children: <Widget>[
      Text(
        menus[index].descripcion,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.blueGrey[200]),
      ),
    ],
  ),
),


                    ],
                  ),
                ),
                onTap: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => menus[index].pagina),
                  )
                },
              ),
            ),
          );
        }),
      ),
    );
  }
}
