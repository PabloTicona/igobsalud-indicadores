import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';

typedef void TapCallback(Proceso proceso);

class Combos extends StatelessWidget {
  List<DropdownMenuItem<Proceso>> dropdownMenuItems;
  List<Proceso> procesos;
  Proceso selectedItem;
  TapCallback changeDropdownItem;
  Combos(
      {this.procesos,
      this.dropdownMenuItems,
      this.changeDropdownItem,
      this.selectedItem});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 1.0,
      height: 80,
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.fromLTRB(5.0, 1.0, 5.0, 0.0),
          child: Card(
            elevation: 8.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: (procesos != null)
                  ? new DropdownButton(
                      isExpanded: true,
                      value:
                          (selectedItem != null) ? selectedItem : procesos[0],
                      items: dropdownMenuItems,
                      onChanged: changeDropdownItem, //changeDropdownItem,
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(color: Colors.black),
                      underline: Container(
                        height: 2,
                        color: Colors.red,
                      ),
                    )
                  : Center(
                      child: Text("Cargando.."),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
