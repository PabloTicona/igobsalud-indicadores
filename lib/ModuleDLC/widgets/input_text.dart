import 'package:flutter/material.dart';

class InputText extends StatelessWidget {

  final TextEditingController ctl;
  final String labelText;
  InputText({this.ctl, this.labelText});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return           Container(
      width: MediaQuery.of(context).size.width * 1.0,
        height: 61,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 13.0, 7.0, 8.0),
        child: TextField(
          controller: ctl,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            // icon: Icon(Icons.perm_identity),
            labelText: labelText,
          ),
        ),
      ),
    );
  }

}