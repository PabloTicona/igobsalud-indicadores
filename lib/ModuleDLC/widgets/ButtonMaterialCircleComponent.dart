import 'package:flutter/material.dart';


typedef void PressCallback();

class ButtonMaterialCircleComponent extends StatelessWidget {

  final double size;
  final PressCallback onPressed;

  ButtonMaterialCircleComponent({this.size, this.onPressed});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child:  new RawMaterialButton(
        onPressed: onPressed,
        child: new Icon(
          Icons.search,
          color: Colors.white,
          size: size,
        ),
        shape: new CircleBorder(),
        elevation: 8.0,
        fillColor: Colors.redAccent,
        padding: const EdgeInsets.all(15.0),
      ),
    );
  }

}