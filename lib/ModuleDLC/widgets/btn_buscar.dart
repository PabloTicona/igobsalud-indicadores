import 'package:flutter/material.dart';
typedef void PressCallback();
class BtnBuscar extends StatelessWidget {

  final PressCallback onPressed;

  BtnBuscar({this.onPressed});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return               Container(
      width: MediaQuery.of(context).size.width * 1.0,
      height: 35,
      child: Padding(
        padding: EdgeInsets.fromLTRB(8.0, 1.0, 8.0, 0.0),
        child: Material(
          borderRadius: BorderRadius.circular(5.0),
          elevation: 5.0,
          color: Colors.red,
          child: InkWell(
            borderRadius: BorderRadius.circular(5.0),
            onTap: onPressed, // handle your onTap here
            child: Container(
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding:
                      EdgeInsets.fromLTRB(0.0, 0.0, 6.9, 0.0),
                      child: Icon(
                        Icons.search,
                        color: Colors.white,
                        // size: 30.0,
                      ),
                    ),
                    Text(
                      "BUSCAR",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

}