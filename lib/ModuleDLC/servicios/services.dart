import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

Future<dynamic> getResponse(String funcserv) async {
  var url_ = "http://sercontrataciones.lapaz.bo/api";
  var response = await http.get("${url_}/${funcserv}");
//print(response.body);
  if (response.statusCode == 200) {
    return convert.jsonDecode(response.body);
  } else {
    throw Exception("Error de conexión de red");
  }
}

Future<dynamic> postResponse(String funcserv, String data) async {
  var url_ = "http://sercontrataciones.lapaz.bo/api";
  var client = http.Client();
  try {
    var response = await client.post("${url_}/${funcserv}",
        body: convert.json.decode(
            data) 
        );
    print('response 123');
    print(response.body);
    if (response.statusCode == 200) {
      return convert.jsonDecode(response.body);
    } else {
      throw Exception("Error de conexión de red");
    }
  } catch (e) {
    print(e);
  }
}
