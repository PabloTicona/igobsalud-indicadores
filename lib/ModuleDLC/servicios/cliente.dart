import 'dart:convert';

import 'package:igob_salud_app/ModuleDLC/model/BuscaporNroContrato.dart';
import 'package:igob_salud_app/ModuleDLC/model/CombotodosAnalistas.dart';
import 'package:igob_salud_app/ModuleDLC/model/Contrato.dart';
import 'package:igob_salud_app/ModuleDLC/model/Cronograma.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proponentes.dart';
import 'package:igob_salud_app/ModuleDLC/model/DatosProponente.dart';
import 'package:igob_salud_app/ModuleDLC/model/ContratosProponente.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepAnalistaModelo.dart';
import 'package:igob_salud_app/ModuleDLC/model/TotalesProc.dart';
import 'package:igob_salud_app/ModuleDLC/model/Tramites.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepAnalista.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepDaue.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/services.dart';

class Cliente {
  Future<List<Proceso>> BuscarDatoProceso(String parametro) async {
    var resp_ = await getResponse(parametro);
    List<dynamic> body = resp_;
    List<Proceso> result = body
        .map(
          (dynamic item) => Proceso.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Tramites>> ListarTramites(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);
    print('hola');
    print(resp_);

    List<dynamic> body = resp_;
    List<Tramites> result = body
        .map(
          (dynamic item) => Tramites.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Contrato>> ListarContrato(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);
    print('hola');
    print(resp_);

    List<dynamic> body = resp_;
    List<Contrato> result = body
        .map(
          (dynamic item) => Contrato.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Cronograma>> ListarCronograma(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);
    print('hola');
    print(resp_);

    List<dynamic> body = resp_;
    List<Cronograma> result = body
        .map(
          (dynamic item) => Cronograma.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Proponentes>> ListarProponentes(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<Proponentes> result = body
        .map(
          (dynamic item) => Proponentes.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<DatosProponente>> ListarDatosProponente(
      String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<DatosProponente> result = body
        .map(
          (dynamic item) => DatosProponente.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<ContratosProponente>> ListarContratosProponente(
      String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<ContratosProponente> result = body
        .map(
          (dynamic item) => ContratosProponente.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<BuscaporNroContrato>> ListarBuscarContrato(
      String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<BuscaporNroContrato> result = body
        .map(
          (dynamic item) => BuscaporNroContrato.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Usuario>> DatosUsuario(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);
    print('hola');
    print(resp_);

    List<dynamic> body = resp_;
    List<Usuario> result = body
        .map(
          (dynamic item) => Usuario.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<TotalesProc>> buscarModalidad(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<TotalesProc> result = body
        .map(
          (dynamic item) => TotalesProc.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<RepAnalista>> buscarAnalista(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<RepAnalista> result = body
        .map(
          (dynamic item) => RepAnalista.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<RepAnalistaModelo>> buscarAnalistaModalidad(
      String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<RepAnalistaModelo> result = body
        .map(
          (dynamic item) => RepAnalistaModelo.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Proceso>> todoslosAnalsitas(
      String parametro, data) async {
    var resp_ = await postResponse(parametro, data);
    List<dynamic> body = resp_;
    List<Proceso> result = body
        .map(
          (dynamic item) => Proceso.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<RepDaue>> buscarDaue(String parametro, data) async {
    var resp_ = await postResponse(parametro, data);

    List<dynamic> body = resp_;
    List<RepDaue> result = body
        .map(
          (dynamic item) => RepDaue.fromJson(item),
        )
        .toList();
    return result;
  }
}
