import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Usuario> getDataSession() async {
  Usuario usuariodlc;

  SharedPreferences prefs = await SharedPreferences.getInstance();
  var usuarioD_ = await prefs.getString('usuario');
  //var nombreD_ = await prefs.getString('nombre');
  var data = '{"usuario": "$usuarioD_"}';
  var datosusuario = await Cliente().DatosUsuario('logueo', data);

  usuariodlc = datosusuario[0];
  return usuariodlc;
}
