import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Menu {
  final String nombre;
  final Icon icono;
  final String descripcion;
  final Widget pagina;
  
  Menu({
    @required this.nombre,
    @required this.icono,
    @required this.descripcion,
    @required this.pagina,
  });


}
