import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RepDaue {
  final String name;
  final int y;
  

  RepDaue({
    @required this.name,
    @required this.y,
  });

  factory RepDaue.fromJson(Map<dynamic, dynamic> json) {
    return RepDaue(
      name: json['name'] as String,
      y: json['y'] as int,
    );
  }
}