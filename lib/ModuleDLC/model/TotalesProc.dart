import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TotalesProc {
  final String name;
  final int y;
  final String drilldown;
  

  TotalesProc({
    @required this.name,
    @required this.y,
    @required this.drilldown,
  });

  factory TotalesProc.fromJson(Map<dynamic, dynamic> json) {
    return TotalesProc(
      name: json['name'] as String,
      y: json['y'] as int,
      drilldown: (json['drilldown'] as dynamic).toString(),
    );
  }
}
