import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Tramites {
  final int id;
  final int idproc;
  final String analista;
  final String abogado;
  final String infraestructura;
  final String da;
  final String ue;
  final String codigo;
  final String objeto;
  final String modalidad;
  final String hr;
  final String monto;
  final String plazo;
  final String convocatoria;
  final String tipo;
  final String fechaTarea;
  final String fechaSicoes;
  final String fechaApertura;
  final String estado;
  final String observaciones;

  Tramites({
  @required this.id,
  @required this.idproc,
  @required this.analista,
  @required this.abogado,
  @required this.infraestructura,
  @required this.da,
  @required this.ue,
  @required this.codigo,
  @required this.objeto,
  @required this.modalidad,
  @required this.hr,
  @required this.monto,
  @required this.plazo,
  @required this.convocatoria,
  @required this.tipo,
  @required this.fechaTarea,
  @required this.fechaSicoes,
  @required this.fechaApertura,
  @required this.estado,
  @required this.observaciones,
  });

  factory Tramites.fromJson(Map<dynamic, dynamic> json) {
    return Tramites(
      id: json['ID'] as int,
      idproc: json['Id_proceso'] as int,
      analista: json['analista'] as String,
      abogado: json['abogado'] as String,
      infraestructura: json['infraestructura'] as String,
      da: json['da'] as String,
      ue: json['ue'] as String,
      codigo: json['codigo'] as String,
      objeto: json['objeto'] as String,
      modalidad: json['modalidad'] as String,
      hr: json['hr'] as String,
      monto: (json['monto'] as dynamic).toString(),
      plazo: (json['plazo'] as dynamic).toString(),
      convocatoria: json['convocatoria'] as String,
      tipo: json['tipo'] as String,
      fechaTarea: json['fecha_tarea'] as String,
      fechaSicoes: json['fecha_sicoes'] as String,
      fechaApertura: json['fecha_apertura'] as String,
      estado: json['estado'] as String,
      observaciones: json['observaciones'] as String,
    );
  }
}