import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ContratosProponente {

  final int id;
  final int idproc;
  final int idprop;
  final String codigo;
  final String carpeta;
  final String objeto;
  final String modalidad;
  final String monto;
  final String fechacontrato;
  final String plazo;
  final String multa;
  final String tipo;

  ContratosProponente ({
  @required this.id,
  @required this.idproc,
  @required this.idprop,
  @required this.codigo,
  @required this.carpeta,
  @required this.objeto,
  @required this.modalidad,
  @required this.monto,
  @required this.fechacontrato,
  @required this.plazo,
  @required this.multa,
  @required this.tipo,
  });

  factory ContratosProponente.fromJson(Map<dynamic, dynamic> json) {
    return ContratosProponente(
  id: json['id'] as int,
  idproc: json['idproc'] as int,
  idprop: json['idprop'] as int,
  codigo: json['codigo'] as String,
  carpeta: (json['carpeta'] as dynamic).toString(),
  objeto: json['objeto'] as String,
  modalidad: json['modalidad'] as String,
  monto: (json['monto'] as dynamic).toString(),
  fechacontrato: json['fechacontrato'] as String,
  plazo: json['plazo'] as String,
  multa: json['multa'] as String,
  tipo: json['tipo'] as String,
    );
  }
}