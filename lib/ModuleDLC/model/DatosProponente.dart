import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DatosProponente {

  final int id;
  final String racsocial;
  final String institu;
  final String nit;
  final String matricula;
  final String licencia;
  final String direccion;
  final String zona;
  final String ciudad;
  final String telf;
  final String celular;
  final String correo;
  final String tipo;
  final String riesgo;
  final String replegal;
  final String ci;
  final String estado;

  DatosProponente ({
  @required this.id,
  @required this.racsocial,
  @required this.institu,
  @required this.nit,
  @required this.matricula,
  @required this.licencia,
  @required this.direccion,
  @required this.zona,
  @required this.ciudad,
  @required this.telf,
  @required this.celular,
  @required this.correo,
  @required this.tipo,
  @required this.riesgo,
  @required this.replegal,
  @required this.ci,
  @required this.estado,
  });

  factory DatosProponente.fromJson(Map<dynamic, dynamic> json) {
    return DatosProponente(
  id: json['id'] as int,
  racsocial: json['racsocial'] as String,
  institu: json['institu'] as String,
  nit: json['nit'] as String,
  matricula: json['matricula'] as String,
  licencia: json['licencia'] as String,
  direccion: json['direccion'] as String,
  zona: json['zona'] as String,
  ciudad: json['ciudad'] as String,
  telf: json['telf'] as String,
  celular: json['celular'] as String,
  correo: json['correo'] as String,
  tipo: json['tipo'] as String,
  riesgo: json['riesgo'] as String,
  replegal: json['replegal'] as String,
  ci: json['ci'] as String,
  estado: json['estado'] as String,
    );
  }
}
