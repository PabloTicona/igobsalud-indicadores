import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RepAnalistaModelo {
  final int y;
  final String nombre;

  RepAnalistaModelo({
    @required this.y,
    @required this.nombre,
  });

  factory RepAnalistaModelo.fromJson(Map<dynamic, dynamic> json) {
    return RepAnalistaModelo(
      y: json['y'] as int,
      nombre: json['nombre'] as String,  
    );
  }
}