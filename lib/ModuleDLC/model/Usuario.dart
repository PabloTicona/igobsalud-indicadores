import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Usuario {

  final int idUsuario;
  final String loginUsu;
  final String passwordUsu;
  final int idTipoUsuarioUsu;
  final int idEmpleadoUsu;
  final String fechaCreacionUsu;
  final bool estadoUsu;
  final int idEmpleado;
  final String ciEmp;
  final String ciExpEmp;
  final String rotuloEmp;
  final String nombreEmp;
  final String apellidoPEmp;
  final String apellidoMEmp;

  final String direccionEmp;
  final String telefonoEmp;
  final String emailEmp;
  final String estadoEmp;
  final String idUnidadEjecutora;
  final String tipo;
  final String nroItem;
  final String fechaBaja;
  final int idCargoUltimo;
  final int idSaf;
  final String cafDescripcion;

  Usuario ({
  @required this.idUsuario,
  @required this.loginUsu,
  @required this.passwordUsu,
  @required this.idTipoUsuarioUsu,
  @required this.idEmpleadoUsu,
  @required this.fechaCreacionUsu,
  @required this.estadoUsu,
  @required this.idEmpleado,
  @required this.ciEmp,
  @required this.ciExpEmp,
  @required this.rotuloEmp,
  @required this.nombreEmp,
  @required this.apellidoPEmp,
  @required this.apellidoMEmp,

  @required this.direccionEmp,
  @required this.telefonoEmp,
  @required this.emailEmp,
  @required this.estadoEmp,
  @required this.idUnidadEjecutora,
  @required this.tipo,
  @required this.nroItem,
  @required this.fechaBaja,
  @required this.idCargoUltimo,
  @required this.idSaf,
  @required this.cafDescripcion,
  });

  factory Usuario.fromJson(Map<dynamic, dynamic> json) {
    return Usuario(
  idUsuario: json['idUsuario'] as int,
  loginUsu: json['login_Usu'] as String,
  passwordUsu: json['password_Usu'] as String,
  idTipoUsuarioUsu: json['idTipoUsuario_Usu'] as int,
  idEmpleadoUsu: json['idEmpleado_Usu'] as int,
  fechaCreacionUsu: json['fechaCreacion_Usu'] as String,
  estadoUsu: json['estado_Usu'] as bool,
  idEmpleado: json['idEmpleado'] as int,
  ciEmp: json['ci_Emp'] as String,
  ciExpEmp: json['ciExp_Emp'] as String,
  rotuloEmp: json['rotulo_Emp'] as String,
  nombreEmp: json['nombre_Emp'] as String,
  apellidoPEmp: json['apellidoP_Emp'] as String,
  apellidoMEmp: json['apellidoM_Emp'] as String,
  direccionEmp: json['direccion_Emp'] as String,
  telefonoEmp: json['telefono_Emp'] as String,
  emailEmp: json['email_Emp'] as String,
  estadoEmp: json['estado_Emp'] as String,
  idUnidadEjecutora: json['idUnidadEjecutora'] as String,
  tipo: json['tipo'] as String,
  nroItem: json['nroItem'] as String,
  fechaBaja: json['fechaBaja'] as String,
  idCargoUltimo: json['idCargoUltimo'] as int,
  idSaf: json['Id_CAF'] as int,
  cafDescripcion: json['CAF_Descripcion'] as String,
    );
  }
}
