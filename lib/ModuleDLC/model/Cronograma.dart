import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Cronograma {

  final int idproc;
  final int id;
  final int numero;
  final String descripcion;
  final String responsable;
  final int plazo;
  final String fecha;
  final String fechaejecucion;
  final int base;

  Cronograma({
  @required this.idproc,
  @required this.id,
  @required this.numero,
  @required this.descripcion,
  @required this.responsable,
  @required this.plazo,
  @required this.fecha,
  @required this.fechaejecucion,
  @required this.base,
  });

  factory Cronograma.fromJson(Map<dynamic, dynamic> json) {
    return Cronograma(
      idproc: json['idproc'] as int,
      id: json['id'] as int,
      numero: json['numero'] as int,
      descripcion: json['descripcion'] as String,
      responsable: json['responsable'] as String,
      plazo: json['plazo'] as int,
      fecha: json['fecha'] as String,
      fechaejecucion: json['fecha_ejecucion'] as String,
      base: json['base'] as int,
    );
  }
}
