import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RepAnalista {
  final int y;
  final String name;

  RepAnalista({
    @required this.y,
    @required this.name,
  });

  factory RepAnalista.fromJson(Map<dynamic, dynamic> json) {
    return RepAnalista(
      y: json['y'] as int,
      name: json['name'] as String,  
    );
  }
}