import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TipoProp {
  final String id;
  final String nombre;


  TipoProp({
    @required this.id,
    @required this.nombre,
  });

  factory TipoProp.fromJson(Map<dynamic, dynamic> json) {
    return TipoProp(
      id: (json['id'] as int).toString(),
      nombre: json['descrpcion'] as String,


    );
  }
}