import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CombotodosAnalistas {
  final int id;
  final String nombre;

  CombotodosAnalistas({
    @required this.id,
    @required this.nombre,
  });

  factory CombotodosAnalistas.fromJson(Map<dynamic, dynamic> json) {
    return CombotodosAnalistas(
      id: json['id'] as int,
      nombre: json['nombre'] as String,  
    );
  }
}