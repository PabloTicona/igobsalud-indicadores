import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Proponentes {
  final int idprop;
  final String razonsocial;
  final int nit;
  final int ci;
  final String correo;
  final int codtip;
  final String tippersona;

  Proponentes({
  @required this.idprop,
  @required this.razonsocial,
  @required this.nit,
  @required this.ci,
  @required this.correo,
  @required this.codtip,
  @required this.tippersona,
  });

  factory Proponentes.fromJson(Map<dynamic, dynamic> json) {
    return Proponentes(
      idprop: json['id_prop'] as int,
      razonsocial: json['razonsocial'] as String,
      nit: json['nit'] as int,
      ci: json['ci'] as int,
      correo: json['correo'] as String,
      codtip: json['cod_tip'] as int,
      tippersona: json['tip_persona'] as String,
    );
  }
}