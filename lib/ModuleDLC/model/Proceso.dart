import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Proceso {
  final String id;
  final String nombre;


  Proceso({
    @required this.id,
    @required this.nombre,
  });

  factory Proceso.fromJson(Map<dynamic, dynamic> json) {
    return Proceso(
      id: (json['id'] as dynamic).toString(),
      nombre: json['nombre'] as String,
    );
  }
}