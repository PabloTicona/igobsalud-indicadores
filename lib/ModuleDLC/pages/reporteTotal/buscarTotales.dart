import 'dart:ffi';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/TotalesProc.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/btn_buscar.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';

class BuscarTotales extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscarTotales_(),
    );
  }
}

class BuscarTotales_ extends StatefulWidget {
  @override
  _BuscarTotalesState createState() => _BuscarTotalesState();
}

class _BuscarTotalesState extends State<BuscarTotales_> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Proceso> procesosmodalidad = [];
  List<Proceso> procesosgestion = [];
  bool viewGraf = false;
  List<TotalesProc> listatotalesproc = [];

  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsModalidad;
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsGestion;

  ScreenshotController buscarTotalesController = ScreenshotController();

  Proceso selectedItemModalidad;
  Proceso selectedItemGestion;
  List<charts.Series<OrdinalSales, String>> datagrafica;
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;
  var usuario;

  CargarMenu() {
    procesosmodalidad
        .add(new Proceso(id: "TODAS", nombre: "TODAS LAS MODALIDADES"));
    procesosmodalidad.add(new Proceso(
        id: "MAYOR", nombre: "APOYO NACIONAL A LA PRODUCCION Y EMPLEO"));
    procesosmodalidad.add(new Proceso(
        id: "MENOR",
        nombre: "APOYO A LA PRODUCCION Y EMPLEO (Hasta 200mil Bolivianos)"));
    procesosmodalidad.add(new Proceso(
        id: "NORMATIVA", nombre: "CON NORMATIVA DEL FINANCIADOR NACIONAL"));
    procesosmodalidad.add(new Proceso(
        id: "DIRECTAM", nombre: "CONTRATACION DIRECTA MAYOR A 200 MIL"));
    procesosmodalidad.add(new Proceso(
        id: "DITECTAMEN",
        nombre: "CONTRATACION DIRECTA MENOR IGUAL A 200 MIL"));
    procesosmodalidad
        .add(new Proceso(id: "MENOR2", nombre: "CONTRATACION MENOR"));
    procesosmodalidad.add(
        new Proceso(id: "EMERGENCIA", nombre: "CONTRATACION POR EMERGENCIA"));
    procesosmodalidad.add(
        new Proceso(id: "EXCEPCION", nombre: "CONTRATACION POR EXCEPCION"));
    procesosmodalidad.add(new Proceso(
        id: "INTERNACIONAL", nombre: "LICITACION PUBLICA INTERNACIONAL"));
    procesosmodalidad.add(
        new Proceso(id: "NACIONAL", nombre: "LICITACION PUBLICA NACIONAL"));

    procesosgestion.add(new Proceso(id: "1", nombre: "PROCESOS VIGENTES"));
    procesosgestion.add(new Proceso(id: "0", nombre: "TODOS LOS PROCESOS"));
  }

  @override
  void initState() {
    super.initState();
    CargarMenu();

    _dropdownMenuItemsModalidad = buildDropdownMenuItems(procesosmodalidad);
    _dropdownMenuItemsGestion = buildDropdownMenuItems(procesosgestion);
  }

  changeDropdownItemModalidad(Proceso proceso) {
    setState(() {
      selectedItemModalidad = proceso;
    });
  }

  changeDropdownItemGestion(Proceso proceso) {
    setState(() {
      selectedItemGestion = proceso;
    });
  }

  Future<void> recuperardatosusu() async {
    var datosusuario = await getDataSession();
    usuariodlc = datosusuario;
    var idusuario = usuariodlc.idUsuario;
    var cargo = usuariodlc.idCargoUltimo;
    if (idusuario == null) {
      idusuario = 0;
    }
    if (cargo == null) {
      cargo = 0;
    }
  }

  Future<void> listaProcesos(String modalidad, String gestion) async {
    setState(() {
      listatotalesproc = null;
      datagrafica = null;
    });

    var data =
        '{"Modalidad":"$modalidad","ue":"0","cargo":"43","gestion":"$gestion","idusuario":"2945"}';

    listatotalesproc = await Cliente().buscarModalidad('repModalidad', data);


    if(listatotalesproc.length!=0){
      List<charts.Series<OrdinalSales, String>> result = [];

      for (TotalesProc item in listatotalesproc) {
        result.add(new charts.Series<OrdinalSales, String>(
          id: "(${item.y}) ${item.name}",
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          colorFn: (OrdinalSales sales, _) => sales.color,
          data: [
            new OrdinalSales('${item.y}', item.y,
                charts.Color.fromHex(code: generateRandomHexColor())),
          ],
        ));
      }

      setState(() {
        datagrafica = result;
      });
    }else{
      setState(() {
        viewGraf=false;
      });
      showInSnackBar("Sin Datos");
    }

    //  print('listatotalesP $listatotalesP');
    setState(() {
      ///   listatotalesproc = listatotalesP;
    });


  }

  onPressed() {

    setState(() {
      viewGraf=true;
    });

    var idmodalidad = 'TODAS';
    var gestion = '1';
    if (selectedItemModalidad != null) {
      idmodalidad = selectedItemModalidad.id;
    }

    if (selectedItemGestion != null) {
      gestion = selectedItemGestion.id;
    }

    listaProcesos(idmodalidad, gestion);

  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  Random random = new Random();
  String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(random.nextInt(16)) | 0];
    return hex;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 5),
    ));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Buscar Por Modalidad"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Combos(
            selectedItem: selectedItemModalidad,
            procesos: procesosmodalidad,
            dropdownMenuItems: _dropdownMenuItemsModalidad,
            changeDropdownItem: changeDropdownItemModalidad,
          ),
          //este es el select de gestion
          Combos(
            selectedItem: selectedItemGestion,
            procesos: procesosgestion,
            dropdownMenuItems: _dropdownMenuItemsGestion,
            changeDropdownItem: changeDropdownItemGestion,
          ),

          BtnBuscar(
            onPressed: onPressed,
          ),

          Padding(
            padding: EdgeInsets.all(5.0),
            child: Screenshot(
              controller: buscarTotalesController,
              child: viewGraf?Card(
                elevation: 8.0,
                child: Column(
                  children: <Widget>[
                    (datagrafica != null)
                        ? ShareGraph(
                            screenshotController: buscarTotalesController,
                            scaffoldKey: _scaffoldKey,
                            description: "Reporte por total de trámites",
                            title: "Modalidad",
                          )
                        : Container(),
                    Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 400 +
                          ((listatotalesproc == null)
                              ? 0.0
                              : listatotalesproc.length * 30.0),
                      child: (datagrafica != null)
                          ? Padding(
                              padding: EdgeInsets.all(9.0),
                              child: new charts.BarChart(
                                datagrafica,
                                behaviors: [
                                  new charts.SeriesLegend(
                                      position: charts.BehaviorPosition.bottom,
                                      horizontalFirst:
                                          bool.fromEnvironment("Pablo")),
                                ],
                                animate: true,
                                barRendererDecorator:
                                    new charts.BarLabelDecorator<String>(),
                                defaultRenderer: new charts.BarRendererConfig(
                                    groupingType:
                                        charts.BarGroupingType.groupedStacked,
                                    strokeWidthPx: 2.0),
                              ),
                            )
                          : Container(
                              child: Center(
                                child: Center(
                                  child: SpinKitWave(
                                      color: Colors.redAccent,
                                      type: SpinKitWaveType.center),
                                ),
                              ),
                            ),
                    )
                  ],
                ),
              ): Container(),
            ),
          )
        ],
      ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales(this.year, this.sales, this.color);
}
