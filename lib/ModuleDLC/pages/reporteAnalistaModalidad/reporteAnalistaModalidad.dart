import 'dart:math';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepAnalistaModelo.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';

class ReporteAnalistaModalidad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ReporteAnalistaModalidad_(),
    );
  }
}

class ReporteAnalistaModalidad_ extends StatefulWidget {
  @override
  _ReporteAnalistaModalidadState createState() =>
      _ReporteAnalistaModalidadState();
}

class _ReporteAnalistaModalidadState extends State<ReporteAnalistaModalidad_> {
  
  List<Proceso> asignacionanalista = []; //el cargarmenu
  List<Proceso> procesosanalistatodos; //combo dependiente
  
  List<RepAnalistaModelo> listaanalistasModalidad = []; //para el reporte
  
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsAnalistatodos; //dropdown del combo dependiente
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsAnalista;

  Proceso selectedItemAnalistatodos; //elegir el item del combo dependiente
  Proceso selectedItemAnalista;

  
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  ScreenshotController reporteAnalistaModalidadController =
  ScreenshotController();
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;
  List<charts.Series<OrdinalSales, String>> datagrafica;

  CargarMenu() {
    asignacionanalista.add(new Proceso(id: "1", nombre: "ANALISTA ASIGNADO"));
    asignacionanalista.add(new Proceso(id: "2", nombre: "ASESOR LEGAL ASIGNADO"));
    asignacionanalista.add(new Proceso(id: "3", nombre: "ANALISTA DE INFRAESTRUCTURA"));
 }

  @override
  void initState() {
    super.initState();
    CargarMenu();
   // listTodosanalistas();
    recuperardatosusu();

     _dropdownMenuItemsAnalista = buildDropdownMenuItems(asignacionanalista);
  }

  changeDropdownItemAnalista(Proceso proceso) {

    setState(() {
      selectedItemAnalista = proceso;
      listTodosanalistas(proceso.id);
    });
  }

  changeDropdownItemAnalistatodos(Proceso proceso) {
    setState(() {
      selectedItemAnalistatodos = proceso;
    });
  }

  Future<void> listTodosanalistas(String analista) async {
 var datoanalista = analista;
 if (datoanalista != null) {datoanalista = analista;} else {datoanalista = '1';}
    var data = '{"analista": "$datoanalista"}';

    var asignacionesporanalista = await Cliente().todoslosAnalsitas('comboanalistamodalidad', data);
      setState(() {
      procesosanalistatodos=asignacionesporanalista;
    });
    setValueDefauldSelect();
    _dropdownMenuItemsAnalistatodos = buildDropdownMenuItems(procesosanalistatodos);
  }
    setValueDefauldSelect() {
      selectedItemAnalistatodos = procesosanalistatodos[0];
  }

  Future<void> recuperardatosusu() async {
    var datosusuario = await getDataSession();
    usuariodlc = datosusuario;
    var idusuario = usuariodlc.idUsuario;
    var cargo = usuariodlc.idCargoUltimo;
    if (idusuario == null) {
      idusuario = 0;
    }
    if (cargo == null) {
      cargo = 0;
    }
  }

  Future<void> listaAnalista(String analista, String asignacion) async {
    setState(() {
      listaanalistasModalidad = null;
      datagrafica = null;
    });

    var data = '{"analistaid":"$asignacion", "asignacion":"$analista"}';
              //{"analistaid":"14419", "asignacion":"2"}
    listaanalistasModalidad =
        await Cliente().buscarAnalistaModalidad('repAnalistaModalidad', data);
       //print('listatotalesP $listatotalesP');
    setState(() {
      //listatotalesproc = listatotalesP;
    });

    List<charts.Series<OrdinalSales, String>> result = [];

    for (RepAnalistaModelo item in listaanalistasModalidad) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.nombre,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }

    setState(() {
      datagrafica = result;
    });
  }

  onPressed() {
    var idanalista = '1';
    var asignacion = '1';
    print(idanalista);
    if (selectedItemAnalista != null) {
      idanalista = selectedItemAnalista.id;
    }
    if (selectedItemAnalistatodos != null) {
      asignacion = selectedItemAnalistatodos.id;
    }
    listaAnalista(idanalista, asignacion);
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  Random random = new Random();
  String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(random.nextInt(16)) | 0];
    return hex;
  }

  List<charts.Series<OrdinalSales, String>> _createSampleData() {
    List<charts.Series<OrdinalSales, String>> result = [];

    for (RepAnalistaModelo item in listaanalistasModalidad) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.nombre,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reporte por analista"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Combos(
            selectedItem: selectedItemAnalista,
            procesos: asignacionanalista,
            dropdownMenuItems: _dropdownMenuItemsAnalista,
            changeDropdownItem: changeDropdownItemAnalista,
          ),
              Combos(
                selectedItem: selectedItemAnalistatodos,
                procesos: procesosanalistatodos,
                dropdownMenuItems: _dropdownMenuItemsAnalistatodos,
                changeDropdownItem: changeDropdownItemAnalistatodos,
              ),
          const SizedBox(height: 20),
          Container(
            width: MediaQuery.of(context).size.width * 1.0,
            height: 60,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Material(
                borderRadius: BorderRadius.circular(5.0),
                elevation: 5.0,
                color: Colors.red,
                child: InkWell(
                  borderRadius: BorderRadius.circular(5.0),
                  onTap: () => {onPressed()}, // handle your onTap here
                  child: Container(
                    child: Center(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 0.0, 6.9, 0.0),
                            child: Icon(
                              Icons.search,
                              color: Colors.white,
                              // size: 30.0,
                            ),
                          ),
                          Text(
                            "BUSCAR",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Screenshot(
              controller: reporteAnalistaModalidadController,
              child: Card(
                elevation: 8.0,
                child: Column(
                  children: <Widget>[
                    ShareGraph(
                      screenshotController: reporteAnalistaModalidadController,
                      scaffoldKey: scaffoldKey,
                      description: "Reporte procesos por analista y modalidad",
                      title: "Reporte por Analista",
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 400 +
                          ((listaanalistasModalidad == null)
                              ? 0.0
                              : listaanalistasModalidad.length * 30.0),
                      child: (datagrafica != null)
                          ? new charts.BarChart(
                              datagrafica,
                              behaviors: [
                                new charts.SeriesLegend(
                                    position: charts.BehaviorPosition.bottom,
                                    horizontalFirst:
                                        bool.fromEnvironment("DLC")),
                              ],
                              vertical: true,
                              barRendererDecorator:
                                  new charts.BarLabelDecorator<String>(),
                              defaultRenderer: new charts.BarRendererConfig(
                                  groupingType:
                                      charts.BarGroupingType.groupedStacked,
                                  strokeWidthPx: 2.0),
                              animate: true,
                            )
                          : Container(
                              //child: Text('cargando ...'),
                              ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales(this.year, this.sales, this.color);
}
