import 'dart:ffi';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepDaue.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';

class ReporteUe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ReporteUe_(),
    );
  }
}

class ReporteUe_ extends StatefulWidget {
  @override
  _ReporteUeState createState() => _ReporteUeState();
}

class _ReporteUeState extends State<ReporteUe_> {
  List<Proceso> procesosue;
  List<RepDaue> listadaue = [];

  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsUe;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  ScreenshotController buscarDaueController = ScreenshotController();
  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  Proceso selectedItemUe;
  List<charts.Series<OrdinalSales, String>> datagrafica;
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;

  Future<void> listUe() async {
    var ue = await Cliente().BuscarDatoProceso("comboue");
    setState(() {
      procesosue= ue;
    });
    _dropdownMenuItemsUe = buildDropdownMenuItems(procesosue);
    print(ue);
  }

  @override
  void initState() {
    super.initState();
    listUe();
  }

  changeDropdownItemUe(Proceso proceso) {
    setState(() {
      selectedItemUe = proceso;
    });
  }

    Future<void> recuperardatosusu() async {
   var datosusuario = await getDataSession();
    usuariodlc = datosusuario;
     var idusuario = usuariodlc.idUsuario;
     var cargo = usuariodlc.idCargoUltimo;
      if (idusuario == null) { idusuario = 0; }
      if (cargo== null) { cargo = 0; }
    
    }

  Future<void> listaDauexxx(String ue, DateTime fechaini, DateTime fechafin) async {
    setState(() {
      listadaue = null;
      datagrafica = null;
    });
    /*var fechaini1 = (fechaini.toString()).split(' ')[0];
    var fechafin1 = (fechafin.toString()).split(' ')[0];*/
    var fechaini1 = (new DateFormat("dd/MM/yyyy").format(fechaini));
    var fechafin1 = (new DateFormat("dd/MM/yyyy").format(fechafin));
    var data = '{"ue":"$ue", "fechaini":"$fechaini1","fechafin":"$fechafin1"}';

    listadaue = await Cliente().buscarDaue('repue', data);
    setState(() {});

    List<charts.Series<OrdinalSales, String>> result = [];

    for (RepDaue item in listadaue) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.name,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }

    setState(() {
      datagrafica = result;
    });
  }

  onPressed() {
    var ue = 'TODAS';
    if (selectedItemUe != null) {
      ue = selectedItemUe.nombre;
    }
listaDauexxx(ue, _fechaDesde, _fechaHasta);
    print(ue);
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  Random random = new Random();
  String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(random.nextInt(16)) | 0];
    return hex;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buscar por Unidad Ejecutora"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            //aqui comienzan los combos
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Combos(
                //combo ue
                selectedItem: selectedItemUe,
                procesos: procesosue,
                dropdownMenuItems: _dropdownMenuItemsUe,
                changeDropdownItem: changeDropdownItemUe,
              ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const SizedBox(height: 20),

        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Padding(
              padding: EdgeInsets.all(0.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new CardDatePickerComponent(
                      nombre: 'Desde: ',
                      fechaInicio: _fechaDesde,
                      formatoFecha: '',
                      formatoFechaComponete: 'dd-MMMM-yyyy',
                      onTap:(dateTime, List<int> index) {
                        setState(() {
                          _fechaDesde = dateTime;
                        });
                      },),
                  new CardDatePickerComponent(
                      nombre: 'Hasta:',
                      fechaInicio: _fechaHasta,
                      formatoFecha: '',
                      formatoFechaComponete: 'dd-MMMM-yyyy',
                      onTap:  (dateTime, List<int> index) {
                        setState(() {
                          _fechaHasta = dateTime;
                        });
                      },
                    ),
                ],
              ),
            ),
          ),
        ),

const SizedBox(height: 20),
              Container(
                width: MediaQuery.of(context).size.width * 1.0,
                height: 60,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Material(
                    borderRadius: BorderRadius.circular(5.0),
                    elevation: 5.0,
                    color: Colors.red,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(5.0),
                      onTap: () =>
                          {onPressed()}, // handle your onTap here
                      child: Container(
                        child: Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.fromLTRB(0.0, 0.0, 6.9, 0.0),
                                child: Icon(
                                  Icons.search,
                                  color: Colors.white,
                                  // size: 30.0,
                                ),
                              ),
                              Text(
                                "BUSCAR",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),

Padding( padding: EdgeInsets.all(15.0),
            child: Screenshot(
              controller: buscarDaueController,
              child: Card(
                elevation: 8.0,
                child: Column(
                  children: <Widget>[
                    ShareGraph(
                      screenshotController: buscarDaueController,
                      scaffoldKey: scaffoldKey,
                      description: "Reporte por total de trámites",
                      title: "Modalidad",
                    ),
                    Container(
                width: MediaQuery.of(context).size.width * 1.0,
                height:
                    400 + ((listadaue == null) ? 0.0 : listadaue.length * 30.0),
                child: (datagrafica != null)
                    ? new charts.BarChart(
                        datagrafica,
                        behaviors: [
                          new charts.SeriesLegend(
                              position: charts.BehaviorPosition.bottom,
                              horizontalFirst: bool.fromEnvironment("DLC")),
                        ],
                        domainAxis: new charts.OrdinalAxisSpec(
                            renderSpec: new charts.SmallTickRendererSpec(
                                minimumPaddingBetweenLabelsPx: 0,
                                labelStyle:
                                    new charts.TextStyleSpec(fontSize: 2))),
                        vertical: true,
                        barRendererDecorator:
                            new charts.BarLabelDecorator<String>(),
                        defaultRenderer: new charts.BarRendererConfig(
                            groupingType: charts.BarGroupingType.groupedStacked,
                            strokeWidthPx: 2.0),
                              animate: true,
                            )
                          : Container(
                              //child: Text('cargando ...'),
                              ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    ],
    ),
     ] 
     ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales(this.year, this.sales, this.color);
}