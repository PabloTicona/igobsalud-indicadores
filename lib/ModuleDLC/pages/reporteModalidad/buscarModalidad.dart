import 'dart:math';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/TotalesProc.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';

class BuscarModalidad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscarModalidad_(),
    );
  }
}

class BuscarModalidad_ extends StatefulWidget {
  @override
  _BuscarModalidadState createState() => _BuscarModalidadState();
}

class _BuscarModalidadState extends State<BuscarModalidad_> {
  List<Proceso> procesosgestion = [];
  List<TotalesProc> listatotalesproc = [];

  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsGestion;
  Proceso selectedItemGestion;

 final scaffoldKey = new GlobalKey<ScaffoldState>();
 ScreenshotController buscarModalidadController = ScreenshotController();

  List<charts.Series<OrdinalSales, String>> datagrafica;
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;
  var usuario;
  
  CargarMenu() {
    procesosgestion.add(new Proceso(id: "1", nombre: "PROCESOS VIGENTES"));
    procesosgestion.add(new Proceso(id: "2", nombre: "TODOS LOS PROCESOS"));
  }

  @override
  void initState() {
    super.initState();
    CargarMenu();

    _dropdownMenuItemsGestion = buildDropdownMenuItems(procesosgestion);
  }

  changeDropdownItemGestion(Proceso proceso) {
    setState(() {
      selectedItemGestion = proceso;
    });
  }

  Future<void> recuperardatosusu() async {
   var datosusuario = await getDataSession();
    usuariodlc = datosusuario;
     var idusuario = usuariodlc.idUsuario;
     var cargo = usuariodlc.idCargoUltimo;
      if (idusuario == null) { idusuario = 0; }
      if (cargo== null) { cargo = 0; }
    
    }

  Future<void> listaProcesos(String gestion) async {
    setState(() {
      listatotalesproc = null;
      datagrafica = null;
    });

    var data =
        '{"ue": "0", "cargo": "43", "gestion": "$gestion", "idusuario": "2945"}';

    listatotalesproc = await Cliente().buscarModalidad('estadistica02', data);
    setState(() {});

    List<charts.Series<OrdinalSales, String>> result = [];

    for (TotalesProc item in listatotalesproc) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.name,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }
    setState(() {
      datagrafica = result;
    });
  }

  onPressed() {
    var gestion = '1';
    if (selectedItemGestion != null) {
      gestion = selectedItemGestion.id;
    }

    listaProcesos(gestion);
    print(gestion);
  }

  Random random = new Random();
  String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(random.nextInt(16)) | 0];
    return hex;
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reporte por Total"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Combos(
            selectedItem: selectedItemGestion,
            procesos: procesosgestion,
            dropdownMenuItems: _dropdownMenuItemsGestion,
            changeDropdownItem: changeDropdownItemGestion,
          ),
          const SizedBox(height: 20),
          Container(
            width: MediaQuery.of(context).size.width * 1.0,
            height: 50,
            child: Material(
              borderRadius: BorderRadius.circular(5.0),
              elevation: 5.0,
              color: Colors.red,
              child: InkWell(
                borderRadius: BorderRadius.circular(5.0),
                onTap: () => {onPressed()}, // handle your onTap here
                child: Container(
                  child: Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 6.9, 0.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.white,
                            // size: 30.0,
                          ),
                        ),
                        Text(
                          "BUSCAR",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),

Padding(padding: EdgeInsets.all(15.0),
child: Screenshot(
controller: buscarModalidadController,
child: Card(
  elevation: 8.0,
  child: Column(
    children: <Widget>[

            ShareGraph(
              screenshotController: buscarModalidadController,
              scaffoldKey: scaffoldKey,
              description: "Reporte por Totales",
              title: "Totales",

            ),
          Container(
            width: MediaQuery.of(context).size.width * 1.0,
            height: 400 +
                ((listatotalesproc == null)
                    ? 0.0
                    : listatotalesproc.length * 30.0),
            child: (datagrafica != null)
                ? new charts.BarChart(
                    datagrafica,
                    behaviors: [
                      new charts.SeriesLegend(
                          position: charts.BehaviorPosition.bottom,
                          horizontalFirst: bool.fromEnvironment("DLC")),
                    ],
                    domainAxis: new charts.OrdinalAxisSpec(
                        renderSpec: new charts.SmallTickRendererSpec(
                            minimumPaddingBetweenLabelsPx: 0,
                            labelStyle: new charts.TextStyleSpec(fontSize: 2))),
                    vertical: true,
                    barRendererDecorator:
                        new charts.BarLabelDecorator<String>(),
                    defaultRenderer: new charts.BarRendererConfig(
                        groupingType: charts.BarGroupingType.groupedStacked,
                        strokeWidthPx: 2.0),
                    animate: true,
                  )
                : Container(
                    //child: Text('cargando ...'),
                  ),
          )
    ],
  ),
),

),

)


        ],
      ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales(this.year, this.sales, this.color);
}
