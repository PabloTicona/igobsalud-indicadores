import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/Contrato.dart';
import 'package:igob_salud_app/ModuleDLC/model/Tramites.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/buscaProceso.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';

class DetalleContrato extends StatelessWidget {
  Tramites tramites;
  DetalleContrato({this.tramites});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DetalleContratoPage(
        tramites: tramites,
      ),
    );
  }
}

class DetalleContratoPage extends StatefulWidget {
  Tramites tramites;
  DetalleContratoPage({this.tramites});

  @override
  _BuscaProcesoState createState() => _BuscaProcesoState();
}

class _BuscaProcesoState extends State<DetalleContratoPage> {
  List<Contrato> listaContratos;

  @override
  void initState() {
    super.initState();

    getContratos();
  }

  getContratos() async {
    var codigo = widget.tramites.codigo;
    var data = '{"codigo": "$codigo"}';
    var lscontratos = await Cliente().ListarContrato('datoscontrato', data);

    setState(() {
      listaContratos = lscontratos;
    });
  }

  descargaDocumento(Contrato parametro) {
    print('data.. 112.');
    //var arch = parametro.archivo;
    //var parsedJson = json.decode(arch.toString());
    print('data...');

    /// print(parsedJson['data']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalle del Contrato"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => BuscaProceso()),
            );
          },
        ),
      ),
      body: (listaContratos != null)
          ? ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: listaContratos.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  //height: 800,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ID:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].id}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Nº DE CONTRATO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].codigo}"),
                              ],
                            ),
                             Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CÓDIGO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].carpeta}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "OBJETO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].objeto}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MODALIDAD:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].modalidad}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ADJUDICADO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].adjudicado}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MONTO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].monto}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "FECHA DE CONTRATO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].fechacontrato}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "PLAZO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].plazo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MULTA:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].multa}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "TIPO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${listaContratos[index].tipo}"),
                              ],
                            ),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.redAccent,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.file_download),
                                        color: Colors.white,
                                        onPressed: () {
                                          descargaDocumento(
                                              listaContratos[index]);
                                          /*Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleCronograma(
                                                      tramites:
                                                          tramitesList[index],
                                                    )),
                                          );*/
                                          //  DetalleTramite
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            )
          : Container(
              child: Text("Cargando.."),
            ),
    );
  }
}
