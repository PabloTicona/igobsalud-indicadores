import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/Cronograma.dart';
import 'package:igob_salud_app/ModuleDLC/model/Tramites.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/buscaProceso.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';

class DetalleCronograma extends StatelessWidget {
  Tramites tramites;
  DetalleCronograma({this.tramites});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DetalleCronogramaPage(
        tramites: tramites,
      ),
    );
  }
}

class DetalleCronogramaPage extends StatefulWidget {
  Tramites tramites;
  DetalleCronogramaPage({this.tramites});

  @override
  _BuscaProcesoState createState() => _BuscaProcesoState();
}

class _BuscaProcesoState extends State<DetalleCronogramaPage> {
  List<Cronograma> listaCronograma;
  @override
  void initState() {
  super.initState();
    getCronograma();
  }

  getCronograma() async {
    var idproceso = widget.tramites.idproc;
    var data = '{"idproceso": "$idproceso"}';
    var lscronograma = await Cliente().ListarCronograma('cronograma', data);

    setState(() {
      listaCronograma = lscronograma;
    });
  }

 @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
      title: Text("Cronograma de actividades"),
      leading: new IconButton(
    icon: new Icon(Icons.arrow_back),
    onPressed: () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BuscaProceso()),
      );
    },
  ),
      ),
      body: (listaCronograma != null)
          ? ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: listaCronograma.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                //  height: 400,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ID:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].id}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Nº:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].numero}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("DESCRIPCIÓN:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].descripcion}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("RESPONSABLE:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].responsable}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("PLAZO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].plazo}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].fecha}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA EJECUCIÓN:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].fechaejecucion}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ID BASE:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaCronograma[index].base}"),
                              ],
                            ),
                             ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            )
          : Container(
              child: Text("Cargando.."),
            ),
    );
  }
}
