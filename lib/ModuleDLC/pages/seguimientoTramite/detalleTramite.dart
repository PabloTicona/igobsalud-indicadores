import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/Tramites.dart';
class DetalleTramite extends StatelessWidget {

 Tramites tramites;
 DetalleTramite({this.tramites});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tramites.codigo),
      ),
      body: ListView(
        children: <Widget>[
          Container(
                  height: 910,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                              Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CÓDIGO DE PROCESO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.idproc}"),
                              ],
                            ),
                              Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("RESPONSABLE ANALISTA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.analista}"),
                              ],
                            ),
                              Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("RESPONSABLE ABOGADO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.abogado}"),
                              ],
                            ),
                              Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("RESPONSABLE SECRETARIO O INFRAESTRUCTURA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.infraestructura}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("DIRECCIÓN ADMINISTRATIVA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.da}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("UNIDAD EJECUTORA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.ue}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CÓDIGO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.codigo}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("OBJETO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.objeto}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MODALIDAD:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.modalidad}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("HOJA DE RUTA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.hr}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MONTO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.monto}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("PLAZO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.plazo}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Nº DE CONVOCATORIA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.convocatoria}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("TIPO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.tipo}"), 
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA DE LA TAREA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.fechaTarea}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA EN SICOES:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.fechaSicoes}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA APERTURA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.fechaApertura}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ACTIVIDAD ACTUAL:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.estado}"),
                              ],
                            ),
                             Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("OBSERVACIONES:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${tramites.observaciones}"),
                              ],
                            ),
                            const SizedBox(height: 15),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
        ],
      ),
    );
  }
}