import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/Tramites.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/detalleContrato.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/detalleCronograma.dart';
import 'package:igob_salud_app/ModuleDLC/pages/seguimientoTramite/detalleTramite.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/btn_buscar.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/input_text.dart';

class BuscaProceso extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscaProceso_(),
    );
  }
}

class BuscaProceso_ extends StatefulWidget {
  @override
  _BuscaProcesoState createState() => _BuscaProcesoState();
}

class _BuscaProcesoState extends State<BuscaProceso_> {
  List<Proceso> procesosmodalidad = [];
  List<Proceso> procesosgestion = [];
  List<Proceso> procesosanalista;

  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsModalidad;
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsGestion;
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsAnalista;

  Proceso selectedItemModalidad;
  Proceso selectedItemGestion;
  Proceso selectedItemAnalista;

  TextEditingController inputBuscarController = new TextEditingController();
  List<Tramites> tramitesList = [];
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;

  CargarMenu() {
    procesosmodalidad
        .add(new Proceso(id: "TODAS", nombre: "TODAS LAS MODALIDADES"));
    procesosmodalidad.add(new Proceso(
        id: "MAYOR", nombre: "APOYO NACIONAL A LA PRODUCCION Y EMPLEO"));
    procesosmodalidad.add(new Proceso(
        id: "MENOR",
        nombre: "APOYO A LA PRODUCCION Y EMPLEO (Hasta 200mil Bolivianos)"));
    procesosmodalidad.add(new Proceso(
        id: "NORMATIVA", nombre: "CON NORMATIVA DEL FINANCIADOR NACIONAL"));
    procesosmodalidad.add(new Proceso(
        id: "DIRECTAM", nombre: "CONTRATACION DIRECTA MAYOR A 200 MIL"));
    procesosmodalidad.add(new Proceso(
        id: "DITECTAMEN",
        nombre: "CONTRATACION DIRECTA MENOR IGUAL A 200 MIL"));
    procesosmodalidad
        .add(new Proceso(id: "MENOR2", nombre: "CONTRATACION MENOR"));
    procesosmodalidad.add(
        new Proceso(id: "EMERGENCIA", nombre: "CONTRATACION POR EMERGENCIA"));
    procesosmodalidad.add(
        new Proceso(id: "EXCEPCION", nombre: "CONTRATACION POR EXCEPCION"));
    procesosmodalidad.add(new Proceso(
        id: "INTERNACIONAL", nombre: "LICITACION PUBLICA INTERNACIONAL"));
    procesosmodalidad.add(
        new Proceso(id: "NACIONAL", nombre: "LICITACION PUBLICA NACIONAL"));

    procesosgestion.add(new Proceso(id: "1", nombre: "PROCESOS VIGENTES"));
    procesosgestion.add(new Proceso(id: "0", nombre: "TODOS LOS PROCESOS"));
  }

  Future<void> listDa() async {
    var asignado = await Cliente().BuscarDatoProceso("comboanalista");
    procesosanalista = [];
    procesosanalista.add(new Proceso(id: "0", nombre: "TODOS LOS ANALISTAS"));
    setState(() {
      procesosanalista.addAll(asignado);
    });
    setValueDefauldSelect();
    _dropdownMenuItemsAnalista = buildDropdownMenuItems(procesosanalista);

    //print(asignado);
  }

  setValueDefauldSelect() {
//List<Proceso> procesosmodalidad = [];
    // List<Proceso> procesosgestion = [];
    // List<Proceso> procesosanalista;

    selectedItemModalidad = procesosmodalidad[0];
    selectedItemGestion = procesosgestion[0];
    selectedItemAnalista = procesosanalista[0];
  }

  @override
  void initState() {
    super.initState();
    CargarMenu();
    listDa();
    recuperardatosusu();
    _dropdownMenuItemsModalidad = buildDropdownMenuItems(procesosmodalidad);
    _dropdownMenuItemsGestion = buildDropdownMenuItems(procesosgestion);
  }

  changeDropdownItemModalidad(Proceso proceso) {
    setState(() {
      selectedItemModalidad = proceso;
    });
  }

  changeDropdownItemGestion(Proceso proceso) {
    setState(() {
      selectedItemGestion = proceso;
    });
  }

  changeDropdownItemAnalista(Proceso proceso) {
    setState(() {
      selectedItemAnalista = proceso;
    });
  }

  Future<void> recuperardatosusu() async {
    var datosusuario = await getDataSession();
    setState(() {
      usuariodlc = datosusuario;
    });

    //print("apellidoMEmp");
    //print(usuariodlc.loginUsu);
  }

  Future<void> onPressed() async {
    var textoBuscar = inputBuscarController.text;

    //print(textoBuscar);
    var idmodalidad = selectedItemModalidad.id;
    var gestion = selectedItemGestion.id;
    var analista = selectedItemAnalista.id;

    var idusuario = usuariodlc.idUsuario;
    var cargo = usuariodlc.idCargoUltimo;
    var ue = usuariodlc.idUnidadEjecutora;

    if (textoBuscar == "") {
      textoBuscar = '0';
    }
    if (idusuario == null) {
      idusuario = 0;
    }
    if (cargo == null) {
      cargo = 0;
    }
    if (ue == null) {
      ue = '0';
    }
    //print(idmodalidad); print(gestion); print(analista);

    var data =
        '{"PalabraClave": "$textoBuscar", "Modalidad":"$idmodalidad","analista":"$analista","ue":"$ue","cargo":"$cargo","gestion":"$gestion","idusuario":"$idusuario"}';
    //print('data = $data');
    var tramitesL = await Cliente().ListarTramites('buscador', data);
    setState(() {
      tramitesList = tramitesL;
    });
    //print(tramitesList);
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buscar Proceso"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[

          InputText(
            ctl: inputBuscarController,
            labelText: 'Código, Objeto, Unidad Ejecutora',
          ),

          //este es el select de gestion
          Combos(
            selectedItem: selectedItemModalidad,
            procesos: procesosmodalidad,
            dropdownMenuItems: _dropdownMenuItemsModalidad,
            changeDropdownItem: changeDropdownItemModalidad,
          ),
          /*este es para el analista*/
          Combos(
            selectedItem: selectedItemAnalista,
            procesos: procesosanalista,
            dropdownMenuItems: _dropdownMenuItemsAnalista,
            changeDropdownItem: changeDropdownItemAnalista,
          ),

          //este es el select de gestion
          Combos(
            selectedItem: selectedItemGestion,
            procesos: procesosgestion,
            dropdownMenuItems: _dropdownMenuItemsGestion,
            changeDropdownItem: changeDropdownItemGestion,
          ),


          const SizedBox(height: 5.0),

          BtnBuscar(
            onPressed: onPressed,
          ),




          Container(
            width: 100,
            height: 700,
            child: ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: tramitesList.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 500,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "DIRECCIÓN ADMINISTRATIVA:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].da}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CÓDIGO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].codigo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "OBJETO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].objeto}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "UNIDAD EJECUTORA:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].ue}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "FECHA DE LA ÚLTIMA ACTIVIDAD:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].fechaTarea}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ACTIVIDAD ACTUAL:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].estado}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "OBSERVACIONES:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${tramitesList[index].observaciones}"),
                              ],
                            ),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.deepOrangeAccent,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.visibility),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleTramite(
                                                      tramites:
                                                          tramitesList[index],
                                                    )),
                                          );
                                          //  DetalleTramite
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.greenAccent,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.date_range),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleCronograma(
                                                      tramites:
                                                          tramitesList[index],
                                                    )),
                                          );
                                          //  DetalleTramite
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.lightBlue,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.insert_drive_file),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleContrato(
                                                      tramites:
                                                          tramitesList[index],
                                                    )),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          )
        ],
      ),
    );
  }
}
