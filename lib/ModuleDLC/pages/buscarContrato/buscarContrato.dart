import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/model/BuscaporNroContrato.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';

import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/btn_buscar.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/input_text.dart';

class BuscarContrato extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscarContrato_(),
    );
  }
}

class BuscarContrato_ extends StatefulWidget {
  @override
  _BuscarContrato createState() => _BuscarContrato();
}

class _BuscarContrato extends State<BuscarContrato_> {
  
  List<Proceso> procesosproponente;

  TextEditingController inputNroContratoController = new TextEditingController();
  List<BuscaporNroContrato> buscarcontratosList = [];



  @override
  void initState() {
    super.initState();
  }

  Future<void> onPressed() async {
    var textonrocontrato = inputNroContratoController.text;
    if (textonrocontrato == "") {
      textonrocontrato = '0';
    }

    print(textonrocontrato);

    var data =
        '{"contrato":"$textonrocontrato"}';
    //print('data 11 ;;; $data');
    var buscarcontr = await Cliente().ListarBuscarContrato('buscarContratos', data);
    setState(() {
      buscarcontratosList = buscarcontr;
    });

    print(buscarcontratosList);
  }

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buscar Contrato"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[

          InputText(
            ctl: inputNroContratoController,
            labelText:'Número de Contrato',
          ),

          BtnBuscar(
            onPressed: onPressed,
          ),

          Container(
            width: 100,
            height: 700,
            child: ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: buscarcontratosList.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  //height: 500,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ID: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].id}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ID PROCESO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].idproc}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Nº DE CONTRATO: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].codigo}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CÓDIGO: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].carpeta}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "OBJETO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].objeto}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MODALIDAD:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].modalidad}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MONTO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].monto}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "FECHA DE CONTRATO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].fechacontrato}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "PLAZO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].plazo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "MULTA:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].multa}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "TIPO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].tipo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ADJUDICADO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${buscarcontratosList[index].adjudicado}"),
                              ],
                            ),
                            const SizedBox(height: 15),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          )
        ],
      ),
    );
  }
}
