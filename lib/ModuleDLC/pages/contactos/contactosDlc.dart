import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';

class ContactosDlc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: _ContactosDlcState(),
    );
  }
}

class _ContactosDlcState extends StatefulWidget {
  @override
  _ContactosDlcsState createState() => _ContactosDlcsState();
}

class _ContactosDlcsState extends State<_ContactosDlcState> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Organización de la Dirección"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePageDLC()),
              );
            },
          ),
        ),
        body:
          Center(
            child: Image(image: AssetImage("assets/images/dlc/contact.png")),
          ),
        );
  }
}