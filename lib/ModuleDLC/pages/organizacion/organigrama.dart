import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';

class Organigrama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: _OrganigramaState(),
    );
  }
}

class _OrganigramaState extends StatefulWidget {
  @override
  _BuscarAnalistaState createState() => _BuscarAnalistaState();
}

class _BuscarAnalistaState extends State<_OrganigramaState> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Organización de la Dirección"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePageDLC()),
              );
            },
          ),
        ),
        body: Center(
            child: Image(image: AssetImage("assets/images/dlc/organi.png")),
          ),
        );
  }
}
