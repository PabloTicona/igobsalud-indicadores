import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/ContratosProponente.dart';
import 'package:igob_salud_app/ModuleDLC/model/DatosProponente.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proponentes.dart';
import 'package:igob_salud_app/ModuleDLC/pages/adjudicacionProponente/buscarProponente.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';

class DetalleContrato extends StatelessWidget {
  Proponentes proponentes;
  DetalleContrato({this.proponentes});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DetalleContratoPage(
        proponentes: proponentes,
      ),
    );
  }
}

class DetalleContratoPage extends StatefulWidget {
  Proponentes proponentes;
  DetalleContratoPage({this.proponentes});

  @override
  _BuscaProponenteState createState() => _BuscaProponenteState();
}

class _BuscaProponenteState extends State<DetalleContratoPage> {
  List<ContratosProponente> listaDetalleContratos;

  @override
  void initState() {
    super.initState();

    getContratos();
  }

    getContratos() async {
    var idprop = widget.proponentes.idprop;
    var data = '{"idprop": "$idprop"}';
    var lsdetallecontratos = await Cliente().ListarContratosProponente('contratosProponente', data);

    setState(() {
      listaDetalleContratos = lsdetallecontratos;
    });
  }

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalle del Contrato"),
       leading: new IconButton(
    icon: new Icon(Icons.arrow_back),
    onPressed: () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BuscaProponente()),
      );
    },
  ),
      ),
      body: (listaDetalleContratos != null)
          ? ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: listaDetalleContratos.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                 // height: 700,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ID: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].id}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Nº DE CONTRATO: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].codigo}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CÓDIGO: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].carpeta}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("OBJETO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].objeto}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MODALIDAD:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].modalidad}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MONTO: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].monto}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("FECHA DE CONTRATO: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].fechacontrato}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("PLAZO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].plazo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MULTA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].multa}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("TIPO: ", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleContratos[index].tipo}"),
                              ],
                            ),
                             ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            )
          : Container(
              child: Text("Cargando.."),
            ),
    );
  }
}
