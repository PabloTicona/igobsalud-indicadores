import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/model/DatosProponente.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proponentes.dart';
import 'package:igob_salud_app/ModuleDLC/pages/adjudicacionProponente/buscarProponente.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';

class DetalleProponente extends StatelessWidget {
  Proponentes proponentes;
  DetalleProponente({this.proponentes});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DetalleProponentePage(
        proponentes: proponentes,
      ),
    );
  }
}

class DetalleProponentePage extends StatefulWidget {
  Proponentes proponentes;
  DetalleProponentePage({this.proponentes});

  @override
  _BuscaProponenteState createState() => _BuscaProponenteState();
}

class _BuscaProponenteState extends State<DetalleProponentePage> {
  List<DatosProponente> listaDetalleProponente;
  @override
  void initState() {
  super.initState();
    getDatosProponente();
  }

  getDatosProponente() async {
    var idprop = widget.proponentes.idprop;
    var data = '{"idprop": "$idprop"}';
    var lsdetalleproponente = await Cliente().ListarDatosProponente('masdatosProponente', data);

    setState(() {
      listaDetalleProponente = lsdetalleproponente;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalle del Proponente"),
       leading: new IconButton(
    icon: new Icon(Icons.arrow_back),
    onPressed: () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BuscaProponente()),
      );
    },
  ),
      ),
      body: (listaDetalleProponente != null)
          ? ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: listaDetalleProponente.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 800,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CÓDIGO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].id}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("RAZÓN SOCIAL:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].racsocial}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("TIPO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].institu}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("NIT:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].nit}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("MATRÍCULA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].matricula}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("LICENCIA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].licencia}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("DIRECCIÓN:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].direccion}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ZONA:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].zona}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CIUDAD:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].ciudad}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("TELÉFONO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].telf}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CELULAR:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].celular}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CORREO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].correo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("REPRESENTATE LEGAL:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].replegal}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CARNET DE IDENTIDAD:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].ci}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("ESTADO:", style: TextStyle(fontWeight: FontWeight.bold),),
                                Text("${listaDetalleProponente[index].estado}"),
                              ],
                            ),
                             ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            )
          : Container(
              child: Text("Cargando.."),
            ),
    );
  }
}