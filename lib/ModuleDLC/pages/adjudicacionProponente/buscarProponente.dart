import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proponentes.dart';
import 'package:igob_salud_app/ModuleDLC/pages/adjudicacionProponente/detalleContrato.dart';
import 'package:igob_salud_app/ModuleDLC/pages/adjudicacionProponente/detalleProponente.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/btn_buscar.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/input_text.dart';

class BuscaProponente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscaProponente_(),
    );
  }
}

class BuscaProponente_ extends StatefulWidget {
  @override
  _BuscaProponente createState() => _BuscaProponente();
}

class _BuscaProponente extends State<BuscaProponente_> {
  List<Proceso> procesosproponente;
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsProponente;

  Proceso selectedItemProponente;
  TextEditingController inputRacSocController = new TextEditingController();
  TextEditingController inputNitController = new TextEditingController();
  TextEditingController inputCiController = new TextEditingController();
  List<Proponentes> proponentesList = [];

  Future<void> listDa() async {
    var tipo = await Cliente().BuscarDatoProceso("listatipoinstitucion");
    procesosproponente = [];
    procesosproponente.add(new Proceso(id: "0", nombre: "TODOS LOS TIPOS DE INSTITUCIÓN"));
    setState(() {
      procesosproponente.addAll(tipo);
    });
    setValueDefauldSelect();
    _dropdownMenuItemsProponente = buildDropdownMenuItems(procesosproponente);

    //print(asignado);
  }

  setValueDefauldSelect() {
    selectedItemProponente = procesosproponente[0];
  }

  @override
  void initState() {
    super.initState();
    listDa();
  }

  changeDropdownItemProponente(Proceso proceso) {
    setState(() {
      selectedItemProponente = proceso;
    });
  }

  Future<void> onPressed() async {
    var textoRacSocial = inputRacSocController.text;
    var textoNit = inputNitController.text;
    var textoCi = inputCiController.text;
    var tipo = selectedItemProponente.id;

    print("textoRacSocial ${textoRacSocial}");

    if (textoRacSocial == "") {
      textoRacSocial = '0';
    }
    if (textoNit == "") {
      textoNit = '0';
    }
    if (textoCi == "") {
      textoCi = '0';
    }

    print(inputNitController);
    print(textoNit);
    print(textoCi);
    print(tipo);

    var data =
        '{"razonsocial": "$textoRacSocial","nit": "$textoNit","ci": "$textoCi","tipo": "$tipo"}';
    //print('data 11 ;;; $data');
    var peoponentesL =
        await Cliente().ListarProponentes('buscarProponente', data);
    setState(() {
      proponentesList = peoponentesL;
    });

    print(proponentesList);
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buscar Proponente"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[

         new InputText(
            ctl: inputRacSocController,
            labelText:'Razón social',
          ),

          new InputText(
            ctl: inputNitController,
            labelText:'NIT',
          ),

          new InputText(
            ctl: inputCiController,
            labelText:'Carnet de identidad',
          ),

          Combos(
            selectedItem: selectedItemProponente,
            procesos: procesosproponente,
            dropdownMenuItems: _dropdownMenuItemsProponente,
            changeDropdownItem: changeDropdownItemProponente,
          ),

          BtnBuscar(
            onPressed: (){
              onPressed();
            },
          ),


          Container(
            width: 100,
            height: 700,
            child: ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: proponentesList.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  //height: 500,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CÓDIGO: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].idprop}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "RAZÓN SOCIAL:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].razonsocial}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "NIT: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].nit}"),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CARNET DE IDENTIDAD: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].ci}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "CORREO:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].correo}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "TIPO DE PERSONA:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("${proponentesList[index].tippersona}"),
                              ],
                            ),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.deepOrangeAccent,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.visibility),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleProponente(
                                                      proponentes:
                                                          proponentesList[
                                                              index],
                                                    )),
                                          );
                                          //  DetalleTramite
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  color: Colors.white,
                                  child: Center(
                                    child: Ink(
                                      decoration: const ShapeDecoration(
                                        color: Colors.lightBlue,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Icon(Icons.insert_drive_file),
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetalleContrato(
                                                      proponentes:
                                                          proponentesList[
                                                              index],
                                                    )),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          )
        ],
      ),
    );
  }
}
