import 'dart:math';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:igob_salud_app/ModuleDLC/ayudas/serviciosSesion.dart';
import 'package:igob_salud_app/ModuleDLC/model/Proceso.dart';
import 'package:igob_salud_app/ModuleDLC/model/RepAnalista.dart';
import 'package:igob_salud_app/ModuleDLC/model/Usuario.dart';
import 'package:igob_salud_app/ModuleDLC/servicios/cliente.dart';
import 'package:igob_salud_app/ModuleDLC/widgets/combos.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';

class BuscarAnalista extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dirección de licitaciones y contrataciones',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: BuscarAnalista_(),
    );
  }
}

class BuscarAnalista_ extends StatefulWidget {
  @override
  _BuscarAnalistaState createState() => _BuscarAnalistaState();
}

class _BuscarAnalistaState extends State<BuscarAnalista_> {
  List<Proceso> procesosanalista = [];
  List<Proceso> procesosgestion = [];
  List<RepAnalista> listaprocanalistas = [];

  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsAnalista;
  List<DropdownMenuItem<Proceso>> _dropdownMenuItemsGestion;

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  ScreenshotController buscarAnalistaController = ScreenshotController();

  Proceso selectedItemAnalista;
  Proceso selectedItemGestion;
  
  List<Usuario> recuperacargousuario = [];
  Usuario usuariodlc;

  List<charts.Series<OrdinalSales, String>> datagrafica;
  CargarMenu() {
    procesosanalista.add(new Proceso(id: "1", nombre: "ANALISTA ASIGNADO"));
    procesosanalista.add(new Proceso(id: "2", nombre: "ASESOR LEGAL ASIGNADO"));
    procesosanalista.add(new Proceso(
        id: "3",
        nombre: "SECRETARIO DE LA COMISIÓN O ANALISTA DE INFRAESTRUCTURA"));

    procesosgestion.add(new Proceso(id: "1", nombre: "PROCESOS VIGENTES"));
    procesosgestion.add(new Proceso(id: "2", nombre: "TODOS LOS PROCESOS"));
  }

  @override
  void initState() {
    super.initState();
    CargarMenu();

    _dropdownMenuItemsAnalista = buildDropdownMenuItems(procesosanalista);
    _dropdownMenuItemsGestion = buildDropdownMenuItems(procesosgestion);
  }

  changeDropdownItemAnalista(Proceso proceso) {
    setState(() {
      selectedItemAnalista = proceso;
    });
  }

  changeDropdownItemGestion(Proceso proceso) {
    setState(() {
      selectedItemGestion = proceso;
    });
  }

  Future<void> recuperardatosusu() async {
   var datosusuario = await getDataSession();
    usuariodlc = datosusuario;
     var idusuario = usuariodlc.idUsuario;
     var cargo = usuariodlc.idCargoUltimo;
      if (idusuario == null) { idusuario = 0; }
      if (cargo== null) { cargo = 0; }
    
    }

  Future<void> listaAnalista(String analista, String gestion) async {
    setState(() {
      listaprocanalistas = null;
      datagrafica = null;
    });

    var data = '{"analista":"$analista","gestion":"$gestion"}';

    listaprocanalistas = await Cliente().buscarAnalista('repAnalista', data);
    //  print('listatotalesP $listatotalesP');
    setState(() {
      ///   listatotalesproc = listatotalesP;
    });

    List<charts.Series<OrdinalSales, String>> result = [];

    for (RepAnalista item in listaprocanalistas) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.name,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }

    setState(() {
      datagrafica = result;
    });
  }

  onPressed() {
    var idanalista = '1';
    var gestion = '1';
    print(idanalista);
    print(gestion);
    if (selectedItemAnalista != null) {
      idanalista = selectedItemAnalista.id;
    }

    if (selectedItemGestion != null) {
      gestion = selectedItemGestion.id;
    }

    listaAnalista(idanalista, gestion);
  }

  List<DropdownMenuItem<Proceso>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Proceso>> items = List();

    if (items_ != null) {
      for (Proceso itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.nombre),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  Random random = new Random();
  String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(random.nextInt(16)) | 0];
    return hex;
  }

  List<charts.Series<OrdinalSales, String>> _createSampleData() {
    List<charts.Series<OrdinalSales, String>> result = [];

    for (RepAnalista item in listaprocanalistas) {
      result.add(new charts.Series<OrdinalSales, String>(
        id: item.name,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (OrdinalSales sales, _) => sales.color,
        data: [
          new OrdinalSales('${item.y}', item.y,
              charts.Color.fromHex(code: generateRandomHexColor())),
        ],
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reporte por analista"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageDLC()),
            );
          },
        ),
      ),
      body: ListView(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Combos(
            selectedItem: selectedItemAnalista,
            procesos: procesosanalista,
            dropdownMenuItems: _dropdownMenuItemsAnalista,
            changeDropdownItem: changeDropdownItemAnalista,
          ),
          //este es el select de gestion
          Combos(
            selectedItem: selectedItemGestion,
            procesos: procesosgestion,
            dropdownMenuItems: _dropdownMenuItemsGestion,
            changeDropdownItem: changeDropdownItemGestion,
          ),
          const SizedBox(height: 20),
          Container(
            width: MediaQuery.of(context).size.width * 1.0,
            height: 60,
            child: Padding(padding: EdgeInsets.all(10.0),
            child: Material(
              borderRadius: BorderRadius.circular(5.0),
              elevation: 5.0,
              color: Colors.red,
              child: InkWell(
                borderRadius: BorderRadius.circular(5.0),
                onTap: () => {onPressed()}, // handle your onTap here
                child: Container(
                  child: Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 6.9, 0.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.white,
                            // size: 30.0,
                          ),
                        ),
                        Text(
                          "BUSCAR",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ),
          ),

Padding(padding: EdgeInsets.all(15.0),
child: Screenshot(
controller: buscarAnalistaController,
child: Card(
  elevation: 8.0,
  child: Column(
    children: <Widget>[

            ShareGraph(
              screenshotController: buscarAnalistaController,
              scaffoldKey: scaffoldKey,
              description: "Reporte procesos por analista",
              title: "Reporte por Analista",

            ),

          Container(
            width: MediaQuery.of(context).size.width * 1.0,
            height: 400 +
                ((listaprocanalistas == null)
                    ? 0.0
                    : listaprocanalistas.length * 30.0),
            child: (datagrafica != null)
                ? new charts.BarChart(
                    datagrafica,
                    behaviors: [
                      new charts.SeriesLegend(
                          position: charts.BehaviorPosition.bottom,
                          horizontalFirst: bool.fromEnvironment("DLC")),
                    ],
                    vertical: true,
                    barRendererDecorator:
                        new charts.BarLabelDecorator<String>(),
                    defaultRenderer: new charts.BarRendererConfig(
                        groupingType: charts.BarGroupingType.groupedStacked,
                        strokeWidthPx: 2.0),
                    animate: true,
                  )
                : Container(
                    //child: Text('cargando ...'),
                  ),
          )
    ],
  ),
),

),

)


        ],
      ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales(this.year, this.sales, this.color);
}
