import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:global_configuration/global_configuration.dart';

Future<List<dynamic>> getResponseDinamico(String query) async {
  print("--------------------------------");
  print("==> query dinamico : $query");

  //"SALUD_REPORTES": "http://sersalud.lapaz.bo/wsSalud",

  // var url_ = GlobalConfiguration().getString("SALUD_REPORTES");
  var url_ = "http://sersalud.lapaz.bo/wsSalud";
  //print("url_ $url_");
  var resp = [];
  var response = await http.post("${url_}/dinamico", body: {'consulta': query});
  // print("response.body ${response.body}");
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);

    resp = jsonResponse['success']['data'][0]['sp_dinamico'];
    if (resp == null) {
      return [];
    }
    print("--------------------------------");
    print("==> resp : $resp");
  } else {
    //  showInSnackBar("Error de conexión de red");
    throw Exception("Error de conexión de red");
    // resp = null;
  }

  return resp;
}


