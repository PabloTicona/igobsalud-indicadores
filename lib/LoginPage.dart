import 'package:flutter/material.dart';


import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:igob_salud_app/style/theme.dart' as Theme;

var url = 'http://sersalud.lapaz.bo/wsSalud';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Login(),
    );
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  void initState() {}

  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodeName = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextLogin = true;
  bool _obscureTextSignup = true;
  bool _obscureTextSignupConfirm = true;

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  String _user, _pass;
  final _formKey = GlobalKey<FormState>();

  bool loadLoginButton = true;

  Widget _buildSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[

              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 190.0,
                  child: Form(
                    key: _formKey,
                    child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextFormField(
                          focusNode: myFocusNodeEmailLogin,
                          controller: loginEmailController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: 16.0,
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.user,
                              color: Colors.black,
                              size: 22.0,
                            ),
                            hintText: "Usuario",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: 17.0),
                          ),
                          validator: (String val) {
                            if (val.length == 0) {
                            //  return "";// "La contraseña es requerido";
                            } else {
                              // return null;
                            }
                          },
                          onSaved: (str) {
                            _user = str;
                          },
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextFormField(
                          focusNode: myFocusNodePasswordLogin,
                          controller: loginPasswordController,
                          obscureText: _obscureTextLogin,
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: 16.0,
                              color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              size: 22.0,
                              color: Colors.black,
                            ),
                            hintText: "Contraseña",
                            hintStyle: TextStyle(
                                fontFamily: "WorkSansSemiBold", fontSize: 17.0),
                            suffixIcon: GestureDetector(
                              onTap: _toggleLogin,
                              child: Icon(
                                _obscureTextLogin
                                    ? FontAwesomeIcons.eye
                                    : FontAwesomeIcons.eyeSlash,
                                size: 15.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          validator: (val) {
                            if (val.length == 0) {
                            //  return "" ;//"La contraseña es requerido";
                            } else {
                              // return null;
                            }
                          },

                          onSaved: (str) {
                            _pass = str;
                          },

                        ),
                      ),
                    ],
                  ),),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 170.0),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Theme.ColorsSiis.loginGradientStart,
                      offset: Offset(0.0, 0.0),
                      blurRadius: 0.0,
                    ),
                    BoxShadow(
                      color: Theme.ColorsSiis.loginGradientEnd,
                      offset: Offset(0.0, 0.0),
                      blurRadius: 0.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        Theme.ColorsSiis.loginGradientEnd,
                        Theme.ColorsSiis.loginGradientStart
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: loadLoginButton? MaterialButton(
                  highlightColor: Colors.transparent,
                  splashColor: Theme.ColorsSiis.loginGradientEnd,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 13.0, horizontal: 40.0),
                    child: Text(
                      "Entrar",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: "WorkSansBold"),
                    ),
                  ),
                  onPressed: () {
                    final form = _formKey.currentState;
                    form.save();

                    if (form.validate()) {
                      autentificacion();
                    }
                  },
                ):MaterialButton(
                  highlightColor: Colors.transparent,
                  splashColor: Theme.ColorsSiis.loginGradientEnd,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 13.0, horizontal: 40.0),
                    child: Container(
                      width: 50,
                      child: SpinKitThreeBounce(
                        color: Colors.white,
                        size: 20.0,
                      ),
                    ),
                  ),

                ),
              ),
            ],
          ),

          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.white10,
                          Colors.white,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    "GAMLP",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontFamily: "WorkSansMedium"),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.white,
                          Colors.white10,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }




  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void _toggleSignup() {
    setState(() {
      _obscureTextSignup = !_obscureTextSignup;
    });
  }

  void _toggleSignupConfirm() {
    setState(() {
      _obscureTextSignupConfirm = !_obscureTextSignupConfirm;
    });
  }

  Future<void> setDataSession(data) async {
    var usuario_ = data['data'][0]['usr'];
    var nombre_ =
        "${data['data'][0]['nombre']} ${data['data'][0]['paterno']} ${data['data'][0]['materno']}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('usuario', usuario_);
    await prefs.setString('nombre', nombre_);
    setState(() {
      loadLoginButton = true;
    });

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
     //  MaterialPageRoute(builder: (context) => HomePageDLC()),
    );
  }

  Future<void> autentificacion() async {
    setState(() {
      loadLoginButton = false;
    });

    FocusScope.of(context).requestFocus(FocusNode());
    var nomUser = _user.toString().toLowerCase().trim();
    var contr = _pass.toString().trim();


    print("nomUser :  ${nomUser}");
    print("contr  : ${contr}");

    var urlLogin = "http://serceropapel.lapaz.bo/wsCP/autenticacion";
    var credenciales = '{"usuario":"$nomUser","contrasenia":"$contr"}';
    var body_ = {
      'path': '/dreamfactory/dist/adLDAP/auth_ceropapel_app.php',
      'cuerpo': credenciales,
      'host': '192.168.5.141'
    };

  
    var response = await http.post(urlLogin, body: body_);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      if (jsonResponse['success'] != null) {
        setDataSession(jsonResponse['success']);
      } else {
        setState(() {
          loadLoginButton = true;
        });


        scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("Verifique su usuario y contraseña."),
          duration: new Duration(seconds: 8),
        ));
      }
    } else {
      setState(() {
        loadLoginButton = true;
      });

      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("Verifique su conexión a Internet."),
        duration: new Duration(seconds: 5),
      ));
      //  showInSnackBar("Error de conexión de red");
      throw Exception("Error de conexión de red");
      // resp = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      body: Container(

        decoration: new BoxDecoration(
          image: new DecorationImage(
            //  colorFilter: ColorFilter.mode(Color(0xffe2cb4a), BlendMode.colorBurn),
            image: new ExactAssetImage('assets/images/background_siis.jpg',
                scale: 1.0),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height >= 775.0
                ? MediaQuery.of(context).size.height
                : 775.0,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 35.0),
                  child: Image.asset(
                    'assets/images/logo1.png',
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: PageView(
                    children: <Widget>[
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignIn(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}
