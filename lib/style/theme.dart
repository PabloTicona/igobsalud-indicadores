import 'dart:ui';

import 'package:flutter/cupertino.dart';

class ColorsSiis {
  const ColorsSiis();

  static const Color loginGradientStart = const Color(0xFFff1a1a);
  static const Color loginGradientEnd = const Color(0xFFffd800);

  static const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static Widget AppBarColor() {
    return new Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              stops: [0.0, 1.8],
              tileMode: TileMode.clamp,
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(0.0, 0.9),
              colors: <Color>[


                Color(0xFFC62828),
                Color(0xFFFF151A),
                


              //  Color(0xFFdab355),
               // Color(0xFF6fab11),
               // Color(0xFF6fab11),
              ])),
    );
  }
  static Decoration FondoImagen(){
    return BoxDecoration(
     // color: const Color(0xffFAFAFA),
      image: DecorationImage(

        image: AssetImage("assets/images/fondo_app.jpg"),
        repeat: ImageRepeat.repeat,
       // fit: BoxFit.contain
        //fit: BoxFit.cover,
       // alignment: const Alignment(2.5, 3.5),
        // fit: BoxFit.cover,
      ),
    );
  }



}
