import 'package:flutter/material.dart';

Widget AppBarColor(){
  return  new Container(
    decoration: BoxDecoration(
        gradient: LinearGradient(
            stops: [0.0, 1.8],
            tileMode: TileMode.clamp,
            begin: const FractionalOffset(0.0, 2.9),
            end: const FractionalOffset(0.0, 0.0),
            colors: <Color>[
              Color(0xFFdab355),
              Color(0xFF6fab11),
            ])
    ),
  );
}


