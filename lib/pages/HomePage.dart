import 'dart:io';

import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:igob_salud_app/ModuleDLC/HomePageDLC.dart';

import 'package:igob_salud_app/pages/consultaExterna/HomeConsultaExterna.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/pages/hermodialisis/HomeHemodialisis.dart';
import 'package:igob_salud_app/pages/hospitalizacion/HomeHospitalizacion.dart';
import 'package:igob_salud_app/pages/serviosComplementarios/HomeServiciosComplementarios.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:igob_salud_app/LoginPage.dart';

import 'package:fancy_dialog/FancyAnimation.dart';
import 'package:fancy_dialog/FancyGif.dart';
import 'package:fancy_dialog/FancyTheme.dart';
import 'package:fancy_dialog/fancy_dialog.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;

import 'package:igob_salud_app/pages/asignacionRolesTurnos/HomeAsignacion.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Login',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          '/': (context) => Home(),
          '/consultaexterna': (context) => HomeConsultaExterna(),
          '/emergencias': (context) => HomeEmergencias(),
          '/servicioscomplementarios': (context) =>
              HomeServiciosComplementarios(),
          '/hospitalizacion': (context) => HomeHospitalizacion(),
          '/hermodialisis': (context) => HomeHermodialisis(),
        });
  }
}

//https://www.youtube.com/watch?v=I26rzdjqVXA
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var usuarioDrawer_ = "";
  var nombreDrawer_ = "";

  @override
  void initState() {
    super.initState();
    // BackButtonInterceptor.add(myInterceptor);
    getDataSessionDrawer();
    BackButtonInterceptor.add(myInterceptor, zIndex: 2, name: "SomeName");
  }

  @override
  void dispose() {
    //BackButtonInterceptor.remove(myInterceptor);
    BackButtonInterceptor.removeByName("SomeName");
    super.dispose();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      _showDialog();
    }

    print("Navigator ${Navigator.canPop(context)}");

    print("BACK BUTTON!"); // Do some stuff.
    return true;
  }

  Future<void> getDataSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var usuario_ = await prefs.getString('usuario');
    var nombre_ = await prefs.getString('nombre');

    showDialog(
        context: context,
        builder: (BuildContext context) => FancyDialog(
          title: usuario_,
          descreption: "${nombre_}  ",
          animationType: FancyAnimation.TOP_BOTTOM,
          theme: FancyTheme.FANCY,
          cancel: "Cerrar",
          ok: "Salir",
          gifPath: FancyGif.FUNNY_MAN, //'./assets/walp.png',
          okFun: () => exit(0),
        ));
  }

  Future<void> cerrarSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var borrarDatos = await prefs.clear();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false);
  }

  Future<void> getDataSessionDrawer() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var usuarioD_ = await prefs.getString('usuario');
    var nombreD_ = await prefs.getString('nombre');

    setState(() {
      usuarioDrawer_ = usuarioD_;
      nombreDrawer_ = nombreD_;
    });
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Atención"),
          content: new Text("¿Desea salir de la aplicación?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("SI"),
              onPressed: () => exit(0),
            ),

            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("NO"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _offsetPopup() => PopupMenuButton<int>(
    padding: EdgeInsets.all(0.0),
    tooltip: 'Usuario',
    onSelected: (value) {
      switch (value) {
        case 2:
          cerrarSession();
          break;

        case 3:
          exit(0);
          break;

        default:
      }

     
    },
    itemBuilder: (context) => [
      PopupMenuItem(
        enabled: false,
        value: 1,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: <Widget>[
            Icon(
              MdiIcons.accountCircle,
            ),
            Text(usuarioDrawer_)
          ],
        ),
      ),
      PopupMenuItem(
        value: 2,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: <Widget>[
            Icon(
              MdiIcons.exitToApp,
            ),
            Text(" Cerrar Sesión")
          ],
        ),
      ),
      PopupMenuItem(
        value: 3,
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: <Widget>[
            Icon(
              MdiIcons.locationExit,
            ),
            Text(" Salir de la Aplicación")
          ],
        ),
      ),
    ],
    elevation: 10.0,
    icon: Icon(
      Icons.account_circle,
      color: Colors.white,
      size: 32,
    ),
//          offset: Offset(0, 100),
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

resizeToAvoidBottomPadding: false,
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(

              accountName: new Text(usuarioDrawer_),
              accountEmail: new Text(nombreDrawer_, style: TextStyle(fontFamily: "WorkSansSemiBold", color: Colors.white),),

              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new ExactAssetImage('assets/images/image1.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                  backgroundImage:
                  ExactAssetImage("assets/images/usuario.jpg")),
            ),
            new ListTile(
              title: Text("Cerrar sesión"),
              trailing: new Icon(Icons.exit_to_app),
              onTap: () {
                cerrarSession();
                /*
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false); */
              },
            ),


            new ListTile(
              title: Text("Asignación"),
              subtitle: Text("Roles y Turnos"),
              trailing: new Icon(Icons.group_add),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeAsignacion()),
                  );
              },
            ),

                        new ListTile(
              title: Text("DLC"),
              subtitle: Text("Contrataciones"),
              trailing: new Icon(Icons.airplay),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePageDLC()),
                  );
              },
            ),

          ],
        ),
      ),

      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 340,
            floating: true,
            pinned: true,
            actions: <Widget>[
              _offsetPopup(),
              /*   IconButton(
                icon: Icon(Icons.account_circle),
                tooltip: 'Usuario',
                onPressed: () {
                  getDataSession();

                  // handle the press
                },
              ), */
            ],
            flexibleSpace: FlexibleSpaceBar(
              //   titlePadding: EdgeInsets.all(18.0),
              //  title: Text("Reportes SIIS"),

              background: Carousel(
                images: [
                  ExactAssetImage("assets/images/hosp_cotahuma.jpg"),
                  ExactAssetImage("assets/images/hosp_lamerced.jpg"),
                  ExactAssetImage("assets/images/hosp_pinos.jpg"),
                  ExactAssetImage("assets/images/hosp_portada.jpg")
                ],
                dotSize: 1.5,
                dotSpacing: 15.0,
                dotColor: Color(0xff485154),
                indicatorBgPadding: 0.5,
                // dotBgColor: Color(0xffe2cb4a).withOpacity(0.5),
                borderRadius: false,
              ),
            ),
          ),
          SliverFillRemaining(
          //  fillOverscroll: false,
           // hasScrollBody: false,
              child: Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CardReporte(
                          nom: 'Consulta Externa',
                          imagen: 'assets/images/home/consulta_externa.jpg',
                          descripcion:
                          'Resporte por especialidad, indicadores por proceso. Solicitantes, tendidos...',
                          page: HomeConsultaExterna(),
                          ruta: '/consultaexterna',
                        ),
                        new CardReporte(
                          nom: 'Emergencias',
                          imagen: 'assets/images/home/emergencias.jpg',
                          descripcion:
                          'Atencion por triaje. Resporte de cubiculos, indicadores por proceso...',
                          page: HomeEmergencias(),
                          ruta: '/emergencias',
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CardReporte(
                          nom: 'Servicios Complementarios',
                          imagen: 'assets/images/home/servicios.jpg',
                          descripcion: 'Indicadores por mes...',
                          page: HomeServiciosComplementarios(),
                          ruta: '/servicioscomplementarios',
                        ),
                        new CardReporte(
                          nom: 'Hospitalización',
                          imagen: 'assets/images/home/hospitalizacion.jpg',
                          descripcion:
                          'Cantidad de camas por hospital, Los Pinos, La merced...',
                          page: HomeHospitalizacion(),
                          ruta: '/hospitalizacion',
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CardReporte(
                          nom: 'Hermodialisis',
                          imagen: 'assets/images/home/hemodialisis.jpg',
                          descripcion: 'Cantidad de pacientes por hospital...',
                          page: HomeHermodialisis(),
                          ruta: '/hermodialisis',
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Text(""),
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 160,
                      child: Text(""),
                    )

                  ],
                )
              )
          )
        ],
      ),
    );
  }
}

class CardReporte extends StatelessWidget {
  final String nom;
  final String imagen;
  final String descripcion;
  final StatelessWidget page;
  final String ruta;

  const CardReporte(
      {this.nom, this.imagen, this.descripcion, this.page, this.ruta});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(
      width: MediaQuery.of(context).size.width * 0.5, //210

      child: Padding(
        padding: EdgeInsets.all(9.0),
        child: Material(
          borderRadius: BorderRadius.circular(5.0),
          elevation: 5.0,
          child: InkWell(
            borderRadius: BorderRadius.circular(5.0),
            child: Column(
              children: <Widget>[
                Material(
                  elevation: 4.0,
                  shape: CircleBorder(),
                  color: Colors.transparent,
                  child: Ink.image(
                    image: AssetImage(imagen),
                    fit: BoxFit.cover,
                    //  width: 120.0,
                    height: 84.0,
                    child: InkWell(
                      child: null,
                    ),
                  ),
                ),
                new Padding(
                    padding: new EdgeInsets.all(7.0),
                    child: new Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            nom,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 17,
                                color: Color(0xffe2cb4a),
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            descripcion,
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 10, color: Colors.black),
                          ),
                        ),
                      ],
                    ))
              ],
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => page),
              );
            },
          ),
        ),
      ),
    );
  }
}
