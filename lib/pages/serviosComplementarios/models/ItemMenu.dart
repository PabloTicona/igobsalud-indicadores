
class ItemMenu {
  final int id;
  final String servicio;
  final String tipoIndicador;
  final String indicador;
  final String especialidad;
  final String numerador;
  final String denominador;

  ItemMenu(this.id, this.servicio, this.tipoIndicador, this.indicador,
      this.especialidad, this.numerador, this.denominador);

  @override
  String toString() {
    return 'ItemMenu{id: $id, servicio: $servicio, tipoIndicador: $tipoIndicador, indicador: $indicador, especialidad: $especialidad, numerador: $numerador, denominador: $denominador}';
  }


}
