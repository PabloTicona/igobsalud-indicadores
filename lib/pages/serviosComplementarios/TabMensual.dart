import 'package:flutter/material.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/pages/serviosComplementarios/models/ItemMenu.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;

typedef void TapCallback(dateTime, List<int> index);
typedef void TapCallbackItem(ItemMenu i);
typedef void OnChangeDropdownItem(ItemMenu item);

class TabMensual extends StatelessWidget {
  final List<ItemMenu> listaItems;
  final DateTime anio;
  final DateTime mesInicio;
  final DateTime mesfin;

  final ItemMenu selectedItem;

  final TapCallback onTapAnio;
  final TapCallback onTapMesInicio;
  final TapCallback onTapMesFin;



  final OnChangeDropdownItem changeDropdownItem;
  final PressCallback onPressed;

  TabMensual(
      {this.listaItems,
      this.anio,
      this.mesInicio,
      this.mesfin,
      this.selectedItem,
      this.changeDropdownItem,
      this.onPressed,
      this.onTapAnio,
      this.onTapMesInicio,
      this.onTapMesFin,
      });

  List<DropdownMenuItem<ItemMenu>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<ItemMenu>> items = List();

    if (items_ != null) {
      for (ItemMenu itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.indicador),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  List<DropdownMenuItem<ItemMenu>> _dropdownMenuItems;

  @override
  Widget build(BuildContext context) {

    _dropdownMenuItems = buildDropdownMenuItems(listaItems);

    return Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Año: ',
                    fechaInicio: anio,
                    formatoFecha: 'A',
                    formatoFechaComponete: 'yyyy',
                    onTap: onTapAnio),
                new CardDatePickerComponent(
                    nombre: 'Mes Inicio: ',
                    fechaInicio: mesInicio,
                    formatoFecha: 'M',
                    formatoFechaComponete: 'MMMM',
                    onTap: onTapMesInicio),
                new CardDatePickerComponent(
                    nombre: 'Mes Fin: ',
                    fechaInicio: mesfin,
                    formatoFecha: 'M',
                    formatoFechaComponete: 'MMMM',
                    onTap: onTapMesFin),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.78,
                  height: 94,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: (listaItems != null)
                              ? new DropdownButton(
                                  isExpanded: true,
                                  value: (selectedItem != null)
                                      ? selectedItem
                                      : listaItems[0],
                                  items: _dropdownMenuItems,
                                  onChanged:
                                      changeDropdownItem, //changeDropdownItem,
                                  iconSize: 24,
                                  elevation: 16,
                                  style: TextStyle(color: Colors.black),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.red,
                                  ),
                                )
                              : Center(
                                  child: Text("Cargando.."),
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.22,
                  child: new ButtonMaterialCircleComponent(
                      size: 20.0, onPressed: onPressed),
                ),
              ],
            ),

          ],
        ));
  }
}
