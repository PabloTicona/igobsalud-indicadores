import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DetalleItemMensual extends StatelessWidget {

  final List<charts.Series> seriesList;
  final bool animate;
  final bool showChartMensual;

  DetalleItemMensual(this.seriesList, {this.animate, this.showChartMensual});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalle"),
      ),
      body: Card(
        child: Padding(padding: EdgeInsets.all(8.0),

        child: Container(
          height: 350,
          width: double.infinity,

          child: showChartMensual? new charts.TimeSeriesChart(
              seriesList,
              animate: animate,
              defaultRenderer: new charts.LineRendererConfig(includePoints: true),


              behaviors: [
                new charts.SeriesLegend(
                    position: charts.BehaviorPosition.bottom,
                    horizontalFirst: bool.fromEnvironment("Pablo")),
              ],

              domainAxis: new charts.DateTimeAxisSpec(
                  tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                      day: new charts.TimeFormatterSpec(
                        format: 'd',
                        transitionFormat: 'MM',
                      )
                  )
              ),



              /// Customize the gridlines to use a dash pattern.
              primaryMeasureAxis: new charts.NumericAxisSpec(
                  renderSpec: charts.GridlineRendererSpec(
                      lineStyle: charts.LineStyleSpec(
                        dashPattern: [4, 4],
                      )
                  )
              )
              
          ): Text("Cargando..."), ///h

        ),
        ),
      ),
    );
  }
}

/// Sample time series data type.
class MyRow {
  final DateTime timeStamp;
  final int cost;
  MyRow(this.timeStamp, this.cost);
}