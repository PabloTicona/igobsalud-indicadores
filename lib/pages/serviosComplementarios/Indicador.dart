
class Indicador {
  final int id;
  final String servicio;
  final String tipoIndicador;
  final String indicador;
  final String especialidad;
  final String numerador;
  final String denominador;

  Indicador(this.id, this.servicio, this.tipoIndicador, this.indicador, this.especialidad, this.numerador, this.denominador);
}
