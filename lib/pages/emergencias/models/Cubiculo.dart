class Cubiculo {
 final int cubicLibres;
 final int cubicOcupados;
 final int vhspId;

 Cubiculo(this.cubicLibres, this.cubicOcupados, this.vhspId);

 @override
 String toString() {
  return 'Cubiculo{cubicLibres: $cubicLibres, cubicOcupados: $cubicOcupados, vhspId: $vhspId}';
 }


}
