import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:igob_salud_app/pages/serviosComplementarios/models/ItemMenu.dart';

class TabMensualDetalle extends StatelessWidget {
  String fechasSeleccionadas;
  ItemMenu itemMenu;
  List<Mes> listMeses;
  List<IndicadorGraf> indicadores;
  TabMensualDetalle({this.fechasSeleccionadas, this.itemMenu, this.listMeses, this.indicadores});

  @override
  Widget build(BuildContext context) {
    List<SalesData> dataLosPinos = [];
    List<SalesData> dataLaPortada = [];
    List<SalesData> dataCotahuma = [];
    List<SalesData> dataLaMerced = [];

    List<LineSeries<SalesData, String>> _createSampleData() {
      indicadores.forEach((IndicadorGraf ig) {
        print("ig : ${ig.toStringgraf()}");
        if (ig.hospitalId == 1) {
          dataLosPinos.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 2) {
          dataLaPortada.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 3) {
          dataCotahuma.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 5) {
          dataLaMerced.add(SalesData(ig.variable, ig.total));
        }
      });

      return [
        LineSeries<SalesData, String>(
            dataSource: dataLosPinos,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Los Pinos",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaPortada,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Portada",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataCotahuma,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Cotahuma",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaMerced,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Merced",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
      ];
    }

    return Scaffold(
      appBar: new AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              itemMenu.indicador,
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
            Text(
              "Emergencias",
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            )
          ],
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[

            Card(
              elevation: 8.0,
              child: Padding(padding: EdgeInsets.all(8.0),
                child: Text(fechasSeleccionadas, style: TextStyle(fontStyle: FontStyle.italic),),
              ),
            ),


            Container(
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: SfCartesianChart(
                    primaryXAxis: CategoryAxis(),
                    // Chart title
                    title: ChartTitle(text: itemMenu.indicador),
                    // Enable legend
                    legend: Legend(
                      isVisible: true,
                      position: LegendPosition.top,
                    ),

                    // Enable tooltip
                    tooltipBehavior: TooltipBehavior(enable: false),
                    series: _createSampleData(),
                    trackballBehavior: TrackballBehavior(
                        enable: true,
                        lineType: TrackballLineType.vertical,
                        activationMode: ActivationMode.singleTap,
                        //  tooltipAlignment: _alignment,
                        tooltipDisplayMode: TrackballDisplayMode.floatAllPoints,
                        tooltipSettings:
                        InteractiveTooltip(format: 'point.x : point.y'),
                        shouldAlwaysShow: true),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Sample time series data type.
class MyRow {
  final DateTime timeStamp;
  final double headcount;
  MyRow(this.timeStamp, this.headcount);
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
