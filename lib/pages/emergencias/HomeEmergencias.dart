import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/pages/emergencias/IndicadoresProceso.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/pages/emergencias/models/Cubiculo.dart';

import 'package:igob_salud_app/pages/emergencias/ReporteCubiculos.dart';
import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:igob_salud_app/pages/emergencias/reportes/Mes.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/Especialidad.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/Estado.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/Medico.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/Paciente.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/Triaje.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;

class HomeEmergencias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Emergencias(),
    );
  }
}

class Emergencias extends StatefulWidget {
  @override
  _EmergenciasState createState() => _EmergenciasState();
}

class _EmergenciasState extends State<Emergencias> {
  //var send = [{'latitude': widget.lat}, {'latitude': 123.456}];
  var url_dinamico = "http://sersalud.lapaz.bo/wsSalud";// GlobalConfiguration().getString("SALUD_REPORTES");
  DateTime _fechaDesde = formatoFecha2(DateTime.now());
  DateTime _fechaHasta = DateTime.now();
  bool dialVisible = true;

  DateTime _fechahoy = DateTime.now();
  bool viewTable = false;
  var totalLaMerced = 0;
  var totalLosPinios = 0;
  var totalCotahuma = 0;
  var totalPortada = 0;

  var valoresHospitalLaMerced = new HospitalValores(0, 0, 0, 0, 0, 0);
  var valoresHospitalLosPinos = new HospitalValores(0, 0, 0, 0, 0, 0);
  var valoresHospitalCotahuma = new HospitalValores(0, 0, 0, 0, 0, 0);
  var valoresHospitalPortada = new HospitalValores(0, 0, 0, 0, 0, 0);

  List<Cubiculo> listCubiculos = [];
  bool btnReporteCubiculos = true;
  Cubiculo cubiculoAux_;

  bool cardTriaje = false;

  @override
  void initState() {
    getReporte(_fechaDesde, _fechaHasta);
  }

  Future<void> selecthospital(int idhospital) {
    setState(() {
      cubiculoAux_ = buscarhospital(1);
      print("cubiculoAux_ ${cubiculoAux_}");
    });
  }

  Future<void> SelectFloatingAction(String tipo) {
    print("tipo : ${tipo}");

    switch (tipo) {
      case "mes":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Mes()),
        );
        break;

      case "especialidad":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Especialidad()),
        );
        break;

      case "medico":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Medico()),
        );
        break;

      case "paciente":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Paciente()),
        );
        break;

      case "triaje":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Triaje()),
        );
        break;

      case "estado":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Estado()),
        );
        break;

      default:
    }
  }

  Cubiculo buscarhospital(int idHospital) {
    Cubiculo resp_;

    listCubiculos.forEach((Cubiculo cubiculo) {
      if (cubiculo.vhspId == idHospital) {
        resp_ = cubiculo;
      }
    });

    return resp_;
  }

  Future<void> getReporte(DateTime fecini, DateTime fecfin) async {
    print("fecini : $fecini  - fecfin :: $fecfin ");
    var fecini_ = fecini.toString().split(" ")[0];
    var fecfin_ = fecfin.toString().split(" ")[0];
    setState(() {
      viewTable = false;
    });

    var response = await http.post("${url_dinamico}/dinamico", body: {
      'consulta':
          "select * from sp_lst_reporte_atenciones_emergencias(\$\$ $fecini \$\$,\$\$ $fecfin \$\$)"
    });

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      print("resp_ : ${resp_}");

      setState(() {
        totalLaMerced = 0;
        totalLosPinios = 0;
        totalCotahuma = 0;
        totalPortada = 0;
      });

      for (var entry in resp_) {
        //Merced
        if (entry["vpres_hospital_id"] == 1) {
          totalLaMerced = totalLaMerced + entry["vcount"];

          setState(() {
            if (entry["vtria_clasificacion"] == "ROJO") {
              valoresHospitalLaMerced.ROJO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "NARANJA") {
              valoresHospitalLaMerced.NARANJA = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AMARILLO") {
              valoresHospitalLaMerced.AMARILLO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "VERDE") {
              valoresHospitalLaMerced.VERDE = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AZUL") {
              valoresHospitalLaMerced.AZUL = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "LILA-PROCEDIMIENTOS") {
              valoresHospitalLaMerced.LILA = entry["vcount"];
            }
          });

          //  print("entry  Merced ::: $entry");
        }
        // Los pinos
        if (entry["vpres_hospital_id"] == 2) {
          totalLosPinios = totalLosPinios + entry["vcount"];

          setState(() {
            if (entry["vtria_clasificacion"] == "ROJO") {
              valoresHospitalLosPinos.ROJO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "NARANJA") {
              valoresHospitalLosPinos.NARANJA = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AMARILLO") {
              valoresHospitalLosPinos.AMARILLO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "VERDE") {
              valoresHospitalLosPinos.VERDE = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AZUL") {
              valoresHospitalLosPinos.AZUL = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "LILA-PROCEDIMIENTOS") {
              valoresHospitalLosPinos.LILA = entry["vcount"];
            }
          });

          //   print("entry  pinos ::: $entry");
        }

        // cotahuma
        if (entry["vpres_hospital_id"] == 5) {
          totalCotahuma = totalCotahuma + entry["vcount"];

          setState(() {
            if (entry["vtria_clasificacion"] == "ROJO") {
              valoresHospitalCotahuma.ROJO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "NARANJA") {
              valoresHospitalCotahuma.NARANJA = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AMARILLO") {
              valoresHospitalCotahuma.AMARILLO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "VERDE") {
              valoresHospitalCotahuma.VERDE = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AZUL") {
              valoresHospitalCotahuma.AZUL = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "LILA-PROCEDIMIENTOS") {
              valoresHospitalCotahuma.LILA = entry["vcount"];
            }
          });

          print("entry  cotahuma ::: $entry");
        }

        //Portada
        if (entry["vpres_hospital_id"] == 3) {
          totalPortada = totalPortada + entry["vcount"];

          setState(() {
            if (entry["vtria_clasificacion"] == "ROJO") {
              valoresHospitalPortada.ROJO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "NARANJA") {
              valoresHospitalPortada.NARANJA = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AMARILLO") {
              valoresHospitalPortada.AMARILLO = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "VERDE") {
              valoresHospitalPortada.VERDE = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "AZUL") {
              valoresHospitalPortada.AZUL = entry["vcount"];
            }

            if (entry["vtria_clasificacion"] == "LILA-PROCEDIMIENTOS") {
              valoresHospitalPortada.LILA = entry["vcount"];
            }
          });

          ///////////////////////////////////// print("entry  Portada ::: $entry");
        }

        //  print("entry  EMERGENCIAS ::: $entry");

      }

      // print("totalLaMerced : $totalLaMerced");

      setState(() {
        viewTable = true;
      });
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }
  }

  static DateTime formatoFecha2(DateTime dateTime) {
    var fechaHoyArray = (dateTime.toString().split(' ')[0]).split("-");

    var mes_ = int.parse(fechaHoyArray[1]);

    var mesRestado = mes_ - 1;

    if (mesRestado == 0) {
      mesRestado = 01;
      return dateTime;
    }

    String mesRestado_ = mesRestado.toString();
    if (mesRestado.toString().length == 1) {
      mesRestado_ = "0${mesRestado.toString()}";
    }
    var res = "${fechaHoyArray[0]}-${mesRestado_}-${fechaHoyArray[2]}";
    return DateTime.parse("${res} 00:00:00.000");
  }

  String formatDateToday(DateTime dateTime) {
    var fechaHoyArray = (dateTime.toString().split(' ')[0]).split('-');
    var fechahoy =
        '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';

    return fechahoy;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Emergencias",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            )
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.info),
            tooltip: 'Mas infomación',
            onPressed: () {
              if (cardTriaje) {
                setState(() {
                  cardTriaje = false;
                });
              } else {
                setState(() {
                  cardTriaje = true;
                });
              }
            },
          ),
        ],
      ),
      body: Center(
          child: Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Triaje"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      getReporte(_fechaDesde, _fechaHasta);

                      print('boton presionado');
                    }),
              ],
            ),
            Container(
              width: double.infinity,
              height: 278,
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: viewTable
                      ? Column(
                          children: <Widget>[
                            Table(
                              defaultColumnWidth: FixedColumnWidth(80.0),
                              border: TableBorder(
                                horizontalInside: BorderSide(
                                  color: Colors.black,
                                  style: BorderStyle.solid,
                                  width: 1.0,
                                ),
                                verticalInside: BorderSide(
                                  color: Colors.black,
                                  style: BorderStyle.solid,
                                  width: 1.0,
                                ),
                              ),
                              children: [
                                _buildTableRow(Colors.white, Colors.black,
                                    "Merced, Pinos, Cotahuma, Portada"),
                                _buildTableRow(Colors.red, Colors.white,
                                    "${valoresHospitalLaMerced.ROJO}, ${valoresHospitalLosPinos.ROJO}, ${valoresHospitalCotahuma.ROJO}, ${valoresHospitalPortada.ROJO}"),
                                _buildTableRow(
                                    Colors.deepOrangeAccent,
                                    Colors.white,
                                    "${valoresHospitalLaMerced.NARANJA}, ${valoresHospitalLosPinos.NARANJA}, ${valoresHospitalCotahuma.NARANJA}, ${valoresHospitalPortada.NARANJA}"),
                                _buildTableRow(Colors.amberAccent, Colors.white,
                                    "${valoresHospitalLaMerced.AMARILLO}, ${valoresHospitalLosPinos.AMARILLO}, ${valoresHospitalCotahuma.AMARILLO}, ${valoresHospitalPortada.AMARILLO}"),
                                _buildTableRow(Colors.green, Colors.white,
                                    "${valoresHospitalLaMerced.VERDE}, ${valoresHospitalLosPinos.VERDE}, ${valoresHospitalCotahuma.VERDE}, ${valoresHospitalPortada.VERDE}"),
                                _buildTableRow(Colors.blue, Colors.white,
                                    "${valoresHospitalLaMerced.AZUL}, ${valoresHospitalLosPinos.AZUL}, ${valoresHospitalCotahuma.AZUL}, ${valoresHospitalPortada.AZUL}"),
                                _buildTableRow(
                                    Colors.deepPurpleAccent,
                                    Colors.white,
                                    "${valoresHospitalLaMerced.LILA}, ${valoresHospitalLosPinos.LILA}, ${valoresHospitalCotahuma.LILA}, ${valoresHospitalPortada.LILA}"),
                                _buildTableRow(Colors.white, Colors.black,
                                    "$totalLaMerced, $totalLosPinios, $totalCotahuma, $totalPortada"),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: IconButton(
                                    color: Colors.red,
                                    icon: const Icon(Icons.info),
                                    tooltip: 'Mas infomación',
                                    onPressed: () {
                                      if (cardTriaje) {
                                        setState(() {
                                          cardTriaje = false;
                                        });
                                      } else {
                                        setState(() {
                                          cardTriaje = true;
                                        });
                                      }
                                    },
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      : Center(
                          child: SpinKitWave(
                              color: Colors.redAccent,
                              type: SpinKitWaveType.center),
                        ),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: cardTriaje
                    ? Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Container(
                            child: Image.asset("assets/images/triajef.png"),
                          ),
                        ),
                      )
                    : Container(),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    width: double.infinity, // match_parent
                    child: btnReporteCubiculos
                        ? FlatButton.icon(
                            color: Colors.redAccent,
                            textColor: Colors.white,
                            icon: Icon(Icons.favorite), //`Icon` to display
                            label: Text(
                                'Reporte De Cubiculos'), //`Text` to display
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ReporteCubiculos(),
                                ),
                              );
                            },
                          )
                        : Center(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            ),
                          ),
                  ),
                  SizedBox(
                    width: double.infinity, // match_parent
                    child: FlatButton.icon(
                      color: Colors.red,
                      textColor: Colors.white,
                      icon: Icon(Icons.insert_chart), //`Icon` to display
                      label:
                          Text('Indicadores Por Proceso'), //`Text` to display
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => IndicadorProceso()),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        child: Icon(Icons.apps, color: Colors.red),
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        visible: dialVisible,
        backgroundColor: Colors.redAccent,
        curve: Curves.bounceIn,
        children: [
          SpeedDialChild(
            child: new Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('estado'),
            label: 'Estado',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('triaje'),
            label: 'Triaje',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('paciente'),
            label: 'Paciente',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('medico'),
            label: 'Médico',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('especialidad'),
            label: 'Especialidad',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.finance, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('mes'),
            label: 'Por Mes',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
        ],
      ),
    );
  }

  TableRow _buildTableRow(
      Color colorFondo_, Color textColor, String listOfNames) {
    return TableRow(
      children: listOfNames.split(',').map((name) {
        return Container(
          decoration: BoxDecoration(color: colorFondo_),
          alignment: Alignment.center,
          child: Text(name, style: TextStyle(fontSize: 13.1, color: textColor)),
          padding: EdgeInsets.all(5.0),
        );
      }).toList(),
    );
  }
}

class HospitalesColores {
  final String color;
  final int valor;

  HospitalesColores(this.color, this.valor);
}

class HospitalValores {
  int ROJO;
  int NARANJA;
  int AMARILLO;
  int VERDE;
  int AZUL;
  int LILA;

  HospitalValores(
      this.ROJO, this.NARANJA, this.AMARILLO, this.VERDE, this.AZUL, this.LILA);
}
