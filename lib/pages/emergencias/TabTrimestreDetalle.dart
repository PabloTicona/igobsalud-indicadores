import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:igob_salud_app/pages/serviosComplementarios/models/ItemMenu.dart';

class TabTrimestreDetalle extends StatelessWidget {
  // IndicadorProces indicadorProces;
  String fechasSeleccionadas;
  ItemMenu itemMenu;
  List<Mes> listMeses;
  List<IndicadorGraf> indicadoresInicial;
  List<IndicadorGraf> indicadoresComparativo;

  TabTrimestreDetalle(
      {
        this.fechasSeleccionadas,
        this.itemMenu,
        //this.indicadorProces,
        this.listMeses,
        this.indicadoresInicial,
        this.indicadoresComparativo});

  @override
  Widget build(BuildContext context) {
    List<SalesData> dataLosPinosInicial = [];
    List<SalesData> dataLaPortadaInicial = [];
    List<SalesData> dataCotahumaInicial = [];
    List<SalesData> dataLaMercedInicial = [];

    List<SalesData> dataLosPinosComparartivo = [];
    List<SalesData> dataLaPortadaComparartivo = [];
    List<SalesData> dataCotahumaComparartivo = [];
    List<SalesData> dataLaMercedComparartivo = [];

    List<LineSeries<SalesData, String>> _createeDataInicial() {



      indicadoresInicial.forEach((IndicadorGraf ig) {
        print("ig : ${ig.toStringgraf()}");
        if (ig.hospitalId == 1) {
          dataLosPinosInicial.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 2) {
          dataLaPortadaInicial.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 3) {
          dataCotahumaInicial.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 5) {
          dataLaMercedInicial.add(SalesData(ig.variable, ig.total));
        }
      });

      return [
        LineSeries<SalesData, String>(
            dataSource: dataLosPinosInicial,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Los Pinos",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaPortadaInicial,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Portada",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataCotahumaInicial,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Cotahuma",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaMercedInicial,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Merced",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
      ];
    }

    /***
     * COMPARARTIVO
     */
    List<LineSeries<SalesData, String>> _createDataComparativo() {
      indicadoresComparativo.forEach((IndicadorGraf ig) {
        print("ig : ${ig.toStringgraf()}");
        if (ig.hospitalId == 1) {
          dataLosPinosComparartivo.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 2) {
          dataLaPortadaComparartivo.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 3) {
          dataCotahumaComparartivo.add(SalesData(ig.variable, ig.total));
        }

        if (ig.hospitalId == 5) {
          dataLaMercedComparartivo.add(SalesData(ig.variable, ig.total));
        }
      });

      return [
        LineSeries<SalesData, String>(
            dataSource: dataLosPinosComparartivo,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Los Pinos",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaPortadaComparartivo,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Portada",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataCotahumaComparartivo,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "Cotahuma",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
        LineSeries<SalesData, String>(
            dataSource: dataLaMercedComparartivo,
            xValueMapper: (SalesData sales, _) => sales.year,
            yValueMapper: (SalesData sales, _) => sales.sales,
            legendItemText: "La Merced",
            width: 3,
            markerSettings: MarkerSettings(isVisible: true),
            dataLabelSettings: DataLabelSettings(isVisible: false)),
      ];
    }

    return Scaffold(
      appBar: new AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              itemMenu.indicador,
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
            Text(
              "Emergencias",
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            )
          ],
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[


            Card(
              elevation: 8.0,
              child: Padding(padding: EdgeInsets.all(8.0),
                child: Text(fechasSeleccionadas, style: TextStyle(fontStyle: FontStyle.italic),),
              ),
            ),


            new CardGraf(
              titulo: "Año Inicial",
              listData: _createeDataInicial(),
            ),
            new CardGraf(
              titulo: "Año Comparartivo",
              listData: _createDataComparativo(),
            ),
          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}

class CardGraf extends StatelessWidget {
  String titulo;
  List<LineSeries<SalesData, String>> listData;

  CardGraf({this.titulo, this.listData});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Card(
        elevation: 8.0,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: (listData.length !=0)? SfCartesianChart(
            primaryXAxis: CategoryAxis(),
            // Chart title
            title: ChartTitle(text: titulo),
            // Enable legend
            legend: Legend(
              isVisible: true,
              position: LegendPosition.top,
            ),

            // Enable tooltip
            tooltipBehavior: TooltipBehavior(enable: false),
            series: listData,
            trackballBehavior: TrackballBehavior(
                enable: true,
                lineType: TrackballLineType.vertical,
                activationMode: ActivationMode.singleTap,
                //  tooltipAlignment: _alignment,
                tooltipDisplayMode: TrackballDisplayMode.floatAllPoints,
                tooltipSettings:
                InteractiveTooltip(format: 'point.x : point.y'),
                shouldAlwaysShow: true),
          ): Center(
            child: Text("Sin datos, Seleccione otras fechas"),
          ),
        ),
      ),
    );
  }
}
