import 'dart:math';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:screenshot/screenshot.dart';
import 'package:igob_salud_app/pages/emergencias/reportes/CardPieChartEspecialidad.dart';

import 'package:igob_salud_app/helps/DateFormatConverter.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;


class Especialidad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Especialidades(),
    );
  }
}

class Especialidades extends StatefulWidget {
  @override
  _EspecialidadesState createState() => _EspecialidadesState();
}

class _EspecialidadesState extends State<Especialidades> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScreenshotController SCEspecialidad = ScreenshotController();
  List _hospitales = [
    "-- Hospital --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  List _triajes = [
    "-- Triaje --",
    "Reanimación",
    "Emergencia",
    "Urgencia",
    "Urgencia Menor",
    "No Urgente",
    "Procedimientos Enfermería"
  ];

  int getTriajeId(String nombreTriaje) {
    int resp = 0;
    switch (nombreTriaje) {
      case "Reanimación":
        resp = 1;
        break;

      case "Emergencia":
        resp = 2;
        break;

      case "Urgencia":
        resp = 3;
        break;

      case "Urgencia Menor":
        resp = 4;
        break;

      case "No Urgente":
        resp = 5;
        break;

      case "Procedimientos Enfermería":
        resp = 6;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();
  bool isShowGrafica = false;

  List<DropdownMenuItem<String>> _dropDownMenuItemsHospitales;
  String _itemValuehospital;

  List<DropdownMenuItem<String>> _dropDownMenuItemsTriaje;
  String _itemValueTriaje;

  List<HospitalEspecialidades> hospitalEspecialidadesList = [];
  List<charts.Series<OrdinalSales4, String>> listDataGrafica;

  @override
  void initState() {
    super.initState();
    _dropDownMenuItemsHospitales = getDropDownMenuItems(_hospitales);
    _itemValuehospital = _dropDownMenuItemsHospitales[0].value;

    _dropDownMenuItemsTriaje = getDropDownMenuItems(_triajes);
    _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;
  }

  static DateTime CambiaFecha(DateTime fechaInicio) {
    var fecha_ = fechaInicio.toString().split(" ")[0];

    var minutos_ = fechaInicio.toString().split(" ")[1];
    print("minutos_ :: ${minutos_}");
    var dia = fecha_.toString().split("-")[2];
    var mes = fecha_.toString().split("-")[1];
    var anio = fecha_.toString().split("-")[0];

    var mesSum = int.parse(mes) - 1;
    var fechaF = "${anio}-${mesSum}-${dia} 00:00:00.0000";
    print("fecha_ :: ${fecha_}");

    return DateTime.parse(fechaF.toString().trim());
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in lista) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  Future<void> pressedReporte() async {
    setState(() {
      isShowGrafica = true;
    });

    hospitalEspecialidadesList = await getDataResponse(
        getTriajeId(_itemValueTriaje),
        getHospitalId(_itemValuehospital),
        _fechaDesde,
        _fechaHasta);

    var dataRes = await _createSampleData(hospitalEspecialidadesList);

    setState(() {
      listDataGrafica = dataRes;
    });
  }

  Future<List<HospitalEspecialidades>> getDataResponse(int triajeId,
      int hospitalId, DateTime fechaDesde, DateTime fechaHasta) async {
    List<HospitalEspecialidades> resp = [];

    var query =
        "select * from sp_lst_totales_emergencia_por_triaje(${triajeId},${hospitalId},\$\$${fechaDesde}\$\$,\$\$${fechaHasta}\$\$)";
    var resp_ = await getResponseDinamico(query);

    if (resp_.length != 0) {
      for (var entry in resp_) {
        resp.add(new HospitalEspecialidades(
            entry['vcount'],
            entry['vusuario'],
            entry['vidusuario'],
            entry['intidtriaje'],
            entry['backgroundcolor']));
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas.");

      setState(() {
        isShowGrafica = false;
      });
    }
    return resp;
  }

  void changedDropDownItemHospital(String selected) {
    setState(() {
      _itemValuehospital = selected;
    });
  }

  void changedDropDownItemTriaje(String selected) {
    setState(() {
      _itemValueTriaje = selected;
    });
  }

  final Random _random = Random();

  Future<List<charts.Series<OrdinalSales4, String>>> _createSampleData(
      List<HospitalEspecialidades> dataResponse) async {
    List<charts.Series<OrdinalSales4, String>> resp = [];

    ///
    for (HospitalEspecialidades item in dataResponse) {
      var colorNum =
          item.backgroundcolor.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

      var colorRandomR = _random.nextInt(256);
      var colorRandomG = _random.nextInt(256);
      var colorRandomB = _random.nextInt(256);

      resp.add(new charts.Series<OrdinalSales4, String>(
        id: "${item.vusuario} (${item.vcount})",
        colorFn: (OrdinalSales4 sales, _) => charts.Color(
            r: colorRandomR, g: colorRandomG, b: colorRandomB, a: 255),
        domainFn: (OrdinalSales4 sales, _) => "${sales.year}",
        measureFn: (OrdinalSales4 sales, _) => sales.sales,
        data: [
          new OrdinalSales4("${item.vcount}", item.vcount,
              charts.MaterialPalette.blue.shadeDefault),
        ],
      ));
    }
    return resp;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Especialidades",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeEmergencias()),
            );
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Especialista De Emergencia"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 90,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            value: _itemValuehospital,
                            items: _dropDownMenuItemsHospitales,
                            onChanged: changedDropDownItemHospital,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 90,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValueTriaje,
                            items: _dropDownMenuItemsTriaje,
                            onChanged: changedDropDownItemTriaje,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = DateTime.parse(dateTime.toString());
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = DateTime.parse(dateTime.toString());
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                    }),
              ],
            ),
            Container(
              child: new CardPieChartEspecialidad(
                dataMap: listDataGrafica,
                isCardGraf: isShowGrafica,
                SCEspecialidad: SCEspecialidad,
                scaffoldKey: _scaffoldKey,
                descripcion: "Hospital ${_itemValuehospital}, Triaje ${_itemValueTriaje} de fecha: ${dateTimeFormat(_fechaDesde)} a ${dateTimeFormat(_fechaHasta)}",
              ),
            )
          ],
        ),
      ),
    );
  }
}



class HospitalEspecialidades {
  final int vcount;
  final String vusuario;
  final int vidusuario;

  final intidtriaje;
  final String backgroundcolor;

  HospitalEspecialidades(this.vcount, this.vusuario, this.vidusuario,
      this.intidtriaje, this.backgroundcolor);
}


//I/flutter ( 5597): entry : {vcount: 1, vusuario: alejandra.claros, vidusuario: 1104, idtriaje: 2, backgroundcolor: rgba(246, 127, 8, 0.9)}
