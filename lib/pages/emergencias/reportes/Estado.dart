import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/style/theme.dart' as Theme;


class Estado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Estados(),
    );
  }
}

class Estados extends StatefulWidget {
  @override
  _EstadosState createState() => _EstadosState();
}

class _EstadosState extends State<Estados> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List _hospitales = [
    "-- Hospitales --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  List _tipos = [
    "-- Tipo de Presentación --",
    "Sin Atención",
    "Pendiente",
    "Atendido",
    "Finalizado",
    "Todos"
  ];

  String getTiposId(String nombreTriaje) {
    String resp = "Todos";
    switch (nombreTriaje) {
      case "Sin Atención":
        resp = "SIN ATENCION";
        break;

      case "Pendiente":
        resp = "PENDIENTE";
        break;

      case "Atendido":
        resp = "ATENDIDO";
        break;

      case "Finalizado":
        resp = "FINALIZADO";
        break;

      case "Todos":
        resp = "TODOS";
        break;

      default:
        resp = "Todos";
    }

    return resp;
  }

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();
  bool isShowGrafica = false;

  List<DropdownMenuItem<String>> _dropDownMenuItemsHospitales;
  String _itemValuehospital;

  List<DropdownMenuItem<String>> _dropDownMenuItemsTipo;
  String _itemValueTipo;

  List<HospitalGrafEstado> hospitalGrafEstadoList = [];

  List<charts.Series<OrdinalSales4, String>> listDataGrafica;
  @override
  void initState() {
    _dropDownMenuItemsHospitales = getDropDownMenuItems(_hospitales);
    _itemValuehospital = _dropDownMenuItemsHospitales[0].value;

    _dropDownMenuItemsTipo = getDropDownMenuItems(_tipos);
    _itemValueTipo = _dropDownMenuItemsTipo[0].value;

    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in lista) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  Future<void> pressedReporte() async {
    print("esatdos..");

    print("_itemValuehospital  : $_itemValuehospital");

    print("_itemValueTipo  : $_itemValueTipo");

    hospitalGrafEstadoList = await getDataResponse(
        getHospitalId(_itemValuehospital),
        getTiposId(_itemValueTipo),
        _fechaDesde,
        _fechaHasta);

    var dataRes = await _createSampleData(hospitalGrafEstadoList);

    setState(() {
      listDataGrafica = dataRes;
    });
  }

  Future<List<HospitalGrafEstado>> getDataResponse(int idHospital,
      String tipoValor, DateTime fechaInicio, DateTime fechaFinal) async {
    List<HospitalGrafEstado> resp = [];

    var query =
        "select * from sp_lst_totales_tipo_prestacion_emergencia(${idHospital},\$\$${tipoValor}\$\$,\$\$${fechaInicio}\$\$,\$\$${fechaFinal}\$\$)";
    var resp_ = await getResponseDinamico(query);

    // print(" ** TodosLosHospitales ${resp_}");
    if (resp_.length != 0) {
      for (var entry in resp_) {
        print(" >>> entry ${entry}");
        resp.add(new HospitalGrafEstado(
            entry['contador'],
            entry['idhospital'],
            entry['desc_hospital'],
            entry['idtriaje'],
            entry['desc_triaje'],
            entry['def_triaje'],
            entry['backgroundcolor']));
        /* resp.add(new HospitalGrafEstado(
            entry['contador'],
            entry['idhospital'],
            entry['desc_hospital'],
            entry['desc_triaje'],
            entry['def_triaje'],
            entry['backgroundcolor'])); */
      }

      setState(() {
        isShowGrafica = true;
      });
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas.");

      setState(() {
        isShowGrafica = false;
      });
    }
    return resp;
  }

  void changedDropDownItemHospital(String selected) {
    setState(() {
      _itemValuehospital = selected;
    });
  }

  void changedDropDownItemTriaje(String selected) {
    setState(() {
      _itemValueTipo = selected;
    });
  }

  Future<List<charts.Series<OrdinalSales4, String>>> _createSampleData(
      List<HospitalGrafEstado> dataResponse) async {
    List<charts.Series<OrdinalSales4, String>> resp = [];

    ///
    for (HospitalGrafEstado item in dataResponse) {
      var colorNum =
      item.backgroundcolor.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

      resp.add(new charts.Series<OrdinalSales4, String>(
        id: "${item.def_triaje} (${item.contador})",
        colorFn: (OrdinalSales4 sales, _) => charts.Color(
            r: int.parse(arraycolor[0].trim()),
            g: int.parse(arraycolor[1].trim()),
            b: int.parse(arraycolor[2].trim()),
            a: 255),
        domainFn: (OrdinalSales4 sales, _) => "${sales.year}",
        measureFn: (OrdinalSales4 sales, _) => sales.sales,
        data: [
          new OrdinalSales4("${item.contador}",
              item.contador, charts.MaterialPalette.blue.shadeDefault),
        ],
      ));
    }
    return resp;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Estados",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeEmergencias()),
            );
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Especialista De Emergencia"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 88,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValuehospital,
                            items: _dropDownMenuItemsHospitales,
                            onChanged: changedDropDownItemHospital,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 88,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValueTipo,
                            items: _dropDownMenuItemsTipo,
                            onChanged: changedDropDownItemTriaje,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                      //    formatoFecha(_fechaHasta, 0));

                      // print('boton presionado');
                    }),
              ],
            ),
            Container(
              child: new CardPieChartEstado(
                dataMap: listDataGrafica,
                isCardGraf: isShowGrafica,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CardPieChartEstado extends StatelessWidget {
  List<charts.Series<OrdinalSales4, String>> dataMap;
  bool isCardGraf;

  CardPieChartEstado({this.dataMap, this.isCardGraf});
  // CardPieChartEstado({this.dataMap});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(
        width: MediaQuery.of(context).size.width * 1.0,
        height: 350,
        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Card(
            elevation: 8.0,
            child: Padding(
              padding: new EdgeInsets.all(13.0),
              child: Container(
                child: (dataMap != null)
                    ? new charts.BarChart(
                  dataMap,
                  animate: true,
                  behaviors: [
                    new charts.SeriesLegend(
                        position: charts.BehaviorPosition.bottom,
                        horizontalFirst:
                        bool.fromEnvironment("Pablo")),
                  ],
                  barRendererDecorator:
                  new charts.BarLabelDecorator<String>(),
                  defaultRenderer: new charts.BarRendererConfig(
                      groupingType:
                      charts.BarGroupingType.groupedStacked,
                      strokeWidthPx: 2.0),
                )
                    : Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          )
              : Container(),
        ));
  }
}

//{contador: 12, idhospital: 2, desc_hospital: Hospital Los Pinos, idtriaje: 2, desc_triaje: NARANJA, def_triaje: EMERGENCIA, backgroundcolor: rgba(246, 127, 8, 0.9)}

class HospitalGrafEstado {
  final int contador;
  final int idhospital;
  final String desc_hospital;
  final int idtriaje;
  final String desc_triaje;
  final String def_triaje;
  final String backgroundcolor;

  HospitalGrafEstado(this.contador, this.idhospital, this.desc_hospital,
      this.idtriaje, this.desc_triaje, this.def_triaje, this.backgroundcolor);
}

/// Sample ordinal data type.
class OrdinalSales4 {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales4(this.year, this.sales, this.color);
}
