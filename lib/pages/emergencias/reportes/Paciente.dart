import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/style/theme.dart' as Theme;


class Paciente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Pacientes(),
    );
  }
}

class Pacientes extends StatefulWidget {
  @override
  _PacientesState createState() => _PacientesState();
}

class _PacientesState extends State<Pacientes> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List _hospitales = [
    "-- Seleccionar --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  List _tipoPaciente = [
    "-- Tipo Paciente --",
  ];
  List<charts.Series<LinearSales3, int>> dataGraficaMedicoData;
  bool isCardGraf = false;

  int getTriajeId(String nombreTriaje) {
    int resp = 0;
    switch (nombreTriaje) {
      case "Reanimación":
        resp = 1;
        break;

      case "Emergencia":
        resp = 2;
        break;

      case "Urgencia":
        resp = 3;
        break;

      case "Urgencia Menor":
        resp = 4;
        break;

      case "No Urgente":
        resp = 5;
        break;

      case "Procedimientos Enfermería":
        resp = 6;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  List<DropdownMenuItem<String>> _dropDownMenuItemsHospitales;
  String _itemValuehospital;

  List<DropdownMenuItem<String>> _dropDownMenuItemsTriaje;
  String _itemValueTriaje;

  List<HospitalGrafPaciente> hospitalGrafList = [];

  List<TipoPacienteClass> tipoPacienteList = [];

  @override
  void initState() {
    _dropDownMenuItemsHospitales = getDropDownMenuItems(_hospitales);
    _itemValuehospital = _dropDownMenuItemsHospitales[0].value;

    _dropDownMenuItemsTriaje = getDropDownMenuItems(_tipoPaciente);
    _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;

    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in lista) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  Future<void> pressedReporte() async {
    var idHospital = getHospitalId(_itemValuehospital);
    var idTipoPaciente = getIdTipoPaciente(_itemValueTriaje);

    hospitalGrafList = await getDataResponse(
        idHospital, _fechaDesde, _fechaHasta, idTipoPaciente);

    var dataResp = _dataGraficaMedico(hospitalGrafList);

    setState(() {
      dataGraficaMedicoData = dataResp;
    });
  }

  Future<List<HospitalGrafPaciente>> getDataResponse(
      int idHospital, DateTime dataIni, DateTime dataFin, int paci) async {
    List<HospitalGrafPaciente> resp = [];

    setState(() {
      isCardGraf = true;
    });

    var query =
        "select * from sp_lst_totales_pacientes(${idHospital}, \$\$${dataIni}\$\$, \$\$${dataFin}\$\$, ${paci})";
    var resp_ = await getResponseDinamico(query);

    print(" internacionesTodosLosHospitales ${resp_}");
    if (resp_.length != 0) {
      for (var entry in resp_) {
        resp.add(new HospitalGrafPaciente(entry['data'], entry['label'],
            entry['backgroundcolor'], entry['borderwidth']));
        print(">> entry : ${entry}");
      }
    } else {
      setState(() {
        isCardGraf = false;
      });
      showInSnackBar("No Existen datos. Seleccione otras fechas.");
    }

    return resp;
  }

  Future<List<TipoPacienteClass>> getTipoPacienteResponse(
      int hospitalId) async {
    List<TipoPacienteClass> resp = [];
    var query =
        "select tp_cod_tipo_paciente,tp_tipo_paciente from _hsp_tipo_seguro_programa where tp_id_hospital = \$\$${hospitalId}\$\$ and tp_estado = \$\$A\$\$";
    var resp_ = await getResponseDinamico(query);
    print(" internacionesTodosLosHospitales ${resp_}");
    if (resp_.length != 0) {
      for (var entry in resp_) {
        print("=> ${entry}");

        resp.add(new TipoPacienteClass(
            int.parse("${entry['tp_cod_tipo_paciente']}"),
            "${entry['tp_tipo_paciente']}"));
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas.");
    }
    return resp;
  }

  int getIdTipoPaciente(String nom) {
    for (TipoPacienteClass item in tipoPacienteList) {
      print("${item.tp_tipo_paciente} == ${nom}");

      if (item.tp_tipo_paciente == nom) {
        return item.tp_cod_tipo_paciente;
      }
    }
    return 0;
  }

  List<charts.Series<LinearSales3, int>> _dataGraficaMedico(
      List<HospitalGrafPaciente> resp_) {
    List<LinearSales3> dataReporte = [];
    List<LinearSales3> dataReporte2 = [];
    int cont = 0;
    for (HospitalGrafPaciente entry in resp_) {
      print("entry :: ${entry}");
      var color_ = entry.backgroundcolor.toString().trim();

      var colorNum = color_.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

      dataReporte2.add(new LinearSales3(
          entry.data,
          entry.data,
          entry.label,
          charts.Color(
              r: int.parse(arraycolor[0].trim()),
              g: int.parse(arraycolor[1].trim()),
              b: int.parse(arraycolor[2].trim()),
              a: 255)));

      cont++;
    }

    //   dataReporte.add(new LinearSales3( 0, 100, 'label 1', charts.MaterialPalette.purple.shadeDefault));
    //  dataReporte.add(new LinearSales3( 1, 75, 'label 2', charts.MaterialPalette.blue.shadeDefault));
    // dataReporte.add(new LinearSales3( 2, 25, 'label 3', charts.MaterialPalette.green.shadeDefault));
    // dataReporte.add(new LinearSales3(3, 5, 'label 4', charts.MaterialPalette.pink.shadeDefault));

    return [
      new charts.Series<LinearSales3, int>(
        id: 'Sales',
        domainFn: (LinearSales3 sales, _) => sales.year,
        measureFn: (LinearSales3 sales, _) => sales.sales,
        data: dataReporte2,
        colorFn: (LinearSales3 sales, _) => sales.color,
        labelAccessorFn: (LinearSales3 sales, _) => sales.label,
      ),
    ];
  }

  void changedDropDownItemHospital(String selected) async {
    setState(() {
      _tipoPaciente = [
        "-- Tipo Paciente --",
      ];
      _dropDownMenuItemsTriaje = getDropDownMenuItems(_tipoPaciente);
      _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;
    });

    tipoPacienteList = await getTipoPacienteResponse(getHospitalId(selected));

    for (TipoPacienteClass item in tipoPacienteList) {
      _tipoPaciente.add(item.tp_tipo_paciente);
    }

    //  _dropDownMenuItemsTriaje = getDropDownMenuItems(_tipoPaciente);
    // _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;

    setState(() {
      _dropDownMenuItemsTriaje = getDropDownMenuItems(_tipoPaciente);
      _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;
      _itemValuehospital = selected;
    });
  }

  void changedDropDownItemTriaje(String selected) {
    setState(() {
      _itemValueTriaje = selected;
    });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Pacientes",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeEmergencias()),
                )
              },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Tipo de Paciente"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 80,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValuehospital,
                            items: _dropDownMenuItemsHospitales,
                            onChanged: changedDropDownItemHospital,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 80,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValueTriaje,
                            items: _dropDownMenuItemsTriaje,
                            onChanged: changedDropDownItemTriaje,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                      //    formatoFecha(_fechaHasta, 0));

                      // print('boton presionado');
                    }),
              ],
            ),
            Container(
              child: new CardPieChartMedico(
                dataMap: dataGraficaMedicoData,
                isCardGraf: isCardGraf,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HospitalGrafPaciente {
  final int data;
  final String label;
  final String backgroundcolor;
  final int borderwidth;
  HospitalGrafPaciente(
      this.data, this.label, this.backgroundcolor, this.borderwidth);
}

class TipoPacienteClass {
  final int tp_cod_tipo_paciente;
  final String tp_tipo_paciente;
  TipoPacienteClass(this.tp_cod_tipo_paciente, this.tp_tipo_paciente);

  @override
  String toString() {
    return 'TipoPacienteClass{tp_cod_tipo_paciente: $tp_cod_tipo_paciente, tp_tipo_paciente: $tp_tipo_paciente}';
  }
}

class LinearSales3 {
  final int year;
  final int sales;
  final String label;
  final charts.Color color;

  LinearSales3(this.year, this.sales, this.label, this.color);
}

class CardPieChartMedico extends StatelessWidget {
  final List<charts.Series<LinearSales3, int>> dataMap;
  bool isCardGraf;

  CardPieChartMedico({this.dataMap, this.isCardGraf});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    print("dataMap :: ${dataMap}");

    return new Container(
        width: MediaQuery.of(context).size.width * 1.0,
        height: 200,
        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: new EdgeInsets.all(13.0),
                    child: Container(
                      child: (dataMap != null)
                          ? new charts.PieChart(
                              dataMap,
                              animate: true,
                              defaultRenderer: new charts.ArcRendererConfig(
                                arcWidth: 17,
                                arcRendererDecorators: [
                                  new charts.ArcLabelDecorator(
                                    outsideLabelStyleSpec: charts.TextStyleSpec(
                                      color: charts.Color.black,
                                      fontSize: 9,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ),
                  ),
                )
              : Container(),
        ));
  }
}
