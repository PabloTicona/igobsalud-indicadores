import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/style/theme.dart' as Theme;


class Triaje extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Triajes(),
    );
  }
}

class Triajes extends StatefulWidget {
  @override
  _TriajesState createState() => _TriajesState();
}

class _TriajesState extends State<Triajes> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List _hospitales = [
    "-- Seleccionar --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  List _triajes = [
    "-- Seleccionar --",
    "Reanimación",
    "Emergencia",
    "Urgencia",
    "Urgencia Menor",
    "No Urgente",
    "Procedimientos Enfermería"
  ];

  int getTriajeId(String nombreTriaje) {
    int resp = 0;
    switch (nombreTriaje) {
      case "Reanimación":
        resp = 1;
        break;

      case "Emergencia":
        resp = 2;
        break;

      case "Urgencia":
        resp = 3;
        break;

      case "Urgencia Menor":
        resp = 4;
        break;

      case "No Urgente":
        resp = 5;
        break;

      case "Procedimientos Enfermería":
        resp = 6;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();
  bool isShowGrafica = false;

  List<DropdownMenuItem<String>> _dropDownMenuItemsHospitales;
  String _itemValuehospital;

  List<DropdownMenuItem<String>> _dropDownMenuItemsTriaje;
  String _itemValueTriaje;

  List<HospitalGrafTriaje> hospitalGrafList = [];
  List<charts.Series<OrdinalSales5, String>> listDataGrafica;

  @override
  void initState() {
    _dropDownMenuItemsHospitales = getDropDownMenuItems(_hospitales);
    _itemValuehospital = _dropDownMenuItemsHospitales[0].value;

    _dropDownMenuItemsTriaje = getDropDownMenuItems(_triajes);
    _itemValueTriaje = _dropDownMenuItemsTriaje[0].value;

    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in lista) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  Future<void> pressedReporte() async {
    hospitalGrafList = await getDataResponse(
        getHospitalId(_itemValuehospital), _fechaDesde, _fechaHasta);

    var dataRes = await _createSampleData(hospitalGrafList);

    setState(() {
      listDataGrafica = dataRes;
    });
  }

  Future<List<HospitalGrafTriaje>> getDataResponse(
      int idHospital, DateTime dataIni, DateTime dataFin) async {
    List<HospitalGrafTriaje> resp = [];
    var query =
        "select * from sp_lst_totales_emergencia(${idHospital},\$\$${dataIni}\$\$,\$\$${dataFin}\$\$)";
    var resp_ = await getResponseDinamico(query);

    if (resp_.length != 0) {
      for (var entry in resp_) {
        // HospitalGrafList.add(new HospitalGraf(entry['odts_total'], entry['odts_mes'], entry['odts_anio']));
        resp.add(new HospitalGrafTriaje(entry['data'], entry['label'],
            entry['backgroundcolor'], entry['borderwidth']));
        //  print(">> entry : ${entry}");

        setState(() {
          isShowGrafica = true;
        });
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas.");
      setState(() {
        isShowGrafica = false;
      });
    }

    return resp;
  }

  void changedDropDownItemHospital(String selected) {
    setState(() {
      _itemValuehospital = selected;
    });
  }

  void changedDropDownItemTriaje(String selected) {
    setState(() {
      _itemValueTriaje = selected;
    });
  }

  Future<List<charts.Series<OrdinalSales5, String>>> _createSampleData(
      List<HospitalGrafTriaje> dataResponse) async {
    List<charts.Series<OrdinalSales5, String>> resp = [];

    ///
    for (HospitalGrafTriaje item in dataResponse) {
      var colorNum =
          item.backgroundcolor.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

      resp.add(new charts.Series<OrdinalSales5, String>(
        id: "${item.label}",
        colorFn: (OrdinalSales5 sales, _) => charts.Color(
            r: int.parse(arraycolor[0].trim()),
            g: int.parse(arraycolor[1].trim()),
            b: int.parse(arraycolor[2].trim()),
            a: 255),
        domainFn: (OrdinalSales5 sales, _) => "${sales.year}",
        measureFn: (OrdinalSales5 sales, _) => sales.sales,
        data: [
          new OrdinalSales5("${item.data}", item.data,
              charts.MaterialPalette.blue.shadeDefault),
        ],
      ));
    }
    return resp;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Triajes",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeEmergencias()),
            );
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Tipo De Triaje"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 1.0,
                  height: 80,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(38, 15, 38, 15),
                          child: new DropdownButton(
                            isExpanded: true,
                            value: _itemValuehospital,
                            items: _dropDownMenuItemsHospitales,
                            onChanged: changedDropDownItemHospital,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                      //    formatoFecha(_fechaHasta, 0));

                      // print('boton presionado');
                    }),
              ],
            ),
            Container(
              child: new CardPieChartTriaje(
                dataMap: listDataGrafica,
                isCardGraf: isShowGrafica,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CardPieChartTriaje extends StatelessWidget {
  List<charts.Series<OrdinalSales5, String>> dataMap;
  bool isCardGraf;

  CardPieChartTriaje({this.dataMap, this.isCardGraf});
  // CardPieChartTriaje({this.dataMap});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(
        width: MediaQuery.of(context).size.width * 1.0,
        height: 430,
        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: new EdgeInsets.all(13.0),
                    child: Container(
                      child: (dataMap != null)
                          ? new charts.BarChart(
                              dataMap,
                              animate: true,
                              behaviors: [
                                new charts.SeriesLegend(
                                    position: charts.BehaviorPosition.bottom,
                                    horizontalFirst:
                                        bool.fromEnvironment("Pablo")),
                              ],
                              barRendererDecorator:
                                  new charts.BarLabelDecorator<String>(),
                              defaultRenderer: new charts.BarRendererConfig(
                                  groupingType:
                                      charts.BarGroupingType.groupedStacked,
                                  strokeWidthPx: 2.0),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ),
                  ),
                )
              : Container(),
        ));
  }
}

class HospitalGrafTriaje {
  final int data;
  final String label;
  final String backgroundcolor;
  final int borderwidth;
  HospitalGrafTriaje(
      this.data, this.label, this.backgroundcolor, this.borderwidth);
}

/// Sample ordinal data type.
class OrdinalSales5 {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales5(this.year, this.sales, this.color);
}
