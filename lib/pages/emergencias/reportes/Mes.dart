import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/helps/DateFormatConverter.dart';
import 'package:screenshot/screenshot.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;

class Mes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Meses(),
    );
  }
}

class Meses extends StatefulWidget {
  @override
  _MesesState createState() => _MesesState();
}

class _MesesState extends State<Meses> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  ScreenshotController SCMes = ScreenshotController();


  List _cities = [
    "-- Seleccionar --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  List<charts.Series<TimeSeriesSales, DateTime>> dataGraficaMesData;

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;
  List<HospitalGraf> hospitalGrafList = [];
  bool isCardGraf = false;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentCity = _dropDownMenuItems[0].value;
    super.initState();
  }



  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  void pressedReporte() async {

    setState(() {
      isCardGraf = true;
    });

    hospitalGrafList= await getDataResponse(getHospitalId(_currentCity), _fechaDesde, _fechaHasta);

    var restData = _createSampleData(hospitalGrafList);

    setState(() {
      dataGraficaMesData = restData;
    });
  }



  int getAnio(String res) {
    var anio = res.split(" ")[1];
    return int.parse(anio);
  }

  List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData(
      List<HospitalGraf> list) {
    List<TimeSeriesSales> atendidosSalesData_ = [];

    for (HospitalGraf item in list) {
      atendidosSalesData_.add(new TimeSeriesSales(
          new DateTime(
              getAnio("${item.odts_anio}"), codigoMes("${item.odts_mes}"), 19),
          item.odts_total));
    }
/*
    atendidosSalesData_.add(new TimeSeriesSales(new DateTime(2017, 4, 19), 5));
    atendidosSalesData_.add(new TimeSeriesSales(new DateTime(2017, 5, 26), 25));
    atendidosSalesData_.add(new TimeSeriesSales(new DateTime(2017, 6, 3), 100));
    atendidosSalesData_.add(new TimeSeriesSales(new DateTime(2017, 7, 10), 75));
    atendidosSalesData_.add(new TimeSeriesSales(new DateTime(2017, 8, 11), 90));*/

    final tableSalesData = [
      // new TimeSeriesSales(new DateTime(2017, 9, 19), 10),
      new TimeSeriesSales(new DateTime(2017, 9, 26), 50),
      //   new TimeSeriesSales(new DateTime(2017, 10, 3), 200),
      new TimeSeriesSales(new DateTime(2017, 10, 10), 150),
      new TimeSeriesSales(new DateTime(2017, 11, 19), 40),
    ];

    final mobileSalesData = [
      // new TimeSeriesSales(new DateTime(2017, 9, 19), 10),
      new TimeSeriesSales(new DateTime(2017, 9, 14), 80),
      //   new TimeSeriesSales(new DateTime(2017, 10, 3), 200),
      new TimeSeriesSales(new DateTime(2017, 10, 10), 10),
      new TimeSeriesSales(new DateTime(2017, 11, 10), 150),
    ];

    return [
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Atendidos',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: atendidosSalesData_,
      ),
      /* new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Solicitados',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: tableSalesData

      ),
 */
    ];
  }

  Future<List<HospitalGraf>> getDataResponse(
      int idHospital, DateTime fechaDesde, DateTime fechaHasta) async {
    List<HospitalGraf> resp = [];



    var query =
        "select * from sp_lst_totales_mes_emergencias(${idHospital},\$\$${fechaDesde}\$\$,\$\$${fechaHasta}\$\$)";

    print("query :: ${query}");

    var resp_ = await getResponseDinamico(query);

    print(" internacionesTodosLosHospitales ${resp_}");
    if (resp_.length != 0) {
      for (var entry in resp_) {
        resp.add(new HospitalGraf(
            entry['odts_total'], entry['odts_mes'], entry['odts_anio']));

        print("entry : ${entry}");
      }
    } else {
      setState(() {
        isCardGraf = false;
      });

      showInSnackBar("No Existen datos. Seleccione otras fechas.");
    }

    return resp;
  }

  void changedDropDownItem(String selectedCity) {
    setState(() {
      _currentCity = selectedCity;
    });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Meses",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeEmergencias()),
                )
              },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Mes"),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: new DropdownButton(
                      value: _currentCity,
                      items: _dropDownMenuItems,
                      onChanged: changedDropDownItem,
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(color: Colors.black),
                      underline: Container(
                        height: 2,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                      //    formatoFecha(_fechaHasta, 0));

                      // print('boton presionado');
                    }),
              ],
            ),
            Container(
              child: new CardPieChartMes(
                dataMap: dataGraficaMesData,
                isCardGraf: isCardGraf,
                scaffoldKey: _scaffoldKey ,
                screenshotController: SCMes,
                descripcion: "Hospital ${_currentCity} de la fecha: ${dateTimeFormat(_fechaDesde)} a ${dateTimeFormat(_fechaHasta)}",
              ),
            ),


          ],
        ),
      ),
    );
  }
}

class CardPieChartMes extends StatelessWidget {
  final List<charts.Series<TimeSeriesSales, DateTime>> dataMap;
  bool isCardGraf;
  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String descripcion;

  CardPieChartMes({this.dataMap, this.isCardGraf, this.screenshotController,this.scaffoldKey, this.descripcion});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(


        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Screenshot(
            controller: screenshotController,
            child: Card(
              elevation: 8.0,
              child: Column(
                children: <Widget>[

                  Padding(padding: new EdgeInsets.all(0.0),
                  child: ShareGraph(
                    title: 'Atención por meses',
                    description: descripcion,
                    screenshotController: screenshotController,
                    scaffoldKey: scaffoldKey,
                  ),
                  ),

                  Padding(
                    padding: new EdgeInsets.all(13.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 400,
                      child: (dataMap != null)
                          ? new charts.TimeSeriesChart(
                        dataMap,
                        animate: true,
                        defaultRenderer: new charts.LineRendererConfig(),
                        // Custom renderer configuration for the point series.
                        customSeriesRenderers: [
                          new charts.PointRendererConfig(
                            // ID used to link series to this renderer.
                              customRendererId: 'customPoint')
                        ],
                        behaviors: [
                          new charts.SeriesLegend(
                            position: charts.BehaviorPosition.bottom,
                            horizontalFirst: false,
                            // This defines the padding around each legend entry.
                            cellPadding: new EdgeInsets.only(
                                right: 4.0, bottom: 4.0),
                            showMeasures: true,
                            measureFormatter: (num value) {
                              return value == null ? '-' : '${value}';
                            },
                          ),
                        ],
                        domainAxis: new charts.DateTimeAxisSpec(
                            tickFormatterSpec:
                            new charts.AutoDateTimeTickFormatterSpec(
                                day: new charts.TimeFormatterSpec(
                                    format: 'M',
                                    transitionFormat: 'MM/dd/yyyy'))),
                        dateTimeFactory:
                        const charts.LocalDateTimeFactory(),
                      )
                          : Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
              : Container(),
        ));
  }
}

class HospitalGraf {
  final int odts_total;
  final String odts_mes;
  final String odts_anio;
  HospitalGraf(this.odts_total, this.odts_mes, this.odts_anio);
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final int sales;

  TimeSeriesSales(this.time, this.sales);
}
