import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:screenshot/screenshot.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';

class CardPieChartEspecialidad extends StatelessWidget {
  final String descripcion;
  final List<charts.Series<OrdinalSales4, String>> dataMap;
  final bool isCardGraf;
  final ScreenshotController SCEspecialidad;
  final GlobalKey<ScaffoldState> scaffoldKey;

  CardPieChartEspecialidad(
      {this.descripcion,
      this.dataMap,
      this.isCardGraf,
      this.SCEspecialidad,
      this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(
        width: MediaQuery.of(context).size.width * 1.0,
        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Screenshot(
                  controller: SCEspecialidad,
                  child: Card(
                    elevation: 8.0,
                    child: Padding(
                      padding: new EdgeInsets.all(13.0),
                      child: Column(
                        children: <Widget>[
                          ShareGraph(
                            title: 'Atención por especialidad de emergencia',
                            description: descripcion,
                            screenshotController: SCEspecialidad,
                            scaffoldKey: scaffoldKey,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 1.0,
                            height: 600,
                            child: (dataMap != null)
                                ? new charts.BarChart(
                                    dataMap,
                                    animate: true,
                                    behaviors: [
                                      new charts.SeriesLegend(
                                          position:
                                              charts.BehaviorPosition.bottom,
                                          horizontalFirst:
                                              bool.fromEnvironment("Pablo")),
                                    ],
                                    primaryMeasureAxis: new charts
                                            .NumericAxisSpec(
                                        tickProviderSpec: new charts
                                                .BasicNumericTickProviderSpec(
                                            dataIsInWholeNumbers: true,
                                            desiredTickCount: 8)),
                                    barRendererDecorator:
                                        new charts.BarLabelDecorator<String>(),
                                    defaultRenderer:
                                        new charts.BarRendererConfig(
                                            groupingType: charts
                                                .BarGroupingType.groupedStacked,
                                            strokeWidthPx: 2.0),
                                  )
                                : Center(
                                    child: CircularProgressIndicator(),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ));
  }
}

class OrdinalSales4 {
  final String year;
  final int sales;
  final charts.Color color;

  OrdinalSales4(this.year, this.sales, this.color);
}
