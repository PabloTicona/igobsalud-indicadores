import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/style/theme.dart' as Theme;


class Medico extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Medicos(),
    );
  }
}

class Medicos extends StatefulWidget {
  @override
  _MedicosState createState() => _MedicosState();
}

class _MedicosState extends State<Medicos> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isCardGraf = false;
  List<HospitalMedico> HospitalMedicoList = [];
  List _hospitales = [
    "-- Hospitales --",
    "Los Pinos",
    "La Portada",
    "Cotahuma",
    "La Merced"
  ];

  List _medicos = [
    "-- Medicos --",
  ];
  bool isLoadMedicos = false;

  int getTriajeId(String nombreTriaje) {
    int resp = 0;
    switch (nombreTriaje) {
      case "Reanimación":
        resp = 1;
        break;

      case "Emergencia":
        resp = 2;
        break;

      case "Urgencia":
        resp = 3;
        break;

      case "Urgencia Menor":
        resp = 4;
        break;

      case "No Urgente":
        resp = 5;
        break;

      case "Procedimientos Enfermería":
        resp = 6;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  int getHospitalId(String nombreHospital) {
    int resp = 0;
    switch (nombreHospital) {
      case "Los Pinos":
        resp = 2;
        break;

      case "La Portada":
        resp = 3;
        break;

      case "Cotahuma":
        resp = 5;
        break;

      case "La Merced":
        resp = 1;
        break;

      default:
        resp = 0;
    }

    return resp;
  }

  List<charts.Series<LinearSales3, int>> dataGraficaMedicoData;
  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  List<DropdownMenuItem<String>> _dropDownMenuItemsHospitales;
  String _itemValuehospital;

  List<DropdownMenuItem<String>> _dropDownMenuItemsMedicos;
  String _itemValueMedicos;

  List<HospitalGraf> HospitalGrafList = [];

  @override
  void initState() {
    _dropDownMenuItemsHospitales = getDropDownMenuItems(_hospitales);
    _itemValuehospital = _dropDownMenuItemsHospitales[0].value;

    _dropDownMenuItemsMedicos = getDropDownMenuItems(_medicos);
    _itemValueMedicos = _dropDownMenuItemsMedicos[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in lista) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  void pressedReporte() {
    print("_fechaDesde : ${_fechaDesde}");

    print("_fechaHasta : ${_fechaHasta}");
    print("_itemValueMedicos : ${_itemValueMedicos}");

    var hospitalId_ = getHospitalId(_itemValuehospital);

    print("hospitalId_ : ${hospitalId_}");

    print("_itemValuehospital : ${_itemValuehospital}");
    var medicoId = getMedicoId(_itemValueMedicos);
    print("medicoId : ${medicoId}");

    getDataReporte(hospitalId_, _fechaDesde, _fechaHasta, medicoId);
  }

  int getMedicoId(String nomMedico) {
    int resp = 0;
    for (var medico in HospitalMedicoList) {
      if (medico.nombre_completo == nomMedico) {
        resp = medico.usr_id;
        break;
      }
    }
    return resp;
  }

  static DateTime CambiaFecha(DateTime fechaInicio) {
    var fecha_ = fechaInicio.toString().split(" ")[0];
    var minutos_ = fechaInicio.toString().split(" ")[1];
    var dia = fecha_.toString().split("-")[2];
    var mes = fecha_.toString().split("-")[1];
    var anio = fecha_.toString().split("-")[0];
    var mesSum = int.parse(mes) - 1;
    var fechaF = "${anio}-${mesSum}-${dia} 00:00:00.0000";
    return DateTime.parse(fechaF.toString().trim());
  }

  Future<void> getDataResponseMedicos(int idHospital) async {
    setState(() {
      isLoadMedicos = true;
      HospitalGrafList = [];
      _medicos = ["-- Medicos --"];
      _dropDownMenuItemsMedicos = getDropDownMenuItems(_medicos);
      _itemValueMedicos = _dropDownMenuItemsMedicos[0].value;
    });
    var query =
        "select usr_id, usr_usuario, prs_nombres, prs_paterno, prs_materno from _bp_personas inner join _bp_usuarios on prs_id = usr_prs_id and usr_estado = \$\$A\$\$ and usr_hsp_id = ${idHospital} where prs_tipo_medico = \$\$E\$\$ and prs_estado = \$\$A\$\$ and prs_hsp_id = ${idHospital}";
    var resp_ = await getResponseDinamico(query);
    if (resp_.length != 0) {
      for (var entry in resp_) {
        var nomCompleto =
            "${entry['prs_nombres']} ${entry['prs_paterno']} ${entry['prs_materno']}";
        _medicos.add(nomCompleto);
        HospitalMedicoList.add(new HospitalMedico(
            entry['usr_id'],
            entry['usr_usuario'],
            entry['prs_nombres'],
            entry['prs_paterno'],
            entry['prs_materno'],
            nomCompleto));
      }
    } else {
      showInSnackBar("No Existen medicos. Seleccione un hospital");
    }
    setState(() {
      _dropDownMenuItemsMedicos = getDropDownMenuItems(_medicos);
      isLoadMedicos = false;
    });
  }

  Future<void> getDataReporte(
      int idHospital, DateTime dataIni, DateTime dataFin, int idMedico) async {
    setState(() {
      isCardGraf = true;
    });

    var query =
        "select * from sp_lst_totales_medico_emergencia(${idHospital},\$\$${dataIni}\$\$,\$\$${dataFin}\$\$,${idMedico})";
    var resp_ = await getResponseDinamico(query);
    if (resp_.length != 0) {
      setState(() {
        dataGraficaMedicoData = _dataGraficaMedico(resp_);
      });
    } else {
      setState(() {
        isCardGraf = false;
      });
      showInSnackBar("No Existen datos. Seleccione otros valores de entrada");
    }
  }

  void changedDropDownItemHospital(String selected) {
    setState(() {
      _itemValuehospital = selected;
      getDataResponseMedicos(getHospitalId(selected));
    });
  }

  void changedDropDownItemTriaje(String selected) {
    setState(() {
      _itemValueMedicos = selected;
    });
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  charts.Color getColorMaterialPalette(int pos) {
    List colorArray = [
      charts.MaterialPalette.red.shadeDefault,
      charts.MaterialPalette.blue.shadeDefault,
      charts.MaterialPalette.cyan.shadeDefault,
      charts.MaterialPalette.indigo.shadeDefault,
      charts.MaterialPalette.pink.shadeDefault,
      charts.MaterialPalette.yellow.shadeDefault,
      charts.MaterialPalette.teal.shadeDefault,
      charts.MaterialPalette.purple.shadeDefault,
      charts.MaterialPalette.lime.shadeDefault,
    ];

    return colorArray[pos];
  }

  List<charts.Series<LinearSales3, int>> _dataGraficaMedico(resp_) {
    List<LinearSales3> dataReporte = [];
    List<LinearSales3> dataReporte2 = [];
    int cont = 0;
    for (var entry in resp_) {
      print("entry :: ${entry}");
      var color_ = entry['backgroundcolor'].toString().trim();

      var colorNum = color_.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

      dataReporte2.add(new LinearSales3(
          entry['data'],
          entry['data'],
          entry['label'],
          charts.Color(
              r: int.parse(arraycolor[0].trim()),
              g: int.parse(arraycolor[1].trim()),
              b: int.parse(arraycolor[2].trim()),
              a: 255)));

      cont++;
    }

    //   dataReporte.add(new LinearSales3( 0, 100, 'label 1', charts.MaterialPalette.purple.shadeDefault));
    //  dataReporte.add(new LinearSales3( 1, 75, 'label 2', charts.MaterialPalette.blue.shadeDefault));
    // dataReporte.add(new LinearSales3( 2, 25, 'label 3', charts.MaterialPalette.green.shadeDefault));
    // dataReporte.add(new LinearSales3(3, 5, 'label 4', charts.MaterialPalette.pink.shadeDefault));

    return [
      new charts.Series<LinearSales3, int>(
        id: 'Sales',
        domainFn: (LinearSales3 sales, _) => sales.year,
        measureFn: (LinearSales3 sales, _) => sales.sales,
        data: dataReporte2,
        colorFn: (LinearSales3 sales, _) => sales.color,
        labelAccessorFn: (LinearSales3 sales, _) => sales.label,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Medicos",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeEmergencias()),
                )
              },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ),
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Atención Por Medico De Emergencia"),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 93,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: new DropdownButton(
                            value: _itemValuehospital,
                            items: _dropDownMenuItemsHospitales,
                            onChanged: changedDropDownItemHospital,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 2,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 93,
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Card(
                        elevation: 8.0,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: !isLoadMedicos
                              ? new DropdownButton(
                                  isExpanded: true,
                                  value: _itemValueMedicos,
                                  items: _dropDownMenuItemsMedicos,
                                  onChanged: changedDropDownItemTriaje,
                                  iconSize: 24,
                                  elevation: 16,
                                  style: TextStyle(color: Colors.black),
                                  underline: Container(
                                    height: 2,
                                    color: Colors.red,
                                  ),
                                )
                              : Center(
                                  child: SpinKitThreeBounce(
                                    color: Colors.green,
                                    size: 30.0,
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Desde: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Hasta:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonMaterialCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      pressedReporte();
                    }),
              ],
            ),
            Container(
              child: new CardPieChartMedico(
                dataMap: dataGraficaMedicoData,
                isCardGraf: isCardGraf,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HospitalGraf {
  final int odts_total;
  final String odts_mes;
  final String odts_anio;
  HospitalGraf(this.odts_total, this.odts_mes, this.odts_anio);
}

class HospitalMedico {
  final int usr_id;
  final String usr_usuario;
  final String prs_nombres;
  final String prs_paterno;
  final String prs_materno;
  final String nombre_completo;
  HospitalMedico(this.usr_id, this.usr_usuario, this.prs_nombres,
      this.prs_paterno, this.prs_materno, this.nombre_completo);
}

class LinearSales3 {
  final int year;
  final int sales;
  final String label;
  final charts.Color color;

  LinearSales3(this.year, this.sales, this.label, this.color);
}

class CardPieChartMedico extends StatelessWidget {
  final List<charts.Series<LinearSales3, int>> dataMap;
  bool isCardGraf;

  CardPieChartMedico({this.dataMap, this.isCardGraf});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    print("dataMap :: ${dataMap}");

    return new Container(
        width: MediaQuery.of(context).size.width * 1.0,
        height: 200,
        child: Padding(
          padding: new EdgeInsets.all(0.0),
          child: isCardGraf
              ? Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: new EdgeInsets.all(13.0),
                    child: Container(
                      child: (dataMap != null)
                          ? new charts.PieChart(
                              dataMap,
                              animate: true,
                              defaultRenderer: new charts.ArcRendererConfig(
                                arcWidth: 17,
                                arcRendererDecorators: [
                                  new charts.ArcLabelDecorator(
                                    outsideLabelStyleSpec: charts.TextStyleSpec(
                                      color: charts.Color.black,
                                      fontSize: 9,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    ),
                  ),
                )
              : Container(),
        ));
  }
}
