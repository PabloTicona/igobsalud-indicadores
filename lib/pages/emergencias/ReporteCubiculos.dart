import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:igob_salud_app/components/CardContador.dart';
import 'package:igob_salud_app/pages/emergencias/models/Cubiculo.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;


class ReporteCubiculos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ReporteCub(),
    );
  }
}

class ReporteCub extends StatefulWidget {
  @override
  _ReporteCub createState() => _ReporteCub();
}

class _ReporteCub extends State<ReporteCub> {
  List<Cubiculo> listCubiculos = [];
  bool btnReporteCubiculos = true;
  Cubiculo cubiculoAux;

  int contLosPinos = 0;
  int contLaMerced = 0;
  int contLaPortada = 0;
  int contcotahuma = 0;

  @override
  void initState() {
    BtnReporteCubiculos();
  }


  //atenciones_emergencias
  Future<void> BtnReporteCubiculos() async {
    listCubiculos = await getReporteCubiculos();

    setState(() {
      cubiculoAux = buscarhospital(2);
    });

    listCubiculos.forEach((Cubiculo cubiculo) {
      if (cubiculo.vhspId == 3) {
        contLaPortada = cubiculo.cubicLibres + cubiculo.cubicOcupados;
      }

      if (cubiculo.vhspId == 2) {
        contLosPinos = cubiculo.cubicLibres + cubiculo.cubicOcupados;
        // cubiculoAux = cubiculo;
      }

      if (cubiculo.vhspId == 1) {
        contLaMerced = cubiculo.cubicLibres + cubiculo.cubicOcupados;
      }

      if (cubiculo.vhspId == 5) {
        contcotahuma = cubiculo.cubicLibres + cubiculo.cubicOcupados;
      }
    });
  }

  Future<List<Cubiculo>> getReporteCubiculos() async {
    List<Cubiculo> resp = [];

    var query = "select * from  listar_cubiculos_emergencias_libres_ocupados()";

    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        print("getReporteCubiculos $entry");
        resp.add(new Cubiculo(
            entry['cubic_libres'], entry['cubic_ocupados'], entry['vhsp_id']));
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
    return resp;
  }

  Cubiculo buscarhospital(int idHospital) {
    Cubiculo resp_;

    listCubiculos.forEach((Cubiculo cubiculo) {
      if (cubiculo.vhspId == idHospital) {
        resp_ = cubiculo;
      }
    });

    return resp_;
  }

  List<charts.Series<LinearSales, int>> _createLibresData() {
    var porcentajeLibres = (cubiculoAux != null)
        ? double.parse(((cubiculoAux.cubicLibres /
                    (cubiculoAux.cubicLibres + cubiculoAux.cubicOcupados)) *
                100)
            .toStringAsFixed(2))
        : 0.0;

    final data = [
      new LinearSales(1, porcentajeLibres, charts.MaterialPalette.green.shadeDefault),
      new LinearSales(2, 100 - porcentajeLibres, charts.Color.fromHex(code: '#eff0f1')),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: "Libres ${(cubiculoAux != null) ? cubiculoAux.cubicLibres : '0.0'}",
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
         colorFn: (LinearSales segment, _) => segment.color,
        data: data,
      )
    ];
  }

  List<charts.Series<LinearSales, int>> _createOcupadosData() {
    var porcentajeOcupadas = (cubiculoAux != null)
        ? double.parse(((cubiculoAux.cubicOcupados /
                    (cubiculoAux.cubicLibres + cubiculoAux.cubicOcupados)) *
                100)
            .toStringAsFixed(2))
        : 0.0;

    final data = [
      new LinearSales(1, porcentajeOcupadas, charts.MaterialPalette.green.shadeDefault),
      new LinearSales(2, 100 - porcentajeOcupadas, charts.Color.fromHex(code: '#eff0f1')),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: "Ocupados ${(cubiculoAux != null) ? cubiculoAux.cubicOcupados : '0.0'}",
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        colorFn: (LinearSales segment, _) => segment.color,
        //  colorFn: (__, _) => charts.MaterialPalette.cyan.shadeDefault,
        data: data,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: new AppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Reporte De Cubiculos",
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
              Text(
                "Emergencias",
                style: TextStyle(color: Colors.white, fontSize: 12.0),
              )
            ],
          ),
          flexibleSpace: Theme.ColorsSiis.AppBarColor(),

          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => {
            Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomeEmergencias()),
          )
            },
          )),
      body: Container(
          decoration: Theme.ColorsSiis.FondoImagen(),
          child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              new CardBarChartH(
                titulo: "Libre",
                porcentaje: (cubiculoAux != null)
                    ? double.parse(((cubiculoAux.cubicLibres /
                                (cubiculoAux.cubicLibres +
                                    cubiculoAux.cubicOcupados)) *
                            100)
                        .toStringAsFixed(2))
                    : 0.0,
                listData: _createLibresData(),
              ),
              new CardBarChartH(
                titulo: "Ocupados",
                porcentaje: (cubiculoAux != null)
                    ? double.parse(((cubiculoAux.cubicOcupados /
                                (cubiculoAux.cubicLibres +
                                    cubiculoAux.cubicOcupados)) *
                            100)
                        .toStringAsFixed(2))
                    : 0.0,
                listData: _createOcupadosData(),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              CardContador(
                nombre: 'Los Pinos',
                icono: Icons.local_hospital,
                cantidad: "Total ${contLosPinos}",
                onTap: () {
                  setState(() {
                    cubiculoAux = buscarhospital(2);
                  });
                },
              ),
              CardContador(
                nombre: 'La Merced',
                icono: Icons.local_hospital,
                cantidad: "Total : ${contLaMerced}",
                onTap: () {
                  setState(() {
                    cubiculoAux = buscarhospital(1);
                  });
                },
              ),
            ],
          ),
          Row(
            children: <Widget>[
              CardContador(
                nombre: 'La Portada',
                icono: Icons.local_hospital,
                cantidad: "Total ${contLaPortada}",
                onTap: () {
                  setState(() {
                    cubiculoAux = buscarhospital(3);
                  });
                },
              ),
              CardContador(
                nombre: 'Cotahuma',
                icono: Icons.local_hospital,
                cantidad: "Total : ${contcotahuma}",
                onTap: () {
                  setState(() {
                    cubiculoAux = buscarhospital(5);
                  });
                },
              ),
            ],
          )
        ],
      )),
    );
  }
}

class CardBarChartH extends StatelessWidget {
  final String titulo;

  final double porcentaje;
  final List<charts.Series<LinearSales, int>> listData;

  CardBarChartH({this.titulo, this.porcentaje, this.listData});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Column(
                  children: <Widget>[
                    Text("${porcentaje.toString()}%"),
                    Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width * 0.47,
                      child: (true)
                          ? new charts.PieChart(listData,
                              animate: true,
                              behaviors: [
                                new charts.SeriesLegend(
                                    position: charts.BehaviorPosition.bottom,
                                    horizontalFirst:
                                        bool.fromEnvironment("Pablo")),
                              ],
                              defaultRenderer:
                                  new charts.ArcRendererConfig(arcWidth: 15))
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    )
                  ],
                ),
              ),
            ),
          ),

          // Text(titulo),
        ],
      ),
    );
  }
}

class LinearSales {
  final int year;
  final double sales;
  final charts.Color color;

  LinearSales(this.year, this.sales, this.color);
}
