import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/consultaExterna/Witget/TimeSeriesChartModule.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'package:igob_salud_app/pages/consultaExterna/TabMensual.dart';
import 'package:igob_salud_app/pages/consultaExterna/TabTrimestral.dart';


import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';

import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:igob_salud_app/pages/consultaExterna/Servicios/HttpServiceConsultaExterna.dart';
import 'package:screenshot/screenshot.dart';

class IndicadorProceso extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Indicadores por proceso',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: IndicadorProcesos(),
    );
  }
}

class IndicadorProcesos extends StatefulWidget {
  @override
  _IndicadorProcesos createState() => _IndicadorProcesos();
}

class _IndicadorProcesos extends State<IndicadorProcesos> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  DateTime anio_mensual = DateTime.now();
  DateTime mesInicio_mensual = DateTime.now();
  DateTime mesfin_mensual = DateTime.now();

  DateTime anioInicial_trimestral = DateTime.now();
  DateTime anioComparativo_trimestral = DateTime.now();
  DateTime mesInicio_trimestral = DateTime.now();
  DateTime mesFin_trimestral = DateTime.now();

  List<Mes> listMeses = [];
  List<IndicadorGraf> listIndicadoresMensual = [];
  List<IndicadorGraf> listIndicadoresTrimestralInicial = [];
  List<IndicadorGraf> listIndicadoresTrimestralComparativo = [];

  String tip_indic = "RGL";
  String ges_mes = "ME";
  String hospitals = "h1";

  bool graficaMensual = false;
  bool graficaTrimestral = false;

  List<Mes> mesesMensualList = [];
  List<Mes> mesesTrimestrallList = [];

  List<IndicadorProces> listaEspecialidades;

  IndicadorProces selectedItemMensual;
  IndicadorProces selectedItemTrimestral;

  ScreenshotController SCMensual = ScreenshotController();
  ScreenshotController SCTrimestralInicial = ScreenshotController();
  ScreenshotController SCTrimestralComparartivo = ScreenshotController();

  @override
  void initState() {
    iniciar();

  }

  Future<void> iniciar() async {
    var listE = await HttpServiceConsultaExterna().getReporteSelect("EM");
    setState(() {
      listaEspecialidades = listE;
    });
  }

  void clickSelectItem(IndicadorProces indicadorProces, String tipo) async {
    print("indicadorProces ${indicadorProces.toString()}");
print("tipo tab $tipo");
    try {
      var indi = indicadorProces.indicador;
    } catch (e) {
      print(">>>ERROR");
      tipo = "E";
      showInSnackBar(" Seleccione un indicador");
    }

    if (tipo == "T") {
      setState(() {
        listIndicadoresTrimestralInicial = null;
        listIndicadoresTrimestralComparativo = null;
        graficaTrimestral = true;
        mesesTrimestrallList = [];
      });

      var listTrimestrarInical = await HttpServiceConsultaExterna()
          .getIndicadores(anioInicial_trimestral, mesInicio_trimestral,
              mesFin_trimestral, indicadorProces, "EM");
      var listTrimestrarComparativo = await HttpServiceConsultaExterna()
          .getIndicadores(anioComparativo_trimestral, mesInicio_trimestral,
              mesFin_trimestral, indicadorProces, "EM");

      var listMTrimestral = await HttpServiceConsultaExterna()
          .getMeses(mesInicio_trimestral, mesFin_trimestral, "EM", indicadorProces);

      setState(() {
        mesesTrimestrallList = listMTrimestral;
      });

      if (listTrimestrarInical != null) {
        setState(() {
          listIndicadoresTrimestralInicial = listTrimestrarInical;
        });
      } else {
        showInSnackBar(" Seleccione otro año inicial");
        setState(() {
          graficaTrimestral = false;
          mesesTrimestrallList = [];
        });
      }

      if (listTrimestrarComparativo != null) {
        setState(() {
          listIndicadoresTrimestralComparativo = listTrimestrarComparativo;
        });
      } else {
        setState(() {
          graficaTrimestral = false;
          mesesTrimestrallList = [];
        });
        showInSnackBar(" Seleccione otro año comparativo");
      }
    }
    if (tipo == "M") {
      setState(() {
        listIndicadoresMensual = null;
        graficaMensual = true;
        mesesMensualList = [];
      });

      var listM = await HttpServiceConsultaExterna().getMeses(
          mesInicio_mensual,
          mesfin_mensual,
          "EM", indicadorProces); //await  getMeses(mesInicio_mensual.toString(), mesfin_mensual.toString());

      var serv = await HttpServiceConsultaExterna().getIndicadores(anio_mensual,
          mesInicio_mensual, mesfin_mensual, indicadorProces, "EM");

      if (serv != null) {
        setState(() {
          listIndicadoresMensual = serv;
          mesesMensualList = listM;
        });
      } else {
        showInSnackBar("Seleccione otro indicador");
        setState(() {
          graficaMensual = false;
        });
      }
    }
  }

  /***
   * TRIMESTRAL
   */


  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                new Tab(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.insert_chart),
                      Text("MENSUAL")
                    ],
                  ),
                ),
                new Tab(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.equalizer),
                      Text("TRIMESTRAL")
                    ],
                  ),
                )
              ],
            ),
            flexibleSpace: Theme.ColorsSiis.AppBarColor(),
            title: Text('Indicador de procesos'),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeEmergencias()),
                )
              },
            ),
            actions: <Widget>[
              /*  IconButton(
                icon: const Icon(Icons.autorenew),
                tooltip: 'Actualizar',
                onPressed: () {
                  iniciar();
                },
              ), */
            ],
          ),
          body: TabBarView(
            children: [
// TAB MENSUAL
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                child: ListView(
                  children: <Widget>[
                    TabMensual(
                      listaEspecialidades: listaEspecialidades,
                      anio: anio_mensual,
                      mesInicio: mesInicio_mensual,
                      mesfin: mesfin_mensual,
                      onTapAnio: (dateTime, List<int> index) {
                        setState(() {
                          anio_mensual = dateTime;
                        });
                      },
                      onTapMesInicio: (dateTime, List<int> index) {
                        setState(() {
                          mesInicio_mensual = dateTime;
                        });
                      },
                      onTapMesFin: (dateTime, List<int> index) {
                        setState(() {
                          mesfin_mensual = dateTime;
                        });
                      },
                      selectedItem: selectedItemMensual,
                      changeDropdownItem: (IndicadorProces item) {
                        setState(() {
                          selectedItemMensual = item;
                        });
                      },
                      onPressed: () {
                        clickSelectItem(selectedItemMensual, "M");
                        print("Buscar");
                      },
                    ),
                    graficaMensual
                        ? Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Container(
                              padding: EdgeInsets.all(0.0),
                              // color: Colors.red,
                              width: MediaQuery.of(context).size.width * 1.0,
                              height: 450 +
                                  ((mesesMensualList.length).toDouble() * 30.8),
                              child: TimeSeriesChartModule(
                                indicadores: listIndicadoresMensual,
                                mesesList: mesesMensualList,
                                screenshotController: SCMensual,
                                scaffoldKey: _scaffoldKey,
                                title: "Indicador Por Proceso",
                                descripcion: "Indicador Por Proceso",
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              // FIN TAB MENSUAL
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                child: ListView(
                  children: <Widget>[
                    TabTrimestral(
                      listaEspecialidades: listaEspecialidades,
                      anioInicial: anioInicial_trimestral,
                      anioComparativo: anioComparativo_trimestral,
                      mesInicio: mesInicio_trimestral,
                      mesfin: mesFin_trimestral,
                      onTapAnioInicial: (dateTime, List<int> index) {
                        setState(() {
                          anioInicial_trimestral = dateTime;
                        });
                      },
                      onTapAnioComparativo: (dateTime, List<int> index) {
                        setState(() {
                          anioComparativo_trimestral = dateTime;
                        });
                      },
                      onTapMesInicio: (dateTime, List<int> index) {
                        setState(() {
                          mesInicio_trimestral = dateTime;
                        });
                      },
                      onTapMesFin: (dateTime, List<int> index) {
                        setState(() {
                          mesFin_trimestral = dateTime;
                        });
                      },
                      tapSelectItem: (IndicadorProces indicadorProces) {
                        //  selectItem(indicadorProces, "T");
                      },
                      selectedItem: selectedItemTrimestral,
                      changeDropdownItem: (IndicadorProces item) {
                        setState(() {
                          selectedItemTrimestral = item;
                        });

                        // print("IndicadorProces ${item.indicador}");
                      },
                      onPressed: () {
                        clickSelectItem(selectedItemTrimestral, "T");
                      },
                    ),
                    graficaTrimestral
                        ? Container(
                            width: MediaQuery.of(context).size.width * 1.0,
                            height: 460 +
                                ((mesesTrimestrallList.length).toDouble() *
                                    30.9),
                            child: TimeSeriesChartModule(
                              indicadores: listIndicadoresTrimestralInicial,
                              mesesList: mesesTrimestrallList,
                              screenshotController: SCTrimestralInicial,
                              scaffoldKey: _scaffoldKey,
                              title: "Año Inicial",
                              descripcion: "Año Inicial",
                            ),
                          )
                        : Container(),
                    graficaTrimestral
                        ? Container(
                            width: MediaQuery.of(context).size.width * 1.0,
                            height: 460 +
                                ((mesesTrimestrallList.length).toDouble() *
                                    20.9),
                            child: TimeSeriesChartModule(
                              indicadores: listIndicadoresTrimestralComparativo,
                              mesesList: mesesTrimestrallList,
                              screenshotController: SCTrimestralComparartivo,
                              scaffoldKey: _scaffoldKey,
                              title: "Año Comparativo",
                              descripcion: "Año Comparativo",
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
