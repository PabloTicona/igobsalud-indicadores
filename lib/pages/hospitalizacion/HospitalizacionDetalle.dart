import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

//typedef void TapCallback(HospitalEspecialidades i);
typedef dynamic TapCallback();
class HospitalizacionDetalle extends StatelessWidget {
  final String titulo;
  final List<Ceph> listCephs_;
  final TapCallback onTapWhatsapp;
  final TapCallback onTapCorreo;
  final TapCallback onTapGenerico;

  HospitalizacionDetalle({this.titulo, this.listCephs_, this.onTapWhatsapp, this.onTapCorreo, this.onTapGenerico});

  List<charts.Color> coloresList = [
    charts.MaterialPalette.blue.shadeDefault,
    charts.MaterialPalette.red.shadeDefault,
    charts.MaterialPalette.yellow.shadeDefault,
    charts.MaterialPalette.green.shadeDefault,
    charts.MaterialPalette.purple.shadeDefault,
    charts.MaterialPalette.cyan.shadeDefault,
    charts.MaterialPalette.deepOrange.shadeDefault,
    charts.MaterialPalette.lime.shadeDefault,
    charts.MaterialPalette.indigo.shadeDefault,
    charts.MaterialPalette.pink.shadeDefault,
    charts.MaterialPalette.teal.shadeDefault,
  ];

  List<charts.Series<OrdinalSales, String>> _createSampleData() {
    List<charts.Series<OrdinalSales, String>> res_ = [];

    int cont = 0;

    // coloresList.shuffle();




    listCephs_.forEach((Ceph c) {
    //  print("cont $cont");


      var colorNum = c.color.replaceAll("rgba(", "").replaceAll(")", "");
      var arraycolor = colorNum.split(",");

    //  print("arraycolor ${arraycolor[0]}");

    //  print("coloresList :  ${coloresList.elementAt(cont)} ");

      res_.add(new charts.Series<OrdinalSales, String>(
        id: " ${c.count} - ${c.espDescEspecialidad}",
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (__, _) => new charts.Color(r: int.parse(arraycolor[0].trim()) , g: int.parse(arraycolor[1].trim()), b: int.parse(arraycolor[2].trim()), a: 255),
        data: [new OrdinalSales("${c.count} ", c.count)],
      ));


    });

    return res_;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titulo),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[


            Card(
              elevation: 8.0,
              child: Padding(padding: EdgeInsets.all(8.0),

              child: Text("CAMAS EXISTENTES POR HOSPITAL ", style: TextStyle(fontWeight: FontWeight.bold),),
              ),
            ),


            Container(
              width: double.infinity,
              height: 750,
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 650,
                        width: double.infinity,
                        child:  new charts.BarChart(
                          _createSampleData(),
                          animate: true,
                          behaviors: [
                            new charts.SeriesLegend(
                                position: charts.BehaviorPosition.bottom,
                                horizontalFirst: bool.fromEnvironment("Pablo")),
                          ],
                          vertical: false,
                          barRendererDecorator: new charts.BarLabelDecorator<String>(),
                          defaultRenderer: new charts.BarRendererConfig(
                              groupingType: charts.BarGroupingType.groupedStacked,
                              strokeWidthPx: 2.0),
                        ),
                      ),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new IconButton(
                            icon: Icon(MdiIcons.whatsapp),
                            //   tooltip: 'Increase volume by 10',
                            onPressed: onTapWhatsapp,
                          ),
                          new IconButton(
                            // Use the MdiIcons class for the IconData
                            icon: new Icon(MdiIcons.email),
                            onPressed: onTapCorreo,
                          ),
                          new IconButton(
                            icon: new Icon(MdiIcons.shareVariant),
                            onPressed: onTapGenerico,
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Ceph {
  final int count;
  final int salIdEspecialidad;
  final String espDescEspecialidad;
  final String color;

  Ceph(
      this.count, this.salIdEspecialidad, this.espDescEspecialidad, this.color);

  @override
  String toStringCeph() {
    return 'Ceph{count: $count, salIdEspecialidad: $salIdEspecialidad, espDescEspecialidad: $espDescEspecialidad, color: $color}';
  }


}

class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
