import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/pages/hospitalizacion/model/Cama.dart';

class CardEstadoCamas extends StatelessWidget {
  final String titulo;
  final String tipo;
  final Cama hospital;

  final List<Cama> listCamas;

  CardEstadoCamas(
      {this.titulo, this.tipo, this.hospital, this.listCamas});



  @override
  Widget build(BuildContext context) {




    List<charts.Series<LinearSales2, int>> _createSampleData() {
      List<LinearSales2> data = [];

      List<charts.Series<LinearSales2, int>> resp= [];

      switch (tipo) {
        case "L":
          if (hospital != null) {
            var sumTotalH = (((hospital.camas_ocupadas!=null)?hospital.camas_ocupadas:0) + ((hospital.camas_desinfeccion!=null)?hospital.camas_desinfeccion:0)+ ((hospital.camas_libres!=null)?hospital.camas_libres:0));
            var porcentajeL = (hospital.camas_libres!=null)? hospital.camas_libres: 0;
            data.add(new LinearSales2(porcentajeL, porcentajeL, getColorhospital(hospital.id_hospital), hospital.nombre_hospital));
            var rest = sumTotalH - porcentajeL;
            data.add(new LinearSales2(rest, rest, charts.Color.fromHex(code: '#eff0f1'), hospital.nombre_hospital));
          } else {
            for (Cama entry in listCamas) {
              var porcentajeL = (entry.camas_libres!=null)? entry.camas_libres: 0;
              data.add(new LinearSales2(porcentajeL, porcentajeL,
                  getColorhospital(entry.id_hospital), entry.nombre_hospital));
            }
          }

          break;

        case "O":
          if (hospital != null) {

            var sumTotalH = (((hospital.camas_ocupadas!=null)?hospital.camas_ocupadas:0) + ((hospital.camas_desinfeccion!=null)?hospital.camas_desinfeccion:0)+ ((hospital.camas_libres!=null)?hospital.camas_libres:0));
            var porcentajeL = (hospital.camas_ocupadas!=null)? hospital.camas_ocupadas:0;
            data.add(new LinearSales2(porcentajeL, porcentajeL, getColorhospital(hospital.id_hospital), hospital.nombre_hospital));
            var rest = sumTotalH - porcentajeL;
            data.add(new LinearSales2(rest, rest, charts.Color.fromHex(code: '#eff0f1'), hospital.nombre_hospital));


          } else {
            for (Cama entry in listCamas) {
              var porcentajeO = (entry.camas_ocupadas!=null)? entry.camas_ocupadas: 0;
              data.add(new LinearSales2(porcentajeO, porcentajeO,
                  getColorhospital(entry.id_hospital), entry.nombre_hospital));
            }
          }

          break;

        case "D":
          if (hospital != null) {

            var sumTotalH = (((hospital.camas_ocupadas!=null)?hospital.camas_ocupadas:0) + ((hospital.camas_desinfeccion!=null)?hospital.camas_desinfeccion:0)+ ((hospital.camas_libres!=null)?hospital.camas_libres:0));
            var porcentajeL =(hospital.camas_desinfeccion!=null)? hospital.camas_desinfeccion:0;
            data.add(new LinearSales2(porcentajeL, porcentajeL, getColorhospital(hospital.id_hospital), hospital.nombre_hospital));
            var rest = sumTotalH - porcentajeL;
            data.add(new LinearSales2(rest, rest, charts.Color.fromHex(code: '#eff0f1'), hospital.nombre_hospital));

          } else {
            for (Cama entry in listCamas) {
              var porcentajeD = (entry.camas_desinfeccion!=null)? entry.camas_desinfeccion:0;
              data.add(new LinearSales2(porcentajeD, porcentajeD,
                  getColorhospital(entry.id_hospital), entry.nombre_hospital));
            }
          }

          break;
      }


      resp.add( new charts.Series<LinearSales2, int>(
        id: 'Sales 44',
        domainFn: (LinearSales2 sales, _) => sales.year,
        measureFn: (LinearSales2 sales, _) => sales.sales,
        data: data,
        colorFn: (LinearSales2 sales, _) => sales.color,
        // Set a label accessor to control the text of the arc label.

      ));
      return resp;

    }

    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Column(
                  children: <Widget>[
                    // Text("11 %"),
                    Text(
                      titulo,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width * 0.31,
                      child: new charts.PieChart(
                        _createSampleData(),
                        animate: true,
                        // Configure the width of the pie slices to 60px. The remaining space in
                        // the chart will be left as a hole in the center.
                        behaviors: [
                          new charts.SeriesLegend(
                              position: charts.BehaviorPosition.bottom,
                              horizontalFirst: bool.fromEnvironment("Pablo")),
                          new charts.DatumLegend(
                            // Positions for "start" and "end" will be left and right respectively
                            // for widgets with a build context that has directionality ltr.
                            // For rtl, "start" and "end" will be right and left respectively.
                            // Since this example has directionality of ltr, the legend is
                            // positioned on the right side of the chart.
                            position: charts.BehaviorPosition.bottom,
                            // By default, if the position of the chart is on the left or right of
                            // the chart, [horizontalFirst] is set to false. This means that the
                            // legend entries will grow as new rows first instead of a new column.
                            horizontalFirst: false,
                            // This defines the padding around each legend entry.
                            cellPadding:
                            new EdgeInsets.only(right: 4.0, bottom: 4.0),
                            // Set [showMeasures] to true to display measures in series legend.
                            showMeasures: true,
                            // Configure the measure value to be shown by default in the legend.
                            legendDefaultMeasure:
                            charts.LegendDefaultMeasure.lastValue,
                            // Optionally provide a measure formatter to format the measure value.
                            // If none is specified the value is formatted as a decimal.
                            measureFormatter: (num value) {
                              return value == null ? '-' : '';
                            },
                          ),
                        ],
                        defaultRenderer:
                        new charts.ArcRendererConfig(arcWidth: 10),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),

          // Text(titulo),
        ],
      ),
    );
  }
}

class LinearSales2 {
  final int year;
  final int sales;
  final charts.Color color;
  final String hospital;
  LinearSales2(this.year, this.sales, this.color, this.hospital);
}

class coloresHospitales {
  final int hospital;
  final charts.Color color;
  coloresHospitales(this.hospital, this.color);

  @override
  String toString() {
    return 'coloresHospitales{hospital: $hospital, color: $color}';
  }
}
class coloresHospitales2 {
  final int hospital;
  final Color color;
  coloresHospitales2(this.hospital, this.color);

  @override
  String toString() {
    return 'coloresHospitales{hospital: $hospital, color: $color}';
  }
}


class HospitalesTod {
  final String hospital;
  final int id_hospital;
  final int cam_libre;
  final int cam_ocupado;
  final int cam_desinfecion;

  HospitalesTod(this.hospital, this.id_hospital, this.cam_libre,
      this.cam_ocupado, this.cam_desinfecion);

  @override
  String toString() {
    return 'HospitalesTod{hospital: $hospital, id_hospital: $id_hospital, cam_libre: $cam_libre, cam_ocupado: $cam_ocupado, cam_desinfecion: $cam_desinfecion}';
  }
}

charts.Color getColorhospital(int idHospital) {

  List<coloresHospitales> listColores = [];
  listColores.add(
      new coloresHospitales(1, charts.MaterialPalette.purple.shadeDefault));
  listColores.add(
      new coloresHospitales(2, charts.MaterialPalette.green.shadeDefault));
  listColores.add(
      new coloresHospitales(3, charts.MaterialPalette.indigo.shadeDefault));
  listColores.add(
      new coloresHospitales(5, charts.MaterialPalette.blue.shadeDefault));
  charts.Color resp = charts.MaterialPalette.red.shadeDefault;

  for (coloresHospitales entry in listColores) {
    if (entry.hospital == idHospital) {
      resp = entry.color;

      break;
    }
  }

  return resp;
}

Color getColorhospital2(int idHospital) {

  List<coloresHospitales2> listColores = [];

  listColores.add(
      new coloresHospitales2(1, Colors.purple));
  listColores.add(
      new coloresHospitales2(2, Colors.green));
  listColores.add(
      new coloresHospitales2(3, Colors.indigo));
  listColores.add(
      new coloresHospitales2(5, Colors.blue));
  Color resp = Colors.white;

  for (coloresHospitales2 entry in listColores) {
    if (entry.hospital == idHospital) {
      resp = entry.color;

      break;
    }
  }

  return resp;
}
