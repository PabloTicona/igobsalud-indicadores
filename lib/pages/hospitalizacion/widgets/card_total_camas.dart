import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Cama.dart';
typedef void TapCallback();
class CardTotalCamas extends StatelessWidget {

  final String nombre;
  final IconData icono;
  final Cama cama;
  final Color color;
  final TapCallback onTap;

  CardTotalCamas({this.nombre, this.icono, this.cama, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    String total="0";
    if(cama!=null){
       total = (((cama.camas_ocupadas!=null)? cama.camas_ocupadas:0) + ((cama.camas_desinfeccion!= null)?cama.camas_desinfeccion:0) + ((cama.camas_libres!=null)?cama.camas_libres:0)  ).toString();

    }


    return new Container(
      child: Expanded(
        child: Padding(padding: EdgeInsets.all(4.0),
          child: Material(
            borderRadius: BorderRadius.circular(8.0)
            ,elevation: 5.0,
            child: InkWell(
              borderRadius: BorderRadius.circular(8.0),
              child: Padding(padding: EdgeInsets.all(10.0),
                child: Column(

                  children: <Widget>[


                    Icon(
                      icono,
                      color: color,
                      size: 40.0,
                    ),
                    Text(nombre, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),


                    Chip(
                      backgroundColor: color,
                      padding: EdgeInsets.all(0),

                      label: (total !='0')? Row(


                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[

                          Text(total, style: TextStyle(color: Colors.white)),
                          Icon(
                            Icons.hotel,
                            color: Colors.white,
                            size: 20.0,
                          ),
                        ],
                      ): SpinKitThreeBounce(color: Colors.white, size: 15.0,),
                    )
                  ],
                ),
              ),
              onTap: onTap,
            ),
          ),
        ),
      ),
    );
  }
}