import 'package:flutter/foundation.dart';

class Cama {
  final String nombre_hospital;
  final int id_hospital;
  final int camas_libres;
  final int camas_ocupadas;
  final int camas_desinfeccion;

  Cama({
    @required this.nombre_hospital,
    @required this.id_hospital,
    @required this.camas_libres,
    @required this.camas_ocupadas,
    @required this.camas_desinfeccion,
  });

  factory Cama.fromJson(Map<dynamic, dynamic> json) {
    return Cama(
      nombre_hospital: json['nombre_hospital'] as String,
      id_hospital: json['id_hospital'] as int,
      camas_libres: json['camas_libres'] as int,
      camas_ocupadas: json['camas_ocupadas'] as int,
      camas_desinfeccion: json['camas_desinfeccion'] as int,
      
    );
  }

  @override
  String toString() {
    return 'Cama{nombre_hospital: $nombre_hospital, id_hospital: $id_hospital, camas_libres: $camas_libres, camas_ocupadas: $camas_ocupadas, camas_desinfeccion: $camas_desinfeccion}';
  }


}
