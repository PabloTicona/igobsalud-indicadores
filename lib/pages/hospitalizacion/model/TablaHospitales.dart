class TablaHospitales {
  final String log_tipo;
  final int log_cantidad;
  final int log_id_especialidad;
  final String log_desc_especialidad;

  TablaHospitales(this.log_tipo, this.log_cantidad, this.log_id_especialidad,
      this.log_desc_especialidad);

  @override
  String toString() {
    return 'TablaHospitales{log_tipo: $log_tipo, log_cantidad: $log_cantidad, log_id_especialidad: $log_id_especialidad, log_desc_especialidad: $log_desc_especialidad}';
  }
}
