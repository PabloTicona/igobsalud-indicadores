import 'package:flutter/foundation.dart';

class Fecha {
  final String logfecha;

  Fecha({
    @required this.logfecha,
  });

  factory Fecha.fromJson(Map<dynamic, dynamic> json) {
    return Fecha(
      logfecha: json['log_fecha'] as String,
    );
  }
}
