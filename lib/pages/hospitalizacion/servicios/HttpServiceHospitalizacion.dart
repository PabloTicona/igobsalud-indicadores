
import 'package:igob_salud_app/pages/hospitalizacion/model/Cama.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Fecha.dart';
import 'package:igob_salud_app/web-services/services.dart';

class HttpServiceHospitalizacion {
  Future<List<Fecha>> getFechas() async {
    var query =
        "select log_fecha from internaciones._inter_camas_log where log_estado = \$\$A\$\$ group by log_fecha order by log_fecha desc";
    var resp_ = await getResponseDinamico(query);
    //List<dynamic> body = jsonDecode(resp_);
    List<dynamic> body = resp_;
    List<Fecha> result = body
        .map(
          (dynamic item) => Fecha.fromJson(item),
        )
        .toList();

    return result;
  }

  Future<List<Cama>> getTotalCamas(String fecha) async {
    var query =
        "select * from internaciones.sp_sacar_totales_log_camas(\$\$${fecha}\$\$)";
    var resp_ = await getResponseDinamico(query);
    //List<dynamic> body = jsonDecode(resp_);
    List<dynamic> body = resp_;
    List<Cama> result = body
        .map(
          (dynamic item) => Cama.fromJson(item),
        )
        .toList();

    return result;
  }
}
