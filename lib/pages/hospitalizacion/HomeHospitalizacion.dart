import 'package:flutter/material.dart';
import 'package:igob_salud_app/components/CardBarChartHospitales.dart';
import 'package:igob_salud_app/components/CardContadorHospital.dart';

import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:igob_salud_app/pages/hospitalizacion/TablaDinamica.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Cama.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Fecha.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/TablaHospitales.dart';
import 'package:igob_salud_app/pages/hospitalizacion/servicios/HttpServiceHospitalizacion.dart';
import 'package:igob_salud_app/pages/hospitalizacion/widgets/card_estado_camas.dart';
import 'package:igob_salud_app/pages/hospitalizacion/widgets/card_total_camas.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:search_widget/search_widget.dart';

import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/SelectedItemWidget.dart';
import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/MyTextField.dart';
import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/SelectedItemWidget.dart';
import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/MyTextField.dart';

class HomeHospitalizacion extends StatelessWidget {
  final HttpServiceHospitalizacion httpService = HttpServiceHospitalizacion();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hospitalización',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Hospitalizacion(),
    );
  }
}

class Hospitalizacion extends StatefulWidget {
  @override
  _HospitalizacionState createState() => _HospitalizacionState();
}

//final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

class _HospitalizacionState extends State<Hospitalizacion> {
  List<LeaderBoard> listSearch = [];

  List<Cama> todasCamasList = [];
  String fecha_ = "Cargando..";

  Cama laMercedCama;
  Cama losPinosCama;
  Cama laPortadaCama;
  Cama cotahumaCama;

  Cama clickHospital;

  List<TablaHospitales> tablaHospitalesList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    iniciarServicios();
  }

  Future<void> iniciarServicios() async {
    List<Fecha> resp = await HttpServiceHospitalizacion().getFechas();

    var listSearchResp = await dataSearchList(resp);
    fecha_ = invertirFecha(resp[0].logfecha);
    print("fecha_ => ${fecha_}");
    TotasLasCamas(fecha_);
    setState(() {
      clickHospital = null;
      tablaHospitalesList=null;
      listSearch = listSearchResp;
    });
  }

  Future<void> TotasLasCamas(String fecha) async {
    setState(() {
      laMercedCama = null;
      losPinosCama = null;
      laPortadaCama = null;
      cotahumaCama = null;
    });

    var todasCamasList_ =
        await HttpServiceHospitalizacion().getTotalCamas(fecha);

    setState(() {
      todasCamasList = todasCamasList_;
      laMercedCama = getCamaHospitalId(todasCamasList, 1);
      losPinosCama = getCamaHospitalId(todasCamasList, 2);
      laPortadaCama = getCamaHospitalId(todasCamasList, 3);
      cotahumaCama = getCamaHospitalId(todasCamasList, 5);
    });

    //getCamaHospitalId(todasCamasList, 1),
  }

  Future<List<LeaderBoard>> dataSearchList(List<Fecha> resp) async {
    List<LeaderBoard> listSearch_ = [];
    for (Fecha entry in resp) {
      listSearch_.add(new LeaderBoard(invertirFecha(entry.logfecha), 12.25));
    }
    return listSearch_;
  }

  selectHospital(List<Cama> todasCamas, int idHospital) async {
    Cama cama = getCamaHospitalId(todasCamas, idHospital);

 var tablaHospitalesList_=  await libresOcupadosTabla(cama.id_hospital, fecha_);


    setState(() {
      tablaHospitalesList = tablaHospitalesList_;
      clickHospital = cama;
    });
    print("idHospital ${cama.toString()}");
  }

  Cama getCamaHospitalId(List<Cama> todasCamas, int hospitalId) {
    for (Cama cama in todasCamas) {
      if (cama.id_hospital == hospitalId) return cama;
    }
    return null;
  }



  selectFecha(var item) {
    print("item 111 :: ${item.fecha}");

    TotasLasCamas(item.fecha);

    setState(() {
      clickHospital = null;
      tablaHospitalesList=null;
      fecha_ = item.fecha;
    });

    // getTotalCamas(fecha_);
  }

  static String invertirFecha(String dateTime) {
    String _res = 'Error Fecha';
    var fechaHoyArray = dateTime.split('-');

//B: DD-MM-AAA
    var fechahoy =
        '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';

    return fechahoy;
  }


  Future<List<TablaHospitales>> libresOcupadosTabla(
      int hospitanId, String fecha_) async {
    // var fecha_ = '18-11-2019';
    //var hospitanId = 5;
    List<TablaHospitales> ListResp_ = [];
    var query =
        "select log_tipo,log_cantidad,log_id_especialidad,log_desc_especialidad from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_id_hospital = ${hospitanId} and log_fecha = \$\$${fecha_}\$\$";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        ListResp_.add(new TablaHospitales(
            entry['log_tipo'],
            entry['log_cantidad'],
            entry['log_id_especialidad'],
            entry['log_desc_especialidad']));
        // print("===> entry:::::  ${entry}");
      }
    } else {
     // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return ListResp_;
  }

/*
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  } */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      // key: scaffoldKey,
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Hospitalización",
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
            Text(
              "Camas Disponibles",
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            )
          ],
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            )
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {
              iniciarServicios();
            },
          ),
        ],
      ),
      body: Center(
          child: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Center(
                            child: Text("Buscar fechas"),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.55,
                          child: SearchWidget<LeaderBoard>(
                            dataList: listSearch,

                            hideSearchBoxWhenItemSelected: false,
                            listContainerHeight:
                                MediaQuery.of(context).size.height / 4,
                            queryBuilder:
                                (String query, List<LeaderBoard> list) {
                              return list
                                  .where((LeaderBoard item) => item.fecha
                                      .toLowerCase()
                                      .contains(query.toLowerCase()))
                                  .toList();
                            },
                            popupListItemBuilder: (LeaderBoard item) {
                              return PopupListItemWidget(item);
                            },
                            selectedItemBuilder: (LeaderBoard selectedItem,
                                VoidCallback deleteSelectedItem) {
                              return SelectedItemWidget(
                                  selectedItem, deleteSelectedItem);
                            },

                            // widget customization
                            noItemsFoundWidget: NoItemsFound(),
                            textFieldBuilder: (TextEditingController controller,
                                FocusNode focusNode) {
                              return MyTextField(controller, focusNode, fecha_);
                            },

                            onItemSelected: selectFecha,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),

            Row(
              children: <Widget>[
                CardTotalCamas(
                  nombre: "La Merced",
                  cama: laMercedCama,
                  icono: Icons.local_hospital,
                  color: Colors.purple,
                  onTap: () {
                    selectHospital(todasCamasList, 1);
                  },
                ),
                CardTotalCamas(
                  nombre: "Los Pinos",
                  cama: losPinosCama,
                  icono: Icons.local_hospital,
                  color: Colors.green,
                  onTap: () {
                    selectHospital(todasCamasList, 2);
                  },
                ),
                CardTotalCamas(
                  nombre: "La Portada",
                  cama: laPortadaCama,
                  icono: Icons.local_hospital,
                  color: Colors.indigo,
                  onTap: () {
                    selectHospital(todasCamasList, 3);
                  },
                ),
                CardTotalCamas(
                  nombre: "Cotahuma",
                  cama: cotahumaCama,
                  icono: Icons.local_hospital,
                  color: Colors.blue,
                  onTap: () {
                    selectHospital(todasCamasList, 5);
                  },
                )
              ],
            ),

            Container(
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  color: getColorhospital2((clickHospital != null)?clickHospital.id_hospital:0),
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          MdiIcons.hospitalBuilding,
                          color: (clickHospital != null)? Colors.white: Colors.black,
                          size: 18.0,
                        ),
                        Text(
                          (clickHospital != null)
                              ? clickHospital.nombre_hospital
                              : "TODOS LOS HOSPITALES",
                          style: TextStyle(fontWeight: FontWeight.bold, color:(clickHospital != null)?  Colors.white: Colors.black),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                new CardEstadoCamas(
                  titulo: "Libres",
                  tipo: "L",
                  hospital: clickHospital,
                  listCamas: todasCamasList,
                ),
                new CardEstadoCamas(
                  titulo: "Ocupados",
                  tipo: "O",
                  hospital: clickHospital,
                  listCamas: todasCamasList,
                ),
                new CardEstadoCamas(
                  titulo: "Desinfección",
                  tipo: "D",
                  hospital: clickHospital,
                  listCamas: todasCamasList,
                )
              ],
            ),



            Container(
              child: (tablaHospitalesList!=null)?Card(
                child: Padding(padding: EdgeInsets.all(8.0),
                child:  new TablaDinamica(
                  listHospitales: tablaHospitalesList,
                  hospitalId: 0,
                  color: getColorhospital2((clickHospital != null)?clickHospital.id_hospital:0),
                ),
                ),
              ):Text(""),
            )

          ],
        ),
      )),
    );
  }
}
