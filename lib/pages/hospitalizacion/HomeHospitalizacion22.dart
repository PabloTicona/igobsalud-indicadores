import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:igob_salud_app/components/CardContadorHospital.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
import 'package:igob_salud_app/pages/hospitalizacion/HospitalizacionDetalle.dart';
import 'package:igob_salud_app/pages/hospitalizacion/HospitalizacionDetalleOcupadasLibres.dart';
import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:igob_salud_app/pages/hospitalizacion/TablaDinamica.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/TablaHospitales.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:igob_salud_app/components/CardBarChartHospitales.dart';
import 'package:search_widget/search_widget.dart';

import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/SelectedItemWidget.dart';
import 'package:igob_salud_app/pages/hospitalizacion/SearchWidgetHelps/MyTextField.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:igob_salud_app/web-services/services.dart';


class HomeHospitalizacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hospitalización',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Hospitalizacion(),
    );
  }
}

class Hospitalizacion extends StatefulWidget {
  @override
  _HospitalizacionState createState() => _HospitalizacionState();
}

/////

TableRow _buildTableRow(
    Color colorFondo_, Color textColor, String listOfNames) {
  return TableRow(
    children: listOfNames.split(',').map((name) {
      return Container(
        decoration: BoxDecoration(color: colorFondo_),
        alignment: Alignment.center,
        child: Text(name, style: TextStyle(fontSize: 9.8, color: textColor)),
        padding: EdgeInsets.all(5.0),
      );
    }).toList(),
  );
}

class NewItem {
  bool isExpanded;
  final String header;
  final Widget body;
  final Icon iconpic;
  NewItem(this.isExpanded, this.header, this.body, this.iconpic);
}

double discretevalue = 2.0;
double hospitaldiscretevalue = 25.0;
/////

List<LeaderBoard> listSearch = [];
List<TablaHospitales> tablaHospitalesListLosPinos = [];
List<TablaHospitales> tablaHospitalesListLaMerced = [];
List<TablaHospitales> tablaHospitalesListLaPortada = [];
List<TablaHospitales> tablaHospitalesListCotahuma = [];

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class _HospitalizacionState extends State<Hospitalizacion> {
  List<NewItem> items = [];

  ListView List_Criteria;

  ProgressDialog pr;

  HospitalH hospitalLosPinos;
  HospitalH hospitalLaPortada;
  HospitalH hospitalLaMerced;
  HospitalH hospitalCotahuma;

  HospitalH hospitalAux = null;
  String hospitalAuxNombre = "Los Pinos";

  List<HospitalEspecialidades> lisEspecialidades = [
    new HospitalEspecialidades('2', 'Hospital Los Pinos', 2, 2),
    new HospitalEspecialidades('3', 'Hospital La Portada', 3, 3),
    new HospitalEspecialidades('5', 'Hospital Cotahuma', 5, 5),
    new HospitalEspecialidades('1', 'Hospital La Merced', 1, 1),
  ];
  HospitalesTod hospitalesTodSelect = null;
  List<Ceph> listCeph = [];
  List<HospitalesTod> listHospitalesTod = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
    iniciarServicios();
  }

//  List<TablaHospitales> tablaHospitalesListResp;
  DateTime _fechaHoy = DateTime.now();
  String fechaDefecto = formatDate(DateTime.now(), "B");
  Future<void> internacionesTodosLosHospitales() async {
    var query = "select * from internaciones.sp_listar_tcol()";
    var resp_ = await getResponseDinamico(query);

    listHospitalesTod = [];

    print(" internacionesTodosLosHospitales ${resp_}");
    if (resp_ != null) {
      for (var entry in resp_) {
        listHospitalesTod.add(new HospitalesTod(
            entry['hospital'],
            entry['id_hospital'],
            entry['cam_libre'],
            entry['cam_ocupado'],
            entry['cam_desinfecion']));
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  Future<void> getFechasResporte() async {
    var query =
        "select log_fecha from internaciones._inter_camas_log where log_estado = \$\$A\$\$ group by log_fecha order by log_fecha desc";

    List<LeaderBoard> listSearchResp_ = [];

    var resp_ = await getResponseDinamico(query);

    listHospitalesTod = [];

    print(" getFechasResporte ${resp_}");
    if (resp_ != null) {
      for (var entry in resp_) {
        print("entry fechas : ${entry}");
        var formatoFecha = formatDate(
            DateTime.parse('${entry['log_fecha']} 00:00:00.000'), "B");
//print("formatoFecha :: ${formatoFecha}");

        listSearchResp_.add(new LeaderBoard(formatoFecha, 54.0));
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    String fecha_ = listSearchResp_[0].fecha;
    print("FECHA : ${fecha_}");
    getTotalCamas(fecha_);
    setState(() {
      fechaDefecto = fecha_;

      /// formatDate(DateTime.parse('${fecha_} 00:00:00.000'), "B");

      listSearch = listSearchResp_;
    });
  }

  static String formatDate(DateTime dateTime, String formato) {
    String _res = 'Error Fecha';
    var fechaHoyArray = (dateTime.toString().split(' ')[0]).split('-');

//B: DD-MM-AAA
    var fechahoy =
        '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';

    if (formato == "A") {
      // AAAA-MM-DD
      fechahoy = '${fechaHoyArray[0]}-${fechaHoyArray[1]}-${fechaHoyArray[2]}';
    }

    return fechahoy;
  }

  Future<void> getTotalCamas(String fechahoy) async {
    //select * from internaciones.sp_sacar_totales_log_camas('2019-11-18')
    // var query = "select * from internaciones.sp_sacar_totales_log_camas(\$\$2019-11-19\$\$)";

    var query =
        "select * from internaciones.sp_sacar_totales_log_camas(\$\$${fechahoy}\$\$)";

    print("1 => query query :: ${query}");

    var resp_ = await getResponseDinamico(query);

    if (resp_ != null) {
      for (var entry in resp_) {
        print("3 => total fechas : ${entry}");
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  Future<void> selectHospital(int idHospital) async {
    print("idHospital $idHospital");

    setState(() {
      hospitalesTodSelect = getHospital(idHospital);
    });

    print("Hospital ${getHospital(idHospital)}");
  }

  HospitalesTod getHospital(int idHospital) {
    HospitalesTod resp = null;
    for (HospitalesTod entry in listHospitalesTod) {
      if (entry.id_hospital == idHospital) {
        resp = entry;
        break;
      }
    }
    return resp;
  }

  Future<void> iniciarServicios() async {
    await getFechasResporte();

    setState(() {
      hospitalesTodSelect = null;
    });
    internacionesTodosLosHospitales();

    setState(() {
      hospitalLosPinos = null;
      hospitalLaPortada = null;
      hospitalLaMerced = null;
      hospitalCotahuma = null;

      // hospitalAux = null;
      hospitalAuxNombre = "Los Pinos";
    });

    hospitalLosPinos = await dataEspecialidad(2);
    hospitalLaPortada = await dataEspecialidad(3);
    hospitalLaMerced = await dataEspecialidad(1);
    hospitalCotahuma = await dataEspecialidad(5);

    setState(() {
      hospitalLosPinos = hospitalLosPinos;
      hospitalLaPortada = hospitalLaPortada;
      hospitalLaMerced = hospitalLaMerced;
      hospitalCotahuma = hospitalCotahuma;

      hospitalAux = hospitalLosPinos;
    });

    var listT2 = await libresOcupadosTabla(2, fechaDefecto);
    var listT3 = await libresOcupadosTabla(3, fechaDefecto);
    var listT5 = await libresOcupadosTabla(5, fechaDefecto);
    var listT1 = await libresOcupadosTabla(1, fechaDefecto);
    setState(() {
      tablaHospitalesListLaMerced = listT1;
      tablaHospitalesListLosPinos = listT2;
      tablaHospitalesListLaPortada = listT3;
      tablaHospitalesListCotahuma = listT5;
    });

    setState(() {
      items = <NewItem>[

        new NewItem(
          false,
          'Los Pinos',
          new Padding(
              padding: new EdgeInsets.all(1.0),
              child: new Column(children: <Widget>[
                new TablaDinamica(
                  listHospitales: tablaHospitalesListLosPinos,
                  hospitalId: 2,
                ),
                //put the children here
              ])),
          new Icon(Icons.add),
        ),
        new NewItem(
          false,
          'La Portada',
          new Padding(
              padding: new EdgeInsets.all(1.0),
              child: new Column(children: <Widget>[
                new TablaDinamica(
                  listHospitales: tablaHospitalesListLaPortada,
                  hospitalId: 3,
                ),
                //put the children here
              ])),
          new Icon(Icons.add),
        ),
        new NewItem(
          false,
          'Cotahuma',
          new Padding(
              padding: new EdgeInsets.all(1.0),
              child: new Column(children: <Widget>[
                new TablaDinamica(
                  listHospitales: tablaHospitalesListCotahuma,
                  hospitalId: 5,
                ),
                //put the children here
              ])),
          new Icon(Icons.add),
        ),
        new NewItem(
          false,
          'La Merced',
          new Padding(
              padding: new EdgeInsets.all(1.0),
              child: new Column(children: <Widget>[
                new TablaDinamica(
                  listHospitales: tablaHospitalesListCotahuma,
                  hospitalId: 1,
                ),
                //put the children here
              ])),
          new Icon(Icons.add),
        ),
      ];
    });
  }

  Future<HospitalH> dataEspecialidad(int idhospital) async {
    HospitalH hospitalH;

    var query = "select * from internaciones.sp_listar_totales($idhospital)";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        hospitalH = HospitalH(entry['total'], entry['libres'],
            entry['ocupadas'], entry['reservadas'], entry['mantenimiento']);
        print("entry $entry");
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return hospitalH;
  }

  Future<void> libresOcupadosHospital() async {
    var hospitanId = 5;
    var fecha_ = '18-11-2019';

    var query =
        "select sum(log_cantidad)::integer as LIBRES,(select sum(log_cantidad) from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_id_hospital = ${hospitanId} and log_tipo = \$\$OCUPADO\$\$ and log_fecha = \$\$${fecha_}\$\$)::integer as OCUPADO ,(select sum(log_cantidad) from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_id_hospital = ${hospitanId} and log_tipo = \$\$DESINFECCION\$\$ and log_fecha = \$\$${fecha_}\$\$)::integer as DESINFECCION from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_id_hospital = ${hospitanId} and log_tipo = \$\$LIBRES\$\$ and log_fecha = \$\$${fecha_}\$\$";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        print("entry:::::  ${entry}");
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  Future<void> libresOcupadosHospitalTodos() async {
    var fecha_ = '18-11-2019';
    var query =
        "select sum(log_cantidad)::integer as LIBRES,(select sum(log_cantidad) from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_tipo = \$\$OCUPADO\$\$ and log_fecha = \$\$${fecha_}\$\$)::integer as OCUPADO ,(select sum(log_cantidad) from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_tipo = \$\$DESINFECCION\$\$ and log_fecha = \$\$${fecha_}\$\$)::integer as DESINFECCION from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_tipo = \$\$LIBRES\$\$ and log_fecha = \$\$${fecha_}\$\$";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        print("entry:::::  ${entry}");
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  Future<List<TablaHospitales>> libresOcupadosTabla(
      int hospitanId, String fecha_) async {
    // var fecha_ = '18-11-2019';
    //var hospitanId = 5;
    List<TablaHospitales> ListResp_ = [];
    var query =
        "select log_tipo,log_cantidad,log_id_especialidad,log_desc_especialidad from internaciones._inter_camas_log where log_estado = \$\$A\$\$ and log_id_hospital = ${hospitanId} and log_fecha = \$\$${fecha_}\$\$";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        ListResp_.add(new TablaHospitales(
            entry['log_tipo'],
            entry['log_cantidad'],
            entry['log_id_especialidad'],
            entry['log_desc_especialidad']));
        // print("===> entry:::::  ${entry}");
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return ListResp_;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 9),
    ));
  }

  List<charts.Series<LinearSales, int>> _createLibresData() {
    var lib_ = (hospitalAux != null)
        ? double.parse(
            ((hospitalAux.libres / hospitalAux.total) * 100).toStringAsFixed(2))
        : 0.0;
//var lib2_ = double.parse (((hospitalAux.ocupadas/hospitalAux.total)*100).toStringAsFixed(2));

    final data = [
      new LinearSales(1, lib_),
      new LinearSales(2, 100 - lib_),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: " ${(hospitalAux != null) ? hospitalAux.libres : '0.0'}",
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        //  colorFn: (__, _) => charts.MaterialPalette.cyan.shadeDefault,
        data: data,
      )
    ];
  }

  List<charts.Series<LinearSales, int>> _createOcupadosData() {
    var lib2_ = (hospitalAux != null)
        ? double.parse(((hospitalAux.ocupadas / hospitalAux.total) * 100)
            .toStringAsFixed(2))
        : 0.0;

    final data = [
      new LinearSales(1, lib2_),
      new LinearSales(2, 100 - lib2_),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: " ${(hospitalAux != null) ? hospitalAux.ocupadas : '0.0'}",
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        //  colorFn: (__, _) => charts.MaterialPalette.cyan.shadeDefault,
        data: data,
      )
    ];
  }

  List<charts.Series<LinearSales, int>> _createDesinfectadosData() {
    var lib2_ = (hospitalAux != null)
        ? double.parse(((hospitalAux.mantenimiento / hospitalAux.total) * 100)
            .toStringAsFixed(2))
        : 0.0;

    final data = [
      new LinearSales(1, lib2_),
      new LinearSales(2, 100 - lib2_),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: " ${(hospitalAux != null) ? hospitalAux.mantenimiento : '0.0'}",
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        //  colorFn: (__, _) => charts.MaterialPalette.cyan.shadeDefault,
        data: data,
      )
    ];
  }

  Future<void> selectItem(HospitalEspecialidades he, String tipo) async {
    pr.style(message: 'Espere por favor..');
    pr.show();
//selectItem(he, 'ceph');
    if (tipo == 'ceph') {
      List<Ceph> listCephs =
          await dataCamasExistentesHospital(he.trn_hspcat_id);
      //"select count(*),sal_id_especialidad,esp_desc_especialidad , $$rgba($$||(trunc(random() * 255)||$$,$$||trunc(random() * 255)||$$, 255,0.9)$$)::text as color  from internaciones._inter_sala INNER JOIN internaciones._inter_camas  ON cam_sal_id =  sal_id INNER JOIN _hsp_srv_especialidad  ON  esp_id  = sal_id_especialidad where sal_id_hospital = 3 and sal_estado = $$A$$ and cam_estado = $$A$$ group by sal_id_especialidad , esp_desc_especialidad"

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HospitalizacionDetalle(
                  titulo: he.especialidad,
                  listCephs_: listCephs,
                )),
      );

      pr.hide();
    }

    if (tipo == "coloed") {
      List<OcupadasLibres> listOcupadasLibres =
          await dataCamasOcupadasLibres(he.trn_hspcat_id);
      pr.style(message: 'Espere por favor..');
      pr.show();

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HospitalizacionDetalleOcupadasLibres(
                  titulo: he.especialidad,
                  listOcupadasLibres_: listOcupadasLibres,
                )),
      );
      pr.hide();
    }

/*
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HospitalizacionDetalle()),
    ); */
  }

  Future<List<Ceph>> dataCamasExistentesHospital(int idhospital) async {
    List<Ceph> listCephResp = [];

    var query =
        "select count(*),sal_id_especialidad,esp_desc_especialidad , \$\$rgba(\$\$||(trunc(random() * 255)||\$\$,\$\$||trunc(random() * 255)||\$\$, 255,0.9)\$\$)::text as color  from internaciones._inter_sala INNER JOIN internaciones._inter_camas  ON cam_sal_id =  sal_id INNER JOIN _hsp_srv_especialidad  ON  esp_id  = sal_id_especialidad where sal_id_hospital = $idhospital and sal_estado = \$\$A\$\$ and cam_estado = \$\$A\$\$ group by sal_id_especialidad , esp_desc_especialidad";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        print("dataCamasExistentesHospital $entry");
        listCephResp.add(new Ceph(entry['count'], entry['sal_id_especialidad'],
            entry['esp_desc_especialidad'], entry['color']));
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return listCephResp;
  }

  Future<List<OcupadasLibres>> dataCamasOcupadasLibres(int idhospital) async {
    List<OcupadasLibres> listOcupadasLibres = [];

    var query =
        "select count(cam_tipoestado) as data ,cam_tipoestado as labels from internaciones._inter_sala inner join internaciones._inter_camas on sal_id = cam_sal_id inner join _hsp_srv_especialidad on sal_id_especialidad = esp_id where sal_id_hospital = $idhospital and cam_id_hospital = $idhospital  and sal_estado = \$\$A\$\$ and cam_estado = \$\$A\$\$ group by cam_tipoestado";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        listOcupadasLibres
            .add(new OcupadasLibres(entry['data'], entry['labels']));

        print("dataCamasOcupadasLibres $entry");
        /*   listCephResp.add(new Ceph(entry['count'], entry['sal_id_especialidad'],
            entry['esp_desc_especialidad'], entry['color'])); */
      }
    } else {
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return listOcupadasLibres;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Hospitalización",
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
              Text(
                "Caamas Disponibles",
                style: TextStyle(color: Colors.white, fontSize: 12.0),
              )
            ],
          ),

        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                )
              },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {
              iniciarServicios();

             // getTotalCamas('18-11-2019');
             //   libresOcupadosHospital();
              //libresOcupadosHospitalTodos();
            },
          ),
        ],
      ),
      body: Center(
          child: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Center(
                            child: Text("Buscar fechas"),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.55,
                          child: SearchWidget<LeaderBoard>(
                            dataList: listSearch,
                            hideSearchBoxWhenItemSelected: false,
                            listContainerHeight:
                                MediaQuery.of(context).size.height / 4,
                            queryBuilder:
                                (String query, List<LeaderBoard> list) {
                              return list
                                  .where((LeaderBoard item) => item.fecha
                                      .toLowerCase()
                                      .contains(query.toLowerCase()))
                                  .toList();
                            },
                            popupListItemBuilder: (LeaderBoard item) {
                              return PopupListItemWidget(item);
                            },
                            selectedItemBuilder: (LeaderBoard selectedItem,
                                VoidCallback deleteSelectedItem) {
                              print(
                                  "**** select item : ${selectedItem.fecha}");
                              getTotalCamas(selectedItem.fecha);
                              return SelectedItemWidget(
                                  selectedItem, deleteSelectedItem);
                            },

                            // widget customization
                            noItemsFoundWidget: NoItemsFound(),
                            textFieldBuilder: (TextEditingController controller,
                                FocusNode focusNode) {
                              return MyTextField(
                                  controller, focusNode, fechaDefecto);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                new CardBarChartHospitales(
                  titulo: "Libres",
                  tipo: "L",
                  HospitalesTod_: hospitalesTodSelect,
                  listHospitalesTod: listHospitalesTod,
                ),
                new CardBarChartHospitales(
                  titulo: "Ocupados",
                  tipo: "O",
                  HospitalesTod_: hospitalesTodSelect,
                  listHospitalesTod: listHospitalesTod,
                ),
                new CardBarChartHospitales(
                  titulo: "Desinfección",
                  tipo: "D",
                  HospitalesTod_: hospitalesTodSelect,
                  listHospitalesTod: listHospitalesTod,
                )
              ],
            ),
            Row(
              children: <Widget>[
                CardContadorHospital(
                  nombre: "La Merced",
                  cantidad: (hospitalLaMerced == null)
                      ? '0'
                      : hospitalLaMerced.total.toString(),
                  icono: Icons.local_hospital,
                  color: Colors.purple,
                  onTap: () {
                    selectHospital(1);
                  },
                ),
                CardContadorHospital(
                  nombre: "Los Pinos",
                  cantidad: (hospitalLosPinos == null)
                      ? '0'
                      : hospitalLosPinos.total.toString(),
                  icono: Icons.local_hospital,
                  color: Colors.green,
                  onTap: () {
                    selectHospital(2);
                  },
                ),
                CardContadorHospital(
                  nombre: "La Portada",
                  cantidad: (hospitalLaPortada == null)
                      ? '0'
                      : hospitalLaPortada.total.toString(),
                  icono: Icons.local_hospital,
                  color: Colors.indigo,
                  onTap: () {
                    selectHospital(3);
                  },
                ),
                CardContadorHospital(
                  nombre: "Cotahuma",
                  cantidad: (hospitalCotahuma == null)
                      ? '0'
                      : hospitalCotahuma.total.toString(),
                  icono: Icons.local_hospital,
                  color: Colors.blue,
                  onTap: () {
                    selectHospital(5);
                  },
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: new ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  new Padding(
                    padding: new EdgeInsets.fromLTRB(5.0, 9.0, 5.0, 0.0),
                    child: new ExpansionPanelList(
                      expansionCallback: (int index, bool isExpanded) {
                        setState(() {
                          items[index].isExpanded = !items[index].isExpanded;
                        });
                      },
                      children: items.map((NewItem item) {
                        return new ExpansionPanel(
                          canTapOnHeader: true,
                          headerBuilder:
                              (BuildContext context, bool isExpanded) {
                            return new ListTile(
                                leading: item.iconpic,
                                title: new Text(
                                  item.header,
                                  textAlign: TextAlign.left,
                                  style: new TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ));
                          },
                          isExpanded: item.isExpanded,
                          body: item.body,
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),
            /*   Container(
              width: MediaQuery.of(context).size.width * 1.0,
              height: 50,
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.local_hospital),
                      Text(
                        hospitalAuxNombre.toUpperCase(),
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
            ),

          
            Row(
              children: <Widget>[
                CardContador(
                  nombre: "Los Pinos",
                  cantidad: (hospitalLosPinos == null)
                      ? '0'
                      : hospitalLosPinos.total.toString(),
                  icono: Icons.local_hospital,
                  onTap: () {
                    setState(() {
                      hospitalAux = hospitalLosPinos;
                      hospitalAuxNombre = "Los Pinos";
                    });
                  },
                ),
                CardContador(
                  nombre: "La Merced",
                  cantidad: (hospitalLaMerced == null)
                      ? '0'
                      : hospitalLaMerced.total.toString(),
                  icono: Icons.local_hospital,
                  onTap: () {
                    setState(() {
                      hospitalAux = hospitalLaMerced;
                      hospitalAuxNombre = "La Merced";
                    });
                  },
                )
              ],
            ),
            Row(
              children: <Widget>[
                CardContador(
                  nombre: "La Portada",
                  cantidad: (hospitalLaPortada == null)
                      ? '0'
                      : hospitalLaPortada.total.toString(),
                  icono: Icons.local_hospital,
                  onTap: () {
                    setState(() {
                      hospitalAux = hospitalLaPortada;
                      hospitalAuxNombre = "La Portada";
                    });
                  },
                ),
                CardContador(
                  nombre: "Cotahuma",
                  cantidad: (hospitalCotahuma == null)
                      ? '0'
                      : hospitalCotahuma.total.toString(),
                  icono: Icons.local_hospital,
                  onTap: () {
                    setState(() {
                      hospitalAux = hospitalCotahuma;
                      hospitalAuxNombre = "Cotahuma";
                    });
                  },
                )
              ],
            ),

*/
/*

            Container(
              width: MediaQuery.of(context).size.width * 1.0,
              height: 50,
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.local_hospital),
                      Text(
                        "CAMAS EXISTENTES POR HOSPITAL",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: new CardListComponent(
                especialidades: lisEspecialidades,
                onClickItem: (HospitalEspecialidades he) {
                  selectItem(he, 'ceph');
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 1.0,
              height: 62,
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.local_hospital),
                      Expanded(
                          child: Text(
                        "CAMAS OCUPADAS, LIBRES O EN DESINFECTACIÓN",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ))
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: new CardListComponent(
                especialidades: lisEspecialidades,
                onClickItem: (HospitalEspecialidades he) {
                  selectItem(he, 'coloed');
                },
              ),
            ),*/
          ],
        ),
      )),
    );
  }
}

class HospitalH {
  final int total;
  final int libres;
  final int ocupadas;
  final int reservadas;
  final int mantenimiento;

  HospitalH(this.total, this.libres, this.ocupadas, this.reservadas,
      this.mantenimiento);
}

class LinearSales {
  final int year;
  final double sales;

  LinearSales(this.year, this.sales);
}

class CardBarChartH extends StatelessWidget {
  final String titulo;
  final HospitalH hospitalAux;
  final double porcentaje;
  final List<charts.Series<LinearSales, int>> listData;

  CardBarChartH(
      {this.titulo, this.hospitalAux, this.porcentaje, this.listData});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Column(
                  children: <Widget>[
                    Text("${porcentaje.toString()}%"),
                    Text(
                      titulo,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width * 0.31,
                      child: (hospitalAux != null)
                          ? new charts.PieChart(
                              listData,
                              animate: true,
                              behaviors: [
                                new charts.SeriesLegend(
                                    position: charts.BehaviorPosition.bottom,
                                    horizontalFirst:
                                        bool.fromEnvironment("Pablo")),
                              ],
                              defaultRenderer:
                                  new charts.ArcRendererConfig(arcWidth: 10),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            ),
                    )
                  ],
                ),
              ),
            ),
          ),

          // Text(titulo),
        ],
      ),
    );
  }
}



////////////
typedef void TapCallback(HospitalEspecialidades i);

class CardListComponent extends StatelessWidget {
  final List<HospitalEspecialidades> especialidades;
  final TapCallback onClickItem;

  CardListComponent({this.especialidades, this.onClickItem});

  bool selectItem = false;

  FixedExtentScrollController fixedExtentScrollController =
      new FixedExtentScrollController();
  final _pageController = PageController(viewportFraction: 0.8);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Container(
      child: Card(
        elevation: 8.0,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: (especialidades.length != 0)
              ? ListView.separated(
                  //  controller: fixedExtentScrollController,
                  shrinkWrap: true,
                  physics: true
                      ? const NeverScrollableScrollPhysics()
                      : const AlwaysScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(0.0),
                  itemCount: especialidades.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: EdgeInsets.all(0.0),
                      child: Material(
                        borderRadius: BorderRadius.circular(12.0),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(12.0),
                          child: Padding(
                            padding: EdgeInsets.all(15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  '${especialidades[index].especialidad}',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Container(
                                  width: 20,
                                  height: 20,
                                  child: selectItem
                                      ? CircularProgressIndicator()
                                      : Icon(Icons.chevron_right),
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            onClickItem(especialidades[index]);
                          },
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ),
      ),
    );
  }
}









