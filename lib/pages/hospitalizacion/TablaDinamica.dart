import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/TablaHospitales.dart';

class TablaDinamica extends StatelessWidget {
  final List<TablaHospitales> listHospitales;
  final int hospitalId;
  final Color color;

  TablaDinamica({this.listHospitales, this.hospitalId, this.color});

bool exiteEspecialidad(int idEspecialidad, List<Map<String, String>> esp) {
  for (var item in esp) {
    //  print(">> ${item['id']} == ${idEspecialidad}");
    if (int.parse("${item['id']}") == idEspecialidad) return true;
  }
  return false;
}

List<TablaHospitales> buscarEspecialidades(
    int idEspecialidad, List<TablaHospitales> esp) {
  List<TablaHospitales> resp = [];

  for (TablaHospitales item in esp) {
    //  print(">> ${item['id']} == ${idEspecialidad}");
    if (item.log_id_especialidad == idEspecialidad) {
      resp.add(item);
    }
  }
  return resp;
}

Map<String, String> buscarTablaHospital(int id, List<Map<String, String>> esp) {
  for (var item in esp) {
    //  print(">> ${item['id']} == ${idEspecialidad}");
    if (int.parse(item['id']) == id) {
      // resp.add(item);
      return item;
    }
  }
  return null;
}


  @override
  Widget build(BuildContext context) {
    var heightHead = 18.0;
    var colorHead = color;
    var TextStyleHead = TextStyle(
      color: Colors.white,
      fontSize: 8.0,
    );

    var heightTablebody = 18.0;

    var colorTableBody = Colors.white;

    var TextStyleTableBody = TextStyle(
      color: Colors.black,
      fontSize: 9.0,
    );

    var TextStyleTableBodyEspecialidad = TextStyle(
      color: Colors.black,
      fontSize: 6.0,
    );

    List<Map<String, String>> listOfColumns = [];
    List<Map<String, String>> especialidades = [];
    List<Map<String, String>> especialidadesTotalSuma = [];

    for (TablaHospitales item in listHospitales) {
      // print(">>>>>> TablaHospitales :: ${item.toString()}");

      if (!exiteEspecialidad(item.log_id_especialidad, especialidades)) {
        especialidades.add({
          "id": "${item.log_id_especialidad}",
          "especialidad": "${item.log_desc_especialidad}",
          "total": "0",
          "libres": "0",
          "ocupados": "0",
          "desinfeccion": "0"
        });
      }
    }

    for (var item in especialidades) {
      List<TablaHospitales> especList =
          buscarEspecialidades(int.parse(item['id']), listHospitales);

      for (TablaHospitales especialidads_ in especList) {
        // print("especialidads_ : ${especialidads_.toString()}");
        var tablaH = buscarTablaHospital(
            especialidads_.log_id_especialidad, especialidades);

        switch (especialidads_.log_tipo) {
          case "LIBRES":
            tablaH['libres'] =
                "${(especialidads_.log_cantidad != null) ? especialidads_.log_cantidad : 0}";
            break;

          case "OCUPADO":
            tablaH['ocupados'] =
                "${(especialidads_.log_cantidad != null) ? especialidads_.log_cantidad : 0}";
            break;

          default:
            tablaH['desinfeccion'] =
                "${(especialidads_.log_cantidad != null) ? especialidads_.log_cantidad : 0}";
        }

        //  print(">> tablaH :: ${tablaH}");
      }
      //print("-------");

      //  print(">>> especialidades : ${item}");
    }
//suma del total
    for (var item in especialidades) {
      var total_ = int.parse(item['libres']) +
          int.parse(item['ocupados']) +
          int.parse(item['desinfeccion']);

      especialidadesTotalSuma.add({
        "id": "${item['id']}",
        "especialidad": "${item['especialidad']}",
        "total": "${total_}",
        "libres": "${item['libres']}",
        "ocupados": "${item['ocupados']}",
        "desinfeccion": "${item['desinfeccion']}",
      });

      //  item['total'] = "${total}";
    }

    /// llebar la tabla
    for (var item in especialidadesTotalSuma) {
      listOfColumns.add({
        "especialdiad": item['especialidad'],
        "total": item['total'],
        "libres": item['libres'],
        "ocupados": item['ocupados'],
        "desinfeccion": item['desinfeccion'],
      });
    }

/*
    
    listOfColumns.add({
      "especialdiad": "1 AAAAAA",
      "total": "1122",
      "libres": "34",
      "ocupados": "34",
      "desinfeccion": "21"
    });
    listOfColumns.add({
      "especialdiad": "2 BBBBBB",
      "total": "456",
      "libres": "90",
      "ocupados": "456",
      "desinfeccion": "087"
    });
    listOfColumns.add({
      "especialdiad": "3 CCCCCC",
      "total": "309",
      "libres": "29",
      "ocupados": "223",
      "desinfeccion": "634"
    }); */

    List<TableRow> rowsTable = [];

    /**
     * HEADER
     * */

    rowsTable.add(TableRow(children: [
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Especialidad",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Total",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Libres",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Ocupados",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Container(
          margin: EdgeInsets.all(0),
          color: colorHead,
          width: 48.0,
          height: heightHead,
          child: Center(
            child: Text(
              "Desinfección",
              textAlign: TextAlign.center,
              style: TextStyleHead,
            ),
          ),
        ),
      ),
    ]));

    /**
     * FIN HEADER
     * */

    listOfColumns.forEach((item) {
     // print("* item:  ${item}");

      rowsTable.add(TableRow(children: [
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item['especialdiad'],
                textAlign: TextAlign.center,
                style: TextStyleTableBodyEspecialidad,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item['total'],
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item['libres'],
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item['ocupados'],
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Container(
            margin: EdgeInsets.all(0),
            color: colorTableBody,
            width: 48.0,
            height: heightTablebody,
            child: Center(
              child: Text(
                item['desinfeccion'],
                textAlign: TextAlign.center,
                style: TextStyleTableBody,
              ),
            ),
          ),
        ),
      ]));
    });

    // TODO: implement build
    return Container(
      child: SizedBox(
        child: Table(
          border: TableBorder.all(),
          children: rowsTable,
        ),
      ),
    );
  }
}