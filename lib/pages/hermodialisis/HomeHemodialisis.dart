
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
//import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/components/CardCharts/CardBarChart.dart';
import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:screenshot/screenshot.dart';


class HomeHermodialisis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Especialidad(),
    );
  }
}

class Especialidad extends StatefulWidget {
  @override
  _EspecialidadState createState() => _EspecialidadState();
}

class _EspecialidadState extends State<Especialidad> {
  bool inside = false;
  Uint8List imageInMemory;

  ScreenshotController screenshotController = ScreenshotController();

  ScreenshotController pacientesHospitalController = ScreenshotController();
  ScreenshotController serologiaPositivoController = ScreenshotController();
  ScreenshotController serologiaNegativoController = ScreenshotController();
  ScreenshotController vascularCateterController = ScreenshotController();
  ScreenshotController vascularFistulaController = ScreenshotController();

  bool buttonShare_ = true;

  //Cantidad de pacientes por hospital
  bool pacientesHospitalGraf = false;
  List<charts.Series<ClicksPerYear, String>> pacientesHospitalList = [];

  //Reporte por serologia
  bool serologiaTipoGrafPositivo = false;
  bool serologiaTipoGrafNegativo = false;
  List<charts.Series<ClicksPerYear, String>> serologiaTipoPositivo = [];
  List<charts.Series<ClicksPerYear, String>> serologiaTipoNegativo = [];

  //Reporte por acceso vascular
  bool accesoVascularGrafCateter = false;
  bool accesoVascularGrafFistula = false;
  List<charts.Series<ClicksPerYear, String>> accesoVascularCateter = [];
  List<charts.Series<ClicksPerYear, String>> accesoVascularFistula = [];

  List<Color> coloresList = [
    Colors.blue,
    Colors.amber,
    Colors.brown,
    Colors.cyan,
    Colors.deepOrange,
    Colors.deepPurple,
    Colors.green,
    Colors.cyanAccent,
    Colors.blueAccent,
    Colors.teal,
    Colors.amberAccent,
  ];

  /*
  * Cantidad de pacientes por hospital
  * */
  var url_hemodialisis = "http://52.168.143.241:4415/wsSalud";// GlobalConfiguration().getString("SALUD_HEMODIALISIS");
  var url_hemodialisis_interno ="http://sersalud.lapaz.bo/wsSalud_hemo";// GlobalConfiguration().getString("SALUD_HEMODIALISIS_INTERNO");

  Future<List<PacientesHospital>> getHermodialisisExterno() async {
    List<PacientesHospital> hospitalesList_ = [];
    var response = await http.post("$url_hemodialisis/verifica", body: {
      'consulta':
          'select replace((cen_datos->0->\$\$cen_nombre\$\$)::text, \$\$"\$\$, \$\$\$\$) as hospital ,pac_cen_id,count(*) from hemo_pacientes inner join hemo_centros on pac_cen_id = cen_id where pac_estado = \$\$A\$\$ and cen_estado = \$\$A\$\$ group by pac_cen_id,cen_datos->0->\$\$cen_nombre\$\$'
    });
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      for (var entry in resp_) {
        hospitalesList_.add(new PacientesHospital(
            entry['hospital'], entry['pac_cen_id'], entry['count']));
      }
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }
    return hospitalesList_;
  }

  Future<List<PacientesHospital>> getHermodialisisInterno() async {
    List<PacientesHospital> hospitalesList_ = [];
    var response =
        await http.post("$url_hemodialisis_interno/dinamico", body: {
      'consulta':
          'select replace((cen_datos->0->\$\$cen_nombre\$\$)::text, \$\$"\$\$, \$\$\$\$) as hospital ,pac_cen_id,count(*) from hemo_pacientes inner join hemo_centros on pac_cen_id = cen_id where pac_estado = \$\$A\$\$ and cen_estado = \$\$A\$\$ group by pac_cen_id,cen_datos->0->\$\$cen_nombre\$\$'
    });
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];
      for (var entry in resp_) {
        hospitalesList_.add(new PacientesHospital(
            entry['hospital'], entry['pac_cen_id'], entry['count']));
      }
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }
    return hospitalesList_;
  }

  Future<List<PacientesHospital>> hermodialisisInternoExterno(
      List<PacientesHospital> listInterno,
      List<PacientesHospital> listExterno) async {
    List<PacientesHospital> listRes_ = [];

    var dh_ = 0;
    await listInterno.forEach((PacientesHospital dh1) => {
          listExterno.forEach((PacientesHospital dh2) => {
                if (dh1.pac_cen_id == dh2.pac_cen_id)
                  {
                    dh_ = dh1.count + dh2.count,
                    listRes_.add(new PacientesHospital(
                        dh1.hospital, dh1.pac_cen_id, dh_)),
                  }
                else
                  {
                    if (!existItem(listRes_, dh1)) {listRes_.add(dh1)},
                    if (!existItem(listRes_, dh2)) {listRes_.add(dh2)}
                  },
              }),
        });

    return listRes_;
  }

  /*
  * REPORTE POR SEROLOGIA
   */
  Future<List<Serologia>> getSerologiaExterno(String pacserol) async {
    List<Serologia> hospitalesListSerologia_ = [];

    var response = await http.post("$url_hemodialisis/verifica",
        body: {'consulta': 'select * from sprep_pacientes_serologia ()'});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      for (var entry in resp_) {
        //  print("- getSerologia externo :: $entry");

        if (entry['pacserol'] == pacserol) {
          hospitalesListSerologia_.add(new Serologia(entry['cenid'],
              entry['cennom'], entry['pacserol'], entry['contpar']));
        }
      }

      //hospitalesListAUXSerologia = await getSerologiaInterno();
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }

    return hospitalesListSerologia_;
  }

  Future<List<Serologia>> getSerologiaInterno(String pacserol) async {
    List<Serologia> hospitalesListAUXSerologia_ = [];
    var response = await http.post("$url_hemodialisis_interno/dinamico",
        body: {'consulta': 'select * from sprep_pacientes_serologia ()'});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      for (var entry in resp_) {
        //  print("- getSerologia Interno :: $entry");

        if (entry['pacserol'] == pacserol) {
          await hospitalesListAUXSerologia_.add(new Serologia(entry['cenid'],
              entry['cennom'], entry['pacserol'], entry['contpar']));
        }
      }
      /*
      * Suma duplicados SEROLOGIA
      * */
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }

    return hospitalesListAUXSerologia_;
  }

  Future<List<Serologia>> SerologiaComparaInternoExterno(
      List<Serologia> listInterno,
      List<Serologia> listExterno,
      String pacserol) async {
    List<Serologia> listRes_ = [];

    var dh_ = 0;
    await listInterno.forEach((Serologia dh1) => {
          listExterno.forEach((Serologia dh2) => {
                if (dh1.cenid == dh2.cenid &&
                    dh1.pacserol == pacserol &&
                    dh2.pacserol == pacserol)
                  {
                    dh_ = dh1.contpar + dh2.contpar,
                    listRes_.add(new Serologia(
                        dh2.cenid, dh2.cennom, dh2.pacserol, dh_)),
                  }
                else
                  {
                    if (!existItemSerologia(listRes_, dh1, pacserol))
                      {listRes_.add(dh1)},
                    if (!existItemSerologia(listRes_, dh2, pacserol))
                      {listRes_.add(dh2)},
                  },
              }),
        });

    return listRes_;
  }

  bool existItem(
      List<PacientesHospital> hospitalesListAUX, PacientesHospital dh2) {
    var resp = false;
    hospitalesListAUX.forEach((PacientesHospital dataHospital) => {
          if (dataHospital.pac_cen_id == dh2.pac_cen_id) {resp = true}
        });
    return resp;
  }

  bool existItemSerologia(List<Serologia> hospitalesListAUXSerologia_,
      Serologia dhse, String pacser) {
    var resp_ = false;

    for (Serologia serologia in hospitalesListAUXSerologia_) {
      if (serologia.cenid == dhse.cenid &&
          serologia.cennom.trim() == dhse.cennom.trim() &&
          serologia.pacserol.trim() == pacser.trim()) {
        return true;
      }
    }

    return false;
  }

  List<charts.Series<ClicksPerYear, String>> graficaPacintesPorHospital(
      List<PacientesHospital> hospitalesLis) {
    List<charts.Series<ClicksPerYear, String>> series__ = [];
    setState(() {
      var index = 0;
      hospitalesLis.forEach((PacientesHospital dataHospital) => {
            series__.add(new charts.Series(
              domainFn: (ClicksPerYear clickData, _) => clickData.year,
              measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
              colorFn: (ClicksPerYear clickData, _) => clickData.color,
              id: "${dataHospital.hospital} (${dataHospital.count}) ",
              data: [
                new ClicksPerYear("(${dataHospital.count})", dataHospital.count,
                    coloresList.elementAt(index)),
              ],
            )),
            index++
          });
      // graf = true;
    });

    return series__;
  }

  List<charts.Series<ClicksPerYear, String>> GraficaSerologia(
      List<Serologia> hospitalesLis) {
    List<charts.Series<ClicksPerYear, String>> series__ = [];

    setState(() {
      var index = 0;
      hospitalesLis.forEach((Serologia serologia) => {
            series__.add(new charts.Series(
              domainFn: (ClicksPerYear clickData, _) => clickData.year,
              measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
              colorFn: (ClicksPerYear clickData, _) => clickData.color,
              id: "${serologia.cennom} (${serologia.contpar}) ",
              data: [
                new ClicksPerYear("(${serologia.contpar})", serologia.contpar,
                    coloresList.elementAt(index)),
              ],
            )),
            index++
          });

      //  graf = true;
    });

    return series__;
  }

  List<charts.Series<ClicksPerYear, String>> GraficaVascula(
      List<Vascular> hospitalesLis) {
    List<charts.Series<ClicksPerYear, String>> series__ = [];

    setState(() {
      var index = 0;
      hospitalesLis.forEach((Vascular serologia) => {
            series__.add(new charts.Series(
              domainFn: (ClicksPerYear clickData, _) => clickData.year,
              measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
              colorFn: (ClicksPerYear clickData, _) => clickData.color,
              id: "${serologia.cennom.replaceAll('HOSPITAL', 'H.')} (${serologia.contaccvas}) ",
              data: [
                new ClicksPerYear("(${serologia.contaccvas})",
                    serologia.contaccvas, coloresList.elementAt(index)),
              ],
            )),
            index++
          });

      //  graf = true;
    });

    return series__;
  }

  /**
   * REPORTE POR ACCESO VASCULAR
   */
  Future<List<Vascular>> getAccesoVascularExterno(String hojaaccvas) async {
    List<Vascular> hospitalesListVascular = [];

    var response = await http.post("$url_hemodialisis/verifica",
        body: {'consulta': 'select * from sprep_pacientes_accvas ()'});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      for (var entry in resp_) {
        if (entry['hojaaccvas'] == hojaaccvas) {
          //  print("- getAccesoVascularExterno :: $entry");
          hospitalesListVascular.add(new Vascular(entry['cenid'],
              entry['cennom'], entry['hojaaccvas'], entry['contaccvas']));
        }
      }

      //hospitalesListAUXSerologia = await getSerologiaInterno();
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }

    return hospitalesListVascular;
  }

  Future<List<Vascular>> getAccesoVascularInterno(String hojaaccvas) async {
    List<Vascular> hospitalesListVascular = [];
    var response = await http.post("$url_hemodialisis_interno/dinamico",
        body: {'consulta': 'select * from sprep_pacientes_accvas ()'});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var resp_ = jsonResponse['success']['data'][0]['sp_dinamico'];

      for (var entry in resp_) {
        if (entry['hojaaccvas'] == hojaaccvas) {
          //  print("- getAccesoVascularInterno :: $entry");
          hospitalesListVascular.add(new Vascular(entry['cenid'],
              entry['cennom'], entry['hojaaccvas'], entry['contaccvas']));
        }
/*
        if (entry['pacserol'] == pacserol) {
          await hospitalesListAUXSerologia_.add(new Serologia(entry['cenid'],
              entry['cennom'], entry['pacserol'], entry['contpar']));
        } */
      }
      /*
      * Suma duplicados SEROLOGIA
      * */
    } else {
      /*
      * Mostrar un mensaje de error de red
      * */
    }

    return hospitalesListVascular;
  }

  Future<List<charts.Series<ClicksPerYear, String>>> getListVascular(
      String tipo) async {
    List<Vascular> listExternoInterno = [];

    List<Vascular> listExterno = await getAccesoVascularExterno(tipo);
    List<Vascular> listInterno = await getAccesoVascularInterno(tipo);

    listExternoInterno.addAll(listExterno);
    listExternoInterno.addAll(listInterno);

    // print("listExternoInterno : ${listExternoInterno.length}");

    // print("listExternoInterno sum : ${sumaVascularInternoExterno(listExternoInterno).length}");

    List<Vascular> resuldadoSumaInternoExterno =
        sumaVascularInternoExterno(listExternoInterno);

    resuldadoSumaInternoExterno.forEach((Vascular vas) {
      // print("resuldadoSumaInternoExterno :: ${vas.toString()}");
    });

    return GraficaVascula(resuldadoSumaInternoExterno);
  }

  List<Vascular> sumaVascularInternoExterno(List<Vascular> list) {
    List<Vascular> resp = [];

    List<Vascular> respAux = [];

    List<Vascular> r1 = [];
    List<Vascular> r2 = [];

    list.forEach((Vascular v1) {
      Vascular vascularAux_ = existI(r2, v1);
      if (vascularAux_ == null) {
        r2.add(v1); //lista todos sin repetir
      } else {
        //  Vascular vItem= vascularAux_.  getPosicion(list, v1);
        //  print("vascularAux_ :: ${vascularAux_.toString()}");

        int poss = getPosicion(r2, vascularAux_);
        //  print("r2[poss] :: ${v1.toString()}");

        int suma_ = vascularAux_.contaccvas + v1.contaccvas;
        r2[poss] = Vascular(vascularAux_.cenid, vascularAux_.cennom,
            vascularAux_.hojaaccvas, suma_);

        r1.add(v1); // repetidos
      }
    });
    // print("r1 : ${r1.length}");
    // print("r2 : ${r2.length}");

    r1.forEach((Vascular v1) {
      // print(" list r1 : ${v1.toString()}");
    });

    r2.forEach((Vascular v2) {
      //  print(" list r2 : ${v2.toString()}");
    });
    return r2;
  }

  Vascular existI(List<Vascular> list, Vascular v) {
    Vascular vas = null;
    bool resp = false;
    for (Vascular v1 in list) {
      if (v1.cenid == v.cenid) {
        //  resp = true;
        vas = v1;
        break;
      }
    }
    return vas;
  }

  int getPosicion(List<Vascular> list, Vascular v) {
    int vas = null;
    int indice = 0;
    bool resp = false;
    for (Vascular v1 in list) {
      if (v1.cenid == v.cenid) {
        //  resp = true;
        vas = indice;
        // print("vas ${vas}");
        break;
      }
      indice++;
    }
    return vas;
  }

  @override
  void initState() {
    iniciarDatos();
  }

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<List<charts.Series<ClicksPerYear, String>>> getListSerologia(
      String pacserol) async {
    List<Serologia> listSesologia_ = [];
    List<charts.Series<ClicksPerYear, String>> series_ = [];

    List<Serologia> interno = [];
    List<Serologia> externo = [];

    externo = await getSerologiaExterno(pacserol);
    interno = await getSerologiaInterno(pacserol);

    listSesologia_ =
        await SerologiaComparaInternoExterno(interno, externo, pacserol);

    // series_ = GraficaSerologia(listSesologia_);

    return GraficaSerologia(listSesologia_);
  }

  Future<List<charts.Series<ClicksPerYear, String>>>
      getListHermodialisis() async {
    List<PacientesHospital> listSesologia_ = [];
    List<charts.Series<ClicksPerYear, String>> series_ = [];

    List<PacientesHospital> interno = [];
    List<PacientesHospital> externo = [];

    externo = await getHermodialisisExterno();
    interno = await getHermodialisisInterno();

    listSesologia_ = await hermodialisisInternoExterno(externo, interno);

    return graficaPacintesPorHospital(listSesologia_);
  }

  void iniciarDatos() {
    setState(() {
      pacientesHospitalGraf = false;
      serologiaTipoGrafPositivo = false;
      serologiaTipoGrafNegativo = false;

      accesoVascularGrafCateter = false;
      accesoVascularGrafFistula = false;

      buttonShare_ = true;
    });

    coloresList.shuffle();

    pacientesHospitalList.clear();
    getListHermodialisis().then(
        (List<charts.Series<ClicksPerYear, String>> hospitalesListSerologia_) =>
            {
              pacientesHospitalList = hospitalesListSerologia_,
              pacientesHospitalGraf = true
            });

    coloresList.shuffle();
    serologiaTipoPositivo.clear();
    getListSerologia("Positivo").then(
        (List<charts.Series<ClicksPerYear, String>> hospitalesListSerologia_) =>
            {
              serologiaTipoPositivo = hospitalesListSerologia_,
              serologiaTipoGrafPositivo = true
            });

    coloresList.shuffle();

    serologiaTipoNegativo.clear();
    getListSerologia("Negativo").then(
        (List<charts.Series<ClicksPerYear, String>> hospitalesListSerologia_) =>
            {
              serologiaTipoNegativo = hospitalesListSerologia_,
              serologiaTipoGrafNegativo = true
            });

    // getAccesoVascularInternoExterno();

    coloresList.shuffle();

    getListVascular("Cateter").then(
        (List<charts.Series<ClicksPerYear, String>> accesoVascularCateter_) {
      accesoVascularCateter = accesoVascularCateter_;
      accesoVascularGrafCateter = true;
    });

    coloresList.shuffle();
    getListVascular("Fistula").then(
        (List<charts.Series<ClicksPerYear, String>> accesoVascularFistula_) {
      accesoVascularFistula = accesoVascularFistula_;
      accesoVascularGrafFistula = true;
    });

/*
    bool accesoVascularGrafCateter = false;
    bool accesoVascularGrafFistula = false;
    List<charts.Series<ClicksPerYear, String>> accesoVascularCateter = [];
    List<charts.Series<ClicksPerYear, String>> accesoVascularFistula = [];
    */
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Hemodialisis",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            )
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {
              iniciarDatos();
            },
          ),
        ],
      ),
      body: Center(
          child: Container(
        child: ListView(
          children: <Widget>[
            Screenshot(
              controller: pacientesHospitalController,
              child: Container(
                width: double.infinity,
                height: 620,
                child: new CardBarChart(
                  nombre: "CANTIDAD DE PACIENTES POR HOSPITAL",
                  descripcion: "",
                  graf: pacientesHospitalGraf,
                  series: pacientesHospitalList,
                  vertical: true,
                  scaffoldKey: scaffoldKey,
                  screenshotController: pacientesHospitalController,
                ),
              ),
            ),
            //Cantidad de pacientes por hospital

            Container(
              padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
              height: MediaQuery.of(context).size.height * 0.95,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Screenshot(
                    controller: serologiaPositivoController,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.93,
                      height: 620,
                      child: CardBarChart(
                        nombre: "REPORTE POR SEROLOGIA",
                        descripcion:
                            "Cantidad de pacientes por su tipo de sangre positivo en los distintos hospitales.",
                        graf: serologiaTipoGrafPositivo,
                        series: serologiaTipoPositivo,
                        vertical: false,
                        scaffoldKey: scaffoldKey,
                        screenshotController: serologiaPositivoController,
                      ),
                    ),
                  ),
                  Screenshot(
                    controller: serologiaNegativoController,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 620,
                      child: CardBarChart(
                        nombre: "REPORTE POR SEROLOGIA",
                        descripcion:
                            "Cantidad de pacientes por su tipo de sangre negativo en los distintos hospitales.",
                        graf: serologiaTipoGrafNegativo,
                        series: serologiaTipoNegativo,
                        vertical: false,
                        scaffoldKey: scaffoldKey,
                        screenshotController: serologiaNegativoController,
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
              height: MediaQuery.of(context).size.height * 0.95,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Screenshot(
                    controller: vascularCateterController,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.93,
                      height: 550,
                      child: CardBarChart(
                        nombre: "REPORTE POR ACCESO VASCULAR",
                        descripcion: "Cantidad de atenciones por cateter",
                        graf: accesoVascularGrafCateter,
                        series: accesoVascularCateter,
                        vertical: false,
                        scaffoldKey: scaffoldKey,
                        screenshotController: vascularCateterController,
                      ),
                    ),
                  ),
                  Screenshot(
                    controller: vascularFistulaController,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 1.0,
                      height: 550,
                      child: CardBarChart(
                        nombre: "REPORTE POR ACCESO VASCULAR",
                        descripcion: "Cantidad de atenciones por fistula",
                        graf: accesoVascularGrafFistula,
                        series: accesoVascularFistula,
                        vertical: false,
                        scaffoldKey: scaffoldKey,
                        screenshotController: vascularFistulaController,
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //Cerologia
          ],
        ),
      )),
    );
  }
}

////////Final
class SelectionBarHighlight extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SelectionBarHighlight(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(seriesList,
        animate: animate, behaviors: [new charts.SeriesLegend()]);
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class PacientesHospital {
  final String hospital;
  final int pac_cen_id;
  final int count;

  PacientesHospital(this.hospital, this.pac_cen_id, this.count);
}

class Serologia {
  final int cenid;
  final String cennom;
  final String pacserol;
  final int contpar;
  Serologia(this.cenid, this.cennom, this.pacserol, this.contpar);
}

class Vascular {
  final int cenid;
  final String cennom;
  final String hojaaccvas;
  final int contaccvas;
  Vascular(this.cenid, this.cennom, this.hojaaccvas, this.contaccvas);

  @override
  String toString() {
    return 'Vascular{cenid: $cenid, cennom: $cennom, hojaaccvas: $hojaaccvas, contaccvas: $contaccvas}';
  }
}
