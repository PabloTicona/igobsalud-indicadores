import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/CardListComponent.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/pages/consultaExterna/ReporteEspecialidadDetalle.dart';
import 'package:igob_salud_app/pages/consultaExterna/HomeConsultaExterna.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:igob_salud_app/helps/DateFormatConverter.dart';

class ReporteEspecialidadOld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reporte Por Especialidad',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Especialidad(),
    );
  }
}

class Especialidad extends StatefulWidget {
  @override
  _EspecialidadState createState() => _EspecialidadState();
}

class _EspecialidadState extends State<Especialidad>
    with SingleTickerProviderStateMixin {
  //var send = [{'latitude': widget.lat}, {'latitude': 123.456}];
  ProgressDialog pr;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TabController controller;
  DateTime _fechaDesde = CambiaFecha(DateTime.now());
  DateTime _fechaHasta = DateTime.now();

  final List<String> entries = <String>['A', 'B', 'C'];

  List<HospitalEspecialidades> listhospitalEspecialidades = [];
  List<IndicadorEspecialidad> listIndicadorEspecialidades_ = [];

  var url_ = GlobalConfiguration().getString("SALUD_REPORTES");

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 4);
    especialidadesHospitales(_fechaDesde, _fechaHasta);
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal);
  }

  Future<dynamic> getResponseDinamico(String query) async {
    var resp = null;
    var response =
    await http.post(url_ + '/dinamico', body: {'consulta': query});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      resp = jsonResponse['success']['data'][0]['sp_dinamico'];
    } else {
      showInSnackBar("Error de conexión de red");
      throw Exception("Error de conexión de red");
      // resp = null;
    }

    return resp;
  }

  List<HospitalEspecialidades> filtrarEspecialidad(
      List<HospitalEspecialidades> list, int idEspecialidad) {
    List<HospitalEspecialidades> resp = [];

    list.forEach((HospitalEspecialidades hospitalEspecialidades) {
      if (hospitalEspecialidades.trn_hsp_id == idEspecialidad) {
        resp.add(hospitalEspecialidades);
      }
    });

    return resp;
  }

  Future<dynamic> especialidadesHospitales(
      DateTime fechaDesde, DateTime fechaHasta) async {
    var fechaInicio = fechaDesde.toString().split(' ')[0];
    var fechaFin = fechaHasta.toString().split(' ')[0];

    setState(() {
      listhospitalEspecialidades = [];
    });

    var query =
        "SELECT h.hsp_nombre_hospital,tmp1.especialidad,tmp1.trn_hspcat_id,tmp1.trn_hsp_id from _hsp_hospitales h, (SELECT btrim(split_part(esp_desc_especialidad,\$\$-\$\$,2),\$\$ \$\$) as especialidad,trn_hspcat_id,trn_hsp_id FROM _hsp_srv_especialidad ,(select trn_hsp_id ,trn_hspcat_id from _HSP_TURNOS inner join _hsp_srv_especialidad on trn_hspcat_id = esp_id and trn_estado = \$\$A\$\$ and esp_estado = \$\$A\$\$ WHERE TRN_FECHA::DATE between \$\$ $fechaInicio\$\$ and \$\$ $fechaFin\$\$ group by trn_hsp_id,trn_hspcat_id order by trn_hsp_id )as tmp1 where tmp1.trn_hspcat_id = esp_id order by trn_hsp_id,especialidad )as tmp1 where tmp1.trn_hsp_id = h.hsp_id order by h.hsp_id ,tmp1.especialidad";
    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        setState(() {
          listhospitalEspecialidades.add(new HospitalEspecialidades(
              entry['hsp_nombre_hospital'],
              entry['especialidad'],
              entry['trn_hspcat_id'],
              entry['trn_hsp_id']));
        });

        //  print("entry $entry");
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  Future<void> indicadoresEspecialidad(
      int hospital_id,
      int hspcat_id,
      DateTime fechaDesde,
      DateTime fechaHasta,
      HospitalEspecialidades h) async {
    var fechaInicio = fechaDesde.toString().split(' ')[0];
    var fechaFin = fechaHasta.toString().split(' ')[0];
    List<IndicadorEspecialidad> res_ = [];


    var aux_ = listhospitalEspecialidades;
    pr.style(message:'Espere por favor...');
    pr.show();

    setState(() {
      listIndicadorEspecialidades_ = [];
      // listhospitalEspecialidades = [];
    });

    var query =
        "select * from sp_lst_indicadores_estructura_dinamico($hospital_id,$hspcat_id, \$\$$fechaInicio\$\$,\$\$$fechaFin\$\$)";



    var resp_ = await getResponseDinamico(query);
    if (resp_ != null) {
      for (var entry in resp_) {
        print("indicadores entry $entry");

        setState(() {
          listIndicadorEspecialidades_.add(new IndicadorEspecialidad(
              entry['medico'],
              converteList(entry['fechas']),
              converteList(entry['fichas_planificadas']),
              converteList(entry['fechas_atendidads']),
              int.parse(entry['vcontenedor']),
              converteList(entry['vfichas_solicitadas'])));
        });
      }

      print("TOTAL : ${listIndicadorEspecialidades_.length}");

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ReporteEspecialidadDetalle(
              hospitalEspecialidades: h,
            //  listIndicadorEspecialidades: listIndicadorEspecialidades_, // 29-11-2019
            )),
      );
      pr.hide();
      /* setState(() {
        listhospitalEspecialidades = aux_;
      }); */

    } else {
      showInSnackBar("No Existen datos. Seleccione otra especialidad");
    }

    //return listIndicadorEspecialidades_;
  }

  List<dynamic> converteList(var arrayItem) {
    List<dynamic> res_ = [];
    arrayItem.forEach((var item) {
      // print("item dynamic :: $item");
      res_.add(item);
    });
    return res_;
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Reporte Por Especialidad",
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {
              especialidadesHospitales(_fechaDesde, _fechaHasta);
            },
          ),
        ],
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeConsultaExterna()),
            )

          },
        ),
      ),
      body: Center(
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.all(0.0),
                    child: Padding(
                      padding: EdgeInsets.all(0.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new CardDatePickerComponent(
                              nombre: 'Desde: ',
                              fechaInicio: _fechaDesde,
                              formatoFecha: '',
                              formatoFechaComponete: 'dd-MMMM-yyyy',
                              onTap: (dateTime, List<int> index) {
                                setState(() {
                                  _fechaDesde = dateTime;
                                });
                                //  print(' dateTime ${dateTime}');
                              }),
                          new CardDatePickerComponent(
                              nombre: 'Mes fin:',
                              fechaInicio: _fechaHasta,
                              formatoFecha: '',
                              formatoFechaComponete: 'dd-MMMM-yyyy',
                              onTap: (dateTime, List<int> index) {
                                setState(() {
                                  _fechaHasta = dateTime;
                                });
                                //     print(' dateTime ${dateTime}');
                              }),
                          new ButtonMaterialCircleComponent(
                              size: 20.0,
                              onPressed: () {
                                especialidadesHospitales(_fechaDesde, _fechaHasta);
                              }),
                        ],
                      ),
                    ),
                  ),
                ),

                //TABS

                Expanded(
                  child: new TabBarView(
                    controller: controller,
                    children: <Widget>[
                      new CardListComponent(
                        especialidades: filtrarEspecialidad(listhospitalEspecialidades, 1),
                     /*   onClickItem: (HospitalEspecialidades h) {
                          indicadoresEspecialidad(h.trn_hsp_id, h.trn_hspcat_id,
                              _fechaDesde, _fechaHasta, h);

                          print(" hospital 1 ${h.toString()}");
                        }, */ // 29-11-2019
                      ),
                      new CardListComponent(
                        especialidades: filtrarEspecialidad(listhospitalEspecialidades, 2),
                        onClickItem: (HospitalEspecialidades h) {
                          indicadoresEspecialidad(h.trn_hsp_id, h.trn_hspcat_id,
                              _fechaDesde, _fechaHasta, h);

                          print(" hospital 2 ${h.toString()}");
                        },  // 29-11-2019
                      ),
                      new CardListComponent(
                        especialidades: filtrarEspecialidad(listhospitalEspecialidades, 3),
                        onClickItem: (HospitalEspecialidades h) {
                          indicadoresEspecialidad(h.trn_hsp_id, h.trn_hspcat_id,
                              _fechaDesde, _fechaHasta, h);

                          print(" hospital 3 ${h.toString()}");
                        },  // 29-11-2019
                      ),
                      new CardListComponent(
                        especialidades: filtrarEspecialidad(listhospitalEspecialidades, 5),
                        onClickItem: (HospitalEspecialidades h) {
                          indicadoresEspecialidad(h.trn_hsp_id, h.trn_hspcat_id,
                              _fechaDesde, _fechaHasta, h);

                          print(" hospital 5 ${h.toString()}");
                        },   // 29-11-2019
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
      bottomNavigationBar: new Material(
          color: Colors.blue,
          child: new TabBar(
              labelPadding: EdgeInsets.all(0.0),
              indicatorColor: Colors.red,
              controller: controller,
              tabs: <Tab>[
                new Tab(
                  text: "H. La Merced",
                ),
                new Tab(
                  text: "H. Los Pinos",
                ),
                new Tab(
                  text: "H. La Portada",
                ),
                new Tab(
                  text: "H Cotahuma",
                )
              ])),
    );
  }
}

class Hospital {
  final int value;
  final String text;
  final int hosp_id;
  final bool checked;

  Hospital(this.value, this.text, this.hosp_id, this.checked);
}

class IndicadorEspecialidad {
  final String medico;
  final List<dynamic> fechas; //array
  final List<dynamic> fichas_planificadas;
  final List<dynamic> fechas_atendidads;
  final int vcontenedor;
  final List<dynamic> vfichas_solicitadas;
  IndicadorEspecialidad(this.medico, this.fechas, this.fichas_planificadas,
      this.fechas_atendidads, this.vcontenedor, this.vfichas_solicitadas);

  @override
  String toString() {
    return 'IndicadorEspecialidad{medico: $medico, fechas: $fechas, fichas_planificadas: $fichas_planificadas, fechas_atendidads: $fechas_atendidads, vcontenedor: $vcontenedor, vfichas_solicitadas: $vfichas_solicitadas}';
  }


}
