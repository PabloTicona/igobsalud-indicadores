import 'package:flutter/material.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

typedef void TapCallback(dateTime, List<int> index);
typedef void TapCallbackItem(IndicadorProces i);
typedef void OnChangeDropdownItem(IndicadorProces item);
typedef void PressCallback();

class TabTrimestral extends StatelessWidget {
  List<IndicadorProces> listaEspecialidades;
  final DateTime anioInicial;
  final DateTime anioComparativo;
  final DateTime mesInicio;
  final DateTime mesfin;

  final TapCallback onTapAnioInicial;
  final TapCallback onTapAnioComparativo;
  final TapCallback onTapMesInicio;
  final TapCallback onTapMesFin;

  final TapCallbackItem tapSelectItem;

  final IndicadorProces selectedItem;
  final OnChangeDropdownItem changeDropdownItem;
  final PressCallback onPressed;

  TabTrimestral(
      {this.listaEspecialidades,
      this.anioInicial,
      this.anioComparativo,
      this.mesInicio,
      this.mesfin,
      this.onTapAnioInicial,
      this.onTapAnioComparativo,
      this.onTapMesInicio,
      this.onTapMesFin,
      this.tapSelectItem,
      this.selectedItem,
      this.changeDropdownItem,
      this.onPressed});

  List<DropdownMenuItem<IndicadorProces>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<IndicadorProces>> items = List();

    if (items_ != null) {
      for (IndicadorProces itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.indicador),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  List<DropdownMenuItem<IndicadorProces>> _dropdownMenuItems;

  @override
  Widget build(BuildContext context) {
    _dropdownMenuItems = buildDropdownMenuItems(listaEspecialidades);
    return Container(
        child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            new CardDatePickerComponent(
                nombre: 'Año Inicial ',
                fechaInicio: anioInicial,
                formatoFecha: 'A',
                formatoFechaComponete: 'yyyy',
                onTap: onTapAnioInicial),
            new CardDatePickerComponent(
                nombre: 'Año Comparativo: ',
                fechaInicio: anioComparativo,
                formatoFecha: 'A',
                formatoFechaComponete: 'yyyy',
                onTap: onTapAnioComparativo),
          ],
        ),
        Row(
          children: <Widget>[
            new CardDatePickerComponent(
                nombre: 'Mes Inicio: ',
                fechaInicio: mesInicio,
                formatoFecha: 'M',
                formatoFechaComponete: 'MMMM',
                onTap: onTapMesInicio),
            new CardDatePickerComponent(
                nombre: 'Mes fin: ',
                fechaInicio: mesfin,
                formatoFecha: 'M',
                formatoFechaComponete: 'MMMM',
                onTap: onTapMesFin),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.78,
              height: 94,
              child: SizedBox(
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Card(
                    elevation: 8.0,
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: (listaEspecialidades != null)
                          ? new DropdownButton(
                              isExpanded: true,
                              value: (selectedItem != null)
                                  ? selectedItem
                                  : listaEspecialidades[0],
                              items: _dropdownMenuItems,
                              onChanged:
                                  changeDropdownItem, //changeDropdownItem,
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.black),
                              underline: Container(
                                height: 2,
                                color: Colors.red,
                              ),
                            )
                          : Center(
                              child: Text("Cargando.."),
                            ),
                    ),
                  ),
                ),
              ),
            ),

    Container(
    width: MediaQuery.of(context).size.width * 0.22,
      child: new ButtonMaterialCircleComponent(size: 20.0, onPressed: onPressed),
    )


          ],
        ),
      ],
    ));
  }
}
