import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/pages/consultaExterna/Servicios/HttpServiceConsultaExterna.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Reporte.dart';

import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'ReporteEspecialidad.dart';

import 'IndicadorProceso.dart';
import 'package:igob_salud_app/components/CardContador.dart';
import 'package:global_configuration/global_configuration.dart';

import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:igob_salud_app/components/CardPieChartModule.dart';
import 'package:igob_salud_app/pages/consultaExterna/Atendidos.dart';
import 'package:igob_salud_app/pages/consultaExterna/Solicitadas.dart';
import 'package:igob_salud_app/pages/consultaExterna/Planificados.dart';

class HomeConsultaExterna extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'igob salud',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ConsultaExterna(),
    );
  }
}

class ConsultaExterna extends StatefulWidget {
  @override
  _ConsultaExternaState createState() => _ConsultaExternaState();
}

class _ConsultaExternaState extends State<ConsultaExterna> {
  var url_reportes = GlobalConfiguration().getString("SALUD_REPORTES");
  var myArray = [];
  var mArraySemaforos = [];

  int _planificados = 0;
  int _solicitadas = 0;
  int _atendidos = 0;

  double currentGraf = 0.0;
  double currentGraf2 = 0.0;

  bool toggle = false;
  Map<String, double> dataMapSolicitados;
  Map<String, double> dataMapAtendidos;
  List<Color> colorList = [
    Colors.blue,
    Colors.lightBlueAccent,
  ];

  DateTime _fechaHoy = DateTime.now();

  List<Reporte> reporteList;

  @override
  void initState() {
    getReporte();
  }

  Future<void> getReporte() async {
    var reporteList_ =
        await HttpServiceConsultaExterna().getReporteConsultaExterna(_fechaHoy);

    setState(() {
      reporteList = reporteList_;
    });

    for (Reporte reporte in reporteList_) {
      if (reporte.tipo == 'A') {
        setState(() {
          _atendidos = _atendidos + reporte.cntfichas;
        });
      }

      if (reporte.tipo == 'S') {
        setState(() {
          _solicitadas = _solicitadas + reporte.cntfichas;
        });
      }

      if (reporte.tipo == 'P') {
        setState(() {
          _planificados = _planificados + reporte.cntfichas;
        });
      }
    }

    setState(() {
      currentGraf =
          double.parse(((_atendidos / _planificados) * 100).toStringAsFixed(2));
      currentGraf2 = double.parse(
          ((_solicitadas / _planificados) * 100).toStringAsFixed(2));
      toggle = true;
    });

    print("solicitadosGraf $currentGraf");
    print("solicitadosGraf2 $currentGraf2");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Consulta Externa",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            )
          },
        ),
      ),
      body: Center(
          child: Container(
              decoration: Theme.ColorsSiis.FondoImagen(),
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(0.0),
                    child: Card(
                      elevation: 8.0,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                            "Rendimiento de Consulta Externa Hospitales Municipales"),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Center(
                        child: Container(
                          child: CardPieChartModule(
                            dataMap: currentGraf2,
                            toggle: toggle,
                            label: 'Solicitados',
                            onTap: () {},
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          child: CardPieChartModule(
                            dataMap: currentGraf,
                            toggle: toggle,
                            label: 'Atendidos',
                            onTap: () {},
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(0.0),
                    child: Row(
                      children: <Widget>[
                        CardContador(
                          nombre: 'Planificados',
                          icono: Icons.av_timer,
                          cantidad: _planificados.toString(),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Planificados()),
                            );
                          },
                        ),
                        CardContador(
                          nombre: 'Solicitadas',
                          icono: Icons.bookmark_border,
                          cantidad: _solicitadas.toString(),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Solicitadas()),
                            );
                          },
                        ),
                        CardContador(
                          nombre: 'Atendidas',
                          icono: Icons.check_circle_outline,
                          cantidad: _atendidos.toString(),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Atendidos()),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          width: double.infinity, // match_parent
                          child: FlatButton.icon(
                            color: Colors.redAccent,
                            textColor: Colors.white,
                            icon: Icon(Icons.favorite), //`Icon` to display
                            label: Text(
                                'Reporte Por Especialidad'), //`Text` to display
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ReporteEspecialidad()),
                              );
                              //Code to execute when Floating Action Button is clicked
                              //...
                            },
                          ),
                        ),
                        SizedBox(
                          width: double.infinity, // match_parent
                          child: FlatButton.icon(
                            color: Colors.redAccent,
                            textColor: Colors.white,
                            icon: Icon(Icons.insert_chart), //`Icon` to display
                            label: Text(
                                'Indicadores Por Proceso'), //`Text` to display
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => IndicadorProceso()),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ))),
    );
  }
}

////////////
class DonutAutoLabelChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutAutoLabelChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate,
        defaultRenderer: new charts.ArcRendererConfig(
            arcWidth: 40,
            arcRendererDecorators: [new charts.ArcLabelDecorator()]));
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
