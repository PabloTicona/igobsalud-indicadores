import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/components/CardGaugeChartComponent.dart';
import 'package:igob_salud_app/pages/consultaExterna/HomeConsultaExterna.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Reporte.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:igob_salud_app/web-services/services.dart';
import 'package:screenshot/screenshot.dart';


class Atendidos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Atendidas/Planificadas',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Atendido(),
    );
  }
}

class Atendido extends StatefulWidget {
  @override
  _AtendidoState createState() => _AtendidoState();
}

class _AtendidoState extends State<Atendido> {
  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  ScreenshotController LaMercedController = ScreenshotController();
  ScreenshotController LosPinosController = ScreenshotController();
  ScreenshotController PortadaController = ScreenshotController();
  ScreenshotController CotahumaController = ScreenshotController();

  List<charts.Series<GaugeSegment, String>> listLaMercedGraf = [];
  List<GaugeSegment> listSegmentosLaMerced = [];

  Hospital hospitalLaMerced;
  Hospital hospitalLosPinos;
  Hospital hospitalLaPortada;
  Hospital hospitalCotahuma;

  List<Reporte> listReportes;



  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    //  reporteHospital(_fechaDesde, _fechaHasta);

    getDatos(_fechaDesde, _fechaHasta);
  }

  Future<void> getDatos(DateTime dateIni, DateTime dateFin) async {
    setState(() {
      listReportes = null;
      hospitalLaMerced = new Hospital("La merced", 1, 0, 0);
      hospitalLosPinos = new Hospital("Los Pinos", 1, 0, 0);
      hospitalLaPortada = new Hospital("La Portada", 1, 0, 0);
      hospitalCotahuma = new Hospital("Cotahuma", 1, 0, 0);
    });

    listReportes = await reporteHospital(dateIni, dateFin);
    print("listReportes ${listReportes.length}");

    setState(() {
      CargarDatosGrafica(listReportes);
    });
  }

  void CargarDatosGrafica(List<Reporte> listRepor) {
    listRepor.forEach((Reporte reporte) => {
          if (reporte.idhosp == 1)
            {
              if (reporte.tipo == 'P')
                {hospitalLaMerced.planificados = reporte.cntfichas},
              if (reporte.tipo == 'S')
                {hospitalLaMerced.solicitados = reporte.cntfichas},
              if (reporte.tipo == 'A')
                {hospitalLaMerced.atendidos = reporte.cntfichas},
            },
          if (reporte.idhosp == 2)
            {
              if (reporte.tipo == 'P')
                {hospitalLosPinos.planificados = reporte.cntfichas},
              if (reporte.tipo == 'S')
                {hospitalLosPinos.solicitados = reporte.cntfichas},
              if (reporte.tipo == 'A')
                {hospitalLosPinos.atendidos = reporte.cntfichas},
            },
          if (reporte.idhosp == 3)
            {
              if (reporte.tipo == 'P')
                {hospitalLaPortada.planificados = reporte.cntfichas},
              if (reporte.tipo == 'S')
                {hospitalLaPortada.solicitados = reporte.cntfichas},
              if (reporte.tipo == 'A')
                {hospitalLaPortada.atendidos = reporte.cntfichas},
            },
          if (reporte.idhosp == 5)
            {
              if (reporte.tipo == 'P')
                {hospitalCotahuma.planificados = reporte.cntfichas},
              if (reporte.tipo == 'S')
                {hospitalCotahuma.solicitados = reporte.cntfichas},
              if (reporte.tipo == 'A')
                {hospitalCotahuma.atendidos = reporte.cntfichas},
            },
          //  print('reporte : ${reporte.toString()}')
        });
  }



  Future<List<Reporte>> reporteHospital(
      DateTime dateIni, DateTime dateFin) async {
    List<Reporte> resp = [];

    var fechaInicio_ = dateIni.toString().split(" ")[0];
    var fechafin_ = dateFin.toString().split(" ")[0];

    var query =
        "select * from sp_reporte_hospital1(\$\$ $fechaInicio_ \$\$,\$\$ $fechafin_\$\$)";
    var resp_ = await getResponseDinamico(query);
    print("resp_ :  ${resp_}");
    if (resp_.length != 0) {
      for (var entry in resp_) {
        resp.add(new Reporte(entry['cntfichas'], entry['hspnombrehospital'],
            entry['idhosp'], entry['tipo']));
        //  print("<< entry $entry");
      }
      //  CargarDatosGrafica();
    } else {
      listReportes = [];
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return resp;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Atendidas/Planificadas",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),

        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeConsultaExterna()),
            ),

            // Navigator.pop(context)
            //  Navigator.pushNamed(context, '/reporteespecialidad', arguments: null)
          },
        ),
      ),
      body: Center(
          child: Container(
            decoration: Theme.ColorsSiis.FondoImagen(),
        child: ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new CardDatePickerComponent(
                          nombre: 'Desde: ',
                          fechaInicio: _fechaDesde,
                          formatoFecha: '',
                          formatoFechaComponete: 'dd-MMMM-yyyy',
                          onTap: (dateTime, List<int> index) {
                            setState(() {
                              _fechaDesde = dateTime;
                            });
                          }),
                      new CardDatePickerComponent(
                          nombre: 'Mes fin:',
                          fechaInicio: _fechaHasta,
                          formatoFecha: '',
                          formatoFechaComponete: 'dd-MMMM-yyyy',
                          onTap: (dateTime, List<int> index) {
                            setState(() {
                              _fechaHasta = dateTime;
                            });
                          }),
                      new ButtonMaterialCircleComponent(
                          size: 20.0,
                          onPressed: () {
                            getDatos(_fechaDesde, _fechaHasta);
                            // reporteHospital(_fechaDesde, _fechaHasta);
                            print('boton presionado....');
                          }),
                    ],
                  ),
                ),
              ),
            ),
            Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Rendimiento Hospitales Municipales"),
              ),
            ),
            Row(
              children: <Widget>[
                Screenshot(
                  controller: LaMercedController,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 348,
                    child: new CardGaugeChartComponent(
                      hospitalLaMerced,
                      porcentaje: double.parse(((hospitalLaMerced.atendidos /
                                  hospitalLaMerced.planificados) *
                              100)
                          .toStringAsFixed(2)),
                      scaffoldKey: _scaffoldKey,
                      screenshotController: LaMercedController,
                    ),
                  ),
                ),
                Screenshot(
                  controller: LosPinosController,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 348,
                    child: new CardGaugeChartComponent(
                      hospitalLosPinos,
                      porcentaje: double.parse(((hospitalLosPinos.atendidos /
                                  hospitalLosPinos.planificados) *
                              100)
                          .toStringAsFixed(2)),
                      scaffoldKey: _scaffoldKey,
                      screenshotController: LosPinosController,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Screenshot(
                  controller: PortadaController,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 348,
                    child: new CardGaugeChartComponent(
                      hospitalLaPortada,
                      porcentaje: double.parse(((hospitalLaPortada.atendidos /
                                  hospitalLaPortada.planificados) *
                              100)
                          .toStringAsFixed(2)),
                      scaffoldKey: _scaffoldKey,
                      screenshotController: PortadaController,
                    ),
                  ),
                ),
                Screenshot(
                  controller: CotahumaController,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 348,
                    child: new CardGaugeChartComponent(
                      hospitalCotahuma,
                      porcentaje: double.parse(((hospitalCotahuma.atendidos /
                                  hospitalCotahuma.planificados) *
                              100)
                          .toStringAsFixed(2)),
                      scaffoldKey: _scaffoldKey,
                      screenshotController: CotahumaController,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      )),
    );
  }
}


