import 'package:igob_salud_app/pages/consultaExterna/IndicadorProceso.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Reporte.dart';
import 'package:igob_salud_app/web-services/services.dart';

class HttpServiceConsultaExterna {
//  String tip_indic = "RGL";
  String ges_mes = "ME";
  String hospitals = "h1";

  Future<List<IndicadorProces>> getReporteSelect(String tipo) async {
    List<IndicadorProces> resp = [];

    //var query = "select * from indicadores.sp_lst_indicadores(\$\$CE\$\$,\$\$PROCESO\$\$)";
    var query =
        "select * from indicadores.sp_lst_indicadores(\$\$${tipo}\$\$,\$\$PROCESO\$\$)";
    var resp_ = await getResponseDinamico(query);
   // resp.add(new IndicadorProces("-- Indicador --", "0", "0"));
    resp.add(new IndicadorProces(id: 0, servicio: "-- Indicador --", indicadorTipo: "-- Indicador --", indicador: "-- Indicador --", especialidadCodigo: "-- Indicador --", numerador: "-- Indicador --", denominador: "-- Indicador --" ));



    if (resp_ != null) {
      for (var entry in resp_) {
        resp.add(new IndicadorProces(
        id: entry['o_ind_id'],
        servicio:entry['o_ind_servicio'],
        indicadorTipo:entry['o_ind_tipo_indicador'],
        indicador:entry['o_ind_indicador'],
        especialidadCodigo:entry['o_ind_cod_especialidad'],
        numerador:entry['o_ind_numerador'],
        denominador: entry['o_ind_denominador']
    ));

        //  print("<< entry 11 ${entry}");
      }
    } else {
      //  showInSnackBar("No Existen datos. No se pudo obtener los indicadores");
    }

    return resp;
  }

  ///

  Future<List<IndicadorGraf>> getIndicadores(
      DateTime _anio,
      DateTime _mesInicio,
      DateTime _mesfin,
      IndicadorProces indicadorProces_, String tipo) async {

    List<IndicadorGraf> res = [];
    var anio = (_anio.toString().split(' ')[0]).split("-")[0];
    var mesInicioArray = _mesInicio.toString().split(' ')[0].split("-");
    var mesDiaInicio_ = "${mesInicioArray[1]}-${mesInicioArray[2]}";

    var mesFinArray = (_mesfin.toString().split(' ')[0]).split("-");
    var mesDiaFin_ = "${mesFinArray[1]}-${mesFinArray[2]}";

    var fechaInicio = "${anio}-${mesDiaInicio_}";
    var fechaFin = "${anio}-${mesDiaFin_}";


    var query =
        "SELECT * FROM indicadores.sp_calcular_indicadores(\$\$$fechaInicio\$\$,\$\$$fechaFin\$\$,\$\$\$\$,\$\$CO\$\$,\$\$$ges_mes\$\$,\$\$${tipo}\$\$,\$\$PROCESO\$\$,\$\$${indicadorProces_.especialidadCodigo}\$\$)";

    print("query : : ${query}");

    var resp_ = await getResponseDinamico(query);
    print("resp_ :: ${resp_}");
    if (resp_ != null) {
      for (var entry in resp_) {
    //    print("Grafica por mes $entry");

        // listIndicadores.add(new Indicador(int.parse(entry['total']), entry['variable'], int.parse(entry['hospital_id']), int.parse(entry['numerado']), int.parse(entry['denominador'])));
        res.add(new IndicadorGraf(
            double.parse(entry['total'].toString()),
            entry['variable'].toString(),
            int.parse(entry['hospital_id'].toString()),
            int.parse(entry['numerado'].toString()),
            int.parse(entry['denominador'].toString()),
            codigoMes(entry['variable'].toString())));
      }
    } else {
      return null;
      //  showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return res;
  }

  Future<List<Mes>> getMeses(DateTime fechaInicio, DateTime fechaFin, String tip_indic1,  IndicadorProces indicadorProces_) async {
    List<Mes> res_ = [];
    var fechaInicio_ =fechaInicio.toString().split(" ")[0];
    var fechaFin_ =fechaFin.toString().split(" ")[0];

    var query =
        "SELECT * FROM indicadores.sp_lst_indicadores_primarios(\$\$$fechaInicio_\$\$,\$\$$fechaFin_\$\$,\$\$\$\$,\$\$$ges_mes\$\$,\$\$$tip_indic1\$\$,\$\$PROCESO\$\$,\$\$${indicadorProces_.especialidadCodigo}\$\$)";


    //"SELECT * FROM indicadores.sp_lst_indicadores_primarios(\$\$$fechaInicio\$\$,\$\$$fechaFin\$\$,\$\$\$\$,\$\$$ges_mes\$\$,\$\$CE\$\$,\$\$PROCESO\$\$,\$\$$tip_indic\$\$)";

    print("query MESES $query");
    var resp_ = await getResponseDinamico(query);
      print("resp_ meses  : ${resp_}");

    if (resp_ != null) {
      for (var entry in resp_) {
        //  print("meses $entry");
        res_.add(new Mes(entry['respuesta'], int.parse(entry['codigo'])));
      }
    } else {
      return null;
      // showInSnackBar("No Existen datos. Seleccione otras fechas");
    }

    return res_;
  }

  int codigoMes(String mes) {
    int res = 1;
    switch (mes) {
      case 'ENERO':
        res = 1;
        break;
      case 'FEBRERO':
        res = 2;
        break;
      case 'MARZO':
        res = 3;
        break;
      case 'ABRIL':
        res = 4;
        break;
      case 'MAYO':
        res = 5;
        break;
      case 'JUNIO':
        res = 6;
        break;
      case 'JULIO':
        res = 7;
        break;
      case 'AGOSTO':
        res = 8;
        break;
      case 'SEPTIEMBRE':
        res = 9;
        break;

      case 'OCTUBRE':
        res = 10;
        break;

      case 'NOVIEMBRE':
        res = 11;
        break;

      case 'DICIEMBRE':
        res = 12;
        break;

      default:
        res = 1;
    }
    return res;
  }


  Future<List<Reporte>> getReporteConsultaExterna(DateTime fecha) async{
    List<Reporte> resp = [];
    var query ="select * from sp_reporte_hospital1(\$\$2018-01-01\$\$,\$\$${fecha}\$\$)";
    var resp_ = await getResponseDinamico(query);

    if (resp_ != null) {
      for (var entry in resp_) {
        resp.add(new Reporte(entry['cntfichas'], entry['hspnombrehospital'],
            entry['idhosp'], entry['tipo']));

      }
      //  CargarDatosGrafica();
    }else {

    }


    return resp;
  }

}
