import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:igob_salud_app/helps/TablaDinamicaClass.dart';
import 'package:igob_salud_app/helps/TablaDinamicaHospitales.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class TabMensualDetalle extends StatelessWidget {
  IndicadorProces indicadorProces;

  List<IndicadorGraf> indicadores;
  final List<Mes> mesesList;

  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String descripcion;

  TabMensualDetalle(
      {this.indicadorProces,
      this.indicadores,
      this.mesesList,
      this.screenshotController,
      this.scaffoldKey,
      this.descripcion});

  @override
  Widget build(BuildContext context) {


    List<TablaDinamicaClass> columTabla = [];

    mesesList.forEach((Mes colum){
      columTabla.add(new TablaDinamicaClass( mes: colum.respuesta,merced: "0", pinos: "0", cotahuma: "0",portada: "0"));
    });



    List<LineSeries<SalesData, String>> _createSampleData() {

      List<SalesData> dataLosPinos = [];
      List<SalesData> dataLaPortada = [];
      List<SalesData> dataCotahuma = [];
      List<SalesData> dataLaMerced = [];

      if (indicadores != null) {
        indicadores.forEach((IndicadorGraf ig) {
          if (ig.hospitalId == 1) { //La Merced
            dataLosPinos.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if(tablCol.mes==ig.variable){
                columTabla.elementAt(index).merced= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }

          if (ig.hospitalId == 2) {// Los Pinos
            dataLaPortada.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if(tablCol.mes==ig.variable){
                columTabla.elementAt(index).pinos= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }
          }

          if (ig.hospitalId == 3) { //La Portada
            dataCotahuma.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if(tablCol.mes==ig.variable){
                columTabla.elementAt(index).portada= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }
          }

          if (ig.hospitalId == 5) {//Cotahuma
            dataLaMerced.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if(tablCol.mes==ig.variable){
                columTabla.elementAt(index).cotahuma= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }
          }
        });




        return [
          LineSeries<SalesData, String>(
              dataSource: dataLosPinos,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Los Pinos",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaPortada,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Portada",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataCotahuma,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Cotahuma",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaMerced,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Merced",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
        ];
      } else {
        return null;
      }
    }

    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              child: Screenshot(
                controller: screenshotController,
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: (indicadores != null)
                        ? Column(
                            children: <Widget>[
                              Padding(
                                padding: new EdgeInsets.all(0.0),
                                child: ShareGraph(
                                  title: 'Indicador Por Proceso',
                                  description: descripcion,
                                  screenshotController: screenshotController,
                                  scaffoldKey: scaffoldKey,
                                ),
                              ),
                              SfCartesianChart(
                                primaryXAxis: CategoryAxis(),
                                // Chart title
                                //  title: ChartTitle(text: indicadorProces.indicador),
                                title: ChartTitle(text: ""),
                                // Enable legend
                                legend: Legend(
                                  isVisible: true,
                                  position: LegendPosition.top,
                                ),

                                // Enable tooltip
                                tooltipBehavior: TooltipBehavior(enable: false),
                                series: _createSampleData(),
                                trackballBehavior: TrackballBehavior(
                                    enable: true,
                                    lineType: TrackballLineType.vertical,
                                    activationMode: ActivationMode.singleTap,
                                    //  tooltipAlignment: _alignment,
                                    tooltipDisplayMode:
                                        TrackballDisplayMode.floatAllPoints,
                                    tooltipSettings: InteractiveTooltip(
                                        format: 'point.x : point.y'),
                                    shouldAlwaysShow: true),
                              ),
                              Text(""),
                              new TablaDinamicaHospitales(
                                listOfColumns: columTabla,
                              ),
                            ],
                          )
                        : Center(
                            child: SpinKitRing(
                              color: Colors.blue,
                            ),
                          ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Sample time series data type.
class MyRow {
  final DateTime timeStamp;
  final double headcount;
  MyRow(this.timeStamp, this.headcount);
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
