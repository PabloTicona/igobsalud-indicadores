class IndicadorProces {
  final int id;
  final String servicio;
  final String indicadorTipo;
  final String indicador;
  final String especialidadCodigo;
  final String numerador;
  final String denominador;

  IndicadorProces(
      {this.id,
      this.servicio,
      this.indicadorTipo,
      this.indicador,
      this.especialidadCodigo,
      this.numerador,
      this.denominador});

  @override
  String toString() {
    return 'IndicadorProces{id: $id, servicio: $servicio, indicadorTipo: $indicadorTipo, indicador: $indicador, especialidadCodigo: $especialidadCodigo, numerador: $numerador, denominador: $denominador}';
  }
}
