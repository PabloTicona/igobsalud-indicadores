class HospitalEspecialidades {
  final String hsp_nombre_hospital;
  final String especialidad;
  final int trn_hspcat_id;
  final int trn_hsp_id;


  HospitalEspecialidades(this.hsp_nombre_hospital, this.especialidad, this.trn_hspcat_id, this.trn_hsp_id);

  @override
  String toString() {
    return 'HospitalEspecialidades{hsp_nombre_hospital: $hsp_nombre_hospital, especialidad: $especialidad, trn_hspcat_id: $trn_hspcat_id, trn_hsp_id: $trn_hsp_id}';
  }
}