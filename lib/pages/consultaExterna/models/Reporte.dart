class Reporte {
  final int cntfichas;
  final String hspnombrehospital;
  final int idhosp;
  final String tipo;

  @override
  String toString() {
    return 'Reporte{cntfichas: $cntfichas, hspnombrehospital: $hspnombrehospital, idhosp: $idhosp, tipo: $tipo}';
  }

  Reporte(this.cntfichas, this.hspnombrehospital, this.idhosp, this.tipo);
}
