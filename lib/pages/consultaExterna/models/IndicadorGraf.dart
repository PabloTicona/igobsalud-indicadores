class IndicadorGraf {
  final double total;
  final String variable;
  final int hospitalId;
  final int numerado;
  final int denominador;
  final int codigoMes;
  IndicadorGraf(this.total, this.variable, this.hospitalId, this.numerado, this.denominador, this.codigoMes);

  @override
  String toStringgraf() {
    return 'IndicadorGraf{total: $total, variable: $variable, hospitalId: $hospitalId, numerado: $numerado, denominador: $denominador, codigoMes: $codigoMes}';
  }

}
