import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/components/CardListComponent.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:igob_salud_app/helps/TablaDinamicaClass.dart';
import 'package:igob_salud_app/helps/TablaDinamicaHospitales.dart';
import 'package:igob_salud_app/pages/consultaExterna/ReporteEspecialidad.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'dart:math';

import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class TimeSeriesChartModule extends StatelessWidget {
  List<IndicadorGraf> indicadores;
  final List<Mes> mesesList;
  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String title;
  final String descripcion;

  TimeSeriesChartModule(
      {this.indicadores,
      this.mesesList,
      this.screenshotController,
      this.scaffoldKey,
      this.title,
      this.descripcion});

  @override
  Widget build(BuildContext context) {
    List<TablaDinamicaClass> columTabla = [];

    mesesList.forEach((Mes colum) {
      columTabla.add(new TablaDinamicaClass(
          mes: colum.respuesta,
          merced: "0",
          pinos: "0",
          cotahuma: "0",
          portada: "0"));
    });




    List<charts.Series<TimeSeriesSales, DateTime>> _createData() {
      List<TimeSeriesSales> losPinosSalesData = [];
      List<TimeSeriesSales> laPortadaSalesData = [];
      List<TimeSeriesSales> cotahumaSalesData = [];
      List<TimeSeriesSales> laMercedSalesData = [];





      List<charts.Series<TimeSeriesSales, DateTime>> dataGraf = [];

    //  print("indicadores ${indicadores}");

      if (indicadores != null) {
        indicadores.forEach((IndicadorGraf ig) {
        //  print("IndicadorGraf ==> ${ig.toStringgraf()}");
          if (ig.hospitalId == 1) {
            //La Merced
            laMercedSalesData.add(new TimeSeriesSales(
                new DateTime(2019, ig.codigoMes, 10), ig.total));

            int index = 0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if (tablCol.mes == ig.variable) {
                columTabla.elementAt(index).merced =
                    "${ig.numerado}/${ig.denominador}";
              }
              index++;
            }



          }

          if (ig.hospitalId == 2) {
            // Los Pinos
            losPinosSalesData.add(new TimeSeriesSales(
                new DateTime(2019, ig.codigoMes, 10), ig.total));

            int index = 0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if (tablCol.mes == ig.variable) {
                columTabla.elementAt(index).pinos =
                    "${ig.numerado}/${ig.denominador}";
              }
              index++;
            }


          }

          if (ig.hospitalId == 3) {
            //La Portada
            laPortadaSalesData.add(new TimeSeriesSales(
                new DateTime(2019, ig.codigoMes, 10), ig.total));

            int index = 0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if (tablCol.mes == ig.variable) {
                columTabla.elementAt(index).portada =
                    "${ig.numerado}/${ig.denominador}";
              }
              index++;
            }



          }

          if (ig.hospitalId == 5) {
            //Cotahuma
            cotahumaSalesData.add(new TimeSeriesSales(
                new DateTime(2019, ig.codigoMes, 10), ig.total));
            int index = 0;
            for (TablaDinamicaClass tablCol in columTabla) {
              if (tablCol.mes == ig.variable) {
                columTabla.elementAt(index).cotahuma =
                    "${ig.numerado}/${ig.denominador}";
              }
              index++;
            }


          }
        });


        if(laMercedSalesData.length!=0) {
          dataGraf.add(new charts.Series<TimeSeriesSales, DateTime>(
              id: 'La Merced',
              colorFn: (_, __) => charts.MaterialPalette.cyan.shadeDefault,
              domainFn: (TimeSeriesSales sales, _) => sales.time,
              measureFn: (TimeSeriesSales sales, _) => sales.sales,
              data: laMercedSalesData));
        }

        if(losPinosSalesData.length!=0){
          dataGraf.add(new charts.Series<TimeSeriesSales, DateTime>(
            id: 'Los Pinos',
            colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
            domainFn: (TimeSeriesSales sales, _) => sales.time,
            measureFn: (TimeSeriesSales sales, _) => sales.sales,
            data: losPinosSalesData,
          ));

        }


        if(laPortadaSalesData.length!=0) {
          dataGraf.add(new charts.Series<TimeSeriesSales, DateTime>(
            id: 'La Portada',
            colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
            domainFn: (TimeSeriesSales sales, _) => sales.time,
            measureFn: (TimeSeriesSales sales, _) => sales.sales,
            data: laPortadaSalesData,
          ));
        }


        if(cotahumaSalesData.length!=0) {
          dataGraf.add(new charts.Series<TimeSeriesSales, DateTime>(
              id: 'Cotahuma',
              colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
              domainFn: (TimeSeriesSales sales, _) => sales.time,
              measureFn: (TimeSeriesSales sales, _) => sales.sales,
              data: cotahumaSalesData));
        }

        return dataGraf;

      } else {
        return null;
      }
    }


    return Container(
      child: Screenshot(
        controller: screenshotController,
        child: Card(
            elevation: 8.0,
            child: Column(
              children: <Widget>[
                ShareGraph(
                  title: title,
                  description: descripcion,
                  screenshotController: screenshotController,
                  scaffoldKey: scaffoldKey,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 1.0,
                  height: 300,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: (indicadores != null)
                        ? new charts.TimeSeriesChart(
                            _createData(),
                            animate: true,
                            defaultRenderer: new charts.LineRendererConfig(
                                includePoints: true),
                            behaviors: [
                              new charts.SeriesLegend(
                                position: charts.BehaviorPosition.bottom,
                                horizontalFirst: false,
                                cellPadding: new EdgeInsets.only(
                                    right: 4.0, bottom: 5.0),
                                showMeasures: true,
                                measureFormatter: (num value) {
                                  return value == null ? '-' : '${value}';
                                },
                              ),
                            ],
                            domainAxis: new charts.DateTimeAxisSpec(
                              tickFormatterSpec:
                                  new charts.AutoDateTimeTickFormatterSpec(
                                day: new charts.TimeFormatterSpec(
                                  format: 'MMM',
                                  transitionFormat: 'dd MMM',
                                ),
                              ),
                            ),
                            customSeriesRenderers: [
                              new charts.PointRendererConfig(
                                  customRendererId: 'customPoint')
                            ],
                            dateTimeFactory:
                                const charts.LocalDateTimeFactory(),
                          )
                        : Center(
                            child: SpinKitWave(
                                color: Colors.redAccent, type: SpinKitWaveType.center,
                            ),
                          ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: (columTabla.length != 0)
                      ? new TablaDinamicaHospitales(
                          listOfColumns: columTabla,
                        )
                      : Container(),
                )
              ],
            )),
      ),
    );
  }
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final double sales;

  TimeSeriesSales(this.time, this.sales);
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
