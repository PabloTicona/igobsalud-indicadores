import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/consultaExterna/ReporteEspecialidad.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
import 'package:screenshot/screenshot.dart';

class ReporteEspecialidadDetalle extends StatelessWidget {
  final HospitalEspecialidades hospitalEspecialidades;
  final List<IndicadorEspecialidad> listIndicadorEspecialidades;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final ScreenshotController screenshotController;

  ReporteEspecialidadDetalle(
      {this.hospitalEspecialidades,
      this.listIndicadorEspecialidades,
      this.scaffoldKey,
      this.screenshotController});

  static charts.Color getColorPorcentaje(double porcentaje) {
    charts.Color resp = charts.MaterialPalette.green.shadeDefault;

    if (porcentaje < 60) {
      resp = charts.MaterialPalette.red.shadeDefault;
    } else {
      if (porcentaje >= 60 && porcentaje < 80) {
        resp = charts.MaterialPalette.yellow.shadeDefault;
      }
    }

    return resp;
  }

  Porcentaje sumarItemFichas(int index) {
    List<dynamic> listAtendidas =
        listIndicadorEspecialidades[index].fechas_atendidads;
    List<dynamic> listPlanificadas =
        listIndicadorEspecialidades[index].fichas_planificadas;
    List<dynamic> listSolicitadas =
        listIndicadorEspecialidades[index].vfichas_solicitadas;

    int sumListAtendidas = 0;
    int sumListPlanificadas = 0;
    int sumListSolicitadas = 0;

    Porcentaje porcentaje;

    listAtendidas.forEach((var item) {
      sumListAtendidas = sumListAtendidas + item;
    });

    listPlanificadas.forEach((var item) {
      sumListPlanificadas = sumListPlanificadas + item;
    });

    listSolicitadas.forEach((var item) {
      sumListSolicitadas = sumListSolicitadas + item;
    });

    var solicitadasPorcentaje = double.parse(
        ((sumListSolicitadas / sumListPlanificadas) * 100).toStringAsFixed(2));

    var AtendidasPorcentaje = double.parse(
        ((sumListAtendidas / sumListPlanificadas) * 100).toStringAsFixed(2));

    //   if(parseFloat(((s_solicitadas/s_planificadas)*100).toFixed(2))<60)colorSolicitadas=this.colors_options[0];
    porcentaje = new Porcentaje(solicitadasPorcentaje, AtendidasPorcentaje);

    return porcentaje;
  }

  static List<charts.Series<GaugeSegment, String>> _createData(
      String nombre, double porcentaje) {
    final data = [
      new GaugeSegment('Low', porcentaje, getColorPorcentaje(porcentaje)),
      new GaugeSegment('Acceptable', (100 - porcentaje),
          charts.Color.fromHex(code: '#eff0f1')),
    ];

    return [
      new charts.Series<GaugeSegment, String>(
        id: "${porcentaje}%",
        domainFn: (GaugeSegment segment, _) => segment.segment,
        measureFn: (GaugeSegment segment, _) => segment.size,
        colorFn: (GaugeSegment segment, _) => segment.color,
        data: data,
      )
    ];
  }

  // var solicitadas_ =  sumarItemFichas(listIndicadorEspecialidades[index].fechas_atendidads, listIndicadorEspecialidades[index].fichas_planificadas, listIndicadorEspecialidades[index].vfichas_solicitadas).solicitadas;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*  appBar: new AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${hospitalEspecialidades.especialidad}",
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
            Text(
              hospitalEspecialidades.hsp_nombre_hospital,
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            )
          ],
        ),
      ), */
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 220.0),
                itemCount: listIndicadorEspecialidades.length,
                itemBuilder: (BuildContext context, int index) {
                  return Screenshot(
                    //  controller: screenshotController,

                    child: Container(
                      height: 270,
                      color: Color.fromRGBO(200, 200, 200, 0.2),
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  /*  ShareGraph(
                                  title:
                                      listIndicadorEspecialidades[index].medico,
                                  description: "Reporte por especialidad",
                                  screenshotController: screenshotController,
                                  scaffoldKey: scaffoldKey,
                                ), */

                                  Text(
                                    '${listIndicadorEspecialidades[index].medico}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  Icon(Icons.assignment_ind)
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 0.0, vertical: 0.0),
                              // height: MediaQuery.of(context).size.height * 0.29,
                              height: MediaQuery.of(context).size.height * 0.32,
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.86,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            new CardGaugeChart(

                                              dataMap: _createData(
                                                  'Solicitadas',
                                                  sumarItemFichas(index)
                                                      .solicitadas),
                                              titulo: "Solicitadas",
                                            ),
                                            new CardGaugeChart(
                                              titulo: "Atendidas",
                                              dataMap: _createData(
                                                  'Atendidas',
                                                  sumarItemFichas(index)
                                                      .atendidas),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 1.0,
                                    child: Column(
                                      children: <Widget>[
                                        new CardGraficaChart(
                                          listIndicadorEspecialidades_:
                                              listIndicadorEspecialidades[
                                                  index],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

////////

class CardGaugeChart extends StatelessWidget {
  final String titulo;
  final dynamic dataMap;

  CardGaugeChart({this.titulo,this.dataMap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width * 0.43,

      child: Card(
        elevation: 8.0,
        child: Padding(
          padding: EdgeInsets.all(0.30),
          child: Column(
            children: <Widget>[
                Text(titulo),
              Container(
                width: MediaQuery.of(context).size.width * 1.0,
                height: 175,
                child: new charts.PieChart(
                  dataMap,
                  animate: true,
                  defaultRenderer: new charts.ArcRendererConfig(
                      arcWidth: 15,
                      startAngle: 4 / 5 * pi,
                      arcLength: 7 / 5 * pi),
                  behaviors: [
                    new charts.SeriesLegend(
                        position: charts.BehaviorPosition.bottom,
                        horizontalFirst: bool.fromEnvironment("Pablo")),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CardGraficaChart extends StatelessWidget {
  dynamic listIndicadorEspecialidades_;

  CardGraficaChart({this.listIndicadorEspecialidades_});

  @override
  Widget build(BuildContext context) {
    

    List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData() {
      List<TimeSeriesSales> dataPlanificadas = [];
      int indicePlanificadas = 0;

      List<TimeSeriesSales> dataSolicitadas = [];
      int indiceSolicitadas = 0;

      List<TimeSeriesSales> dataAtendidas = [];
      int indiceAtendidas = 0;

      for (var fecha in listIndicadorEspecialidades_.fechas) {
        var fechA = fecha.toString().split("-");
        var anio_ = fechA[0];
        var mes_ = fechA[1];
        var dia_ = fechA[2];

        dataPlanificadas.add(new TimeSeriesSales(
            new DateTime(int.parse(anio_), int.parse(mes_), int.parse(dia_)),
            listIndicadorEspecialidades_
                .fichas_planificadas[indicePlanificadas]));
        indicePlanificadas++;

        dataSolicitadas.add(new TimeSeriesSales(
            new DateTime(int.parse(anio_), int.parse(mes_), int.parse(dia_)),
            listIndicadorEspecialidades_
                .vfichas_solicitadas[indiceSolicitadas]));
        indiceSolicitadas++;

        dataAtendidas.add(new TimeSeriesSales(
            new DateTime(int.parse(anio_), int.parse(mes_), int.parse(dia_)),
            listIndicadorEspecialidades_.fechas_atendidads[indiceAtendidas]));
        indiceAtendidas++;
      }

      return [
        new charts.Series<TimeSeriesSales, DateTime>(
          id: 'Planificadas',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          domainFn: (TimeSeriesSales sales, _) => sales.time,
          measureFn: (TimeSeriesSales sales, _) => sales.sales,
          data: dataPlanificadas,
        ),
        new charts.Series<TimeSeriesSales, DateTime>(
          id: 'Solicitadas',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (TimeSeriesSales sales, _) => sales.time,
          measureFn: (TimeSeriesSales sales, _) => sales.sales,
          data: dataSolicitadas,
        ),
        new charts.Series<TimeSeriesSales, DateTime>(
          id: 'Atendidas',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (TimeSeriesSales sales, _) => sales.time,
          measureFn: (TimeSeriesSales sales, _) => sales.sales,
          data: dataAtendidas,
        )
      ];
    }

    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width * 1.0,
      height: 200,
      child: Card(
        elevation: 8.0,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: new charts.TimeSeriesChart(
            _createSampleData(),
            animate: true,

            behaviors: [
              new charts.SeriesLegend(
                  position: charts.BehaviorPosition.start,
                  horizontalFirst: bool.fromEnvironment("Pablo")),
            ],
            // Configures an axis spec that is configured to render one tick at each
            // end of the axis range, anchored "inside" the axis. The start tick label
            // will be left-aligned with its tick mark, and the end tick label will be
            // right-aligned with its tick mark.
            domainAxis: new charts.EndPointsTimeAxisSpec(),
          ),
        ),
      ),
    );
  }
}

class TimeSeriesSales {
  final DateTime time;
  final int sales;

  TimeSeriesSales(this.time, this.sales);
}

////////

class Porcentaje {
  final double solicitadas;
  final double atendidas;
  Porcentaje(this.solicitadas, this.atendidas);
}

/// Sample data type.
class GaugeSegment {
  final String segment;
  final double size;
  final charts.Color color;
  GaugeSegment(this.segment, this.size, this.color);
}
