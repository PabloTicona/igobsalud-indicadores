import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:igob_salud_app/helps/TablaDinamicaClass.dart';
import 'package:igob_salud_app/helps/TablaDinamicaHospitales.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorGraf.dart';

import 'package:igob_salud_app/pages/consultaExterna/models/IndicadorProces.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/Mes.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class TabTrimestreDetalle extends StatelessWidget {
  IndicadorProces indicadorProces;

  List<IndicadorGraf> indicadoresInicial;
  List<IndicadorGraf> indicadoresComparativo;
  final List<Mes> mesesList;

  final ScreenshotController screenshotControllerInicial;
  final ScreenshotController screenshotControllerComparartivo;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String descripcion;

  TabTrimestreDetalle(
      {this.indicadorProces,
      this.indicadoresInicial,
      this.indicadoresComparativo,
        this.mesesList,
      this.screenshotControllerInicial,
      this.screenshotControllerComparartivo,
      this.scaffoldKey,
      this.descripcion});

  @override
  Widget build(BuildContext context) {


    List<TablaDinamicaClass> columTablaInicial = [];
    List<TablaDinamicaClass> columTablaComparativo = [];

    mesesList.forEach((Mes colum){
      print("<< mes colum  :: ${colum.toString()}");
      columTablaInicial.add(new TablaDinamicaClass( mes: colum.respuesta,merced: "0", pinos: "0", cotahuma: "0",portada: "0"));
      columTablaComparativo.add(new TablaDinamicaClass( mes: colum.respuesta,merced: "0", pinos: "0", cotahuma: "0",portada: "0"));
    });


    List<SalesData> dataLosPinosInicial = [];
    List<SalesData> dataLaPortadaInicial = [];
    List<SalesData> dataCotahumaInicial = [];
    List<SalesData> dataLaMercedInicial = [];

    List<SalesData> dataLosPinosComparartivo = [];
    List<SalesData> dataLaPortadaComparartivo = [];
    List<SalesData> dataCotahumaComparartivo = [];
    List<SalesData> dataLaMercedComparartivo = [];

    List<LineSeries<SalesData, String>> _createeDataInicial() {
      if (indicadoresInicial != null) {
        indicadoresInicial.forEach((IndicadorGraf ig) {
          print("ig : ${ig.toStringgraf()}");
          if (ig.hospitalId == 1) {//La Merced
            dataLosPinosInicial.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTablaInicial) {
              if(tablCol.mes==ig.variable){
                columTablaInicial.elementAt(index).merced= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }

          if (ig.hospitalId == 2) {// Los Pinos
            dataLaPortadaInicial.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTablaInicial) {
              if(tablCol.mes==ig.variable){
                columTablaInicial.elementAt(index).pinos= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }

          if (ig.hospitalId == 3) {//La Portada
            dataCotahumaInicial.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTablaInicial) {
              if(tablCol.mes==ig.variable){
                columTablaInicial.elementAt(index).portada= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }

          if (ig.hospitalId == 5) {//Cotahuma
            dataLaMercedInicial.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTablaInicial) {
              if(tablCol.mes==ig.variable){
                columTablaInicial.elementAt(index).cotahuma= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }
        });

        return [
          LineSeries<SalesData, String>(
              dataSource: dataLosPinosInicial,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Los Pinos",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaPortadaInicial,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Portada",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataCotahumaInicial,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Cotahuma",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaMercedInicial,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Merced",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
        ];
      } else {
        return null;
      }
    }

/***
 * COMPARARTIVO
 */
    List<LineSeries<SalesData, String>> _createDataComparativo() {
      if (indicadoresComparativo != null) {
        indicadoresComparativo.forEach((IndicadorGraf ig) {
          print("ig : ${ig.toStringgraf()}");
          if (ig.hospitalId == 1) {//La Merced
            dataLosPinosComparartivo.add(SalesData(ig.variable, ig.total));


            int index=0;
            for (TablaDinamicaClass tablCol in columTablaComparativo) {
              if(tablCol.mes==ig.variable){
                columTablaComparativo.elementAt(index).merced= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }


          }

          if (ig.hospitalId == 2) {// Los Pinos
            dataLaPortadaComparartivo.add(SalesData(ig.variable, ig.total));

            int index=0;
            for (TablaDinamicaClass tablCol in columTablaComparativo) {
              if(tablCol.mes==ig.variable){
                columTablaComparativo.elementAt(index).pinos= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }


          }

          if (ig.hospitalId == 3) {//La Portada
            dataCotahumaComparartivo.add(SalesData(ig.variable, ig.total));


            int index=0;
            for (TablaDinamicaClass tablCol in columTablaComparativo) {
              if(tablCol.mes==ig.variable){
                columTablaComparativo.elementAt(index).portada= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }

          if (ig.hospitalId == 5) {//Cotahuma
            dataLaMercedComparartivo.add(SalesData(ig.variable, ig.total));


            int index=0;
            for (TablaDinamicaClass tablCol in columTablaComparativo) {
              if(tablCol.mes==ig.variable){
                columTablaComparativo.elementAt(index).cotahuma= "${ig.numerado}/${ig.denominador}" ;
              }
              index++;
            }

          }
        });

        return [
          LineSeries<SalesData, String>(
              dataSource: dataLosPinosComparartivo,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Los Pinos",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaPortadaComparartivo,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Portada",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataCotahumaComparartivo,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "Cotahuma",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
          LineSeries<SalesData, String>(
              dataSource: dataLaMercedComparartivo,
              xValueMapper: (SalesData sales, _) => sales.year,
              yValueMapper: (SalesData sales, _) => sales.sales,
              legendItemText: "La Merced",
              width: 3,
              markerSettings: MarkerSettings(isVisible: true),
              dataLabelSettings: DataLabelSettings(isVisible: false)),
        ];
      } else {
        return null;
      }
    }

    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            (indicadoresInicial != null)
                ? new CardGraf(
                    titulo: "Año Inicial",
                    listData: _createeDataInicial(),
                    screenshotController: screenshotControllerInicial,
                    scaffoldKey: scaffoldKey,
                    descripcion: indicadorProces.indicador,
                  )
                : Center(
                    child: SpinKitRing(
                      color: Colors.blue,
                    ),
                  ),
            Text(""),
            (indicadoresInicial != null)?new TablaDinamicaHospitales(
              listOfColumns: columTablaInicial,
            ): Container(),


            (indicadoresComparativo != null)
                ? new CardGraf(
                    titulo: "Año Comparartivo",
                    listData: _createDataComparativo(),
                    screenshotController: screenshotControllerComparartivo,
                    scaffoldKey: scaffoldKey,
                    descripcion: indicadorProces.indicador,
                  )
                : Center(
                    child: SpinKitRing(
                      color: Colors.blue,
                    ),
                  ),

            Text(""),
            (indicadoresComparativo != null)? new TablaDinamicaHospitales(
              listOfColumns: columTablaComparativo,
            ):Container(),

          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}

class CardGraf extends StatelessWidget {
  String titulo;
  List<LineSeries<SalesData, String>> listData;

  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String descripcion;

  CardGraf(
      {this.titulo,
      this.listData,
      this.screenshotController,
      this.scaffoldKey,
      this.descripcion});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Screenshot(
        controller: screenshotController,
        child: Card(
          elevation: 8.0,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: (listData.length != 0)
                ? Column(
              children: <Widget>[
                Padding(
                  padding: new EdgeInsets.all(13.0),
                  child: ShareGraph(
                    title: titulo,
                    description: descripcion,
                    screenshotController: screenshotController,
                    scaffoldKey: scaffoldKey,
                  ),
                ),
                SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  // Chart title
                  //  title: ChartTitle(text: titulo),
                  // Enable legend
                  legend: Legend(
                    isVisible: true,
                    position: LegendPosition.top,
                  ),

                  // Enable tooltip
                  tooltipBehavior: TooltipBehavior(enable: false),
                  series: listData,
                  trackballBehavior: TrackballBehavior(
                      enable: true,
                      lineType: TrackballLineType.vertical,
                      activationMode: ActivationMode.singleTap,
                      //  tooltipAlignment: _alignment,
                      tooltipDisplayMode:
                      TrackballDisplayMode.floatAllPoints,
                      tooltipSettings:
                      InteractiveTooltip(format: 'point.x : point.y'),
                      shouldAlwaysShow: true),
                )
              ],
            )
                : Center(
              child: Text("Sin datos, Seleccione otras fechas"),
            ),
          ),
        ),
      ),
    );
  }
}
