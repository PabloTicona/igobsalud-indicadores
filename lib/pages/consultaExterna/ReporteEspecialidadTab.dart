import 'package:flutter/material.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';


typedef void TapCallback(dateTime, List<int> index);
typedef void PressCallback();
typedef void OnChangeDropdownItem(ItemSelect item);

class ReporteEspecialidadTab extends StatelessWidget {
  final int hospitalId;
  final DateTime fechaDesde;
  final DateTime fechaHasta;
  final TapCallback onTapFechaDesde;
  final TapCallback onTapFechaHasta;
  final PressCallback onPressed;
  final List<ItemSelect> itemSelectList;
  final OnChangeDropdownItem changeDropdownItem;
  final ItemSelect selectedItem;

  ReporteEspecialidadTab(
      {this.hospitalId,
      this.fechaDesde,
      this.fechaHasta,
      this.onTapFechaDesde,
      this.onTapFechaHasta,
      this.onPressed,
      this.itemSelectList,
      this.changeDropdownItem,
      this.selectedItem});

  List<DropdownMenuItem<ItemSelect>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<ItemSelect>> items = List();

    if (items_ != null) {
      for (ItemSelect itemSelect in items_) {
        

      //  if (itemSelect.id == hospitalId) {
          items.add(
            DropdownMenuItem(
              value: itemSelect,
              child: Text(itemSelect.name),
            ),
          );
      //  }
      }
    } else {}

    return items;
  }

  List<DropdownMenuItem<ItemSelect>> _dropdownMenuItems;

  @override
  Widget build(BuildContext context) {


    _dropdownMenuItems = buildDropdownMenuItems(itemSelectList);

    // TODO: implement build
    return Container(
     // width: MediaQuery.of(context).size.width * 1.0,
        child: Column(
      children: <Widget>[
        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Padding(
              padding: EdgeInsets.all(0.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new CardDatePickerComponent(
                      nombre: 'Desde: ',
                      fechaInicio: fechaDesde,
                      formatoFecha: '',
                      formatoFechaComponete: 'dd-MMMM-yyyy',
                      onTap: onTapFechaDesde),
                  new CardDatePickerComponent(
                      nombre: 'Mes fin:',
                      fechaInicio: fechaHasta,
                      formatoFecha: '',
                      formatoFechaComponete: 'dd-MMMM-yyyy',
                      onTap: onTapFechaHasta),
                  new ButtonMaterialCircleComponent(
                      size: 20.0, onPressed: onPressed),
                ],
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          height: 70,
          child: SizedBox(
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.all(0.0),
              child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: (itemSelectList != null && _dropdownMenuItems != null)
                      ? new DropdownButton(
                      
                          isExpanded: true,
                          value: (selectedItem != null)
                              ? selectedItem
                              : itemSelectList[0],
                          items: _dropdownMenuItems,
                          onChanged: changeDropdownItem,
                          iconSize: 24,
                          elevation: 16,
                          style: TextStyle(color: Colors.black),
                          underline: Container(
                            height: 2,
                            color: Colors.red,
                          ),
                        )
                      : Center(
                          child: Text("Cargando.."),
                        ),
                ),
              ),
            ),
          ),
        ),
      ],
    ));
  }
}

class ItemSelect {
  int id;
  String name;

  ItemSelect(this.id, this.name);

  static List<ItemSelect> getCompanies() {
    return <ItemSelect>[
      ItemSelect(1, 'Apple 2'),
      ItemSelect(2, 'Google 3'),
      ItemSelect(3, 'Samsung'),
      ItemSelect(4, 'Sony'),
      ItemSelect(5, 'LG'),
    ];
  }
}
