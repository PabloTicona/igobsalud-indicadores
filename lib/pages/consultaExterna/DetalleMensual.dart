import 'package:flutter/material.dart';
class DetalleMensual extends StatelessWidget {

  final DateTime anio;
  final DateTime inicioMes;
  final DateTime finMes;

  DetalleMensual({this.anio, this.inicioMes, this.finMes});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            // Navigate back to the first screen by popping the current route
            // off the stack.
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}