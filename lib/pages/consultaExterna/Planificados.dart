import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/components/CardHospitalBlock.dart';
import 'package:igob_salud_app/pages/consultaExterna/HomeConsultaExterna.dart';
import 'package:igob_salud_app/helps/DateFormatConverter.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:screenshot/screenshot.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Planificados extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Solicitadas/Planificadas',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Planificado(),
    );
  }
}

class Planificado extends StatefulWidget {
  @override
  _PlanificadoState createState() => _PlanificadoState();
}

class _PlanificadoState extends State<Planificado> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScreenshotController SCPlanificados = ScreenshotController();

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  List<charts.Series<OrdinalSales, String>> listFichasEmitidas;
  List<OrdinalSales> hospitalLaMercedData;
  List<OrdinalSales> hospitalLosPinosData;
  List<OrdinalSales> hospitalLaPortadaData;
  List<OrdinalSales> hospitalCotahumaData;



  @override
  void initState() {
    hospitalLaMercedData = [];
    hospitalLosPinosData = [];
    hospitalLaPortadaData = [];
    hospitalCotahumaData = [];

    reporteHospital(_fechaDesde, _fechaHasta);
  }

  void mostrarFichasEmitidasGrafica(resp_) {
    for (var entry in resp_) {
      if (entry['idhosp'] == 1) {
        if (entry['tipo'] == 'P') {
          hospitalLaMercedData
              .add(new OrdinalSales('Planificadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'S') {
          hospitalLaMercedData
              .add(new OrdinalSales('Solicitadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'A') {
          hospitalLaMercedData
              .add(new OrdinalSales('Atendidas', entry['cntfichas']));
        }
      }

      if (entry['idhosp'] == 2) {
        if (entry['tipo'] == 'P') {
          hospitalLosPinosData
              .add(new OrdinalSales('Planificadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'S') {
          hospitalLosPinosData
              .add(new OrdinalSales('Solicitadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'A') {
          hospitalLosPinosData
              .add(new OrdinalSales('Atendidas', entry['cntfichas']));
        }
      }

      if (entry['idhosp'] == 3) {
        if (entry['tipo'] == 'P') {
          hospitalLaPortadaData
              .add(new OrdinalSales('Planificadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'S') {
          hospitalLaPortadaData
              .add(new OrdinalSales('Solicitadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'A') {
          hospitalLaPortadaData
              .add(new OrdinalSales('Atendidas', entry['cntfichas']));
        }
      }

      if (entry['idhosp'] == 5) {
        if (entry['tipo'] == 'P') {
          hospitalCotahumaData
              .add(new OrdinalSales('Planificadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'S') {
          hospitalCotahumaData
              .add(new OrdinalSales('Solicitadas', entry['cntfichas']));
        }
        if (entry['tipo'] == 'A') {
          hospitalCotahumaData
              .add(new OrdinalSales('Atendidas', entry['cntfichas']));
        }
      }
    }

    setState(() {
      listFichasEmitidas.add(new charts.Series<OrdinalSales, String>(
        id: 'H. La Merced',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        data: hospitalLaMercedData,
      ));

      listFichasEmitidas.add(new charts.Series<OrdinalSales, String>(
        id: 'H. Los Pinos',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        data: hospitalLosPinosData,
      ));
      listFichasEmitidas.add(new charts.Series<OrdinalSales, String>(
        id: 'H. La Portada',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        data: hospitalLaPortadaData,
      ));
      listFichasEmitidas.add(new charts.Series<OrdinalSales, String>(
        id: 'H. Cotahuma',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        colorFn: (_, __) => charts.MaterialPalette.cyan.shadeDefault,
        data: hospitalCotahumaData,
      ));
    });
  }



  Future<dynamic> reporteHospital(DateTime dateIni, DateTime dateFin) async {
    setState(() {
      listFichasEmitidas = [];
      hospitalLaMercedData = [];
      hospitalLosPinosData = [];
      hospitalLaPortadaData = [];
      hospitalCotahumaData = [];
    });

    var fechaInicio_ = dateIni.toString().split(" ")[0];
    var fechafin_ = dateFin.toString().split(" ")[0];
// "select * from sp_reporte_hospital1($$2019-08-29$$,$$2019-08-29$$)"
    var query =
        "select * from sp_reporte_hospital1(\$\$$fechaInicio_\$\$,\$\$$fechafin_\$\$)";
    var resp_ = await getResponseDinamico(query);

    if (resp_.length != 0) {
      mostrarFichasEmitidasGrafica(resp_);
    } else {
      showInSnackBar("No Existen datos. Seleccione otras fechas");
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Planificadas",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {
              reporteHospital(_fechaDesde, _fechaHasta);
            },
          ),
        ],
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeConsultaExterna()),
                ),
                //  Navigator.pushNamed(context, '/reporteespecialidad', arguments: null)
              },
        ),
      ),
      body: Center(
          child: Container(
            decoration: Theme.ColorsSiis.FondoImagen(),
        child: ListView(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new CardDatePickerComponent(
                          nombre: 'Desde: ',
                          fechaInicio: _fechaDesde,
                          formatoFecha: '',
                          formatoFechaComponete: 'dd-MMMM-yyyy',
                          onTap: (dateTime, List<int> index) {
                            setState(() {
                              _fechaDesde = dateTime;
                              //   reporteHospital(_fechaDesde, _fechaHasta);
                            });
                          }),
                      new CardDatePickerComponent(
                          nombre: 'Mes fin:',
                          fechaInicio: _fechaHasta,
                          formatoFecha: '',
                          formatoFechaComponete: 'dd-MMMM-yyyy',
                          onTap: (dateTime, List<int> index) {
                            setState(() {
                              _fechaHasta = dateTime;
                              //   reporteHospital(_fechaDesde, _fechaHasta);
                            });
                          }),
                      new ButtonMaterialCircleComponent(
                          size: 20.0,
                          onPressed: () {
                            setState(() {
                              reporteHospital(_fechaDesde, _fechaHasta);
                            });

                            print('boton presionado buscar....');
                          }),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: Screenshot(
                controller: SCPlanificados,
                child: Card(
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: new EdgeInsets.all(0.0),
                          child: ShareGraph(
                            title: 'Planificados',
                            description:
                                "De fecha ${dateTimeFormat(_fechaDesde)} a ${dateTimeFormat(_fechaHasta)}",
                            screenshotController: SCPlanificados,
                            scaffoldKey: _scaffoldKey,
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 1.0,
                          height: 300,
                          child: (listFichasEmitidas.length != 0)
                              ? new charts.BarChart(
                                  listFichasEmitidas,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  behaviors: [
                                    new charts.SeriesLegend(
                                        position:
                                            charts.BehaviorPosition.bottom,
                                        horizontalFirst:
                                            bool.fromEnvironment("Pablo")),
                                  ],
                                )
                              : Center(
                            child: SpinKitWave(
                                color: Colors.redAccent, type: SpinKitWaveType.center),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 150,
              child: Column(
                children: <Widget>[
                  new CardHospitalBlock(
                    hospital: "Hopital La Merced",
                    data: hospitalLaMercedData,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 150,
              child: Column(
                children: <Widget>[
                  new CardHospitalBlock(
                    hospital: "Hopital Los Pinos",
                    data: hospitalLosPinosData,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 150,
              child: Column(
                children: <Widget>[
                  new CardHospitalBlock(
                    hospital: "Hopital La Portada",
                    data: hospitalLaPortadaData,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 150,
              child: Column(
                children: <Widget>[
                  new CardHospitalBlock(
                    hospital: "Hopital Cotahuma",
                    data: hospitalCotahumaData,
                  ),
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }
}

class PlanificadosHospital {
  final String nombre;
  final int planificados;
  final int solicitados;
  final int atendidos;

  PlanificadosHospital(
      this.nombre, this.planificados, this.solicitados, this.atendidos);
}
