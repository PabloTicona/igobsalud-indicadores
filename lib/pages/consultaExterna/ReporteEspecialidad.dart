import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:igob_salud_app/pages/consultaExterna/ReporteEspecialidadDetalle.dart';
import 'package:igob_salud_app/pages/consultaExterna/HomeConsultaExterna.dart';
import 'package:igob_salud_app/pages/consultaExterna/ReporteEspecialidadTab.dart';
import 'package:igob_salud_app/pages/consultaExterna/Witget/ListViewReporteEspecialidad.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:screenshot/screenshot.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;

class ReporteEspecialidad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reporte Por Especialidad',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Especialidad(),
    );
  }
}

class Especialidad extends StatefulWidget {
  @override
  _EspecialidadState createState() => _EspecialidadState();
}

class _EspecialidadState extends State<Especialidad>
    with SingleTickerProviderStateMixin {
  //var send = [{'latitude': widget.lat}, {'latitude': 123.456}];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  List<HospitalEspecialidades> listhospitalEspecialidades;

  List<IndicadorEspecialidad> listIndicadorEspecialidades_ = [];
  List<ItemSelect> itemSelectList;

  List<ItemSelect> itemSelectListLaMerced;
  List<ItemSelect> itemSelectListLosPinos;
  List<ItemSelect> itemSelectListLaportada;
  List<ItemSelect> itemSelectListCotahuma;

  List<IndicadorEspecialidad> indicadorEspecialidadLaMerced;
  List<IndicadorEspecialidad> indicadorEspecialidadLosPinos;
  List<IndicadorEspecialidad> indicadorEspecialidadLaPortada;
  List<IndicadorEspecialidad> indicadorEspecialidadCotahuma;

  ScreenshotController LaMercedController = ScreenshotController();
  ScreenshotController LosPinosController = ScreenshotController();
  ScreenshotController PortadaController = ScreenshotController();
  ScreenshotController CotahumaController = ScreenshotController();

  ItemSelect _selectedItemLaMerced;
  ItemSelect _selectedItemLosPinos;
  ItemSelect _selectedItemLaPortada;
  ItemSelect _selectedItemCotahuma;

  bool viewCardListLaMerced = false;
  bool viewCardListLosPinos = false;
  bool viewCardListLaPortada = false;
  bool viewCardListCotahuma = false;



  @override
  void initState() {
    especialidadesHospitales(_fechaDesde, _fechaHasta);
  }

  List<HospitalEspecialidades> filtrarEspecialidad(
      List<HospitalEspecialidades> list, int idEspecialidad) {
    List<HospitalEspecialidades> resp = [];

    list.forEach((HospitalEspecialidades hospitalEspecialidades) {
      if (hospitalEspecialidades.trn_hsp_id == idEspecialidad) {
        resp.add(hospitalEspecialidades);
      }
    });

    return resp;
  }

  Future<void> clickSearch(DateTime fechaDesde, DateTime fechaHasta,
      ItemSelect itemSelect, int hospitalId) async {


    if (itemSelect != null) {
      setState(() {
        switch (hospitalId) {
          case 1:
            viewCardListLaMerced = true;
            indicadorEspecialidadLaMerced = null;
            break;

          case 2:
            viewCardListLosPinos = true;
            indicadorEspecialidadLosPinos = null;
            break;

          case 3:
            viewCardListLaPortada = true;
            indicadorEspecialidadLaPortada = null;
            break;

          case 5:
            viewCardListCotahuma = true;
            indicadorEspecialidadCotahuma = null;
            break;
        }
      });
    }

    try {
      var especialidadList = await indicadoresEspecialidad(
          hospitalId, itemSelect.id, fechaDesde, fechaHasta);


      if(especialidadList.length!=0){
        switch (hospitalId) {
          case 1:
            setState(() {
              indicadorEspecialidadLaMerced = especialidadList;
            });
            break;

          case 2:
            setState(() {
              indicadorEspecialidadLosPinos = especialidadList;
            });
            break;

          case 3:
            setState(() {
              indicadorEspecialidadLaPortada = especialidadList;
            });
            break;

          case 5:
            setState(() {
              indicadorEspecialidadCotahuma = especialidadList;
            });
            break;
        }
      }else{
        showInSnackBar("Sin Datos");



        setState(() {
          switch (hospitalId) {
            case 1:
              viewCardListLaMerced = false;

              break;

            case 2:
              viewCardListLosPinos = false;

              break;

            case 3:
              viewCardListLaPortada = false;

              break;

            case 5:
              viewCardListCotahuma = false;
              break;
          }
        });

      }

      print("especialidadList :: ${especialidadList}");


    } catch (e) {
      showInSnackBar("Seleccione una especialidad");
    }
  }

  Future<dynamic> especialidadesHospitales(
      DateTime fechaDesde, DateTime fechaHasta) async {
    var listhospitalE =
        await getEspecialidadesHospitales(fechaDesde, fechaHasta);
    listhospitalEspecialidades = listhospitalE;

    var listLaMerced = getListItemsSelect(listhospitalEspecialidades, 1);
    var listLosPinos = getListItemsSelect(listhospitalEspecialidades, 2);
    var listLaPortada = getListItemsSelect(listhospitalEspecialidades, 3);
    var listCotahuma = getListItemsSelect(listhospitalEspecialidades, 5);
    setState(() {
      itemSelectListLaMerced = listLaMerced;
      itemSelectListLosPinos = listLosPinos;
      itemSelectListLaportada = listLaPortada;
      itemSelectListCotahuma = listCotahuma;
    });
  }

  List<ItemSelect> getListItemsSelect(
      List<HospitalEspecialidades> le_, int hospitalId) {
    List<ItemSelect> resp_ = [];
    resp_.add(new ItemSelect(0, "-- Especialidades --"));
    for (HospitalEspecialidades item in le_) {
      if (item.trn_hsp_id == hospitalId) {
        resp_.add(new ItemSelect(item.trn_hspcat_id, item.especialidad));
      }
    }

    return resp_;
  }

  Future<List<HospitalEspecialidades>> getEspecialidadesHospitales(
      DateTime fechaDesde, DateTime fechaHasta) async {
    var fechaInicio = fechaDesde.toString().split(' ')[0];
    var fechaFin = fechaHasta.toString().split(' ')[0];

    List<HospitalEspecialidades> resp = [];

    var query =
        "SELECT h.hsp_nombre_hospital,tmp1.especialidad,tmp1.trn_hspcat_id,tmp1.trn_hsp_id from _hsp_hospitales h, (SELECT btrim(split_part(esp_desc_especialidad,\$\$-\$\$,2),\$\$ \$\$) as especialidad,trn_hspcat_id,trn_hsp_id FROM _hsp_srv_especialidad ,(select trn_hsp_id ,trn_hspcat_id from _HSP_TURNOS inner join _hsp_srv_especialidad on trn_hspcat_id = esp_id and trn_estado = \$\$A\$\$ and esp_estado = \$\$A\$\$ WHERE TRN_FECHA::DATE between \$\$ $fechaInicio\$\$ and \$\$ $fechaFin\$\$ group by trn_hsp_id,trn_hspcat_id order by trn_hsp_id )as tmp1 where tmp1.trn_hspcat_id = esp_id order by trn_hsp_id,especialidad )as tmp1 where tmp1.trn_hsp_id = h.hsp_id order by h.hsp_id ,tmp1.especialidad";
    var resp_ = await getResponseDinamico(query);

    if (resp_.length != 0) {
      for (var entry in resp_) {
        setState(() {
          resp.add(new HospitalEspecialidades(
              entry['hsp_nombre_hospital'],
              entry['especialidad'],
              entry['trn_hspcat_id'],
              entry['trn_hsp_id']));
        });
      }
    } else {
      showInSnackBar("No Existen datos. No se pudo obtener las especialidades");
    }

    return resp;
  }

  Future<List<IndicadorEspecialidad>> indicadoresEspecialidad(int hospital_id,
      int hspcat_id, DateTime fechaDesde, DateTime fechaHasta) async {
    var fechaInicio = fechaDesde.toString().split(' ')[0];
    var fechaFin = fechaHasta.toString().split(' ')[0];
    List<IndicadorEspecialidad> res_ = [];

    var query =
        "select * from sp_lst_indicadores_estructura_dinamico($hospital_id,$hspcat_id, \$\$$fechaInicio\$\$,\$\$$fechaFin\$\$)";

    var resp_ = await getResponseDinamico(query);
    if (resp_.length != 0) {
      for (var entry in resp_) {
        setState(() {
          res_.add(new IndicadorEspecialidad(
              entry['medico'],
              converteList(entry['fechas']),
              converteList(entry['fichas_planificadas']),
              converteList(entry['fechas_atendidads']),
              int.parse(entry['vcontenedor']),
              converteList(entry['vfichas_solicitadas'])));
        });
      }
    } else {
      showInSnackBar("No Existen datos. Seleccione otra especialidad");
    }
    return res_;
  }

  List<dynamic> converteList(var arrayItem) {
    List<dynamic> res_ = [];
    arrayItem.forEach((var item) {
      res_.add(item);
    });
    return res_;
  }

  @override
  void dispose() {
    super.dispose();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            bottom: TabBar(
              labelPadding: EdgeInsets.all(0.0),
              tabs: [
                new Tab(
                  text: "H. La Merced",
                ),
                new Tab(
                  text: "H. Los Pinos",
                ),
                new Tab(
                  text: "H. La Portada",
                ),
                new Tab(
                  text: "H Cotahuma",
                )
              ],
            ),
            flexibleSpace: Theme.ColorsSiis.AppBarColor(),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.autorenew),
                tooltip: 'Actualizar',
                onPressed: () {
                  // especialidadesHospitales(_fechaDesde, _fechaHasta);
                },
              ),
            ],
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeConsultaExterna()),
                )
              },
            ),
            title: Text('Reporte Por Especialidad'),
          ),
          body: TabBarView(
            children: [
              // TAB - LA MERCED
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                width: MediaQuery.of(context).size.width * 1.0,
                child: Column(
                  children: <Widget>[
                    new ReporteEspecialidadTab(
                      hospitalId: 1,
                      fechaDesde: _fechaDesde,
                      fechaHasta: _fechaHasta,
                      onTapFechaDesde: (dateTime, List<int> index) {
                        setState(() {
                          _fechaDesde = dateTime;
                        });
                      },
                      onTapFechaHasta: (dateTime, List<int> index) {
                        setState(() {
                          _fechaHasta = dateTime;
                        });
                      },
                      onPressed: () {
                        clickSearch(
                            _fechaDesde, _fechaHasta, _selectedItemLaMerced, 1);
                      },
                      itemSelectList: itemSelectListLaMerced,
                      changeDropdownItem: (ItemSelect item) {
                        setState(() {
                          _selectedItemLaMerced = item;
                        });
                      },
                      selectedItem: _selectedItemLaMerced,
                    ),
                    viewCardListLaMerced
                        ? new Expanded(
                            child: ListViewReporteEspecialidad(
                            listIndicadorEspecialidades:
                                indicadorEspecialidadLaMerced,
                          ))
                        : Container(),
                  ],
                ),
              ),
              // TAB - LA MERCED - FIN

              // TAB - LOS PINOS
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                width: MediaQuery.of(context).size.width * 1.0,
                child: Column(
                  children: <Widget>[
                    new ReporteEspecialidadTab(
                      hospitalId: 2,
                      fechaDesde: _fechaDesde,
                      fechaHasta: _fechaHasta,
                      onTapFechaDesde: (dateTime, List<int> index) {
                        setState(() {
                          _fechaDesde = dateTime;
                        });
                      },
                      onTapFechaHasta: (dateTime, List<int> index) {
                        setState(() {
                          _fechaHasta = dateTime;
                        });
                      },
                      onPressed: () {
                        clickSearch(
                            _fechaDesde, _fechaHasta, _selectedItemLosPinos, 2);
                      },
                      itemSelectList: itemSelectListLosPinos,
                      changeDropdownItem: (ItemSelect item) {
                        setState(() {
                          _selectedItemLosPinos = item;
                        });
                      },
                      selectedItem: _selectedItemLosPinos,
                    ),
                    viewCardListLosPinos
                        ? new Expanded(
                            child: new ListViewReporteEspecialidad(
                            listIndicadorEspecialidades:
                                indicadorEspecialidadLosPinos,
                          ))
                        : Container(),
                  ],
                ),
              ),
              // TAB - LOS PINOS -  FIN

              // TAB - LA PORTADA
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                width: MediaQuery.of(context).size.width * 1.0,
                child: Column(
                  children: <Widget>[
                    new ReporteEspecialidadTab(
                      hospitalId: 3,
                      fechaDesde: _fechaDesde,
                      fechaHasta: _fechaHasta,
                      onTapFechaDesde: (dateTime, List<int> index) {
                        setState(() {
                          _fechaDesde = dateTime;
                        });
                      },
                      onTapFechaHasta: (dateTime, List<int> index) {
                        setState(() {
                          _fechaHasta = dateTime;
                        });
                      },
                      onPressed: () {
                        clickSearch(_fechaDesde, _fechaHasta,
                            _selectedItemLaPortada, 3);
                      },
                      itemSelectList: itemSelectListLaportada,
                      changeDropdownItem: (ItemSelect item) {
                        setState(() {
                          _selectedItemLaPortada = item;
                        });
                      },
                      selectedItem: _selectedItemLaPortada,
                    ),
                    viewCardListLaPortada
                        ? new Expanded(
                            child: new ListViewReporteEspecialidad(
                            listIndicadorEspecialidades:
                                indicadorEspecialidadLaPortada,
                          ))
                        : Container(),
                  ],
                ),
              ),
              // TAB - LA PORTADA - FIN

              //TAB - COTAHUMA
              Container(
                decoration: Theme.ColorsSiis.FondoImagen(),
                width: MediaQuery.of(context).size.width * 1.0,
                child: Column(
                  children: <Widget>[
                    new ReporteEspecialidadTab(
                      hospitalId: 5,
                      fechaDesde: _fechaDesde,
                      fechaHasta: _fechaHasta,
                      onTapFechaDesde: (dateTime, List<int> index) {
                        setState(() {
                          _fechaDesde = dateTime;
                        });
                      },
                      onTapFechaHasta: (dateTime, List<int> index) {
                        setState(() {
                          _fechaHasta = dateTime;
                        });
                      },
                      onPressed: () {
                        clickSearch(
                            _fechaDesde, _fechaHasta, _selectedItemCotahuma, 5);
                      },
                      itemSelectList: itemSelectListCotahuma,
                      changeDropdownItem: (ItemSelect item) {
                        setState(() {
                          _selectedItemCotahuma = item;
                        });
                      },
                      selectedItem: _selectedItemCotahuma,
                    ),
                    viewCardListCotahuma
                        ? new Expanded(
                            child: new ListViewReporteEspecialidad(
                            listIndicadorEspecialidades:
                                indicadorEspecialidadCotahuma,
                          ))
                        : new Container(),
                  ],
                ),
              ),
              //TAB - COTAHUMA - FIN
            ],
          ),
        ),
      ),
    );
  }
}

class Hospital {
  final int value;
  final String text;
  final int hosp_id;
  final bool checked;

  Hospital(this.value, this.text, this.hosp_id, this.checked);
}

class IndicadorEspecialidad {
  final String medico;
  final List<dynamic> fechas; //array
  final List<dynamic> fichas_planificadas;
  final List<dynamic> fechas_atendidads;
  final int vcontenedor;
  final List<dynamic> vfichas_solicitadas;
  IndicadorEspecialidad(this.medico, this.fechas, this.fichas_planificadas,
      this.fechas_atendidads, this.vcontenedor, this.vfichas_solicitadas);

  @override
  String toString() {
    return 'IndicadorEspecialidad{medico: $medico, fechas: $fechas, fichas_planificadas: $fichas_planificadas, fechas_atendidads: $fechas_atendidads, vcontenedor: $vcontenedor, vfichas_solicitadas: $vfichas_solicitadas}';
  }
}
