import 'package:flutter/foundation.dart';
class Turno {
  final int prog_tur_id;
  final String prog_tur_fecha_literal;
  final String prog_tur_fecha;
  final int prs_id;
  final String prs_ci;
  final String prs_nombres;
  final String prs_paterno;
  final String prs_materno;
  final String prs_celular;
  final String prs_telefono;
  final String rol_servicio;
  final int prog_tur_servicio_turno_id;


  Turno({
    @required this.prog_tur_id,
    @required this.prog_tur_fecha_literal,
    @required this.prog_tur_fecha,
    @required this.prs_id,
    @required this.prs_ci,
    @required this.prs_nombres,
    @required this.prs_paterno,
    @required this.prs_materno,
    @required this.prs_celular,
    @required this.prs_telefono,
    @required this.rol_servicio,
    @required this.prog_tur_servicio_turno_id
  });

  factory Turno.fromJson(Map<dynamic, dynamic> json) {
    return Turno(
      prog_tur_id: json['prog_tur_id'] as int,
      prog_tur_fecha_literal: json['prog_tur_fecha_literal'] as String,
      prog_tur_fecha: json['prog_tur_fecha'] as String,
      prs_id: json['prs_id'] as int,
      prs_ci: json['prs_ci'] as String,
      prs_nombres: json['prs_nombres'] as String,
      prs_paterno: json['prs_paterno'] as String,
      prs_materno: json['prs_materno'] as String,
      prs_celular: json['prs_celular'] as String,
      prs_telefono: json['prs_telefono'] as String,
      rol_servicio: json['rol_servicio'] as String,
      prog_tur_servicio_turno_id: json['prog_tur_servicio_turno_id'] as int,

    );
  }




}
