import 'package:flutter/foundation.dart';
class Personal {
  final int prs_id;
  final String prs_ci;
  final String prs_nombres;
  final String prs_paterno;
  final String prs_materno;
  final String prs_celular;



  Personal({
    @required this.prs_id,
    @required this.prs_ci,
    @required this.prs_nombres,
    @required this.prs_paterno,
    @required this.prs_materno,
    @required this.prs_celular
});

  factory Personal.fromJson(Map<dynamic, dynamic> json) {
    return Personal(
      prs_id: json['prs_id'] as int,
      prs_ci: json['prs_ci'] as String,
      prs_nombres: json['prs_nombres'] as String,
      prs_paterno: json['prs_paterno'] as String,
      prs_materno: json['prs_materno'] as String,
      prs_celular: json['prs_celular'] as String,
    );
  }

  @override
  String toString() {
    return 'Personal{prs_id: $prs_id, prs_ci: $prs_ci, prs_nombres: $prs_nombres, prs_paterno: $prs_paterno, prs_materno: $prs_materno, prs_celular: $prs_celular}';
  }


}
