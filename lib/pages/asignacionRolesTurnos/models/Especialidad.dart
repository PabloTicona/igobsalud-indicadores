import 'package:flutter/foundation.dart';
class Especialidad {
  final int rol_id;
  final String rol_servicio;




  Especialidad({
    @required this.rol_id,
    @required this.rol_servicio,

  });

  factory Especialidad.fromJson(Map<dynamic, dynamic> json) {
    return Especialidad(
      rol_id: json['rol_id'] as int,
      rol_servicio: json['rol_servicio'] as String,
    );
  }

  @override
  String toString() {
    return 'Especialidad{rol_id: $rol_id, rol_servicio: $rol_servicio}';
  }


}
