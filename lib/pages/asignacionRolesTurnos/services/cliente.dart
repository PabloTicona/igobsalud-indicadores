import 'dart:convert';

import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Especialidad.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Personal.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Turno.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Cama.dart';
import 'package:igob_salud_app/pages/hospitalizacion/model/Fecha.dart';
import 'package:igob_salud_app/web-services/services.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Personal.dart';

class Cliente {
  Future<List<Personal>> BuscarNombre(int hospitalId, String nombre) async {
    var query =
        "select prs_id,prs_ci,prs_nombres,prs_paterno,prs_materno,prs_celular,prs_telefono from _bp_personas where prs_hsp_id = $hospitalId and prs_estado = \$\$A\$\$ and prs_nombres ilike \$\$%$nombre%\$\$";
    var resp_ = await getResponseDinamico(query);
    List<dynamic> body = resp_;
    List<Personal> result = body
        .map(
          (dynamic item) => Personal.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<List<Especialidad>> getEspecialidades(int hospitalId) async {
    var query =
        "select rol_id,rol_servicio from internaciones._int_rol_turnos where rol_tur_estado = \$\$A\$\$ and rol_hospital_id = $hospitalId";
    var resp_ = await getResponseDinamico(query);

    List<dynamic> body = resp_;
    List<Especialidad> result = body
        .map(
          (dynamic item) => Especialidad.fromJson(item),
        )
        .toList();
    return result;
  }

  Future<bool> storeEspecialidad(int hospitalId, String especialidad) async {
    var query =
        "select * from internaciones.sp_creacion_de_turnos(0,5,\$\$$especialidad\$\$,$hospitalId,\$\$I\$\$)";
    try{
      var resp_ = await getResponseDinamico(query);

      return true;
    }catch(e){
      return false;
    }

  }

  Future<bool> insertProgramacionRolesTurnos(
      Personal personal,
      Especialidad selectedItem,
      String diasSeleccionados,
      DateTime fechaDesde,
      DateTime fechaHasta) async {
    var diasSemana = diasSeleccionados;// json.decode(diasSeleccionados.toString());
    var query =
        'select * from internaciones.sp_abm_turnos_servicios(\$\$$diasSemana\$\$,\$\$2019-12-26\$\$,\$\$2020-01-09\$\$,5,1901, 9,1 )';
    try {
      var resp_ = await getResponseDinamico(query);
      List<dynamic> body = resp_;
      List<Especialidad> result = body
          .map(
            (dynamic item) => Especialidad.fromJson(item),
          )
          .toList();
      return true;
    } catch (e) {
      return false;
    }
  }


  Future<List<Turno>> listarRolesTurnos(int hospitalId, DateTime fecha_) async {


var fecha= fecha_.toString().split(" ")[0];
print("fecha ${fecha}");
print("hospitalId ${hospitalId}");


    var query ="select prog_tur_id,prog_tur_fecha_literal,prog_tur_fecha::date,prs_id, prs_ci, prs_nombres, prs_paterno, prs_materno, prs_celular, prs_telefono ,rol_servicio,prog_tur_servicio_turno_id from internaciones._int_programacion_turnos_servicios inner join _bp_personas on prs_id = prog_tur_medico_id inner join internaciones._int_rol_turnos on rol_id = prog_tur_servicio_turno_id where prog_tur_estado = \$\$A\$\$ and prog_tur_hospital_id = $hospitalId and prog_tur_fecha::date = \$\$$fecha\$\$";
    var resp_ = await getResponseDinamico(query);
print("resp_ $resp_");
    List<dynamic> body = resp_;
    List<Turno> result = body
        .map(
          (dynamic item) => Turno.fromJson(item),
    )
        .toList();
    return result;
  }

}
