import 'package:flutter/material.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Personal.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

typedef void OnClickItem(Personal item);

class ItemListView extends StatelessWidget {
  Personal personal;
  final OnClickItem changeDropdownItem;

  ItemListView({this.personal, this.changeDropdownItem});


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      // color: Colors.amber[colorCodes[index]],
      child: Material(
        borderRadius: BorderRadius.circular(5.0),
        child: InkWell(
          onTap: () {
            changeDropdownItem(personal);
          },
          borderRadius: BorderRadius.circular(5.0),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${personal.prs_nombres} ${personal.prs_paterno} ${personal.prs_materno}",
                      style: TextStyle(
                          fontSize: 10.0, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
//  height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "${personal.prs_ci}",
                            style: TextStyle(fontSize: 9.0),
                          ),
                          Container(
                            child: (personal.prs_celular != "")
                                ? Row(
                              children: <Widget>[
                                Icon(
                                  MdiIcons.phone,
                                  color: Colors.black,
                                  size: 15.0,
                                ),
                                Text(
                                  "${personal.prs_celular}",
                                  style: TextStyle(fontSize: 9.0),
                                )
                              ],
                            )
                                : Text(""),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Icon(
                  MdiIcons.menuRightOutline,
                  color: Colors.black26,
                  size: 25.0,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
