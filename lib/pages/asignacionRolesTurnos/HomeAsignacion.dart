import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/ButtonSaveCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/Especialidades.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/NuevoRolTurno.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Especialidad.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Turno.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/services/cliente.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:igob_salud_app/pages/HomePage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class HomeAsignacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Asignación',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ConsultaExterna(),
    );
  }
}

class ConsultaExterna extends StatefulWidget {


  @override
  _ConsultaExternaState createState() => _ConsultaExternaState();
}

class _ConsultaExternaState extends State<ConsultaExterna> {
  @override
  bool dialVisible = true;
  bool viewLoader = false;
  String especialidad;
  String _value = 'one';
  List<HospitalEspecialidades> listaHospitales;
  List<Turno> listRolesTurnos;
  List<DropdownMenuItem<HospitalEspecialidades>> _dropdownMenuItems;
  HospitalEspecialidades selectedItem;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int hospitalId_;
  DateTime _fechaDesde = DateTime.now();
  onChangeHospital(String value) {
    print("value:: $value");

    setState(() {
      _value = value;
      hospitalId_ = int.parse(value);
    });
  }

  void initState() {
    getDataHospitales();
  }

  Future<void> getDataHospitales() async {
    setState(() {
      listaHospitales = [];
    });

    listaHospitales.add(new HospitalEspecialidades(
        "Hospital La Merced", "Hospital La Merced", 1, 1));
    listaHospitales.add(new HospitalEspecialidades(
        "Hospital Los Pinos", "Hospital Los Pinos", 2, 2));
    listaHospitales.add(new HospitalEspecialidades(
        "Hospital La Portada", "Hospital La Portada", 3, 3));
    listaHospitales.add(new HospitalEspecialidades(
        "Hospital Cotahuma", "Hospital Cotahuma", 5, 5));

    setState(() {
      _dropdownMenuItems = buildDropdownMenuItems(listaHospitales);
    });
  }

  final List<String> hospitales = <String>[
    'Hospital Los Pinos',
    'Hospital La Portada',
    'Hospital Cotahuma',
    'Hospital La Merced'
  ];

  List<DropdownMenuItem<String>> listHospitales = [
    new DropdownMenuItem(
      child: new Text('> SELECCIONES UN HOSPITAL'),
      value: 'one',
    ),
    new DropdownMenuItem(
      child: new Text('Hospital Los Pinos'),
      value: '2',
    ),
    new DropdownMenuItem(
      child: new Text('Hospital La Portada'),
      value: '3',
    ),
    new DropdownMenuItem(
      child: new Text('Hospital Cotahuma'),
      value: '5',
    ),
    new DropdownMenuItem(
      child: new Text('Hospital La Merced'),
      value: '1',
    ),
  ];

  List<DropdownMenuItem<HospitalEspecialidades>> buildDropdownMenuItems(
      List items_) {
    List<DropdownMenuItem<HospitalEspecialidades>> items = List();
    if (items_ != null) {
      for (HospitalEspecialidades itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.hsp_nombre_hospital),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  onPressed() {
    print("btn buscar ${selectedItem} ");
  }

  SelectFloatingAction(String value) {
    print("value 1122  $value");

    if (hospitalId_ != null) {
      if (value == "especialidad") {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Especialidades(
                    hospitalId: hospitalId_,
                  )),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NuevoRolTurno(
                    hospitalId: hospitalId_,
                  )),
        );
      }
    } else {
      showInSnackBar("Seleccione Un Hospital.");
    }

    print("hospitalId_ :: ${hospitalId_}");
  }

  onPressedGuardar() {
    final form = _formKey.currentState;
    form.save();
    print("btn guardar ");

    if (form.validate()) {
      FocusScope.of(context).requestFocus(FocusNode());

      print("selectedItem ${selectedItem}");

      var especialidad_ = especialidad.toString().toLowerCase().trim();
      print("especialidad_ $especialidad_");

//select * from internaciones.sp_creacion_de_turnos(0,5,$$Emergencias$$,1,$$I$$)

    }
  }

  Widget _offsetPopup() => PopupMenuButton<int>(
        padding: EdgeInsets.all(0.0),
        tooltip: 'Nuevo',
        onSelected: (value) {
          switch (value) {
            case 2:
              // cerrarSession();
              break;

            case 3:
              //exit(0);
              break;

            default:
          }

          print("value :: ${value}");
        },
        itemBuilder: (context) => [
          PopupMenuItem(
            value: 2,
            child: Row(
              children: <Widget>[
                // Icon( MdiIcons.plus, ),
                Text("Nuevo turno")
              ],
            ),
          ),
          PopupMenuItem(
            value: 3,
            child: Row(
              children: <Widget>[
                // Icon( MdiIcons.plus,),
                Text("Nueva Especialidad")
              ],
            ),
          ),
        ],
        elevation: 10.0,
        icon: Icon(
          Icons.add,
          color: Colors.white,
          size: 32,
        ),
//          offset: Offset(0, 100),
      );

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 3),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      /*   appBar: AppBar(
        title: Text(
          "Asignación ",
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                )
              },
        ),
        actions: <Widget>[_offsetPopup()],
      ),*/

      appBar: new AppBar(
        title: new prefix0.Theme(
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton<String>(
              style: TextStyle(color: prefix0.Colors.white),
              iconDisabledColor: Colors.white,
              iconEnabledColor: Colors.white,
              value: _value,
              isExpanded: true,
              items: listHospitales,
              onChanged: onChangeHospital,
            ),
          ),
          // isMaterialAppTheme: true,
          data: prefix0.Theme.of(context).copyWith(
            canvasColor: Colors.red,
          ),
        ),
      ),
      body: Center(
          child: Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new CardDatePickerComponent(
                          nombre: 'Seleccione una fecha : ',
                          fechaInicio: _fechaDesde,
                          formatoFecha: '',
                          formatoFechaComponete: 'dd-MMMM-yyyy',
                          onTap: (dateTime, List<int> index) {
                            setState(() {
                              _fechaDesde = dateTime;
                            });
                          }),
                      new ButtonMaterialCircleComponent(
                          size: 20.0,
                          onPressed: () {
                            buscarRolesTurnos();

                            print('boton presionado buscar....');
                          }),
                    ],
                  ),
                ),
              ),
            ),
            viewLoader
                ? Expanded(
                    child: (listRolesTurnos != null)
                        ? ListView.separated(
                            padding: const EdgeInsets.all(0),
                            itemCount: listRolesTurnos.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                height: 140,
                                child: Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: Card(
                                    elevation: 8.0,
                                    child: Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.account,
                                                  color: Colors.black),
                                              Text(
                                                  "${listRolesTurnos[index].prs_nombres} ${listRolesTurnos[index].prs_paterno} ${listRolesTurnos[index].prs_materno}")
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text("Cedula de identidad : "),
                                              Text(
                                                  "${listRolesTurnos[index].prs_ci}")
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text("Dia : "),
                                              Text(
                                                  "${listRolesTurnos[index].prog_tur_fecha_literal}")
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text("Fecha : "),
                                              Text(
                                                  "${listRolesTurnos[index].prog_tur_fecha}")
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text("Servicio : "),
                                              Text(
                                                  "${listRolesTurnos[index].rol_servicio}")
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.phone,
                                                  color: Colors.black),
                                              Text(
                                                  "${listRolesTurnos[index].prs_celular}")
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    const Divider(),
                          )
                        : Container(
                            height: 100,
                            child: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: Card(
                                child: Center(
                                  child: SpinKitWave(
                                      color: Colors.redAccent,
                                      type: SpinKitWaveType.center),
                                ),
                              ),
                            ),
                          ),
                  )
                : Container(),
          ],
        ),
      )),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        child: Icon(Icons.map, color: Colors.red),
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        visible: dialVisible,
        backgroundColor: Colors.redAccent,
        curve: Curves.bounceIn,
        children: [
          SpeedDialChild(
            child: new Icon(MdiIcons.plus, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('nuevaProgramacion'),
            label: 'Programación De Rol y Turno',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
          SpeedDialChild(
            child: Icon(MdiIcons.plus, color: Colors.white),
            backgroundColor: Colors.amber,
            onTap: () => SelectFloatingAction('especialidad'),
            label: 'Especialidad',
            labelStyle:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
            labelBackgroundColor: Colors.amber,
          ),
        ],
      ),
    );
  }

  void buscarRolesTurnos() async {
    if (hospitalId_ != null) {
      setState(() {
        listRolesTurnos = null;
        viewLoader = true;
      });

      var listRoles =
          await Cliente().listarRolesTurnos(hospitalId_, _fechaDesde);

      if (listRoles.length != 0) {
        setState(() {
          listRolesTurnos = listRoles;
        });
      } else {
        setState(() {
          viewLoader = false;
        });
        showInSnackBar("Sin datos");
      }
    } else {
      showInSnackBar("Seleccione Un Hospital.");
    }
  }
}
