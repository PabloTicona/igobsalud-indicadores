import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/ButtonSaveCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/HomeAsignacion.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Especialidad.dart';

import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Personal.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/services/cliente.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/widget/ItemListView.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/widget/ItemView.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class NuevoRolTurno extends StatelessWidget {
  final int hospitalId;

  const NuevoRolTurno({Key key, this.hospitalId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIIS',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: NuevoRolTurnos(
        hospital_id: hospitalId,
      ),
    );
  }
}

class NuevoRolTurnos extends StatefulWidget {
  final int hospital_id;
  NuevoRolTurnos({this.hospital_id});

  @override
  _NuevoRolTurnosState createState() => _NuevoRolTurnosState();
}

class _NuevoRolTurnosState extends State<NuevoRolTurnos> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController editingController = TextEditingController();
  List<Personal> listPersonal;
  List<DropdownMenuItem<Especialidad>> _dropdownMenuItems;
  Especialidad selectedItem;
  List<Especialidad> listEspecialidades;
  Personal personal;

  DateTime _fechaDesde = DateTime.now();
  DateTime _fechaHasta = DateTime.now();

  bool lunes = false;
  bool martes = false;
  bool miercoles = false;
  bool jueves = false;
  bool viernes = false;
  bool sabado = false;
  bool domingo = false;

  int hospital_id_;

  var diasSeleccionados;

  @override
  void initState() {
    super.initState();
    hospital_id_ = widget.hospital_id;

    print("hospital_id_ $hospital_id_");

    getEspecialidades(hospital_id_);
  }

  Future<void> getEspecialidades(int especialidadId) async {
    var listEspecialidades_ = await Cliente().getEspecialidades(especialidadId);
    listEspecialidades_.insert(
        0,
        new Especialidad(
            rol_id: 0, rol_servicio: "-- Seleccione Especialidad --"));

    setState(() {
      listEspecialidades = listEspecialidades_;
      _dropdownMenuItems = buildDropdownMenuItems(listEspecialidades);
    });
  }

  selecPersonalItem(Personal personal_) {
    editingController.clear();

    setState(() {
      personal = personal_;
      listPersonal = null;
    });

    print("personal ${personal_.toString()}");
  }

  Future<void> filterSearchResults(String query) async {
    var listPersonal_;

    if (query.length > 2) {
      listPersonal_ = await Cliente().BuscarNombre(hospital_id_, query);
      setState(() {
        listPersonal = listPersonal_;
      });
    } else {
      setState(() {
        //  listPersonal = [];
        listPersonal = null;
      });
    }
  }

  List<DropdownMenuItem<Especialidad>> buildDropdownMenuItems(List items_) {
    List<DropdownMenuItem<Especialidad>> items = List();

    if (items_ != null) {
      for (Especialidad itemSelect in items_) {
        items.add(
          DropdownMenuItem(
            value: itemSelect,
            child: Text(itemSelect.rol_servicio),
          ),
        );
      }
    } else {
      return null;
    }
    return items;
  }

  changeDropdownItem(Especialidad especialidad_) {
    setState(() {
      selectedItem = especialidad_;
    });

    print("especialidad ${especialidad_.toString()}");
  }

  guardarProgramacion(DateTime fechaDesde, DateTime fechaHasta) async {
    print("fechaDesde ${fechaDesde.toString()}");
    print("fechaHasta ${fechaHasta.toString()}");

    print("selectedItem  ${selectedItem.toString()}");

    // print("editingController ${editingController.text}");

    print("personal ${personal}");
    if (personal == null) {
      showInSnackBar("Seleccione un personal");
    } else if (selectedItem == null) {
      showInSnackBar("Seleccione una especialidad");
    } else if (diasSeleccionados == null) {
      showInSnackBar("Seleccione al menos un día de la semana");
    } else {
      var resp_ = await Cliente().insertProgramacionRolesTurnos(
          personal, selectedItem, diasSeleccionados, fechaDesde, fechaHasta);
      if (resp_) {
        showInSnackBar("Registrado");
      } else {
        showInSnackBar("ERROR: al guardar los datos.");
      }
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  Widget checkbox(String title, bool boolValue) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: prefix0.TextStyle(fontSize: 12.0),
        ),
        Checkbox(
          activeColor: Colors.red,
          value: boolValue,
          onChanged: (bool value) {
            setState(() {
              switch (title) {
                case "Lunes":
                  lunes = value;
                  break;
                case "Martes":
                  martes = value;
                  break;
                case "Miercoles":
                  miercoles = value;
                  break;
                case "Jueves":
                  jueves = value;
                  break;
                case "Viernes":
                  viernes = value;
                  break;
                case "Sabado":
                  sabado = value;
                  break;
                case "Domingo":
                  domingo = value;
                  break;
              }
            });

            diasSeleccionados =
                '{"DOMINGO":$domingo,"LUNES":$lunes,"MARTES":$martes,"MIERCOLES":$miercoles,"JUEVES":$jueves,"VIERNES":$viernes,"SABADO":$sabado}';
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Programación de Rol y Turno",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeAsignacion()),
            );
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        actions: <Widget>[
          /*  IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ), */
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new ListView(
          children: <Widget>[
            Card(
              elevation: 8.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Column(
                children: <Widget>[
                  prefix0.Container(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: TextField(
                        onChanged: (value) {
                          filterSearchResults(value);
                        },
                        controller: editingController,
                        decoration: InputDecoration(
                            hoverColor: Colors.red,
                            labelText: "Buscar Personal",
                            //  hintText: "Buscar",
                            prefixIcon: Icon(Icons.search),
                            focusColor: Colors.red,
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)))),
                      ),
                    ),
                    height: 80,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 1.0,
                    height: (listPersonal != null) ? 300 : 0,
                    child: (listPersonal != null)
                        ? ListView.separated(
                            padding: const EdgeInsets.all(5),
                            itemCount: listPersonal.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ItemListView(
                                personal: listPersonal[index],
                                changeDropdownItem: selecPersonalItem,
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    const Divider(),
                          )
                        : Container(),
                  )
                ],
              ),
            ),
            // Listar

            Container(
              child: (personal != null)
                  ? Card(
                      elevation: 8.0,
                      child: ItemView(
                        personal: personal,
                        changeDropdownItem: (Personal personal_) {
                          setState(() {
                            personal = null;
                          });

                          print("personal delete :: ${personal_.toString()}");
                        },
                      ),
                    )
                  : Container(),
            ),

            Container(
              width: MediaQuery.of(context).size.width * 0.78,
              height: 83,
              child: SizedBox(
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Card(
                    elevation: 8.0,
                    child: Padding(
                      padding: EdgeInsets.all(12.0),
                      child: (listEspecialidades != null)
                          ? new DropdownButton(
                              isExpanded: true,
                              value: (selectedItem != null)
                                  ? selectedItem
                                  : listEspecialidades[0],
                              items: _dropdownMenuItems,
                              onChanged:
                                  changeDropdownItem, //changeDropdownItem,
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.black),
                              underline: Container(
                                height: 2,
                                color: Colors.red,
                              ),
                            )
                          : Center(
                              child: Text("Cargando.."),
                            ),
                    ),
                  ),
                ),
              ),
            ),

            Container(
              child: prefix0.Padding(
                padding: prefix0.EdgeInsets.all(0.0),
                child: prefix0.Card(
                  elevation: 8.0,
                  child: prefix0.Padding(
                    padding: prefix0.EdgeInsets.all(12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        checkbox("Lunes", lunes),
                        checkbox("Martes", martes),
                        checkbox("Miercoles", miercoles),
                        checkbox("Jueves", jueves),
                        checkbox("Viernes", viernes),
                        checkbox("Sabado", sabado),
                        checkbox("Domingo", domingo),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new CardDatePickerComponent(
                    nombre: 'Fecha Inicio: ',
                    fechaInicio: _fechaDesde,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaDesde = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new CardDatePickerComponent(
                    nombre: 'Fecha Fin:',
                    fechaInicio: _fechaHasta,
                    formatoFecha: '',
                    formatoFechaComponete: 'dd-MMMM-yyyy',
                    onTap: (dateTime, List<int> index) {
                      setState(() {
                        _fechaHasta = dateTime;
                      });
                      print(' dateTime ${dateTime}');
                    }),
                new ButtonSaveCircleComponent(
                    size: 23.0,
                    onPressed: () {
                      guardarProgramacion(_fechaDesde, _fechaHasta);

                      print('boton presionado');
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
