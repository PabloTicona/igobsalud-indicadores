import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:igob_salud_app/components/ButtonMaterialCircleComponent.dart';
import 'package:igob_salud_app/components/ButtonSaveCircleComponent.dart';
import 'package:igob_salud_app/components/CardDatePickerComponent.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/HomeAsignacion.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Especialidad.dart';

import 'package:igob_salud_app/pages/asignacionRolesTurnos/models/Personal.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/services/cliente.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/widget/ItemListView.dart';
import 'package:igob_salud_app/pages/asignacionRolesTurnos/widget/ItemView.dart';
import 'package:igob_salud_app/pages/emergencias/HomeEmergencias.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:igob_salud_app/style/theme.dart' as Theme;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Especialidades extends StatelessWidget {
  final int hospitalId;

  const Especialidades({Key key, this.hospitalId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIIS',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: EspecialidadesPage(
        hospital_id: hospitalId,
      ),
    );
  }
}

class EspecialidadesPage extends StatefulWidget {
  final int hospital_id;
  EspecialidadesPage({this.hospital_id});

  @override
  EspecialidadesState createState() => EspecialidadesState();
}

class EspecialidadesState extends State<EspecialidadesPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController editingController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String especialidad;
  List<Especialidad> listEspecialidades;

  @override
  void initState() {
    super.initState();
    getEspecialidades();
  }

  Future<List<Especialidad>> getEspecialidades() async {
    var listEsp = await Cliente().getEspecialidades(widget.hospital_id);
    setState(() {
      listEspecialidades = listEsp;
    });
  }

  onPressedGuardar() async {
    final form = _formKey.currentState;
    form.save();
    print("btn guardar ");
    if (form.validate()) {
      FocusScope.of(context).requestFocus(FocusNode());

      var resp =
          await Cliente().storeEspecialidad(widget.hospital_id, especialidad);

      if (resp) {
        showInSnackBar("Se guardo la especialidad");
        getEspecialidades();

        _formKey.currentState.reset();
       // _formKey.currentState.cl

      } else {
        showInSnackBar("Error al guardar la especialidad");
      }

      print("String especialidad; $especialidad");

//select * from internaciones.sp_creacion_de_turnos(0,5,$$Emergencias$$,1,$$I$$)

    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: Duration(seconds: 6),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Especialidades",
          style: TextStyle(color: Colors.white),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeAsignacion()),
            );
          },
        ),
        flexibleSpace: Theme.ColorsSiis.AppBarColor(),
        actions: <Widget>[
          /*  IconButton(
            icon: const Icon(Icons.autorenew),
            tooltip: 'Actualizar',
            onPressed: () {},
          ), */
        ],
      ),
      body: new Container(
        decoration: Theme.ColorsSiis.FondoImagen(),
        child: new Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.78,
                    height: 94,
                    child: SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: Card(
                          elevation: 8.0,
                          child: Padding(
                            padding: EdgeInsets.all(15.0),
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(
                                  fontFamily: "WorkSansSemiBold",
                                  fontSize: 16.0,
                                  color: Colors.black),
                              decoration: InputDecoration(
                                // border: InputBorder.none,
                                fillColor: Colors.red,

                                hintText: "Nueva Especialidad",
                                hintStyle: TextStyle(
                                    fontFamily: "WorkSansSemiBold",
                                    fontSize: 17.0),
                              ),
                              validator: (val) {
                                if (val.length == 0) {
                                  return "Es requerido"; // "La contraseña es requerido";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (str) {
                                especialidad = str;
                                print('str ::  $str');
                                //_user = str;
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.22,
                    child: new ButtonSaveCircleComponent(
                        size: 20.0, onPressed: onPressedGuardar),
                  ),
                ],
              ),
            ),

            Expanded(
            //  width: MediaQuery.of(context).size.width * 1.0,
           //   height: MediaQuery.of(context).size.height * 0.7,
              child: (listEspecialidades != null)
                  ? ListView.separated(
                      padding: const EdgeInsets.all(0.0),
                      itemCount: listEspecialidades.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 60,

                          ///  color: Colors.amber[colorCodes[index]],
                          child: Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Card(
                              elevation: 8.0,
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                        "${listEspecialidades[index].rol_servicio}"),
                                    Icon(
                                      MdiIcons.menuRightOutline,
                                      color: Colors.black26,
                                      size: 25.0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(
                        height: 5.0,
                      ),
                    )
                  : Container(
                      child: Text("Cargando.."),
                    ),
            ),

            // Listar
          ],
        ),
      ),
    );
  }
}
