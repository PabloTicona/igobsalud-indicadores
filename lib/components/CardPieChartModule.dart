import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:charts_flutter/flutter.dart' as charts;

typedef void TapCallback();

class CardPieChartModule extends StatelessWidget {
  final double dataMap;
  final bool toggle;
  final String label;
  final TapCallback onTap;

  CardPieChartModule({this.dataMap, this.toggle, this.label, this.onTap});

  List<Color> colorList = [
    Colors.green,
    Color.fromRGBO(200, 200, 200, 0.5),
  ];

   List<charts.Series<LinearSales, int>> _createSampleData() {
    final data = [

      new LinearSales(2, dataMap, charts.Color.fromHex(code: '#59E45E')),
      new LinearSales(3, 100.0 - dataMap, charts.Color.fromHex(code: '#eff0f1')),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: label,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        colorFn: (LinearSales sales, _) => sales.color,
        data: data,
      )
    ];
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
        width: MediaQuery.of(context).size.width * 0.5,
        height: 210,
        child: Padding(
          padding: EdgeInsets.all(3.0),
          child: Material(
            borderRadius: BorderRadius.circular(5.0),
            elevation: 5.0,
            child: InkWell(
              borderRadius: BorderRadius.circular(5.0),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Text(
                      "${dataMap} %",
                      style:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                    ),

                    Container(
                      width: 170,
                      height: 170,
                      child: toggle
                          ? new charts.PieChart(_createSampleData(),
                          animate: true,
                          behaviors: [
                            new charts.SeriesLegend(
                                position: charts.BehaviorPosition.bottom,
                                horizontalFirst: bool.fromEnvironment("Pablo")),
                          ],
                          // Configure the width of the pie slices to 60px. The remaining space in
                          // the chart will be left as a hole in the center.
                          defaultRenderer: new charts.ArcRendererConfig(arcWidth: 15))
                          : Center(
                              child: SpinKitWave(
                                  color: Colors.redAccent, type: SpinKitWaveType.center),
                            ),
                    ),

                  ],
                ),
              ),
              onTap: onTap,
            ),
          ),
        ));
  }
}

class LinearSales {
  final int year;
  final double sales;
  final charts.Color color;

  LinearSales(this.year, this.sales, this.color);
}
