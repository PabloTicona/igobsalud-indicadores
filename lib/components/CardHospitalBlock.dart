import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class CardHospitalBlock extends StatelessWidget {
  final String hospital;

  final List<OrdinalSales> data;

  CardHospitalBlock({this.hospital, this.data});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    print("data :: ${data}");

    return new Container(
      child: Expanded(
        child: Padding(
          padding: EdgeInsets.all(4.0),
          child: Material(
            borderRadius: BorderRadius.circular(8.0),
            elevation: 5.0,
            child: InkWell(
              borderRadius: BorderRadius.circular(8.0),
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.local_hospital),
                          Text(
                            hospital,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 19, fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: (data.length != 0)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("-Fichas Planificadas : "),
                                    Text(
                                      "${data.elementAt(0).sales}",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),
                                Divider(
                                  height: 2,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("-Fichas Solicitadas  : "),
                                    Text(
                                      "${data.elementAt(1).sales}",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),
                                Divider(
                                  height: 2,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("-Fichas Atendidas   : "),
                                    Text(
                                      "${(data.length > 2) ? data.elementAt(2).sales : '0'}",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),
                                Divider(
                                  height: 2,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("-Solicitadas   : "),
                                    Text(
                                      "${(data.length > 2) ? ((data.elementAt(1).sales / data.elementAt(0).sales) * 100).toStringAsFixed(2) : '0'}%",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),
                                Divider(
                                  height: 2,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("-Atendidas   : "),
                                    Text(
                                      "${(data.length > 2) ? ((data.elementAt(2).sales / data.elementAt(0).sales) * 100).toStringAsFixed(2) : '0'}%",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),

                                // Text("- Solicitadas : ${(data.length>2)? ((data.elementAt(1).sales/data.elementAt(0).sales)*100).toStringAsFixed(2):'0' } %"),
                                //  Text("- Atendidas : ${(data.length>2)?((data.elementAt(2).sales/data.elementAt(0).sales)*100).toStringAsFixed(2):'0'} %"),
                              ],
                            )
                          : Center(
                        child: SpinKitWave(
                            color: Colors.redAccent, type: SpinKitWaveType.center),
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {},
            ),
          ),
        ),
      ),
    );
  }
}

class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);

  @override
  String toString() {
    return 'OrdinalSales{year: $year, sales: $sales}';
  }
}
