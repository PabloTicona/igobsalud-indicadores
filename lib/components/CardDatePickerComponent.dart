import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

typedef void TapCallback(dateTime, List<int> index);

class CardDatePickerComponent extends StatelessWidget {
  final String nombre;
  final DateTime fechaInicio;
  final String formatoFecha;
  final String formatoFechaComponete;
  final TapCallback onTap;

  const CardDatePickerComponent(
      {this.nombre,
      this.fechaInicio,
      this.formatoFecha,
      this.formatoFechaComponete,
      this.onTap});

  String formatDateToday(DateTime dateTime, String tipo) {
    String _res = 'Error Fecha';
    var fechaHoyArray = (dateTime.toString().split(' ')[0]).split('-');
    switch (tipo) {
      case 'A':
        _res = '${fechaHoyArray[0]}';
        break;
      case 'M':
        _res = '${fechaHoyArray[1]}';
        break;
      default: _res = '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';
      break;

    }
    var fechahoy =
        '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';
    return _res;
  }

  String funcionPrueba() {
    return 'hola';
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Expanded(
      child: Padding(padding: EdgeInsets.all(4.0),

      child: Material(
          borderRadius: BorderRadius.circular(5.0),
          elevation: 5.0,
          child: new InkWell(

            borderRadius: BorderRadius.circular(5.0),
            onTap: () {
              DatePicker.showDatePicker(
                context,
                pickerTheme: DateTimePickerTheme(
                  showTitle: true,
                  confirm:
                  Text('Confirmar', style: TextStyle(color: Colors.blue)),
                  cancel:
                  Text('Cancelar', style: TextStyle(color: Colors.red)),
                ),
                initialDateTime: fechaInicio,
                dateFormat: formatoFechaComponete,
                locale: DateTimePickerLocale.es,
                onClose: () {
                 // print("----- onClose -----");
                },
                onCancel: () {
                //  print('onCancel');
                },
                onChange: (dateTime, List<int> index) {
                  return "fwef";

                  print(dateTime);
                },
                onConfirm: onTap,
              );
              print("click card 11");
            },
            child: Padding(padding: EdgeInsets.all(8.0),

            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[

                    Text(nombre),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(
                      MdiIcons.calendarMonth,
                      color: Colors.green,
                      size: 16.0,
                    ),
                    Text(
                      formatDateToday(fechaInicio, formatoFecha),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          decoration: TextDecoration.underline,
                          decorationColor: Colors.green,
                          decorationStyle: TextDecorationStyle.dashed
                      ),
                    ),

                  ],
                )
              ],
            ),
            )
          ),
      ),
      ),
    ));
  }
}
