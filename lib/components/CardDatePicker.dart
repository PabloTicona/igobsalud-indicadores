import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';


class CardDatePicker extends StatelessWidget {
  final String nombre;
  final DateTime fechaInicio;
  final String formatoFecha;
  final String formatoFechaComponete;

  const CardDatePicker(
      {this.nombre,
        this.fechaInicio,
        this.formatoFecha,
        this.formatoFechaComponete}
        );

  String formatDateToday(DateTime dateTime, String tipo) {
    String _res = 'Error Fecha';
    var fechaHoyArray = (dateTime.toString().split(' ')[0]).split('-');
    switch (tipo) {
      case 'A':
        _res = '${fechaHoyArray[0]}';
        break;
      case 'M':
        _res = '${fechaHoyArray[1]}';
        break;
    }
    var fechahoy =
        '${fechaHoyArray[2]}-${fechaHoyArray[1]}-${fechaHoyArray[0]}';
    return _res;
  }

  String funcionPrueba(){
    return 'hola';
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(

        child: Expanded(child: Card(
            elevation: 5.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: new GestureDetector(
                onTap: () {
                  DatePicker.showDatePicker(
                    context,
                    pickerTheme: DateTimePickerTheme(
                      showTitle: true,
                      confirm:
                      Text('Confirmar', style: TextStyle(color: Colors.blue)),
                      cancel:
                      Text('Cancelar', style: TextStyle(color: Colors.red)),
                    ),
                    initialDateTime: fechaInicio,
                    dateFormat: formatoFechaComponete,
                    locale: DateTimePickerLocale.es,
                    onClose: () {
                      print("----- onClose -----");
                    },
                    onCancel: () {
                      print('onCancel');
                    },
                    onChange: (dateTime, List<int> index) {

                     // return "fwef";

                      print('onChange');
                    },
                    onConfirm: (dateTime, List<int> index) {
                      //
                    },
                  );

                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.calendar_today,
                          color: Colors.pink,
                          size: 20.0,
                        ),
                        Text(nombre),
                      ],
                    ),
                    Text(
                      formatDateToday(fechaInicio, formatoFecha),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          decoration: TextDecoration.underline,
                          decorationColor: Colors.blue,
                          decorationStyle: TextDecorationStyle.dashed),
                    ),
                  ],
                ),
              ),
            )
        ),
        )
    );
  }
}