import 'package:flutter/material.dart';

typedef void SubmitCallback();
class CardFormLogin extends StatelessWidget {

  final SubmitCallback btnSubmit;
  CardFormLogin({this.btnSubmit});


  String _user, _pass;
final _formKey = GlobalKey<FormState>();

  String _password;
  String _email;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
     // width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 8.0,
        color: Color.fromRGBO(255, 255, 255, 0.2),
        child: Padding(padding: EdgeInsets.all(10.0),

          child: Form(

         //   autovalidate: true,
            key: _formKey,
              child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 15.0),
                child: new TextFormField(

                  decoration: new InputDecoration(

                    labelText: "Usuario",
                    labelStyle: TextStyle(color: Colors.white,fontSize: 20.0),
                    // fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                      BorderSide(color: Colors.white, width: 2.0, ),
                      borderRadius: new BorderRadius.circular(10.0 ),

                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1.0),
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),

                    ),

                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Email cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,

                  style: new TextStyle(
                    color: Colors.white,

                    fontFamily: "Poppins",
                  ),

                  onSaved: (str){
                    _user=str;
                  },

                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 25.0),
                child: new TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Contraseña",
                    labelStyle: TextStyle(color: Colors.white),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 2.0),
                        borderRadius: new BorderRadius.circular(10.0)),
                    fillColor: Colors.white,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 1.0),
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: new BorderSide(color: Colors.red),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "pass cannot be empty";
                    } else {
                     // return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                  obscureText: true,
                  onSaved: (str){
                    _pass=str;
                  },
                ),
              ),
              ButtonTheme(
                minWidth: double.infinity,
                child: MaterialButton(
                  color: Colors.blue,
                  onPressed: btnSubmit,
                  child: Text('Iniciar Sesión',
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                ),
              ),
            ],
          )),
        ),
      ) ,
    );
  }

void onPressed(){
    var form = _formKey.currentState;

    print(form);
    if(form.validate()){
      form.save();
    }

    // Navigator.pushReplacementNamed(context, "/home");
}


}