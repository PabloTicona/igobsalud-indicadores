import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';


typedef dynamic TapCallback();

class CardBarChart extends StatelessWidget {
  final String nombre;
  final String descripcion;
  final List<charts.Series> series;
  final bool graf;
  final bool vertical;


  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  CardBarChart(
      {this.nombre,
      this.descripcion,
      this.series,
      this.graf,
      this.vertical,
      this.screenshotController,
      this.scaffoldKey
      });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Card(
      elevation: 8.0,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[


            ShareGraph(
              screenshotController: screenshotController,
              scaffoldKey: scaffoldKey,
              description: descripcion,
              title: nombre,

            ),

/*
            Row(
              children: <Widget>[
                Icon(Icons.add_box),
                Text(
                  nombre,
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),*/
            Text(
              descripcion,
              style: TextStyle(fontStyle: FontStyle.italic),
              textAlign: TextAlign.left,
            ),
            Container(
              height: 500,
              width: double.infinity,
              child: new Padding(
                padding: new EdgeInsets.all(10.0),
                child: new SizedBox(
                  height: 550,
                  child: graf
                      ? new charts.BarChart(
                          series,
                          behaviors: [
                            new charts.SeriesLegend(
                                position: charts.BehaviorPosition.bottom,
                                horizontalFirst: bool.fromEnvironment("Pablo")),
                          ],
                          animate: true,
                          vertical: vertical,
                          barRendererDecorator:
                              new charts.BarLabelDecorator<String>(),
                          defaultRenderer: new charts.BarRendererConfig(
                              groupingType:
                                  charts.BarGroupingType.groupedStacked,
                              strokeWidthPx: 2.0),
                        )
                      : Center(
                          child: SpinKitWave(
                              color: Colors.redAccent, type: SpinKitWaveType.center),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
