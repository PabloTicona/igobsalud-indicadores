import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
typedef void TapCallback();
class CardContador extends StatelessWidget {

  final String nombre;
  final IconData icono;
  final String cantidad;
  final TapCallback onTap;

  CardContador({this.nombre, this.icono, this.cantidad, this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      child: Expanded(
        child: Padding(padding: EdgeInsets.all(4.0),
        child: Material(
          borderRadius: BorderRadius.circular(8.0)
          ,elevation: 5.0,
          child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            child: Padding(padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Icon(
                  icono,
                  color: Colors.pink,
                  size: 40.0,
                ),
                Text(nombre),

              
                Chip(
                  backgroundColor: Colors.black45,
                  padding: EdgeInsets.all(0),

                  label: (cantidad !='0')? Text(cantidad, style: TextStyle(color: Colors.white)): SpinKitThreeBounce(color: Colors.white, size: 15.0,),
                )
              ],
            ),
            ),
            onTap: onTap,
          ),
        ),
        ),
      ),
    );
  }
}