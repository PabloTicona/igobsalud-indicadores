import 'package:flutter/material.dart';

typedef void SelectCallback(String valInicial);

class DropdownComponentHospitales extends StatelessWidget {

  final String itemValue;
  final SelectCallback onChangeSelect;

  DropdownComponentHospitales({ this.itemValue, this.onChangeSelect});

  @override
  Widget build(BuildContext context) {


    // TODO: implement build
    return Container(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.all(0.0),
          child: Card(
            elevation: 9.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: DropdownButton<String>(
          value: itemValue,
          icon: Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: onChangeSelect,
          items: <String>['Los Pinos', 'La Portada', 'Cotahuma', 'La Merced']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
            ),
          ),
        ),
      ),
    );
  }
}

class ItemSelect {
  final String id;
  final String name;
  ItemSelect(this.id, this.name);
}
