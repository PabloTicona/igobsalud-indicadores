import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:igob_salud_app/pages/consultaExterna/models/HospitalEspecialidades.dart';
typedef void TapCallback(HospitalEspecialidades i);


class CardListComponent extends StatelessWidget {

  final List<HospitalEspecialidades> especialidades;
  final TapCallback onClickItem;


  CardListComponent({this.especialidades, this.onClickItem});

bool selectItem= false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      child: Card(
        elevation: 8.0,
        child: Padding(padding: EdgeInsets.all(8.0),
          child: (especialidades.length !=0)?  ListView.separated(
            padding: const EdgeInsets.all(0.0),
            itemCount: especialidades.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.all(0.0),

                child: Material(
                    borderRadius: BorderRadius.circular(12.0),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(12.0),
                    child: Padding(padding: EdgeInsets.all(15.0),
                    child:  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('${especialidades[index].especialidad}', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          width: 20,
                          height: 20,
                          child: selectItem? CircularProgressIndicator(): Icon(Icons.chevron_right),
                        )

                      ],
                    ),
                    ),
                    onTap: (){

                      onClickItem(especialidades[index]);
                    },
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ): Center(
            child: SpinKitWave(color: Colors.blue, type: SpinKitWaveType.center),
          ),

        ),
      ),
    );
  }
}

