import 'package:flutter/material.dart';

typedef void TapCallback();

class ItemCardEspecialidad extends StatelessWidget {
  final String nombre;

  final TapCallback onTap;

  const ItemCardEspecialidad({this.nombre, this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 2, 0.0, 2.0),
        child: Material(
          borderRadius: BorderRadius.circular(8.0),
          elevation: 5.0,
          child: new InkWell(
              borderRadius: BorderRadius.circular(8.0),
              onTap: onTap,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[Text(nombre.trim())],
                ),
              )),
        ),
      ),
    );
  }
}
