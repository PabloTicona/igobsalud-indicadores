import 'package:flutter/material.dart';
typedef void ClickCallback();

class CardTypeUser extends StatelessWidget {
  final String titulo;
  final IconData icono;
  final ClickCallback onTap;

  CardTypeUser({this.titulo, this.icono, this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      height: 50,
      child: Material(
        elevation: 9.0,
        borderRadius: BorderRadius.circular(8.0),
        color: Color.fromRGBO(255, 255, 255, 0.2),
        child: InkWell(
          child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Icon(
                    icono,
                    size: 32,
                    color: Colors.white,
                  ),
                  Text(
                    titulo,
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  )
                ],
              )),
          onTap: onTap,
        ),
      ),
    );
  }
}