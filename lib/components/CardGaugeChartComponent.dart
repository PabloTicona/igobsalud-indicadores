/// Gauge chart example, where the data does not cover a full revolution in the
/// chart.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:igob_salud_app/helps/ShareGraph.dart';
import 'package:screenshot/screenshot.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
typedef dynamic TapCallback();

class CardGaugeChartComponent extends StatelessWidget {
  final Hospital hospital;
  final double porcentaje;

  final ScreenshotController screenshotController;
  final GlobalKey<ScaffoldState> scaffoldKey;

  CardGaugeChartComponent(this.hospital,
      {this.porcentaje, this.screenshotController, this.scaffoldKey});
  double pi = 3.1415926535897932;

  List<charts.Series<GaugeSegment, String>> _createSampleData() {
    /*
    var porcentaje_ = double.parse(
        ((hospital.atendidos / hospital.planificados) * 100)
            .toStringAsFixed(2)); */

    final data = [
      new GaugeSegment('Low', porcentaje, getColorPorcentaje(porcentaje)),
      new GaugeSegment('Acceptable', (100 - porcentaje),
          charts.Color.fromHex(code: '#eff0f1')),
    ];

    return [
      new charts.Series<GaugeSegment, String>(
        id: '${porcentaje}%',
        domainFn: (GaugeSegment segment, _) => segment.segment,
        measureFn: (GaugeSegment segment, _) => segment.size,
        colorFn: (GaugeSegment segment, _) => segment.color,
        data: data,
      )
    ];
  }

  charts.Color getColorPorcentaje(double porcentaje) {
    charts.Color resp = charts.MaterialPalette.green.shadeDefault;

    if (porcentaje < 60) {
      resp = charts.MaterialPalette.red.shadeDefault;
    } else {
      if (porcentaje >= 60 && porcentaje < 80) {
        resp = charts.MaterialPalette.yellow.shadeDefault;
      }
    }

    return resp;
  }

  void handleResponse(response, {String appName}) {
    if (response == 0) {
      print("failed.");
    } else if (response == 1) {
      print("success");
    } else if (response == 2) {
      print("application isn't installed");
      if (appName != null) {
        // scaffoldKey.currentState.showSnackBar(new SnackBar(
        // content: new Text("${appName} isn't installed."),
        // duration: new Duration(seconds: 4),)
        //);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 8.0,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[


              Padding(
                padding: new EdgeInsets.all(0.0),
                child: ShareGraph(
                  title: hospital.nombre,
                  description: "solicitadas/Planificadas",
                  screenshotController: screenshotController,
                  scaffoldKey: scaffoldKey,
                ),
              ),

              Container(
                width: 150,
                height: 180,
                child: (hospital.planificados != 1 &&
                        hospital.solicitados != 1 &&
                        hospital.atendidos != 1)
                    ? new charts.PieChart(_createSampleData(),
                        animate: true,
                        behaviors: [
                          new charts.SeriesLegend(
                              position: charts.BehaviorPosition.bottom,
                              horizontalFirst: bool.fromEnvironment("Pablo")),
                        ],
                        defaultRenderer: new charts.ArcRendererConfig(
                            arcWidth: 15,
                            startAngle: 4 / 5 * pi,
                            arcLength: 7 / 5 * pi))
                    : Center(
                  child: SpinKitWave(
                      color: Colors.redAccent, type: SpinKitWaveType.center),
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Planificadas:",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  Text(
                      "${(hospital.planificados != 1 && hospital.solicitados != 1 && hospital.atendidos != 1) ? hospital.planificados : 'Cargando..'}",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                ],
              ),
              Divider(
                height: 2,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Solicitadas:",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  Text(
                      "${(hospital.planificados != 1 && hospital.solicitados != 1 && hospital.atendidos != 1) ? hospital.solicitados : 'Cargando..'}",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                ],
              ),
              Divider(
                height: 2,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Atendidas:",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  Text(
                    "${(hospital.planificados != 1 && hospital.solicitados != 1 && hospital.atendidos != 1) ? hospital.atendidos : 'Cargando..'}",
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ],
              ),


              //   Text("Planificadas: ${ (hospital.planificados!=1 && hospital.solicitados!=1 && hospital.atendidos!=1)? hospital.planificados: 'Cargando..'}"),
              //  Text("Solicitadas: ${ (hospital.planificados!=1 && hospital.solicitados!=1 && hospital.atendidos!=1)? hospital.solicitados: 'Cargando..'}"),
              // Text("Atendidas: ${ (hospital.planificados!=1 && hospital.solicitados!=1 && hospital.atendidos!=1)? hospital.atendidos: 'Cargando..'}"),

            ],
          ),
        ),
      ),
    );
  }
}

/// Sample data type.
class GaugeSegment {
  final String segment;
  final double size;
  final charts.Color color;
  GaugeSegment(this.segment, this.size, this.color);
}

class Hospital {
  String nombre;
  int planificados;
  int solicitados;
  int atendidos;

  Hospital(this.nombre, this.planificados, this.solicitados, this.atendidos);
}
