import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
typedef void TapCallback();
class CardContadorHospital extends StatelessWidget {

  final String nombre;
  final IconData icono;
  final String cantidad;
  final Color color;
  final TapCallback onTap;

  CardContadorHospital({this.nombre, this.icono, this.cantidad, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      child: Expanded(
        child: Padding(padding: EdgeInsets.all(4.0),
        child: Material(
          borderRadius: BorderRadius.circular(8.0)
          ,elevation: 5.0,
          child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            child: Padding(padding: EdgeInsets.all(10.0),
            child: Column(

              children: <Widget>[


                Icon(
                  icono,
                  color: color,
                  size: 40.0,
                ),
                Text(nombre, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),

              
                Chip(
                  backgroundColor: color,
                  padding: EdgeInsets.all(0),

                  label: (cantidad !='0')? Row(


                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,

                    children: <Widget>[

                      Text(cantidad, style: TextStyle(color: Colors.white)),
                      Icon(
                        Icons.hotel,
                        color: Colors.white,
                        size: 20.0,
                      ),
                    ],
                  ): SpinKitThreeBounce(color: Colors.white, size: 15.0,),
                )
              ],
            ),
            ),
            onTap: onTap,
          ),
        ),
        ),
      ),
    );
  }
}